<?php

namespace App\Models\Blogs\Traits\Attribute;

/**
 * Class BlogAttribute.
 */
trait BlogAttribute {

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
        return '<div class="btn-group action-btn">' .
                $this->getEditButtonAttribute('edit-blog', 'admin.blogs.edit') .
                $this->getDeleteButtonAttribute('delete-blog', 'admin.blogs.destroy') .
                '</div>';
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute() {
        if ($this->isActive() == 'Published') {
            return "<label class='badge badge-success'>Published</label>";
        }
        if ($this->isActive() == 'Draft') {
            return "<label class='badge badge-warning'>Draft</label>";
        }
        if ($this->isActive() == 'Scheduled') {
            return "<label class='badge badge-info'>Scheduled</label>";
        }
        return "<label class='badge badge-danger'>InActive</label>";
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status;
    }

}

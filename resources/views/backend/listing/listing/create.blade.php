@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.listing.management') . ' | ' . trans('labels.backend.listing.create'))
@section('page-header')
<h1>{{ trans('labels.backend.listing.management') }}</h1>
@endsection
@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::open(['route' => 'admin.listings.store', 'class' => 'form-horizontal add-listing','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'post', 'id' => 'create-listing']) }}
        @include("backend.listing.listing.form")

        <div id="saveForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back">Save and back</span>
                </button>
            </div>
            <a href="{{ route('admin.listings.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection
@section("after_scripts")
@endsection

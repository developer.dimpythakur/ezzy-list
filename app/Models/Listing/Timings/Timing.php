<?php

namespace App\Models\Listing\Timings;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use App\Models\Listing\Timings\Traits\Attribute\Attribute;
use App\Models\Listing\Timings\Traits\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timing extends BaseModel {

    use ModelTrait,
        SoftDeletes,
        Relationship,
        Attribute {
        // PageAttribute::getEditButtonAttribute insteadof ModelTrait;
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'created_by' => 1,
    ];
    protected $fillable = ['name', 'open_time', 'close_time', 'weekends_open', 'status', 'created_by'];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.ezzy_timings.table');
    }

    /**
     * Return ListingTiming map Table
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getMapTable() {
        return config('module.listing_timings.table');
    }

    /**
     * Update timings Table
     * @param type $listing
     * @param type $timings
     * @return type
     */
    public function updateTimings($listing, $timings) {
        $timingsArray = [];
        if ($listing && count($listing->timings)):
            foreach (($listing->timings) as $timingSingle):
                $openingTime = $timings[$timingSingle->name]['opening'];
                $closingTime = $timings[$timingSingle->name]['closing'];
                Timing::where('id', $timingSingle->id)
                        ->update([
                            'open_time' => ($openingTime == 'Closed') ? "00:00:00" : $openingTime,
                            'close_time' => ($closingTime == 'Closed') ? "00:00:00" : $closingTime,
                ]);
                $timingsArray[] = $timingSingle->id;
            endforeach;
        else:
            foreach ($timings as $day => $timing):
                $timingsArray[] = Timing::create([
                            'name' => $day,
                            'open_time' => $timing['opening'],
                            'close_time' => $timing['closing'],
                            'status' => 1,
                            'created_by' => 1
                        ])->id;
            endforeach;
        endif;
        return $timingsArray;
    }

}

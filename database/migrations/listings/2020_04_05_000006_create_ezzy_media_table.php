<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEzzyMediaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ezzy_media';

    /**
     * Run the migrations.
     * @table ezzy_media
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('filename', 200)->nullable()->default(null);
            $table->string('type', 200)->nullable()->default(null);
            $table->string('media_type', 100)->nullable()->default(null);
            $table->integer('media_count')->nullable()->default('0');
            $table->tinyInteger('status')->nullable()->default('1');

            $table->string('path', 100)->nullable()->default(null);
            $table->string('url', 100)->nullable()->default(null);

            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

@extends('frontend.layouts.auth')
@section('content')
<div class="row">
    <?php

    use Collective\Html\FormBuilder;
    ?>
    <div class="col-md-6 mx-auto mt-5">
        <div class="card panel-default">
            <h4 class="card-title mt-4 text-center mb-1">{{ trans('labels.frontend.auth.register_box_title') }}</h4>
            <div class="card-body p-4">
                @if ($errors->any())
                <div class="alert alert-danger mb-2 mt-2">
                    @foreach ($errors->all() as $error)
                    <p class="my-1">{{$error}}</p>
                    @endforeach
                </div>
                @endif

                {{ Form::open(['route' => 'frontend.auth.register', 'class' => 'form-horizontal']) }}
                <div class="form-group row">
                    {{ Form::label('first_name', trans('validation.attributes.frontend.register-user.firstName').'*', ['class' => 'col-md-8 control-label']) }}
                    <div class="col-md-12">
                        {{ Form::input('name', 'first_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.firstName')]) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->
                <div class="form-group row">
                    {{ Form::label('last_name', trans('validation.attributes.frontend.register-user.lastName').'*', ['class' => 'col-md-8 control-label']) }}
                    <div class="col-md-12">
                        {{ Form::input('name', 'last_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.lastName')]) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ Form::label('email', trans('validation.attributes.frontend.register-user.email').'*', ['class' => 'col-md-8 control-label']) }}
                    <div class="col-md-12">
                        {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.email')]) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ Form::label('password', trans('validation.attributes.frontend.register-user.password').'*', ['class' => 'col-md-8 control-label']) }}
                    <div class="col-md-12">
                        {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password')]) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ Form::label('password_confirmation', trans('validation.attributes.frontend.register-user.password_confirmation').'*', ['class' => 'col-md-8 control-label']) }}
                    <div class="col-md-12">
                        {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password_confirmation')]) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->

                <div class="form-group row">
                    <div class="col-xs-7">

                    </div><!--form-group-->
                </div><!--col-md-6-->

                @if (config('access.captcha.registration'))
                <div class="form-group  row">
                    {{ Form::label('captcha', trans('validation.attributes.frontend.register-user.password_confirmation').'*', ['class' => 'col-md-8 control-label']) }}
                    <div class="col-md-12">
                        {!! captcha_img() !!}
                        {{ Form::input('captcha', 'captcha', null,      ['class' => 'form-control mt-1', 'placeholder' =>'Captcha']) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->
                @endif
                <div class="check-types  row">
                    <label for="cbk3" class="w-100">
                        <input name="is_term_accept" type="checkbox" id="cbk3">
                        <span class="cbx rounded-pill">
                            <svg width="10px" height="10px" viewBox="0 0 12 11">
                            <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
                            </svg>
                        </span>
                        <span>I agree to the <a href="#" class="text-primary">Terms &amp; Conditions</a> and privacy policy.</span>
                    </label>
                </div>
                <div class="form-group mt-4 row">
                    <div class="col-md-6 ml-auto">
                        <a class="" href="{{ route('frontend.auth.login')}}">
                            Already have account? Login
                        </a>
                    </div>
                    <div class="col-md-6 ml-auto">
                        {{ Form::submit(trans('labels.frontend.auth.register_button'), ['class' => 'btn btn-primary float-right']) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->

                {{ Form::close() }}

            </div><!-- panel body -->

        </div><!-- panel -->

    </div><!-- col-md-12 -->

</div><!-- row -->
@endsection

@section('after-scripts')
@if (config('access.captcha.registration'))
<!--Captcha::script()-->
@endif

<script type="text/javascript">

    $(document).ready(function () {
        // To Use Select2
        Backend.Select2.init();
    });
</script>
@endsection

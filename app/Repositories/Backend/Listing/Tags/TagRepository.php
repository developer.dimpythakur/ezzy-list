<?php

namespace App\Repositories\Backend\Listing\Tags;

use App\Events\Backend\Listing\Tags\Created;
use App\Events\Backend\Listing\Tags\Deleted;
use App\Events\Backend\Listing\Tags\Updated;
use App\Exceptions\GeneralException;
use App\Models\Listing\Tags\Tag;
use App\Repositories\BaseRepository;
use DB;

/**
 * Class ListingTagsRepository.
 */
class TagRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Tag::class;

    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
//            ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.blog_tags.table') . '.created_by')
                        ->select([
                            config('module.ezzy_tags.table') . '.id',
                            config('module.ezzy_tags.table') . '.name',
                            config('module.ezzy_tags.table') . '.status',
//                config('module.listing_tags.table') . '.created_by',
                            config('module.ezzy_tags.table') . '.created_at',
//                config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * @param array $input
     *
     * @return bool
     * @throws \App\Exceptions\GeneralException
     *
     */
    public function create(array $input) {
        if ($this->query()->where('name', $input['name'])->first()) {
            throw new GeneralException(trans('exceptions.backend.tags.already_exists'));
        }

        DB::transaction(function () use ($input) {
            $input['status'] = isset($input['status']) ? 1 : 0;
            $input['created_by'] = access()->user()->id;
            if ($listingtag = Tag::create($input)) {
                event(new Created($listingtag));
                return true;
            }

            throw new GeneralException(trans('exceptions.backend.tags.create_error'));
        });
    }

    /**
     * @param \App\Models\ListingTags\ListingTag $blogtag
     * @param  $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * return bool
     */
    public function update(ListingTag $blogtag, array $input) {
        if ($this->query()->where('name', $input['name'])->where('id', '!=', $blogtag->id)->first()) {
            throw new GeneralException(trans('exceptions.backend.tags.already_exists'));
        }

        DB::transaction(function () use ($blogtag, $input) {
            $input['status'] = isset($input['status']) ? 1 : 0;
            $input['updated_by'] = access()->user()->id;

            if ($blogtag->update($input)) {
                event(new Updated($blogtag));

                return true;
            }

            throw new GeneralException(
                    trans('exceptions.backend.tags.update_error')
            );
        });
    }

    /**
     * @param \App\Models\ListingTags\ListingTag $blogtag
     *
     * @return bool
     * @throws \App\Exceptions\GeneralException
     *
     */
    public function delete(ListingTag $blogtag) {
        DB::transaction(function () use ($blogtag) {
            if ($blogtag->delete()) {
                event(new Deleted($blogtag));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.tags.delete_error'));
        });
    }

}

<div class="row listing-single">
    <div class="col-md-2 mb-1">
        <div class="row mb-0">
            <figure class="listing-image">
                @if ($listing->banner && !is_null($listing->banner))
                <img src="{{ $listing->banner }}"
                     class="img-fluid" alt="{{ $listing->name }}">
                @else
                <img src="{{route('frontend.index')}}/img/frontend/listings/coffee-1.jpg"
                     class="img-fluid" alt="listing">
                @endif
            </figure>
        </div>
    </div>
    <div class="col-md-8 mb-1">
        <div class="col-box">
            <div class="d-block w-100">
                <div class="d-inline-block mr-3">
                    <a class="listing-slug" target="_blank" href="{{ route('frontend.listing.show',$listing->slug)}}">
                        <h4 class="listing-title my-1">{{ substr( $listing->name ,0,50)}}</h4>
                    </a>
                </div>
                <div class="d-inline-block">
                    <?php
                    $ratings = $listing->averageRating(2);
                    $filterRatings = collect($ratings)->filter();
                    $rating = (count($filterRatings)) ? $listing->averageRating(2) : 'No ratings';
                    ?>
                    <div
                        data-toggle="tooltip" data-placement="top" title="Rating {{ $rating }}"
                        class="star-rating mb-2"
                        data-rating="{{ $rating }}"></div>
                </div>
            </div>
            <div class="text listing-description my-2">{{ $listing->tagline }}</div>
            <ul class="info mt-2">
                @if($listing->categories->first()->name != '')
                <li><span class="la la-list"></span>&nbsp;{{ $listing->categories->first()->name }} </li>
                @endif
                @if($listing->location->pluck('address')->first() != '')
                <li>
                    <span class="la la-location-arrow"></span>&nbsp;
                    {{ substr($listing->location->pluck('address')->first(),0,40) }}
                </li>
                @endif
            </ul>
            <ul class="info mt-2">
                @if($listing->phone != '')
                <li><span class="la la-phone"></span>&nbsp;{{ $listing->phone }} </li>
                @endif
                @if($listing->website !='')
                <li><span class="la la-html5"></span>&nbsp;{{ $listing->website }} </li>
                @endif
            </ul>
        </div>
    </div>
    <div class="col-md-2 mb-4 position-relative">
        <div class="col-box">
            <a data-toggle="tooltip" data-placement="top" title="Update {{ $listing->name }}"  href="{{ route('frontend.user.listings.edit',$listing->id)}}" class="btn btn-flat btn-link supdate-link text-primary">
                <i class="la la-pencil-square-o"></i> Update
            </a>
            <a data-toggle="tooltip" data-placement="top" title="Delete {{ $listing->name }}" class="btn btn-flat btn-link remove-slink text-danger"
               data-method="delete"
               href="{{ route('frontend.user.listings.destroy',$listing->id)}}"
               data-trans-button-cancel="Cancel" 
               data-trans-button-confirm="Delete"
               data-trans-title="Are you sure you want to do this?"
               style="cursor:pointer;" onclick="$(this).find( & quot; form & quot; ).submit();">
                <i class="la la-trash"></i> Delete
            </a>
        </div>
    </div>
</div>
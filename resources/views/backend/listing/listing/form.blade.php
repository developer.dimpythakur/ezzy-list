@push("after_styles")
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
<style>
    .normal-labels label{
        font-weight: 400
    }
    /*
     * File
     */
    .file input:focus ~ .file-custom {
        box-shadow: 0 0 0 .075rem #fff, 0 0 0 .2rem #0074d9;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu .collapse:not(.show){
        display: block !important
    }
    .select2-container .select2-selection--single,
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        height: 40px;
        line-height: 40px;
    }


    /*FAKE FILE*/
    .fake-file-upload {
        border: 2px dashed #ccc;
    }
    .fake-file-upload input {
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
    }
    .fake-file-upload:hover{
        border-color: #e53935 !important;
    }
    .nostyle{list-style: none;margin: 0;padding: 0}

    .uploaded-images ul li,
    .selected-videos ul li, 
    .selected-audios ul li {
        flex: 1 0 30%;
        position: relative;
        padding-left: .5rem;
        padding-right: .5rem;
    }
    .uploaded-images ul li .la, 
    .uploaded-images ul li .las, 
    .selected-videos ul li .la, 
    .selected-audios ul li .la ,
    .uploaded-images ul li img i {
        position: absolute;
        right: 10px;
        top: 10px;
        cursor: pointer;
        font-size: 1.5rem;
    }
    .uploaded-images ul li img{
        position: relative;
        max-height: 450px;
        min-height: 450px;
        width: 100%;
        border-radius: 4px;
    }
    .uploaded-images.gallery ul li img{
        position: relative;
        max-height: 250px;
        min-height: 250px;
        width: 100%;
        border-radius: 4px;
    } 

</style>
@endpush



@include('backend.listing.listing.component.general')
@include('backend.listing.listing.component.contact')
@include('backend.listing.listing.component.social-media')
@include('backend.listing.listing.component.locations')
@include('backend.listing.listing.component.timings')
@include('backend.listing.listing.component.audio')
@include('backend.listing.listing.component.video')
@include('backend.listing.listing.component.media')
@include('backend.listing.listing.component.faq')
@include('backend.listing.listing.component.status')

@push("after_scripts")

<script>
    jQuery(document).ready(function () {

        //-------------------------------
        // Featured Images Upload
        //-------------------------------
        $('#single_img_upload').change(function (event) {
            var imageHTML = '';

            $(event.target.files).each(function (index) {
                var tmppath = URL.createObjectURL(event.target.files[index]);
                imageHTML += '<li class="m-1" id="featured-img-' + index + '"><img class="img-fluid" src="' + tmppath + '"><i data-targetimg="' + index + '" class="remove-featured-img p-1 shadow pointer btn btn-primary border-0 la la-trash"></i></li>';
            });
            $('#uploaded_feat_img').html(imageHTML);
            event.preventDefault();

        });
        //-------------------------------
        // Remove Featured Image
        //-------------------------------
        $(document).on('click', '.remove-featured-img', function (e) {
            e.preventDefault();
            $(this).closest('li').remove();
        });
    });

</script>


<!-- include select2 js-->
<script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
@if (app()->getLocale() !== 'en')
<script src="{{ asset('packages/select2/dist/js/i18n/' . app()->getLocale() . '.js') }}"></script>
@endif
<script src="{{ asset('packages/tinymce/tinymce.min.js') }}"></script>
<script>

    $(document).ready(function () {
//        select2
        $('.select2_multiple').select2();
        //tinymce
        var $element = "textarea.form-control.tinymce";
        tinymce.init(jQuery($element).data('options'));

    });

</script>
@endpush

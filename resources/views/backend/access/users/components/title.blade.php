<div class="col-md-7">
    <h2>Howdy, {{ access()->user()->name}}</h2>
</div>
<!-- Breadcrumbs -->
@if(Breadcrumbs::exists())
{!! Breadcrumbs::render() !!}
@endif
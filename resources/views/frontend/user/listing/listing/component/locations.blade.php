
<?php
$options = [
    'file_picker_callback' => 'elFinderBrowser',
    'selector' => 'textarea.tinymce',
    'plugins' => 'image,link,media,anchor',
];
$location = (isset($listing) && $listing->location) ? $listing->location->first() : null;
$address = (isset($listing->location)) ? $location->address : null;
$region = (isset($listing->location)) ? $location->region_id : null;
$longitude = (isset($listing->location)) ? $location->longitude : null;
$latitude = (isset($listing->location)) ? $location->latitude : null;
?>
<div class = "card">
    <div class="card-header">
        <h6 class="my-2"><i class = "la la-location-arrow"></i> Listing Location</h6>
    </div>
    <div class = "card-body listing-form bold-labels">

        <div class = "row">
            <div class = "col-md-6 form-group">
                {!! Html::decode(Form::label('address', '<i class="la la-address-card"></i>'.trans('validation.attributes.frontend.listing.address'), ['class' => 'control-label required'])) !!}
                {{ Form::text('location[address]', $address, ['id' => 'location','class' => ' border form-control listing-address', 'placeholder' => 'Address ', 'required' => 'required']) }}
            </div>

            <div class = "col-md-6 form-group">
                {!! Html::decode(Form::label('region', '<i class="la la-city"></i>'.trans('validation.attributes.frontend.listing.region'), ['class' => 'control-label required'])) !!}
                {{ Form::select('location[region]', $regions, $region, ['id' => 'region','class' => 'form-control box-size select2_multiple', 'placeholder' => 'Select Region', 'required' => 'required'])}}
            </div>
        </div>
        <div class = "row">
            <div class = "col-md-6 form-group">
                {!! Html::decode(Form::label('longitude', '<i class="la la-map-pin"></i>'.trans('validation.attributes.frontend.listing.longitude'), ['class' => 'control-label required'])) !!}
                {{ Form::text('location[longitude]', $longitude, ['id' => 'longitude','class' => ' border form-control', 'placeholder' => 'Longitude ']) }}
            </div>
            <div class = "col-md-6 form-group">
                {!! Html::decode(Form::label('latitude', '<i class="la la-map-pin"></i>'.trans('validation.attributes.frontend.listing.latitude'), ['class' => 'control-label required'])) !!}
                {{ Form::text('location[latitude]', $latitude, ['id' => 'latitude','class' => ' border form-control', 'placeholder' => 'Latitude ']) }}
            </div>
        </div>
        <div class = "row">
            <div class = "col-md-12 mb-5">
                <div id ="listing-map"></div>
            </div>
        </div>
    </div>
</div>


@push('after_scripts')

<script type='text/javascript'>
    /* <![CDATA[ */
    var EZZY_LOC = {
        "start_geo_long": "-74.0059728",
        "start_geo_lat": "40.7127753",
        "start_point": "New York, NY, USA",
        "enable_map": "1",
        "user_location": [],
        "l10n": {
            "locked": "Lock Pin",
            "unlocked": "Unlock Pin"
        }
    };
    /* ]]> */
</script>
<!-- Map API -->
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAmrxCFuWyNUlMh6wLitVLI3pF7j_HqAFY&#038;libraries=places&#038;ver=1.1'></script>
<!-- Custom Js -->
<script src="{{ asset('listing/rangeslider.js') }}"></script>
<script src="{{ asset('listing/mapify.js') }}"></script>
<script src="{{ asset('listing/geo-tag-text.js') }}"></script>
<script src="{{ asset('listing/map/leaflet.js') }}"></script>
<script src="{{ asset('listing/listing-location.js') }}"></script>

<script type='text/javascript'>
    /* <![CDATA[ */
    var gt3listing_params = {
        "login_url": "https:\/\/livewp.site\/wp\/md\/listingeasy\/wp-login.php",
        "listings_page_url": "https:\/\/livewp.site\/wp\/md\/listingeasy\/listings-with-map\/",
        "strings": {
            "wp-job-manager-file-upload": "Add Image",
            "no_job_listings_found": "No results",
            "results-no": "Results",
            "select_some_options": "Select Some Options",
            "select_an_option": "Select an Option",
            "no_results_match": "No results match"
        }
    };
    /* ]]> */
</script>

@endpush
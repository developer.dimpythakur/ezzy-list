<!-- Tiny MCE -->
@php
$defaultOptions = [
'file_picker_callback' => 'elFinderBrowser',
'selector' => 'textarea.tinymce',
'plugins' => 'image,link,media,anchor',
];

$options = array_merge($defaultOptions, $field['options'] ?? []);
@endphp

<label>Page</label>
<textarea
    name="bpFieldInitTinyMceElement"
    data-init-function="bpFieldInitTinyMceElement"
    data-options="{{ trim(json_encode($options)) }}"
    ></textarea>

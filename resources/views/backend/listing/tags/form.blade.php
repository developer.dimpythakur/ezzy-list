@push("after_styles")
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
@endpush

<div class="form-group col-12">
    {{ Form::label('name', trans('validation.attributes.backend.listingtags.title'), ['class' => 'control-label required']) }}
    {{ Form::text('name', (isset($tags))?($tags)->name:null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.listingtags.title'), 'required' => 'required']) }}
</div><!--form group-->

<div class="form-group col-sm-12" element="div">
    <div>
        <label>Status</label>
    </div>

    <div class="form-check form-check-inline">
        <input type="radio" {{ (isset($tags)&& ($tags->status == 1) ? 'checked' : '' ) }} class="form-check-input" value="1" name="status" id="status_1">
        <label class="radio-inline form-check-label font-weight-normal" for="status_1">Published</label>
    </div>
    <div class="form-check form-check-inline">
        <input type="radio" {{ (isset($tags)&& ($tags->status == 0) ? 'checked' : '' ) }} class="form-check-input" value="0" name="status" id="status_0">
        <label class="radio-inline form-check-label font-weight-normal" for="status_0">Draft</label>
    </div>
</div><!--form group-->

@push("after_scripts")
<script>
    $(function () {
// Multiple images preview with JavaScript
        var multiImgPreview = function (input, imgPreviewPlaceholder) {
            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function (event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            } else {
                console.log(input);
            }
        };

        $('#file').on('change', function () {
            let $el = 'div.imgPreview';
            jQuery($el).html('');
            multiImgPreview(this, 'div.imgPreview');
        });
    });
</script>
@endpush
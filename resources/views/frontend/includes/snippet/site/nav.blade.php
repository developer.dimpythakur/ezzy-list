<!-- Nav bar -->

<div class="et-nav-menu">
    <div class="container">
        <div class="d-flex pt-md-0 pt-3 pb-md-0 pb-3 position-relative flex-nowrap align-items-center">
            <!-- Mobile Nav -->
        @include('frontend.includes.snippet.site.mobile-nav')
        <!-- /Mobile Nav -->
            <!-- Navigation menu -->
            <div class="nav-section align-items-center d-none d-md-block d-lg-flex">
                <nav class="menu-items">
                    <ul class="nostyle">
                        <li>
                            <a href="{{ route('frontend.index') }}">Home</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.listing.all') }}">Listings</a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.blog') }}">Blog</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <!-- /Navigation menu -->
            <!-- Logo -->
            <div class="header-logo ml-auto">
                @if(settings()->logo)
                    <a href="{{ route('frontend.index') }}" class="logo">
                        <img height="48" width="226" class="navbar-brand"
                             src="{{ Storage::url('images/logo/').settings()->logo}}">
                    </a>
                @else
                    <a href="{{ route('frontend.index') }}" class="logo">
                        <img height="48" width="133" class="navbar-brand"
                             src="{{route('frontend.index')}}/img/frontend/logo.png">
                    </a>
                @endif
            </div>
            <!-- /Logo -->
            <!-- Action buttons -->
            <div class="action-buttons d-md-block d-none text-right ml-auto">


                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Features</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Pricing</a>
                        </li>
                        <li class="nav-item dropdown show">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Dropdown link
                            </a>
                            <div class="dropdown-menu show" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                    </ul>
                </div>




                <ul class="navbar-nav mr-auto">
                    @if ($logged_in_user)
                        <li class="nav-item">
                            <a class="btn btn-primary ml-3 border-0 transition-1 text-capitalize text-white"
                               href="add-listing.html">
                                <i class="las la-plus"></i> Add Listing
                            </a>
                        </li>
                    @endif

                    @if (! $logged_in_user)
                        <li class="nav-item">
                            <a id="sign-up" href="#" class="text-white ml-3" data-toggle="modal"
                               data-target="#loginSignup">
                                <i class="icon-block"></i> Login/Signup
                            </a>
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-expanded="false">
                                {{ $logged_in_user->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                @permission('view-backend')
                                <li>{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                                @endauth
                                <li>{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account')) }}</li>
                                <li>{{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- /Action buttons -->
        </div>
    </div>
</div>
<!-- /Nav bar -->
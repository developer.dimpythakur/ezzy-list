@extends ('frontend.layouts.user')
@section ('title', trans('labels.frontend.listing.management') . ' | ' . trans('labels.frontend.listing.create'))
@section('page-header')
<h1>{{ trans('labels.backend.listing.create') }}</h1>
@endsection
@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('frontend.layouts.sections.grouped_errors'))
        {{ Form::open(['route' => 'frontend.user.listings.store', 'class' => 'form-horizontal add-listing','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'post', 'id' => 'create-listing']) }}
        @include("frontend.user.listing.listing.form")

        <div id="saveForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back">Save and back</span>
                </button>
            </div>
            <a href="{{ route('frontend.user.listings.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection
@section("after_scripts")
@endsection

<?php

/**
 * Locations
 *
 */
Route::group(['namespace' => 'Backend\Listing', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {

    Route::group(['namespace' => 'Locations'], function () {
        Route::resource('listing-locations', 'LocationController');
        //For Datatable
        Route::post('listing-locations/get', 'LocationTableController')->name('locations.get');
    });
});

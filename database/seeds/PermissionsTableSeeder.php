<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Database\DisableForeignKeys;
use Database\TruncateTable;

class PermissionsTableSeeder extends Seeder {

    use DisableForeignKeys,
        TruncateTable;

    protected $permissions = [];
    protected $modules = [];

    public function setPermissions() {
        $this->modules = [
//            'backend', //backend
//            'frontend', //frontend
//            'access-management', //access-managemen
            'listing',
            'listing-categories',
            'listing-tags',
            'listing-regions',
            'listing-reviews',
        ];

        $this->permissions = ['view', 'create', 'edit', 'store', 'delete'];
        $this->rolePermission = [
            'listing' => [
                ['view', 'create', 'edit', 'store', 'delete']
            ],
            'listing-categories' => [
                ['view', 'create', 'edit', 'store', 'delete']
            ],
            'listing-tags' => [
                ['view', 'create', 'edit', 'store', 'delete']
            ],
            'listing-regions' => [
                ['view', 'create', 'edit', 'store', 'delete']
            ],
            'listing-reviews' => [
                ['view', 'create', 'edit', 'store', 'delete']
            ],
        ];
    }

    public function compile() {
        $permission_model = config('access.permission');
        $viewBackend = new $permission_model();
        $compilte = $names = [];
        $count = (count($viewBackend->all()) > 0) ? (($viewBackend->get()->last()->id) + 1) : 1;
        foreach ($this->modules as $module):
            foreach (($this->permissions) as $permission):
                echo $count . ' ';
                $name = $permission . '-' . $module;
                $displayName = ($permission) . ' ' . str_replace('-', ' ', $module);
                $compilte[$module] = $this->createPermission($name, $displayName, ($count));
                $names[$name] = $module;
                $count++;
            endforeach;
        endforeach;
        dd($names);
    }

    function createPermission($name, $displayName, $order) {
        $permission_model = config('access.permission');
        $viewBackend = new $permission_model();
        $viewBackend->name = $name;
        $viewBackend->display_name = $displayName;
        $viewBackend->sort = $order;
        $viewBackend->created_by = 1;
        $viewBackend->updated_by = null;
        $viewBackend->created_at = Carbon::now();
        $viewBackend->updated_at = Carbon::now();
        $viewBackend->deleted_at = null;
        $viewBackend->save();
        return $viewBackend;
    }

    public function run() {
        $this->setPermissions();
        $this->compile();
    }

}

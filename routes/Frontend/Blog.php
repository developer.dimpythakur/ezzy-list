<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('blog', 'Posts\PostController@index')->name('blog');


Route::get('posts', 'Posts\PostController@index')->name('posts.all');
Route::get('post/{slug}', 'Posts\PostController@show')->name('post.show');

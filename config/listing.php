<?php

return [
    'blogs' => [
        'https://www.droid-life.com/feed/',
        'https://www.digitaltrends.com/feed/',
        'https://www.slashgear.com/feed/',
        'https://www.zomato.com/blog/feed',
        'https://recipefairy.com/feed/',
        'https://ourbestbites.com/feed/',
        'https://www.101cookbooks.com/feed'
    ],
    'blog_tags' => array(
        array('id' => '1', 'name' => 'Auto Listings', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:49:22', 'updated_at' => '2020-08-09 05:49:22', 'deleted_at' => NULL),
        array('id' => '2', 'name' => 'Hotels', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:49:32', 'updated_at' => '2020-08-09 05:49:32', 'deleted_at' => NULL),
        array('id' => '3', 'name' => 'Restaurants', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:49:38', 'updated_at' => '2020-08-09 05:49:38', 'deleted_at' => NULL),
        array('id' => '4', 'name' => 'Food', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:49:45', 'updated_at' => '2020-08-09 05:49:45', 'deleted_at' => NULL),
        array('id' => '5', 'name' => 'Holidays', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:49:53', 'updated_at' => '2020-08-09 05:49:53', 'deleted_at' => NULL),
        array('id' => '6', 'name' => 'Fun', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:50:00', 'updated_at' => '2020-08-09 05:50:00', 'deleted_at' => NULL)
    ),
    'blog_categories' => array(
        array('id' => '3', 'name' => 'Restaurants', 'slug' => 'restaurants', 'image' => '1596957813Nightlife.jpg', 'icon' => 'icon-dish', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 07:23:33', 'updated_at' => '2020-08-09 07:23:33', 'deleted_at' => NULL),
        array('id' => '4', 'name' => 'Hotels', 'slug' => 'hotels', 'image' => '1596957827location-default.jpeg', 'icon' => 'icon-coffee-cup', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 07:23:47', 'updated_at' => '2020-08-09 07:23:47', 'deleted_at' => NULL),
        array('id' => '5', 'name' => 'Nightlife', 'slug' => 'nightlife', 'image' => 'default-category.jpeg', 'icon' => 'icon-video-camera', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 07:24:00', 'updated_at' => '2020-08-09 07:24:00', 'deleted_at' => NULL),
        array('id' => '6', 'name' => 'Cinema', 'slug' => 'cinema', 'image' => '1596957861canalolympia_sallecinema2.jpg', 'icon' => 'icon-video-camera', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 07:24:21', 'updated_at' => '2020-08-09 07:24:21', 'deleted_at' => NULL),
        array('id' => '7', 'name' => 'Shopping', 'slug' => 'shopping', 'image' => '1596957962alabama.jpg', 'icon' => 'icon-tyre', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 07:26:02', 'updated_at' => '2020-08-09 07:26:02', 'deleted_at' => NULL)
    ),
    'tags' => [
        array('id' => '1', 'name' => 'Restaurants', 'slug' => 'restaurants', 'description' => NULL, 'status' => '1', 'created_by' => NULL, 'updated_by' => NULL, 'created_at' => '2020-08-09 04:47:07', 'updated_at' => '2020-08-09 04:47:07', 'deleted_at' => NULL),
        array('id' => '2', 'name' => 'Good Food', 'slug' => 'good-food', 'description' => NULL, 'status' => '1', 'created_by' => NULL, 'updated_by' => NULL, 'created_at' => '2020-08-09 04:47:56', 'updated_at' => '2020-08-09 04:47:56', 'deleted_at' => NULL),
        array('id' => '3', 'name' => 'Best Hotels', 'slug' => 'best-hotels', 'description' => NULL, 'status' => '1', 'created_by' => NULL, 'updated_by' => NULL, 'created_at' => '2020-08-09 04:48:14', 'updated_at' => '2020-08-09 04:48:14', 'deleted_at' => NULL)
    ],
    'categories' => [
        array('id' => '1', 'name' => 'Restaurants', 'slug' => 'restaurants', 'description' => 'Restaurants', 'image' => '1598244633restaurants.jpg', 'icon' => 'icon-dish', 'type' => 'ezzy', 'parent_id' => NULL, 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-04 06:14:40', 'updated_at' => '2020-08-24 04:50:33', 'deleted_at' => NULL),
        array('id' => '2', 'name' => 'Automobile', 'slug' => 'automobile', 'description' => 'Automobile', 'image' => '1598244623automobile.jpg', 'icon' => 'icon-car', 'type' => 'ezzy', 'parent_id' => NULL, 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-04 06:17:57', 'updated_at' => '2020-08-24 04:50:23', 'deleted_at' => NULL),
        array('id' => '3', 'name' => 'Home Services', 'slug' => 'home-services', 'description' => 'Home Services', 'image' => '1598244611realestate.jpg', 'icon' => 'lar la-building', 'type' => 'ezzy', 'parent_id' => NULL, 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-04 06:19:35', 'updated_at' => '2020-08-24 04:50:11', 'deleted_at' => NULL),
        array('id' => '4', 'name' => 'Shopping', 'slug' => 'shopping', 'description' => 'Shopping', 'image' => '1598244594shop.jpeg', 'icon' => 'icon-shopping-cart', 'type' => 'ezzy', 'parent_id' => NULL, 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-04 06:21:07', 'updated_at' => '2020-08-24 04:49:54', 'deleted_at' => NULL),
        array('id' => '5', 'name' => 'Nightlife', 'slug' => 'nightlife', 'description' => 'Nightlife', 'image' => '1598244582nightlife.jpg', 'icon' => 'icon-speaker', 'type' => 'ezzy', 'parent_id' => NULL, 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-04 06:22:34', 'updated_at' => '2020-08-24 04:49:42', 'deleted_at' => NULL),
        array('id' => '6', 'name' => 'Cinema', 'slug' => 'cinema', 'description' => 'Cinema', 'image' => '1598244572cinema.jpg', 'icon' => 'icon-video-camera', 'type' => 'ezzy', 'parent_id' => NULL, 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-04 06:24:04', 'updated_at' => '2020-08-24 04:49:32', 'deleted_at' => NULL),
    ],
    'regions' => [
        array('id' => '1', 'name' => 'Arroyo Grande', 'slug' => 'arroyo-grande', 'city' => 'NY', 'state' => 'NY', 'region' => 'CA', 'type' => 'ezzy', 'country' => 'USA', 'image' => '15982443741597740823new-york.jpg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-08 01:13:01', 'updated_at' => '2020-08-28 07:31:10', 'deleted_at' => NULL),
        array('id' => '2', 'name' => 'Templeton', 'slug' => 'templeton', 'city' => 'AL', 'state' => 'AL', 'region' => 'CA', 'type' => 'ezzy', 'country' => 'USA', 'image' => '15982443601597740782alabama.jpg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-08 01:13:15', 'updated_at' => '2020-08-28 07:31:58', 'deleted_at' => NULL),
        array('id' => '3', 'name' => 'Do Pismo Beach', 'slug' => 'do-pismo-beach', 'city' => 'AK', 'state' => 'AK', 'region' => 'CA', 'type' => 'ezzy', 'country' => 'USA', 'image' => '15982443491597740770alaska.jpg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-08 01:13:56', 'updated_at' => '2020-08-28 07:30:21', 'deleted_at' => NULL),
        array('id' => '4', 'name' => 'Atascadero', 'slug' => 'atascadero', 'city' => 'AZ', 'state' => 'AZ', 'region' => 'CA', 'type' => 'ezzy', 'country' => 'USA', 'image' => '15982443361597740642arizona.jpeg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-08 01:14:26', 'updated_at' => '2020-08-28 07:29:27', 'deleted_at' => NULL),
        array('id' => '5', 'name' => 'San Luis Obispo', 'slug' => 'san-luis-obispo', 'city' => 'CA', 'state' => 'CA', 'region' => 'CA', 'type' => 'ezzy', 'country' => 'USA', 'image' => '15982443241597740625califronia.jpg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-09 04:32:34', 'updated_at' => '2020-08-28 07:28:30', 'deleted_at' => NULL),
        array('id' => '6', 'name' => 'Paso Robles', 'slug' => 'paso-robles', 'city' => 'FL', 'state' => 'FL', 'region' => 'CA', 'type' => 'ezzy', 'country' => 'USA', 'image' => '15985986211597740625califronia.jpg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-09 04:33:32', 'updated_at' => '2020-08-28 07:23:58', 'deleted_at' => NULL)
    ]
];

@if (config('backend.scripts') && count(config('backend.scripts')))
@foreach (config('backend.scripts') as $path)
<script type="text/javascript" id="{{ ($path) }}" src="{{ asset($path) }}"></script>
@endforeach
@endif
@include('backend.layouts.sections.alerts')
<script type="text/javascript">
$(document).ajaxStart(function () {
    Pace.restart();
});
</script>
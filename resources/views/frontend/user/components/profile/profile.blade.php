<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-user"></i> My Profile</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row normal-labels profile-box">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    <div class="d-flex align-items-center">
                        <div  id="my-profile-picture">
                            <img class="my-profile-img" src="{{  (null !== access()->user()->profile)? (access()->user()->picture):''}}" alt="image">
                        </div>
                        <div class="btn-box ml-3">
                            <div class="fake-file-upload h6 rounded-pill transition-1 d-flex align-items-center justify-content-center p-2 text-center text-muted position-relative">
                                <input type="file" id="profile_picture" name="profile_picture" class="position-absolute pointer" >
                                <i class="la la-cloud-upload-alt h3 m-0"></i>&nbsp; Update Picture
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row normal-labels">
            <div class="form-group col-6 required">
                {{ Form::label('first_name', trans('validation.attributes.frontend.register-user.firstName'), ['class' => 'control-label']) }}
                {{ Form::input('text', 'first_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.firstName')]) }}
            </div>
            <div class="form-group col-6 required">
                {{ Form::label('last_name', trans('validation.attributes.frontend.register-user.lastName'), ['class' => 'control-label']) }}
                {{ Form::input('text', 'last_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.lastName')]) }}
            </div>
        </div>

        <div class="row normal-labels">
            <div class="form-group col-6">
                {{ Form::label('phone', trans('validation.attributes.frontend.register-user.phone'), ['class' => 'control-label']) }}
                {{ Form::input('text', 'phone', (access()->user()->profile->phone), ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.phone')]) }}
            </div>
            <div class="form-group col-6">
                {{ Form::label('website', trans('validation.attributes.frontend.register-user.website'), ['class' => 'control-label']) }}
                {{ Form::input('text', 'website', (access()->user()->profile->website), ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.website')]) }}
            </div>
        </div>
        <div class="row normal-labels">
            <div class="form-group col-12">
                {{ Form::label('bio', trans('validation.attributes.frontend.register-user.bio'), ['class' => 'control-label']) }}
                {{ Form::textarea('bio', (access()->user()->profile->bio), [
                'class' => 'form-control box-size tinymce',
                'placeholder' => trans('validation.attributes.frontend.register-user.bio')] )}}
            </div>
        </div>


        @if ($logged_in_user->canChangeEmail())
        <div class = "row normal-labels">
            <div class = "form-group col-12 required">
                {{ Form::label('email', trans('validation.attributes.frontend.register-user.email'), ['class' => 'control-label']) }}
                <div class = "alert alert-info">
                    <i class = "fa fa-info-circle"></i> {{ trans('strings.frontend.user.change_email_notice') }}
                </div>
                {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.email')]) }}
            </div>
        </div>
        @endif
    </div>
</div>

<!--Social Media-->
<div class = "card">
    <div class = "card-header">
        <h6 class = "my-2"><i class = "la la-user-astronaut"></i> Social Information</h6>
    </div>
    <div class = "card-body listing-form bold-labels">
        <div class = "row my-3 normal-labels">
            <div class = "form-group col-4">
                {{ Form::label('social_media[facebook]', 'Facebook', ['class' => 'control-label required']) }}
                {{ Form::text('social_media[facebook]', userMedia('facebook'), ['class' => 'form-control box-size', 'placeholder' => 'https://www.facebook.com/']) }}
            </div><!--form group-->

            <div class = "form-group col-4">
                {{ Form::label('social_media[twitter]', 'Twitter', ['class' => 'control-label required']) }}
                {{ Form::text('social_media[twitter]', userMedia('twitter'), ['class' => 'form-control box-size', 'placeholder' => 'https://www.twitter.com/']) }}
            </div><!--form group-->
            <div class = "form-group col-4">
                {{ Form::label('social_media[instagram]', 'Instagram', ['class' => 'control-label required']) }}
                {{ Form::text('social_media[instagram]', userMedia('instagram'), ['class' => 'form-control box-size', 'placeholder' => 'Instagram']) }}
            </div><!--form group-->
            <div class = "form-group col-4">
                {{ Form::label('social_media[youtube]', 'Youtube', ['class' => 'control-label required']) }}
                {{ Form::text('social_media[youtube]', userMedia('youtube'), ['class' => 'form-control box-size', 'placeholder' => 'Youtube']) }}
            </div><!--form group-->

            <div class = "form-group col-4">
                {{ Form::label('social_media[whatsapp]', 'WhatsApp', ['class' => 'control-label required']) }}
                {{ Form::text('social_media[whatsapp]', userMedia('whatsapp'), ['class' => 'form-control box-size', 'placeholder' => 'WhatsApp']) }}
            </div><!--form group-->
            <div class = "form-group col-4">
                {{ Form::label('social_media[skype]', 'Skype', ['class' => 'control-label required']) }}
                {{ Form::text('social_media[skype]', userMedia('skype'), ['class' => 'form-control box-size', 'placeholder' => 'Skype']) }}
            </div><!--form group-->




        </div>
    </div>
</div>
<!--Social Media-->
@include('frontend.user.components.profile.update-button')

@push("after_scripts")

<script>
    jQuery(document).ready(function () {
// Multiple images preview with JavaScript
        var multiImgPreview = function (input, imgPreviewPlaceholder) {
            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function (event) {
                        $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            } else {
                console.log(input);
            }
        };

        $('#profile_picture').on('change', function () {
            let $el = 'div#my-profile-picture';
            jQuery($el).html('');
            multiImgPreview(this, 'div#my-profile-picture');
        });

    });

</script>
@endpush
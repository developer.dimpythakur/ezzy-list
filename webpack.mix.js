const mix = require('laravel-mix');
const WebpackRTLPlugin = require('webpack-rtl-plugin');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


/*
 |--------------------------------------------------------------------------
 | Backend JS
 | merge all needed JS into a big bundle file
 |--------------------------------------------------------------------------
 */

mix.js('resources/assets/js/backend/app.js', 'public/js/backend');
mix.js('resources/assets/js/backend/subscription.js', 'public/js/backend');
mix.js('resources/assets/js/backend/user-dashboard.js', 'public/js/backend');
mix.js('resources/assets/js/backend/yelp.js', 'public/js/backend');
mix.scripts([
    'resources/assets/js/backend/backend-features.js'
], 'public/js/backend/backend-scripts.js');
/*
 |--------------------------------------------------------------------------
 | Backend CSS
 | merge all needed CSS into a file
 |--------------------------------------------------------------------------
 */
mix.sass('resources/assets/sass/backend/app.scss', 'public/css/backend')
        .options({
            processCssUrls: false
        });


/*
 |--------------------------------------------------------------------------
 | Backend Fonts
 | copy fonts and other assets
 |--------------------------------------------------------------------------
 */

mix.copy('node_modules/line-awesome/dist/line-awesome', 'public/packages/line-awesome')
        .copy('node_modules/source-sans-pro', 'public/packages/source-sans-pro')
        .copy('node_modules/animate.css/animate.min.css', 'public/packages/animate.css')
        .copy('node_modules/noty/lib', 'public/packages/noty');
mix.styles([
    "node_modules/source-sans-pro/source-sans-pro.css"
], 'public/css/backend/backend-styles.css');




/*
 |--------------------------------------------------------------------------
 | Backend DataTables CSS
 |--------------------------------------------------------------------------
 */

mix.styles([
    'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
    'node_modules/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css',
    'node_modules/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css'
], 'public/css/backend/datatables.css');
mix.styles(['resources/assets/css/backend/user-dashboard.css'],
        'public/css/backend/user-dashboard.css');

/*
 |--------------------------------------------------------------------------
 | Backend DataTables JS
 |--------------------------------------------------------------------------
 */

mix.scripts([
    'node_modules/datatables.net/js/jquery.dataTables.min.js',
    'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
    'node_modules/datatables.net-responsive/js/dataTables.responsive.min.js',
    'node_modules/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js',
    'node_modules/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
    'node_modules/datatables.net-fixedheader-bs4/js/fixedHeader.bootstrap4.min.js'
], 'public/js/backend/datatables.js');

// copy CRUD filters JS into packages
mix.copy('node_modules/bootstrap-datepicker/dist', 'public/packages/bootstrap-datepicker/dist')
        .copy('node_modules/moment/min', 'public/packages/moment/min')
        .copy('node_modules/select2/dist', 'public/packages/select2/dist')
        .copy('node_modules/jquery-colorbox', 'public/packages/jquery-colorbox')
        .copy('node_modules/jquery-ui-dist', 'public/packages/jquery-ui-dist')
        .copy('node_modules/select2-bootstrap-theme/dist', 'public/packages/select2-bootstrap-theme/dist')
        .copy('node_modules/bootstrap-daterangepicker/daterangepicker.css', 'public/packages/bootstrap-daterangepicker/daterangepicker.css')
        .copy('node_modules/bootstrap-daterangepicker/daterangepicker.js', 'public/packages/bootstrap-daterangepicker/daterangepicker.js')
        .copy('node_modules/pc-bootstrap4-datetimepicker/build', 'public/packages/pc-bootstrap4-datetimepicker/build')
        .copy('node_modules/cropperjs/dist', 'public/packages/cropperjs/dist')
        .copy('node_modules/jquery-cropper/dist', 'public/packages/jquery-cropper/dist')
        .copy('node_modules/ckeditor', 'public/packages/ckeditor')
        .copy('node_modules/bootstrap-colorpicker/dist', 'public/packages/bootstrap-colorpicker/dist')
        .copy('node_modules/bootstrap-iconpicker/bootstrap-iconpicker', 'public/packages/bootstrap-iconpicker/bootstrap-iconpicker')
        .copy('node_modules/bootstrap-iconpicker/icon-fonts', 'public/packages/bootstrap-iconpicker/icon-fonts')
        .copy('node_modules/simplemde/dist', 'public/packages/simplemde/dist')
        .copy('node_modules/easymde/dist', 'public/packages/easymde/dist')
        .copy('node_modules/summernote/dist', 'public/packages/summernote/dist')
        .copy('node_modules/tinymce', 'public/packages/tinymce')
        .copy('node_modules/nestedSortable', 'public/packages/nestedSortable')
        .copy('node_modules/datatables.net', 'public/packages/datatables.net')
        .copy('node_modules/datatables.net-bs4', 'public/packages/datatables.net-bs4')
        .copy('node_modules/datatables.net-fixedheader', 'public/packages/datatables.net-fixedheader')
        .copy('node_modules/datatables.net-fixedheader-bs4', 'public/packages/datatables.net-fixedheader-bs4')
        .copy('node_modules/datatables.net-responsive', 'public/packages/datatables.net-responsive')
        .copy('node_modules/datatables.net-responsive-bs4', 'public/packages/datatables.net-responsive-bs4')
        .copy('node_modules/places.js/dist', 'public/packages/places.js/dist');



/*
 |--------------------------------------------------------------------------
 | FRONTEND JS
 | merge all needed JS into a big bundle file
 |--------------------------------------------------------------------------
 */

mix.js('resources/assets/js/frontend/app.js', 'public/js/frontend');

mix.scripts([
    'resources/assets/js/frontend/frontend-scripts.js'
], 'public/js/frontend/frontend-scripts.js');




mix.sass('resources/assets/sass/frontend/app.scss', 'public/css/frontend')
        .options({
            processCssUrls: false
        });
mix.styles([
    'resources/assets/css/frontend/theme/style.css',
    'resources/assets/css/frontend/theme/colors.css',
    'resources/assets/css/frontend/theme/typeface.css',
    'resources/assets/css/frontend/theme/typography.css',
    'resources/assets/css/frontend/theme/nice-select.css',
    'resources/assets/css/frontend/theme/hc-offcanvas-nav.css',
    'resources/assets/css/frontend/theme/icons.css',
    'resources/assets/css/frontend/theme/responsive.css'
], 'public/css/frontend/frontend-styes.css');



mix.copy([
    'resources/assets/css/frontend/listing-map-manager.css'
], 'public/css/frontend/map/listing-map-manager.css');

mix.copy('node_modules/owl.carousel', 'public/packages/owl.carousel');
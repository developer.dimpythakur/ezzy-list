@php
use Illuminate\Support\Facades\Route;
@endphp
<?php
app_name();
?>
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>
            {{ isset($title) ? $title.' :: '.config('backend.project_name') : config('backend.project_name') }}</title>
        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Eazzy List')">
        <meta name="author" content="@yield('meta_author', 'Eazzy List')">
        <meta name="keywords" content="@yield('meta_keywords', 'Eazzy List')">
        <!--Fevicon-->
        <link rel='shortcut icon' type='image/x-icon' href='{{ Storage::url('images/logo/favicon.ico')}}' />
        <link rel="icon" type="image/png" href="{{ Storage::url('images/logo/favicon-16x16.png')}}"/>
        <script type='application/ld+json'> 
            {
            "@context": "http://www.schema.org",
            "@type": "Restaurant",
            "name": "Ezzy List",
            "url": "http://ezzylist.com",
            "sameAs": [
            "http://ezzylist.com"
            ],
            "logo": "http://ezzylist.com/storage/images/frontend/logo.png",
            "image": "http://ezzylist.com/storage/images/frontend/logo.png"
            }
        </script>
        @yield('meta')
        <!-- Styles -->
        @yield('before-styles')
        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        @langrtl
        {{ Html::style(getRtlCss(('css/frontend/frontend.css'))) }}
        @else
        @if (config('backend.styles_frontend') && count(config('backend.styles_frontend')))
        @foreach (config('backend.styles_frontend') as $path)
        <link rel="stylesheet" type="text/css" href="{{ asset($path)}}">
        @endforeach
        @endif
        @endlangrtl

        <!--{!! Html::style('js/select2/select2.min.css') !!}-->
        @yield('after-styles')
        @stack('after-styles')

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]) ?>
        </script>
        <script>
            var user = '<?php echo (null !== access()->user()) ? access()->user()->id : 0 ?>';
        </script>
        <?php
        if (!empty($google_analytics)) {
//            echo $google_analytics;
        }
        ?>
    </head>
    <body id="app-layout"  @php bodyAttrs() @endphp>
        <div id="app">
            <!-- Header -->
            @include('frontend.includes.header')
            <!-- /Header -->
            @yield('content')
            <!-- Footer -->
            @include('frontend.includes.footer')
            <!-- /Footer -->
        </div><!--#app-->
        <script type='text/javascript'>
            var EZZY_MAP = {
                "submitCenterPoint": "40.757662,-73.974741",
                "centerPoint": "40.757662,-73.974741",
                "map_provider": "mapbox",
                "mapbox_access_token": "pk.eyJ1IjoiZmVsb25pIiwiYSI6ImNrOXMzY290cDBxbGIzZW1ybzM2cngzdnYifQ.Adeh0zFoXWDKXwqE6eQQhA",
                "mapbox_retina": "on",
                "start_point": "New York, NY, USA",
                "start_geo_lat": "40.7127753",
                "start_geo_long": "-74.0059728",
                "enable_map": "1",
                "user_location": [],
                "l10n": {
                    "locked": "Lock ",
                    "unlocked": "Unlock "
                },
                "country": "USA",
                "login_url": "",
                "listings_page_url": "",
            };
        </script>
        @yield('before-scripts')
        @stack('before-scripts')

        @include('frontend.layouts.sections.scripts')
        @include('frontend.includes.modal.auth-modal')
        @yield('after-scripts')
        @stack('after-scripts')
    </body>
</html>
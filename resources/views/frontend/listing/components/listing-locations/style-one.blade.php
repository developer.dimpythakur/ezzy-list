<!-- Explore by city -->

<!-- Top Rated Listings -->
@push('after-styles')
<style>
    .img-max{
        height: 100%;
        max-height: 255px;
        background-size: 100%;
    }

</style>
@endpush
@if(isset($regions) && count($regions))
<section class="et-explore-by-city-wrap position-relative filter-index">
    <div class="container">
        <div class="section-heading mb-3 text-center d-block full-width">
            <h2 class="text-capitalize">Explore by city</h2>
            <p>Explore the greatest places in the city</p>
        </div>
        <div class="row">
            @foreach($regions as $location)
            <div class="col-md-4 my-2">
                <div class="et-explore-by-image img-max grid img-overlay img-box transition-1 card-border-radius position-relative shadow overflow-hidden">
                    <a href="{{ route('frontend.location.show',$location->slug) }}" class="d-block">
                        <div class="city-thumbnail img-scale h-300">
                            @if ($location->image && !is_null($location->image))
                            <img src="{{ Storage::url('images/listing/regions/' . $location->image) }}" height="100"
                                 class="img img-preview" width="100%">
                            @else
                            <img src="{{route('frontend.index')}}/img/frontend/listings/coffee-1.jpg"
                                 class="img-fluid" alt="listing">
                            @endif
                        </div>
                        <div class="city-data text-white position-absolute fixed-bottom text-right p-3">
                            <h4 class="title text-white">
                                {{ ($location->locations_count) }} Entries</h4>
                            <h5 class="city-name mb-0 text-white">{{ $location->name }}</h5>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@else
<section class="et-explore-by-city-wrap position-relative filter-index">
    <div class="container">
        <div class="section-heading mb-3 text-center d-block full-width">
            <h2 class="text-capitalize">Explore by city</h2>
            <p>Explore the greatest places in the city</p>
        </div>
        <div class="row mt-5 mt-xs">
            <div class="col-lg-6 text-center p-4 bg-white mx-auto">
                <h3 class="text-primary">We don't have any city to show you </h3>
                <p class="pt-4">
                    <a href="#" class="btn btn-darks border-0 m-auto">
                        <span class="la la-plus-square-o"></span> 
                        Please login or create new litsing.
                    </a>
                </p>
            </div>
        </div>
    </div>
</section>
@endif
<!-- /Explore By City -->

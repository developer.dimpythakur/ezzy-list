<div data-id="{{ $listing->id }}" class="row bg-white mt-5 mt-xs ezzy-single-listing pb-0 single-style-one"
     data-action="{{ URL::to('listing/'.$listing->slug)  }}"
     data-color="#ee4124"
     data-title="{{ $listing->name }}"
     data-website="{{ $listing->website  }}"
     data-phone="{{ $listing->phone  }}"
     data-friendly-address="{{$listing->location->pluck('address')->first()}}"
     data-address="{{$listing->location->pluck('address')->first()}}"
     data-image="{{ $listing->banner }}"
     data-longitude="{{$listing->location->pluck('longitude')->first()}}"
     data-latitude="{{$listing->location->pluck('latitude')->first()}}"
     data-rating="{{ $listing->averageRating(2) }}"
     data-url="{{ URL::to('listing/'.$listing->slug)  }}"
     data-reviews="{{ $listing->averageRating(2) }}"
     data-categories="{{ $listing->categories->pluck('name')->first() }}"
     data-icon="<i class=&quot;{{  ( $listing->categories->count())? $listing->categories->first()->icon:''  }}&quot;></i>">
    <div class="col-lg-4 col-sm-12 pl-md-0 mb-3 mb-md-0">
        <div class="top-rated-thumbnail  img-max  transition-1 overflow-hidden card-border-radius img-overlay position-relative">
            @if ($listing->timings && !is_null($listing->timings))
            @if(timingStatus($listing->timings) && timingStatus($listing->timings) == 'open')
            <span class="p-1 position-absolute small business-time right bg-success text-white pl-2 pr-2">
                <i class="las la-clock"></i> Open Now
            </span>
            @else
            <span class="p-1 position-absolute small business-time right bg-primary text-white pl-2 pr-2">
                <i class="las la-clock"></i> Closed
            </span>
            @endif
            @else
            <span class="p-1 position-absolute small business-time right bg-primary text-white pl-2 pr-2">
                <i class="las la-clock"></i> Closed</span>
            @endif

            <span class="p-2 position-absolute small business-category bg-primary text-white pl-2 pr-2">
                @if ($listing->categories && !is_null($listing->categories) && count( $listing->categories))
                <i class="{{ $listing->categories->first()->icon  }}"></i>
                {{ $listing->categories->first()->name  }}
                @else
                Expesso Coffee
                @endif
            </span>
            <div class="img-scale h-100" data-action="{{ $listing->banner }}">
                @if ($listing->banner && !is_null($listing->banner))
                <img src="{{ $listing->banner }}"
                     class="img-fluid h-100" alt="{{ $listing->name }}">
                @else
                <img src="{{  Storage::url('images/frontend/listings/coffee-1.jpg')}}"
                     class="img-fluid h-100" alt="listing">
                @endif
            </div>
        </div>
    </div>
    <div class="col-lg-8 pt-3">
        <a href="{{ URL::to('listing/'.$listing->slug)  }}"
           class="text-decoration-none text-reset text-capitalize style-on-title">
            <h3 class="listing-title-one">{{ $listing->name  }}
                <i class="las la-check-circle"
                   data-toggle="tooltip"
                   data-placement="top"
                   title=""
                   data-original-title="Claimed"></i>
            </h3>
        </a>
        <p class="my-2">{{ substr($listing->description,0,50)  }}</p>
        <ul class="et-list-info">
            @if(null !== $listing->location->pluck('address')->first())
            <li>
                <i class="las la-map-marker-alt primary-icon"></i>
                <p>{{$listing->location->pluck('address')->first()}}&nbsp;</p>
            </li>
            @endif
            <li>
                <i class="las la-phone primary-icon"></i>
                <p>{{ ($listing->phone)  }}</p>
            </li>
            @if($listing->website !==null) 
            <li>
                <i class="las la-link primary-icon"></i>
                <p>{{ ($listing->website)  }}</p>
            </li>
            @endif
        </ul>
        <div class="et-list-grid-main">
            <div class="grid-rating-section home">
                <div class="star-rating pt-1">
                    <span class="et-view-count">
                        <i class="las la-eye"></i>
                        {{  views($listing)->count() }}
                    </span>
                    <span class="et-location-icon">
                        <i class="las la-map-marker-alt"></i>
                    </span>
                    <span 
                        title="Save listing" 
                        data-title="Save listing" 
                        data-id="{{ $listing->id}}"
                        data-url="{{ route('frontend.listing-bookmark')}}"
                        class="et-save-icon {{ is_bookmarked($listing)}}"><i class="las la-heart"></i></span>
                        <?php
                        $rating = $listing->averageRating(2);
                        $filterRatings = collect($rating)->filter();
                        ?>
                    <span class="badge listing-rating badge-secondary ml-2"
                          data-rating="{{ (count($filterRatings))?$listing->averageRating(2):'No ratings' }}">
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
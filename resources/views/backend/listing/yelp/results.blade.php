@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.yelp.management'))
@section('page-header')
<h1>Search Yelp</h1>
@endsection
@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">Yelp Business</span>
        <div class="dataTables_info" id="crudTable_info" role="status" aria-live="polite">
            <?php
            echo 'Total Result Found ' . (isset($totalResults)) ? $totalResults : 0;
            ?>
        </div>
    </h2>
</div>
@endsection



@section('after_styles')
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">

<style>

    .strip {
        position:relative;
        margin-bottom:30px
    }
    .strip figure {
        margin-bottom:5px;
        overflow:hidden;
        position:relative;
        height:190px;
        -webkit-border-radius:5px;
        -moz-border-radius:5px;
        -ms-border-radius:5px;
        border-radius:5px;
        background-color:#ededed
    }
    .strip figure img {
        position:absolute;
        left:50%;
        top:50%;
        -webkit-transform:translate(-50%,
            -50%) scale(1);
        -moz-transform:translate(-50%,
            -50%) scale(1);
        -ms-transform:translate(-50%,
            -50%) scale(1);
        -o-transform:translate(-50%,
            -50%) scale(1);
        transform:translate(-50%,
            -50%) scale(1);
        -webkit-backface-visibility:hidden;
        -moz-backface-visibility:hidden;
        -ms-backface-visibility:hidden;
        -o-backface-visibility:hidden;
        backface-visibility:hidden;
        width:100%;
        z-index:1;
        -moz-transition:all 0.3s ease-in-out;
        -o-transition:all 0.3s ease-in-out;
        -webkit-transition:all 0.3s ease-in-out;
        -ms-transition:all 0.3s ease-in-out;
        transition:all 0.3s ease-in-out
    }
    .strip figure:hover img {
        -webkit-transform:translate(-50%,
            -50%) scale(1.1);
        -moz-transform:translate(-50%,
            -50%) scale(1.1);
        -ms-transform:translate(-50%,
            -50%) scale(1.1);
        -o-transform:translate(-50%,
            -50%) scale(1.1);
        transform:translate(-50%,
            -50%) scale(1.1)
    }
    .strip figure a.strip_info {
        position:absolute;
        left:0;
        bottom:0;
        right:0;
        height:100%;
        width:100%;
        z-index:5;
        display:block
    }
    .strip figure a.strip_info>small {
        position:absolute;
        background-color:#000;
        background-color:#000;
        left:15px;
        top:15px;
        text-transform:uppercase;
        color:#fff;
        font-weight:600;
        -webkit-border-radius:3px;
        -moz-border-radius:3px;
        -ms-border-radius:3px;
        border-radius:3px;
        padding:6px 8px 4px 8px;
        line-height:1;
        font-size:11px;
        font-size:0.6875rem
    }
    .strip figure .item_title {
        width:100%;
        position:absolute;
        bottom:0;
        left:0;
        padding:65px 15px 10px 15px;
        background:-moz-linear-gradient(top,
            transparent 5%,
            #000 100%);
        background:-webkit-linear-gradient(top,
            transparent 5%,
            #000 100%);
        background:linear-gradient(to bottom,
            transparent 5%,
            #000 100%);
        filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000',
            endColorstr='#000000',
            GradientType=0)
    }
    .strip figure .item_title h3 {
        font-size:16px;
        font-size:1rem;
        margin:0;
        font-weight:500;
        color:#fff;
        margin:0;
        padding:0;
        line-height:1
    }
    .strip figure .item_title small {
        font-weight:500;
        line-height:1;
        font-size:13px;
        font-size:0.8125rem;
        color:rgba(255,
            255,
            255,
            0.8)
    }
    .strip ul {
        padding:0;
        margin:0
    }
    .strip ul li {
        display:inline-block;
        padding-top:12px;
        font-size:13px;
        font-size:0.8125rem
    }
    .strip ul li:last-child {
        padding:0;
        float:right
    }
    .ribbon {
        color:#fff;
        display:inline-block;
        font-size:12px;
        font-size:0.75rem;
        line-height:1;
        position:absolute;
        top:12px;
        right:15px;
        padding:7px 8px 4px 8px;
        font-weight:600;
        min-width:40px;
        z-index:9
    }
    .ribbon.off {
        background-color:#ff3300
    }
    .score strong {
        background-color:#f0f0f0;
        line-height:1;
        -webkit-border-radius:5px 5px 5px 0;
        -moz-border-radius:5px 5px 5px 0;
        -ms-border-radius:5px 5px 5px 0;
        border-radius:5px 5px 5px 0;
        padding:10px 10px 8px 10px;
        display:inline-block;
        font-size:15px;
        font-size:0.9375rem
    }
    .score span {
        display:inline-block;
        position:relative;
        top:7px;
        margin-right:8px;
        font-size:12px;
        font-size:0.75rem;
        text-align:right;
        line-height:1.1;
        font-weight:500
    }
    .score span em {
        display:block;
        font-weight:normal;
        font-size:11px;
        font-size:0.6875rem
    }
</style>

@endsection


@section('content')
<!-- Default box -->
<?php
//dd($business);
?>
<div class="row">
    @if(!$business)
    @foreach($business as $result)
    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
        <div class="strip">
            <figure>
                <span class="ribbon off">-40%</span>
                <img src="{{ $result->image_url}}" data-src="{{ $result->image_url}}" class="img-fluid lazy loaded" alt="" data-was-processed="true">
                <a href="{{ $result->url}}" class="strip_info">
                    <small>{{ $result->name}}</small>
                    <div class="item_title">
                        <h3>{{ $result->name}}</h3>
                        <small>{{ $result->alias}}</small>
                    </div>
                </a>
            </figure>
            <ul>
                <li><span>Avg. Price 14$</span></li>
                <li>
                    <div class="score">
                        <span>Superb<em>{{$result->review_count}} Reviews</em></span>
                        <strong>{{$result->rating}}</strong>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    @endforeach
    @endif
</div>
<div class="row">
    <div class="table-responsive data-table-wrapper">
        <table id="yelp-table" 
               class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2 dataTable dtr-inline">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Rating</th>
                    <th>Reviews</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($business)
                @foreach($business as $result)
                <tr>

                    <td>{{ $result->id}}</td>
                    <td>{{ $result->name}}</td>
                    <td>{{ $result->rating}}</td>
                    <td>{{ $result->review_count}}</td>
                    <td>{{ $result->location->address1}} , {{ $result->location->city}}</td>
                    <td>{{ $result->display_phone}}</td>
                    <td>
                        <button class="btn btn-primary">
                            <span><i class="la la-plus-square-o"></i>&nbsp;Add</span>
                        </button>  
                    </td>
                </tr>

                @endforeach
                @endif
            </tbody>
            <tfoot>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Rating</th>
                    <th>Reviews</th>
                    <th>Address</th>
                    <th>Phone</th>
                </tr>
            </tfoot>
        </table>
    </div><!--table-responsive-->
</div>
@endsection


@section('before_scripts')
{{ Html::script(mix('js/backend.js')) }}
{{ Html::script(mix('js/backend-custom.js')) }}
@endsection

@section('after_scripts')


<!-- DATA TABLES SCRIPT -->
<script type="text/javascript" src="{{ asset('packages/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/datatables.net-fixedheader-bs4/js/fixedHeader.bootstrap4.min.js') }}"></script>




<script>
jQuery(document).ready(function () {

});</script>

@endsection
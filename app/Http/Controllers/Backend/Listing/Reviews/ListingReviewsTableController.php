<?php

namespace App\Http\Controllers\Backend\ListingReviews;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\ListingReviews\ListingReviewRepository;
use App\Http\Requests\Backend\ListingReviews\ManageListingReviewRequest;

/**
 * Class ListingReviewsTableController.
 */
class ListingReviewsTableController extends Controller
{
    /**
     * variable to store the repository object
     * @var ListingReviewRepository
     */
    protected $listingreview;

    /**
     * contructor to initialize repository object
     * @param ListingReviewRepository $listingreview;
     */
    public function __construct(ListingReviewRepository $listingreview)
    {
        $this->listingreview = $listingreview;
    }

    /**
     * This method return the data of the model
     * @param ManageListingReviewRequest $request
     *
     * @return mixed
     */
    public function __invoke(ManageListingReviewRequest $request)
    {
        return Datatables::of($this->listingreview->getForDataTable())
            ->escapeColumns(['id'])
            ->addColumn('created_at', function ($listingreview) {
                return Carbon::parse($listingreview->created_at)->toDateString();
            })
            ->addColumn('actions', function ($listingreview) {
                return $listingreview->action_buttons;
            })
            ->make(true);
    }
}

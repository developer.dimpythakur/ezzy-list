@push('after-styles')
{{ Html::style(('css/frontend/map/listing-map-manager.css')) }}
@endpush
<!-- Slider -->
<div class="et-home-slider et-home-map-banner img-overlay transition-1 no-hover home-slide-search">
    <div class="map" id="map"></div>
    {{ Form::open(['route' => 'frontend.search.result', 'class' => 'home-search position-absolute form-horizontal','autocomplete'=>'off', 'method' => 'GET']) }}
    <div class="input-wrap search-cont d-flex border rounded-pill p-1">
        <div class="near-field position-relative d-flex align-items-center">
            {{ Form::text('search_keywords',null,['placeholder'=>"What You Are Looking For",'class'=>'form-control bg-transparent border-0 ml-2 pl-2 text-white']) }}
        </div>
        <div class="find-field position-relative">
            @if($categories && count($categories))
            @if($categories)
            <select name="category" id="search_categories" class="form-control form-control wide bg-transparent border-0">
                <option value="0" data-display="Any Category">Any Category</option>
                @foreach($categories as $category)
                <option {{ (isset($searchTermCategory) && ($searchTermCategory) ===strtolower($category->name))?'selected':'' }} 
                    value="{{ strtolower($category->name)}}">{{$category->name}}
                   </option>
                @endforeach
            </select>
            @endif
            @endif
        </div>
        <div class="find-field position-relative">
            <select name="location" class="form-control form-control wide bg-transparent border-0">
                @if($regions)
                <option value="0" data-display="Any Location">Any Location</option>
                @foreach($regions as $region)
                <option {{ (isset($location) && ($location) === strtolower($region->name))?'selected':'' }} 
                    value="{{strtolower($region->name)}}">
                    {{$region->name}}</option> 
                @endforeach
                @endif
            </select>
        </div>
        <div class="submit-btn rounded-pill btn btn-primary border-0 transition-1 ml-auto position-relative pointer-event d-flex justify-content-center align-items-center">
            <i class="las la-search" aria-hidden="true"></i>
            <input type="submit" class="btn transition-1 field-btn position-absolute" value="">
        </div>
    </div>
    <div class="col-md-6">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        @if (session('warning'))
        <div class="alert alert-warning">
            {{ session('warning') }}
        </div>
        @endif
    </div>
    {{ Form::close() }}

</div>
<!-- /Slider -->


@push('after-scripts')
<script type='text/javascript'>
    var EZZY_MAP = {
        "submitCenterPoint": "40.757662,-73.974741",
        "centerPoint": "40.757662,-73.974741",
        "map_provider": "mapbox",
        "mapbox_access_token": "pk.eyJ1IjoiZmVsb25pIiwiYSI6ImNrOXMzY290cDBxbGIzZW1ybzM2cngzdnYifQ.Adeh0zFoXWDKXwqE6eQQhA",
        "mapbox_retina": "on",
        "start_point": "New York, NY, USA",
        "start_geo_lat": "40.7127753",
        "start_geo_long": "-74.0059728",
        "enable_map": "1",
        "user_location": [],
        "l10n": {
            "locked": "Lock Pin",
            "unlocked": "Unlock Pin"
        },
        "country": "USA",
        "strings": {
            "file-upload": "Add Image",
            "not_found": "No results",
            "results-no": "Results",
            "select_options": "Select Some Options",
            "select_option": "Select an Option",
            "no_results_match": "No results match"
        }
    };
</script>
<!-- Map API -->
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAmrxCFuWyNUlMh6wLitVLI3pF7j_HqAFY&#038;libraries=places&#038;ver=1.1'></script>
<!-- Custom Js -->
<script src="{{ asset('listing/jquery.cookie.min.js') }}"></script>
<script src="{{ asset('listing/mapify.js') }}"></script>
<script src="{{ asset('listing/geo-tag-text.js') }}"></script>
<script src="{{ asset('listing/map/leaflet.js') }}"></script>
<script src="{{ asset('listing/listing-manager.js') }}"></script>
@endpush
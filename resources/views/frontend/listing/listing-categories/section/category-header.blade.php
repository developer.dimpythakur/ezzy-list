<section class="et-page-header position-relative text-center"
         style="background-image: url({{ Storage::url('images/category/'.$category->image) }})">
    <div class="header-content position-relative text-white">
        <ul class="nostyle list-inline breadcrumb-links mb-1">
            <li><a href="{{ URL::to('/') }}">Home</a>
            </li>
            <li class="current">
                <a href="{{ route('frontend.listing-categories') }}">
                    Categories
                </a>
            </li>
        </ul>
        <h1 class="page-title d-inline-flex align-items-center m-0 position-relative">{{ $category->name }}</h1>
        <p class="mb-0 mt-1 text-white">{{ substr($category->description,0,90) }}</p>
    </div>
</section>
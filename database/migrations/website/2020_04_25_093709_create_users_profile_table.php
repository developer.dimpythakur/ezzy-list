<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersProfileTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users_profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('avatar', 255)->nullable();
            $table->string('phone')->nullable();
            $table->string('bio')->nullable();
            $table->string('website')->nullable();
            $table->text('address')->nullable();
            $table->date('birthday')->nullable();
            $table->string('city', 100)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('social_media', 255)->nullable();
            $table->string('gender')->nullable();
            $table->boolean('active')->default(1);
            $table->timestamp('last_logged')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users_profile');
    }

}

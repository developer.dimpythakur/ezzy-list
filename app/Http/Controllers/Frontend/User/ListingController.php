<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;

/*
  |--------------------------------------------------------------------------
  | Requests
  |--------------------------------------------------------------------------
  |
  | Used to handle the requests
 */
use App\Http\Requests\Frontend\Listing\Create;
use App\Http\Requests\Frontend\Listing\Delete;
use App\Http\Requests\Frontend\Listing\Edit;
use App\Http\Requests\Frontend\Listing\Manage;
use App\Http\Requests\Frontend\Listing\Store;
use App\Http\Requests\Frontend\Listing\Update;

/*
  |--------------------------------------------------------------------------
  | Model
  |--------------------------------------------------------------------------
  |
 */
use App\Models\Listing\Listing\Listing;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\Backend\Listing\EditResponse;
use App\Http\Responses\ViewResponse;
use App\Repositories\Backend\Listings\ListingsRepository;
use Illuminate\Http\Request;
use Carbon\Carbon;
// Helper Models
use App\Models\Listing\Categories\Category;
use App\Models\Listing\Tags\Tag;
use App\Models\Listing\Region\Region;
//Slugable
use Cviebrock\EloquentSluggable\Services\SlugService;

class ListingController extends Controller {

    /**
     * Modal Instance
     * @var type 
     */
    protected $listings;

    /**
     * Tags
     * @var type 
     */
    protected $tags = null;

    /**
     * Categories
     * @var type 
     */
    protected $categories = null;

    /**
     * Regions
     * @var type 
     */
    protected $regions = null;

    /**
     * Listing Status.
     */
    protected $status = [
        'publish' => 'Published',
        'review' => 'In Review',
        'pending' => 'Pending',
        'expired' => 'Expired',
    ];

    /**
     * ListingsController constructor.
     * @param ListingsRepository $listings
     */
    public function __construct(ListingsRepository $listing) {
        $this->listings = $listing;
        $this->tags = Tag::withCount('listing')->orderBy('listing_count', 'desc')->limit(6)->get()->pluck('name', 'id')->toArray();
        $this->categories = Category::withCount('listing')->orderBy('listing_count', 'desc')->limit(20)->get()->pluck('name', 'id')->toArray();
        $this->regions = Region::withCount('locations')->orderBy('locations_count', 'desc')->limit(20)->get()->pluck('name', 'id')->toArray();
    }

    /**
     * Display listings
     * @param Manage $request
     * @return ViewResponse
     */
    public function index() {
        $listings = Listing::inRandomOrder()->where('created_by', access()->id())->where('status', 'publish')->paginate(10);
        return view('frontend.user.listing.listing.index')->with([
                    'listings' => $listings
        ]);
    }

    /**
     * @param Create $request
     * @return ViewResponse
     */
    public function create() {
        return view('frontend.user.listing.listing.create', [
            'tags' => $this->tags,
            'categories' => $this->categories,
            'regions' => $this->regions,
        ]);
    }

    public function yelp() {
        return view('frontend.user.listing.listing.yelp', [
            'tags' => $this->tags,
            'categories' => $this->categories,
            'regions' => $this->regions,
        ]);
    }

    /**
     * @param Store $request
     * @return RedirectResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function store(Store $request) {
//    public function store(Request $request) {
        $this->listings->create($request);
        return new RedirectResponse(route('frontend.user.listings.index'), trans('alerts.backend.listings.created'));
    }

    /**
     * @param Listing $listing
     * @param Edit $request
     * @return EditResponse
     */
    public function edit(Listing $listing, Edit $request) {
        return view('frontend.user.listing.listing.edit')->with([
                    'listing' => $listing,
                    'status' => $this->status,
                    'categories' => $this->categories,
                    'tags' => $this->tags,
                    'regions' => $this->regions,
        ]);
    }

    /**
     * 
     * @param Listing $listing
     * @param Update $request
     * @return RedirectResponse
     */
    public function update(Listing $listing, Update $request) {
        $this->listings->update($listing, $request);
        return new RedirectResponse(route('frontend.user.listings.index'), trans('alerts.backend.listings.updated'));
    }

    /**
     * @param Listing $listing
     * @param Delete $request
     * @return RedirectResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Listing $listing, Delete $request) {
        $this->listings->delete($listing);
        return new RedirectResponse(route('frontend.user.listings.index'), trans('alerts.backend.listings.deleted'));
    }

    /**
     * Check if slug already exist
     * @param Request $request
     * @return type
     */
    public function check_slug(Request $request) {
        // New version: to generate unique slugs
        $slug = SlugService::createSlug(Listing::class, 'slug', $request->name);
        return response()->json(['slug' => $slug]);
    }

    /**
     * Active listings
     * @return type
     */
    public function active() {
        $listings = Listing::published()->paginate(10);

        return view('frontend.user.listing.listing.index')->with([
                    'listings' => $listings
        ]);
    }

    /**
     * Pending Listings
     * @return type
     */
    public function pending() {
        $listings = Listing::pending()->paginate(10);
        return view('frontend.user.listing.listing.index')->with([
                    'listings' => $listings
        ]);
    }

    /**
     * Expired listings
     * @return type
     */
    public function expired() {
        $listings = Listing::expired()->paginate(10);
        return view('frontend.user.listing.listing.index')->with([
                    'listings' => $listings
        ]);
    }

}


@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.blogtags.management') . ' | ' . trans('labels.backend.blogtags.edit'))

@section('page-header')
<h1>{{ trans('labels.backend.blogtags.management') }}</h1>
@endsection

@php
$breadcrumbs = [
'Dashboard' => url(config('backend.route_prefix'), 'dashboard'),
trans('menus.backend.blogtags.all') => route('admin.blogTags.index'),
trans('menus.backend.blogtags.create') => false,
];
@endphp


@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.blogtags.management') }}</span>
    </h2>
</div>
@endsection

@section('content')

<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::open(['route' => 'admin.blogTags.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-blogtags']) }}
        <div class="card">
            <div class="card-body row">
                @include("backend.blogtags.form")
            </div><!-- /.card-body -->
        </div><!--card-->

        <div id="saveForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=save_and_back">Save and Back</span>
                </button>
            </div>
            <a href="{{ route('admin.blogTags.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>


@endsection
@section("after_scripts")


@endsection







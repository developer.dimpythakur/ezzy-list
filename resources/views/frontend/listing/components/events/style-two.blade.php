<!-- Listing Events -->
<section class="et-featured-categories events home position-relative">
    <div class="container">
        <div class="section-heading text-center d-block full-width">
            <h2 class="text-capitalize">Listing Events</h2>
            <p>Events Assoicated With the listings are shown here.</p>
        </div>
        <div class="row">
            <div class="col-md-4 mt-5 mt-xs">
                <div class="text-white text-decoration-none">
                    <div class="c-card-f mb-2 transition-1 img-overlay transition-1 card-border-radius position-relative">
                        <div class="post-date">12
                            <br>jan
                        </div>
                        <div class="et-author-img-box">
                            <img src="{{route('frontend.index')}}/img/frontend/team/team-member-1.jpg" class="et-author-img" alt="author-img">
                            <img src="{{route('frontend.index')}}/img/frontend/team/team-member-2.jpg" class="et-author-img" alt="author-img">
                            <img src="{{route('frontend.index')}}/img/frontend/team/team-member-3.jpg" class="et-author-img" alt="author-img">
                        </div>
                        <img src="{{route('frontend.index')}}/img/frontend/event/event-1.jpg" class="img-fluid event-img" alt="listing">
                        <div class="grid-rating-section p-3 pt-0 pb-4">
                            <div class="star-rating">	<a href="#" class="et-location-icon text-white text-decoration-none"><i class="las la-map-marker-alt"></i></a>
                            </div>
                            <a href="javascript:void(0)" class="btn btn-primary small get-tickets p-2 border-0">Tickets</a>
                        </div>
                        <div class="slide-data text-center midblock position-absolute">
                            <h3 class="text-capitalize text-white">
                                <a href="javascript:void(0)" class="text-white text-decoration-none"> Book Fair </a>
                            </h3>
                            <span class="d-block entries transition-1 mt-2">Organizer: <strong>John</strong></span>
                            <span class="d-block entries transition-1 mt-2">Listing: <strong><a href="#" class="text-white text-decoration-none">Mart Bookshop</a></strong></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-5 mt-xs">
                <div class="text-white text-decoration-none">
                    <div class="c-card-f mb-2 transition-1 img-overlay transition-1 card-border-radius position-relative">
                        <div class="post-date">12
                            <br>jan
                        </div>
                        <div class="et-author-img-box">
                            <img src="{{route('frontend.index')}}/img/frontend/team/team-member-1.jpg" class="et-author-img" alt="author-img">
                            <img src="{{route('frontend.index')}}/img/frontend/team/team-member-2.jpg" class="et-author-img" alt="author-img">
                            <img src="{{route('frontend.index')}}/img/frontend/team/team-member-3.jpg" class="et-author-img" alt="author-img">
                        </div>
                        <img src="{{route('frontend.index')}}/img/frontend/event/event-3.jpg" class="img-fluid event-img" alt="listing">
                        <div class="grid-rating-section p-3 pt-0 pb-4">
                            <div class="star-rating">	<a href="#" class="et-location-icon text-white text-decoration-none"><i class="las la-map-marker-alt"></i></a>
                            </div>
                            <a href="javascript:void(0)" class="btn btn-primary small get-tickets p-2 border-0">Tickets</a>
                        </div>
                        <div class="slide-data text-center midblock position-absolute">
                            <h3 class="text-capitalize text-white">
                                <a href="javascript:void(0)" class="text-white text-decoration-none"> Food Festival </a>
                            </h3>
                            <span class="d-block entries transition-1 mt-2">Organizer: <strong>Morras</strong></span>
                            <span class="d-block entries transition-1 mt-2">Listing: <strong><a href="#" class="text-white text-decoration-none">Sea Restaurant</a></strong></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-5 mt-xs">
                <div class="text-white text-decoration-none">
                    <div class="c-card-f mb-2 transition-1 img-overlay transition-1 card-border-radius position-relative">
                        <div class="post-date">23
                            <br>Feb
                        </div>
                        <div class="et-author-img-box">	<a href="#" class="text-white text-decoration-none attente-more"><i class="fas fa-plus"></i></a>
                            <img src="{{route('frontend.index')}}/img/frontend/team/team-member-1.jpg" class="et-author-img" alt="author-img">
                            <img src="{{route('frontend.index')}}/img/frontend/team/team-member-2.jpg" class="et-author-img" alt="author-img">
                        </div>
                        <img src="{{route('frontend.index')}}/img/frontend/event/event-2.jpg" class="img-fluid event-img" alt="listing">
                        <div class="grid-rating-section p-3 pt-0 pb-4">
                            <div class="star-rating">	<a href="#" class="et-location-icon text-white text-decoration-none"><i class="las la-map-marker-alt"></i></a>
                            </div>
                            <a href="javascript:void(0)" class="btn btn-primary small get-tickets p-2 border-0">Tickets</a>
                        </div>
                        <div class="slide-data text-center midblock position-absolute">
                            <h3 class="text-capitalize text-white">
                                <a href="javascript:void(0)" class="text-white text-decoration-none"> Jack Music Show </a>
                            </h3>
                            <span class="d-block entries transition-1 mt-2">Organizer: <strong>Mart</strong></span>
                            <span class="d-block entries transition-1 mt-2">Listing: <strong>
                                    <a href="#" class="text-white text-decoration-none">Live Music Show</a>
                                </strong></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-center">
                <a href="javascript:void(0)" class="btn btn-primary border-0 m-auto">View More Events</a>
            </div>
        </div>
    </div>
</section>
<!-- /Listing Events -->
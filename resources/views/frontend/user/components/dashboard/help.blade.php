<div class="card rounded-0 border-0 contact py-6 px-3">
    <div class="card-body text-center p-4">
        <div class="contact-icon text-dark mb-3">
            <i class="la la-user-circle-o la-9x"></i>
        </div>
        <div class="text-dark font-size-md mb-5">
            <p class="mb-2">Have any problem and<br> need support? Call Us directly
            </p>
            <p class="font-weight-semibold h5 mb-2">(+12) 345-678-888</p>
            <p>Or chat with us</p>
        </div>
        <a href="#" class="btn btn-primary font-size-md px-8 lh-15">Contact Us</a>
    </div>
</div>
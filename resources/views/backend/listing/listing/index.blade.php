@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.listing.management'))
@section('page-header')
<h1>{{ trans('labels.backend.listing.management') }}</h1>
@endsection
@section('after_styles')
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/backend/datatables.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-12 mb-4">
        <div class="row mb-0">
            <div class="col-sm-6">
                <div class="hidden-print with-border">
                    <a href="{{route('admin.listings.create')}}" class="btn btn-primary" data-style="zoom-in">
                        <span class="ladda-label"><i class="la la-plus"></i> {{trans('labels.backend.listing.create')}}</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 mb-4">
        <div class="table-responsive data-table-wrapper">
            <table id="listings-table" 
                   class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2 dataTable dtr-inline">
                <thead>
                    <tr>
                        <th>{{ trans('labels.backend.listing.table.title') }}</th>
                        <th>{{ trans('labels.backend.listing.category') }}</th>
                        <th>{{ trans('labels.backend.listing.table.status') }}</th>
                        <th>{{ trans('labels.backend.listing.table.createdat') }}</th>
                        <th>{{ trans('labels.backend.listing.table.createdby') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>{{ trans('labels.backend.listing.table.title') }}</th>
                        <th>{{ trans('labels.backend.listing.category') }}</th>
                        <th>{{ trans('labels.backend.listing.table.status') }}</th>
                        <th>{{ trans('labels.backend.listing.table.createdat') }}</th>
                        <th>{{ trans('labels.backend.listing.table.createdby') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </tfoot>
            </table>
        </div><!--table-responsive-->
    </div>
</div>
@endsection

@section('after_scripts')
<!-- DATA TABLES SCRIPT -->
<script type="text/javascript" src="{{ asset('js/backend/datatables.js') }}"></script>
<script>
jQuery(document).ready(function () {
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var dataTable = $('#listings-table').dataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ route("admin.listings.get") }}',
                type: 'post'
            },
            columns: [
                {data: 'name', name: "name"},
                {data: 'category', name: 'category'},
                {data: 'status', name: 'status'},
                {data: 'created_at', name: 'created_at'},
                {data: 'created_by', name: 'created_by', sortable: false},
                {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ],
            order: [[1, "desc"]],
            searchDelay: 500,
            dom: "<'row hidden'<'col-sm-6 hidden-xs'i><'col-sm-6 hidden-print'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row mt-2 '<'col-sm-6 col-md-4'l><'col-sm-2 col-md-4 text-center'B><'col-sm-6 col-md-4 hidden-print'p>>",
            buttons: {},
            language: {
                @lang('datatable.strings')
            },
            initComplete: function () {
//      
            }
        });

        Backend.DataTableSearch.init(dataTable);
    });
});</script>

@endsection
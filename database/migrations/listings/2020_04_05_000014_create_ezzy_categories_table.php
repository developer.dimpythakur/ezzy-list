<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEzzyCategoriesTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ezzy_categories';

    /**
     * Run the migrations.
     * @table ezzy_categories
     *
     * @return void
     */
    public function up() {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100)->nullable()->default(null);
            $table->string('slug', 200)->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->string('image')->nullable()->default(null);
            $table->string('icon')->nullable()->default(null);
            $table->string('type')->nullable()->default('ezzy');
            $table->bigInteger('parent_id')->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default(1);

            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table($this->tableName, function ($table) {
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->tableName);
    }

}

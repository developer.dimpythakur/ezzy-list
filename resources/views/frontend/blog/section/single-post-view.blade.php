<div class="blog-post shadow mb-5">
    <div class="blog-thumbnail rounded-top position-relative transition-1 img-overlay">

        @if(($post->featured_image) && ($post->featured_image) != 'default-image.jpg' && $post->type =='feed')
        <img src="{{ $post->featured_image }}"
             alt="{{ $post->name }}"
             class="img-fluid rounded-top">
        @else
        <img src="{{ Storage::url('images/blog/' . $post->featured_image) }}" alt="{{ $post->name }}"
             class="img-fluid rounded-top">
        @endif
        <a href="#" class="badge badge-primary mt-3 ml-3 position-absolute p-2">{{ $post->blog}}</a>
        <span class="post-type position-absolute h4 text-white bg-dark p-2 card-border-radius icon-picture"></span>
    </div>
    <div class="pt-0 pb-4 pl-4 pr-4 position-relative">
        <div class="post-meta pb-3 pt-3">
            <a class="text-muted pr-2" href="#">
                <i class="las la-clock"></i>
                {{\Carbon\Carbon::parse($post->publish_datetime)->format('M, d, Y ')}}
            </a>
            <a class="text-muted pr-2" href="#">
                <i class="las la-user-circle"></i> By {{ $post->owner->first_name}}
            </a>
            @if(!empty($post->categories))
            @php $limit=1 @endphp
            @foreach($post->categories  as $category)
            <a class="text-muted pr-2" href="javascript:void(0)">
                <i class="las la-save"></i>
                {{$category->name }}
            </a>
            @if($limit==3)
            @php break @endphp
            @endif

            @endforeach
            @else
            <a class="text-muted pr-2" href="javascript:void(0)">
                <i class="icon-icon"></i>
                Travel & lifestyle
            </a>
            @endif
        </div>
        <h4>
            <a href="{{ URL::to('post').'/'.$post->slug }}" class="text-body">
                {{ $post->name }}
            </a>
        </h4>
        <p> {{strip_tags(substr($post->content,0,200)) }}</p>
        <a href="{{ URL::to('post').'/'.$post->slug }}"
           class="btn btn-primary grid-button-single-post rounded-pill submit-btn position-absolute p-0 border-0 transition-1 text-capitalize grid-item shadow text-white pointer">
            <i class="las la-long-arrow-alt-right"></i>
        </a>
    </div>
</div>

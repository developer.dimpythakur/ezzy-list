<!-- Footer -->
<footer class="et-site-footer text-center position-relative transition-1 no-hover">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer-widget about">
                    @if(isset($setting->logo))
                    <img src="{{ Storage::url('images/logo/').($setting->logo)->logo}}" class="img-fluid mb-3" alt="img">
                    @endif
                    <p class="text-white-50">See the world through ezzy.</p>
                </div>
            </div>
            <div class="col-md-12 mb-5 mb-md-0">
                <div class="footer-widget links text-white">
                    <ul class="nostyle list-inline">
                        <li class="m-2">
                            <a href="javascript:void(0)" class="text-white">+ Add Listing</a>
                        </li>
                        <li class="m-2">
                            <a href="javascript:void(0)" class="text-white" data-toggle="modal"
                               data-target="#loginSignup">Login</a>
                        </li>
                        <li class="m-2">
                            <a href="javascript:void(0)" class="text-white" data-toggle="modal"
                               data-target="#loginSignup">Register</a>
                        </li>
                        <li class="m-2">
                            <a href="javascript:void(0)" class="text-white">About Us</a>
                        </li>
                    </ul>
                    <div class="social list-inline mt-3">
                        <a href="javascript:void(0)"><i class="la la-facebook" aria-hidden="true"></i></a>
                        <a href="javascript:void(0)"><i class="la la-google" aria-hidden="true"></i></a>
                        <a href="javascript:void(0)"><i class="la la-instagram" aria-hidden="true"></i></a>
                        <a href="javascript:void(0)"><i class="la la-youtube" aria-hidden="true"></i></a>
                        <a href="javascript:void(0)"><i class="la la-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /Footer -->

<!-- Copyright bar -->
@include('frontend.includes.snippet.site.copyright-bar')
<!-- /Copyright bar -->
@include('includes.partials.ga')
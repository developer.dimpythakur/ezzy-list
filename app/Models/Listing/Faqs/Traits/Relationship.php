<?php

namespace App\Models\Listing\Faqs\Traits;

use App\Models\Access\User\User;
use App\Models\Listing\Listing\Listing;


trait Relationship
{
    /**
     * ListingFaq Belong to Listing
     * @return mixed
     */
    public function listing()
    {
        return $this->belongsTo(Listing::class);
    }

    /**
     * BlogCategories belongs to relationship with state.
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}

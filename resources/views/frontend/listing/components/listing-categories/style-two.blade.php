<!-- Grid Categories -->
@if($listing_categories)

    <section class="et-featured-categories bg-light events map-home position-relative">
        <div class="container">
            <div class="section-heading text-center d-block full-width">
                <h2 class="text-capitalize">What Activities you want to have ?</h2>
                <p>Discover best things to do around the world by categories.</p>
            </div>
            <div class="row">

                @foreach($listing_categories as $category)

                    <?php
                    //                    dump($category->icon);
                    ?>
                    <div class="col-md-4 mt-5 mt-xs">
                        <div class="category-boxes text-white text-decoration-none">
                            <div class="c-card-f transition-1 img-overlay transition-1 card-border-radius position-relative">
                                @if($category->image)
                                    <img src="{{ Storage::url('images/category/'.$category->image) }}" class="img-fluid"
                                         alt="listing">
                                @endif
                                <div class="slide-data text-center midblock position-absolute">
                                    <div class="cat-icon">
                                        @if($category->icon && $category->icon !='null')
                                            <i class="{{ $category->icon }}"></i>
                                        @else
                                            <i class="las la-shopping-cart"></i>
                                        @endif
                                    </div>
                                    <h4 class="content-text text-capitalize text-white">
                                        <a href="#" class="text-white text-decoration-none">
                                            {{$category->name}} </a>
                                    </h4>
                                    <span class="content-count badge badge-pill badge-success">
                                       {{ categoryCount($category->id) }} Listings
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif

<!-- Grid Categories -->
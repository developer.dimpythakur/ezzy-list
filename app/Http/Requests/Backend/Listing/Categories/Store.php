<?php

namespace App\Http\Requests\Backend\Listing\Categories;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return access()->allow('store-listing-category');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|max:191',
        ];
    }

    public function messages() {
        return [
            'name.required' => 'Category name is a required field.111111',
            'name.max' => 'Category name not be greater than 191 characters.',
        ];
    }

}

<div class="rating-types flex-wrap d-md-flex d-block pt-3">
    <div class="rating-span mb-3">
        <strong class="rating-value">5</strong>
        <div class="rating-tooltip listing-review  pl-3 pr-3 pt-2 pb-2 rounded-pill d-inline-block">
            <strong> Service &nbsp;</strong>
            {{ Form::hidden('customer_service_rating',0,['class'=>'rating-value']) }}
            <ul class="star-rating d-inline-block">
            </ul>
        </div>
    </div>
    <div class="rating-span mb-3">
        <strong class="rating-value">5</strong>
        <div class="rating-tooltip listing-review  pl-3 pr-3 pt-2 pb-2 rounded-pill d-inline-block">
            <strong> Quality &nbsp;</strong>
            {{ Form::hidden('quality_rating',0,['class'=>'rating-value']) }}
            <ul class="star-rating d-inline-block">
            </ul>
        </div>
    </div>
    <div class="rating-span mb-3">
        <strong class="rating-value">5</strong>
        <div class="rating-tooltip listing-review  pl-3 pr-3 pt-2 pb-2 rounded-pill d-inline-block">
            <strong> Friendly &nbsp;</strong>
            {{ Form::hidden('friendly_rating',0,['class'=>'rating-value']) }}
            <ul class="star-rating d-inline-block">
            </ul>
        </div>
    </div>
    <div class="rating-span mb-3">
        <strong class="rating-value">5</strong>
        <div class="rating-tooltip listing-review  pl-3 pr-3 pt-2 pb-2 rounded-pill d-inline-block">
            <strong> Value &nbsp;</strong>
            {{ Form::hidden('pricing_rating',0,['class'=>'rating-value']) }}
            <ul class="star-rating d-inline-block">
            </ul>
        </div>
    </div>
</div>

<?php
//dd($listing->timings);

//foreach ($listing->timings as $timings):
//    dump($timings->name);
//endforeach;
//$time = (isset($listing)) ? getTiming($listing, $day, 'open_time') : 'Closed';
//dd(getTiming($listing, 'monday', 'open_time'));
?>

<!-- Timings -->
<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-clock-o"></i> Timings</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row mb-3">
            <div class="col-md-12 form-field">
                <div class="row" id="timing-preview">

                </div>
                <!-- Day -->
                @foreach( [
                'monday' => 'Monday',
                'tuesday' => 'Tuesday',
                'wednesday' => 'Wednesday',
                'thursday' => 'Thursday',
                'friday' => 'Friday',
                'saturday' => 'Saturday',
                'sunday' => 'Sunday',
                ] as $day => $dayName)
                <div class="row my-4">
                    <div class="col-3">
                        <label class="d-inline-block mt-2">{{ $dayName }}</label>
                    </div>
                    <div class="col-3">
                        <?php
                        $selectedOpeningHour = '';
                        $selected = (isset($listing)) ? getTiming($listing, $day, 'open_time') : '';
                        ?>
                        <select class="form-control" name="timings[{{ $day }}][opening]">
                            <option>Closed</option>
                            @foreach (openingHours() as $key => $hour):
                            <option {{ ($selected == $key) ? "selected" : '' }} >{{  $hour  }}</option>
                            @endforeach;
                        </select>
                    </div>
                    <div class="col-3">
                        <select class="form-control" name="timings[{{ $day }}][closing]">
                            <option>Closed</option>
                            <?php
                            $selectedClosingHour = '';
                            $selectedHour = (isset($listing)) ? getTiming($listing, $day, 'close_time') : '';
                            ?>
                            @foreach (closingHours() as $key => $hour):
                            <option {{ ($selectedHour == $key) ? "selected" : '' }} >{{  $hour  }}</option>
                            @endforeach;
                        </select>
                    </div>
                </div>
                @endforeach
            </div>

            <!-- Day / End -->
        </div>

    </div>
</div>
<!--/ Timings -->
@push('after_scripts')


<script>
    jQuery(document).ready(function () {
        let $list = new Array();


        //-------------------------------
// Add Timings
//-------------------------------
        jQuery(document).on('click', ".add-timing", function () {

            let timings = {
                'monday': {'open_time': null, 'close_time': null},
                'tuesday': {'open_time': null, 'close_time': null},
                'wednesday': {'open_time': null, 'close_time': null},
                'thursday': {'open_time': null, 'close_time': null},
                'friday': {'open_time': null, 'close_time': null},
                'saturday': {'open_time': null, 'close_time': null},
                'sunday': {'open_time': null, 'close_time': null}
            };


            var $timingDay = jQuery("select[name='timing_day'] option:selected").val();
            var $timeFrom = jQuery("select[name='open_time'] option:selected").val();
            var $timeTo = jQuery("select[name='close_time'] option:selected").val();
            if ($list.includes($timingDay)) {
                return false;
            }
            timings[$timingDay].open_time = $timeFrom;
            timings[$timingDay].close_time = $timeTo;

            $list.push($timingDay);
            console.log($list);




            Object.entries(timings).forEach(entry => {
                const [key, value] = entry;
                if (value.open_time != null) {
                    let $row = `<div id="${key}" class="lh-18 selected-timings col-12">
                        <span class="font-weight-semibold text-capatalize col-4 col-md-3">${capitalizeFirstLetter(key.slice(0, 3))}</span>
                        <span class="col-4 col-md-3 ">${value.open_time} - ${value.close_time}</span>
                        <span  class="col-4 col-md-3 text-danger text-decoration-none remove">Remove</span>
                    </div>`;
                    jQuery("#timing-preview").append($row);
                }
            });

        });
        jQuery(document).on("click", ".selected-timings .remove", function (u) {
            jQuery(this).parent("div").remove();
            if (jQuery(".selected-timings div").length < 1) {
                jQuery(".selected-timings").hide();
            }
        });
    });

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    function addTime() {}
    function previewTime() {}
</script>
@endpush
<?php

return [
    'theme' => 'admin',
    'layout' => 'sidebar-mini',
    'default_date_format' => 'D MMM YYYY',
    'default_datetime_format' => 'D MMM YYYY, HH:mm',
    'html_direction' => 'ltr',
    'project_name' => 'EZZY LIST',
    'home_link' => '',
    'route_prefix' => 'admin',
    'meta_robots_content' => 'noindex, nofollow',
    'styles' => [
        'css/backend/app.css',
        'packages/source-sans-pro/source-sans-pro.css',
        'packages/line-awesome/css/line-awesome.min.css',
    ],
    'styles_frontend' => [
        'css/frontend/app.css',
        'css/frontend/frontend-styes.css',
        'packages/line-awesome/css/line-awesome.min.css',
        'packages/line-awesome/css/line-awesome.min.css',
    ],
    'mix_styles' => [// file_path => manifest_directory_path
    ],
    'scripts' => [
        'js/backend/app.js',
        'js/backend/backend-scripts.js',
    ],
    'ezzy_app' => [
        'scripts' => [
            'js/frontend/app.js',
            'js/frontend/frontend-scripts.js',
        ],
        'styles' => [
            'packages/owl.carousel/dist/assets/owl.carousel.css',
        ]
    ],
    'mix_scripts' => [// file_path => manifest_directory_path
    ],
    'project_logo' => '<b>Ezzy</b> listing',
    'breadcrumbs' => true,
    'header_class' => 'app-header bg-light border-0 navbar',
    'body_class' => 'app aside-menu-fixed sidebar-lg-show',
    'sidebar_class' => 'sidebar sidebar-pills bg-light',
    'footer_class' => 'app-footer d-print-none',
    'developer_name' => 'Local Host',
    'developer_link' => '',
    'web_middleware' => 'web',
    'middleware_key' => 'admin',
    'authentication_column' => 'email',
    'authentication_column_name' => 'Email',
    'avatar_type' => 'gravatar',
    'default_avatar' => 'user-avatar-default.png',
    'map' => [
        'body_attrs' => [
            'data-theme-color' => "#28b8dc",
            "data-map-skin-style" => "bluewater_skin_map",
            "data-map-latitude" => "51.4825766",
            "data-map-longitude" => "0.0098476",
            "data-mobile-maxzoommap" => "10"
        ]
    ]
];

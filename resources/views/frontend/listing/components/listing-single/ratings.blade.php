<div class="card card-border-radius shadow mb-5 p-4 border-0">
    <h5 class="card-title mb-4">Ratings</h5>
    <p>
        <i class="checked-circle fa fa-check-circle rounded-pill text-primary align-middle"></i>&nbsp;
        {{ round($listing->ratingPercent(10),2) }} % of
        people would recommend it to a friend</p>
    <div class="star-rating">
        <span class="badge ml-2">
            <strong>Overall </strong>
            <span class="badge listing-rating badge-secondary ml-2"
                  data-rating=" {{ $listing->countRating() }}"
                  data-name="Average"
                  ></span>
        </span>
    </div>
    <div class="rating-types flex-wrap d-md-flex d-block pt-3">
        <div class="rating-span mb-3">
            <div class="rating-tooltip px-1 pt-2 pb-2 d-inline-block">
                <strong>Friendly</strong>
                <span class="badge listing-rating badge-secondary ml-2"
                      data-rating=" {{ rating($listing,'friendly') }}"
                      data-name="Friendly"
                      ></span>
            </div>
        </div>
        <div class="rating-span mb-3">
            <div class="rating-tooltip px-1 pt-2 pb-2 d-inline-block">
                <strong>Quality</strong>
                <span class="badge listing-rating badge-secondary ml-2"
                      data-name="Quality"
                      data-rating=" {{ rating($listing,'quality') }}"></span>
            </div>
        </div>
        <div class="rating-span mb-3">
            <div class="rating-tooltip px-1 pt-2 pb-2 d-inline-block">
                <strong>Service</strong>
                <span class="badge listing-rating badge-secondary ml-2" 
                      data-name="Service"
                      data-rating=" {{ rating($listing,'friendly') }}"></span>
            </div>
        </div>
        <div class="rating-span mb-3">
            <div class="rating-tooltip px-1 pt-2 pb-2 d-inline-block">
                <strong> Value &nbsp;</strong>
                <span class="badge listing-rating badge-secondary ml-2" 
                      data-name="Value"
                      data-rating=" {{ rating($listing,'pricing') }}"></span>
            </div>
        </div>
    </div>
</div>

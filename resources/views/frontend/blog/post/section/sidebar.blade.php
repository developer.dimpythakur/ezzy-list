<!-- Sidebar -->
<div class="page-sidebar">
    <!-- Author Info -->
    <div class="sidebar-widget author-info">
        <div class="text-center">
            <div class="author-thumbnail mb-3">
                @if(!empty($post->owner->picture))
                <img height="60" width="60" src="{{ $post->owner->picture }}"
                     alt="{{ $post->owner->first_name }}"
                     class="img-fluid rounded-pill shadow">
                @else
                <img height="60" width="60" src="{{ Storage::url('images/user/profile/user-avatar-default.png') }}" alt="{{ $post->owner->first_name }}"
                     class="img-fluid rounded-pill shadow">
                @endif
            </div>
            <h6>About me</h6>
            <p>{{ $post->owner->profile->bio }}</p>
            <div class="social list-inline">
                <a href="{{ $post->getShareUrl('facebook') }}">
                    <i class="lab la-facebook" aria-hidden="true"></i>
                </a>
                <a href="{{ $post->getShareUrl('google') }}">
                    <i class="lab la-google" aria-hidden="true"></i>
                </a>
                <a href="{{ $post->getShareUrl('pinterest') }}">
                    <i class="lab la-pinterest" aria-hidden="true"></i>
                </a>
                <a href="{{ $post->getShareUrl('instagram') }}">
                    <i class="lab la-instagram" aria-hidden="true"></i>
                </a>
                <a href="{{ $post->getShareUrl('linkedin') }}">
                    <i class="lab la-linkedin" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
    <!-- /Author Info -->
    <hr>
    <!-- Categories -->
    <div class="sidebar-widget categories-list">
        <h6 class="widget-title mb-3">Categories</h6>
        @if($categories)
        <ul class="nostyle">
            @foreach($categories as $category)
            <li>
                <a href="javascript:void(0)">
                    <i class="la la-angle-right transition-1" aria-hidden="true"></i>
                    {{ $category->name }}
                    @if(isset($category->blog) && !empty($category->blog))
                    {{ $category->blog->count() }} (7)
                    @endif
                </a>
            </li>
            @endforeach
        </ul>
        @endif
    </div>
    <!-- /Categories -->
    <hr>

    @if(!empty($tags))
    <!-- Tags -->
    <div class="sidebar-widget tags-cloud">
        <h6 class="widget-title mb-3">Tags</h6>
        @foreach($tags as $tag)
        <a href="javascript:void(0)" class="border rounded-pill pt-1 pb-1 pl-3 pr-3 mr-1 mb-3">
            {{ $tag->name }}
        </a>
        @endforeach
    </div>
    <!-- /Tags -->
    @endif
</div>
<!-- Related Posts -->
<hr>
<!-- /Sidebar -->
<!-- Sidebar -->
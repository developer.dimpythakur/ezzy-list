@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.blogcategories.management') . ' | ' . trans('labels.backend.blogcategories.create'))
@section('page-header')
<h1><span class="text-capitalize">{{ trans('labels.backend.blogcategories.create') }}</span></h1>
@endsection

@section('content')

<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::open(['route' => 'admin.blogCategories.store', 'class' => 'form-horizontal',  'files' => true,'role' => 'form', 'method' => 'post', 'id' => 'create-permission']) }}
        <div class="card">
            <div class="card-body row">
                @include("backend.blogcategories.form")
            </div><!-- /.card-body -->
        </div><!--card-->

        <div id="saveForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back">Save and back</span>
                </button>
            </div>
            <a href="{{ route('admin.blogCategories.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>


@endsection
@section("after_scripts")


@endsection
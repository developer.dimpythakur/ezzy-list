<?php

namespace App\Models\Listing\Media\SocialMedia\Traits\Attribute;

/**
 * Class Attribute.
 */
trait Attribute
{
    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn">
                    ' . $this->getEditButtonAttribute('edit-blog-category', 'admin.listing-categories.edit') . '
                    ' . $this->getDeleteButtonAttribute('delete-blog-category', 'admin.listing-categories.destroy') . '
                </div>';
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive()) {
            return "<label class='badge badge-primary'>" . trans('labels.general.active') . '</label>';
        }

        return "<label class='badge badge-danger'>" . trans('labels.general.inactive') . '</label>';
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status == 1;
    }
}

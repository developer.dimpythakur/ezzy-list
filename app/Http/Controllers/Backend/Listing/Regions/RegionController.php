<?php

namespace App\Http\Controllers\Backend\Listing\Regions;

use App\Models\Listing\Region\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Responses
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Listing\Regions\CreateResponse;
use App\Http\Responses\Backend\Listing\Regions\EditResponse;
//Repositories
use App\Repositories\Backend\Listing\Regions\RegionRepository;
//Requests
use App\Http\Requests\Backend\Listing\Regions\Manage;
use App\Http\Requests\Backend\Listing\Regions\Create;
use App\Http\Requests\Backend\Listing\Regions\Store;
use App\Http\Requests\Backend\Listing\Regions\Edit;
use App\Http\Requests\Backend\Listing\Regions\Update;
use App\Http\Requests\Backend\Listing\Regions\Delete;
//Alert
use Prologue\Alerts\Facades\Alert;

/**
 * RegionController 
 */
class RegionController extends Controller {

    /**
     * variable to store the repository object
     * @var RegionRepository
     */
    protected $repository;

    /**
     * RegionsController constructor.
     * @param RegionRepository $repository
     */
    public function __construct(RegionRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display Index page
     * @param Manage $request
     * @return ViewResponse
     */
    public function index(Manage $request) {
        return new ViewResponse('backend.listing.regions.index');
    }

    /**
     * Show the region
     * @param Create $request
     * @return CreateResponse
     */
    public function create(Create $request) {
        return new CreateResponse('backend.listing.regions.create');
    }

    /**
     * Handle region store request
     * @param Store $request
     * @return RedirectResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function store(Store $request) {
        //Input received from the request
        $request = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($request);
        //return with successfull message
        Alert::success(trans('alerts.backend.regions.created'))->flash();
        return new RedirectResponse(route('admin.listing-regions.index'), []);
    }

    /**
     * Show edit region form
     * @param Region $region
     * @param Edit $request
     * @return EditResponse
     */
    public function edit(Region $listing_region, Edit $request) {
        return new EditResponse($listing_region);
    }

    /**
     * Update region
     * @param Update $request
     * @param Region $listing_region
     * @return RedirectResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function update(Update $request, Region $listing_region) {
        //Input received from the request
        $request = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update($listing_region, $request);
        //return with successfull message
        Alert::success(trans('alerts.backend.regions.updated'))->flash();
        return new RedirectResponse(route('admin.listing-regions.index'), []);
    }

    /**
     * Remove the region
     * @param Region $region
     * @param Delete $request
     * @return RedirectResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Region $listing_region, Delete $request) {
        //Calling the delete method on repository
        $this->repository->delete($listing_region);
        //returning with successfull message
        //return with successfull message
        Alert::success(trans('alerts.backend.regions.deleted'))->flash();
        return new RedirectResponse(route('admin.listing-regions.index'), []);
    }

}

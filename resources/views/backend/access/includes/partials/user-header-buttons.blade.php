@permission('create-user')
<a href="{{route('admin.access.user.create')}}" class="btn btn-primary" data-style="zoom-in">
    <span class="ladda-label"><i class="la la-plus"></i> 
        {{ trans('menus.backend.access.users.create')}}</span>
</a>
@endauth
@permission('view-deleted-user')
<a href="{{route('admin.access.user.deleted')}}" class="btn btn-danger">
    <i class="la la-trash"></i> @lang('menus.backend.access.users.deleted-users')
</a>
@endauth
@permission('view-deactive-user')
<a href="{{route('admin.access.user.deactivated')}}" class="btn btn-default">
    <i class="la la-square"></i>
    @lang('menus.backend.access.users.deactivated-users')</a>
@endauth


<?php

namespace App\Models\Listing\Region\Traits;

use App\Models\Listing\Locations\Location;
use App\Models\Listing\Listing\Listing;

/**
 * Class RegionRelationship
 */
trait RegionRelationship {
    /*
     * put you model relationships here
     * Take below example for reference
     */
    /*
      public function users() {
      //Note that the below will only work if user is represented as user_id in your table
      //otherwise you have to provide the column name as a parameter
      //see the documentation here : https://laravel.com/docs/6.x/eloquent-relationships
      $this->belongsTo(User::class);
      }
     */

    /**
     * Region have many locations 
     * @return type
     */
    public function locations() {
        return $this->hasMany(Location::class, 'region_id');
    }

    /**
     * Belongs to listing
     * @return type
     */
    public function listing() {
        return $this->belongsTo(Listing::class);
    }

}

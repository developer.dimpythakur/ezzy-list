@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.edit'))

@section('page-header')
<h1>
    {{ trans('labels.backend.access.users.management') }}
    <small>{{ trans('labels.backend.access.users.edit') }}</small>
</h1>
@endsection

@section('content')

<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.pages.section.grouped_errors'))
        {{ Form::model($user, ['route' => ['admin.access.user.update', $user], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
        <div class="card mt-2 p-2">
            <div class="card-header">
                <h6 class="my-2"><i class="la la-pencil-alt"></i> {{ trans('labels.backend.access.users.edit') }}</h6>
            </div>
            <div class="card-body bold-labels">
                <div class="row mb-3">
                    <div class="form-group col-12">
                        {{ Form::label('name', trans('validation.attributes.backend.access.users.firstName'), ['class' => 'control-label required']) }}
                        {{ Form::text('first_name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.users.firstName'), 'required' => 'required']) }}

                    </div><!--form control-->
                    <div class="form-group col-12">
                        {{ Form::label('name', trans('validation.attributes.backend.access.users.lastName'), ['class' => 'control-label required']) }}
                        {{ Form::text('last_name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.users.lastName'), 'required' => 'required']) }}
                    </div><!--form control-->

                    <div class="form-group col-12">
                        {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'control-label required']) }}
                        {{ Form::text('email', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.users.email'), 'required' => 'required']) }}
                    </div><!--col-lg-10-->



                    {{-- Status --}}
                    @if ($user->id != 1)
                    <div class="form-group col-12">
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-secondary active">
                                {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'control-label']) }}
                                {{ Form::checkbox('status', '1', $user->status == 1) }}
                            </label>
                            <label class="btn btn-secondary">
                                {{ Form::label('status', trans('validation.attributes.backend.access.users.inactive'), ['class' => 'control-label']) }}
                                {{ Form::checkbox('status', '0', $user->status == 1) }}
                            </label>
                        </div>



                        <div class="col-lg-1">
                            <div class="control-group">
                                <label class="control control--checkbox">
                                    {{ Form::checkbox('status', '1', $user->status == 1) }}
                                    <div class="control__indicator"></div>
                                </label>
                            </div>
                        </div><!--col-lg-1-->
                    </div><!--form control-->

                    {{-- Confirmed --}}
                    <div class="form-group">
                        {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'control-label']) }}

                        <div class="col-lg-1">
                            <div class="control-group">
                                <label class="control control--checkbox">
                                    {{ Form::checkbox('confirmed', '1', $user->confirmed == 1) }}
                                    <div class="control__indicator"></div>
                                </label>
                            </div>
                        </div><!--col-lg-1-->
                    </div><!--form control-->

                    {{-- Associated Roles --}}
                    <div class="form-group">
                        {{ Form::label('status', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'control-label']) }}

                        <div class="col-lg-8">
                            @if (count($roles) > 0)
                            @foreach($roles as $role)
                            <div>
                                <label for="role-{{$role->id}}" class="control control--radio">
                                    <input type="radio" value="{{$role->id}}" name="assignees_roles[]" {{ is_array(old('assignees_roles')) ? (in_array($role->id, old('assignees_roles')) ? 'checked' : '') : (in_array($role->id, $userRoles) ? 'checked' : '') }} id="role-{{$role->id}}" class="get-role-for-permissions" />  &nbsp;&nbsp;{!! $role->name !!}
                                    <div class="control__indicator"></div>
                                    <a href="#" data-role="role_{{$role->id}}" class="show-permissions small">
                                        (
                                        <span class="show-text">{{ trans('labels.general.show') }}</span>
                                        <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                                        {{ trans('labels.backend.access.users.permissions') }}
                                        )
                                    </a>
                                </label>
                            </div>
                            <div class="permission-list hidden" data-role="role_{{$role->id}}">
                                @if ($role->all)
                                {{ trans('labels.backend.access.users.all_permissions') }}
                                @else
                                @if (count($role->permissions) > 0)
                                <blockquote class="small">
                                    @foreach ($role->permissions as $perm)
                                    {{$perm->display_name}}<br/>
                                    @endforeach
                                </blockquote>
                                @else
                                {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                                @endif
                                @endif
                            </div><!--permission list-->
                            @endforeach
                            @else
                            {{ trans('labels.backend.access.users.no_roles') }}
                            @endif
                        </div><!--col-lg-3-->
                    </div><!--form control-->

                    {{-- Associated Permissions --}}
                    <div class="form-group">
                        {{ Form::label('associated-permissions', trans('validation.attributes.backend.access.roles.associated_permissions'), ['class' => 'control-label']) }}
                        <div class="col-lg-10">
                            <div id="available-permissions" style="width: 700px; height: 200px; overflow-x: hidden; overflow-y: scroll;">
                                <div class="row">
                                    <div class="col-xs-12 get-available-permissions">
                                        @if ($permissions)

                                        @foreach ($permissions as $id => $display_name)
                                        <div class="control-group">
                                            <label class="control control--checkbox" for="perm_{{ $id }}">
                                                <input type="checkbox" name="permissions[{{ $id }}]" value="{{ $id }}" id="perm_{{ $id }}" {{ isset($userPermissions) && in_array($id, $userPermissions) ? 'checked' : '' }} /> <label for="perm_{{ $id }}">{{ $display_name }}</label>
                                                <div class="control__indicator"></div>
                                            </label>
                                        </div>
                                        @endforeach
                                        @else
                                        <p>There are no available permissions.</p>
                                        @endif
                                    </div><!--col-lg-6-->
                                </div><!--row-->
                            </div><!--available permissions-->
                        </div><!--col-lg-3-->
                    </div><!--form control-->

                    @endif


                    @if ($user->id == 1)
                    {{ Form::hidden('status', 1) }}
                    {{ Form::hidden('confirmed', 1) }}
                    {{ Form::hidden('assignees_roles[]', 1) }}
                    @endif
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="update">Update</span>
                </button>
            </div>
            <a href="{{ route('admin.access.user.index')}}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection

@section('after-scripts')
<script type="text/javascript">
    Backend.Utils.documentReady(function () {
        csrf = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        Backend.Users.selectors.getPremissionURL = "{{ route('admin.get.permission') }}";
        Backend.Users.init("edit");
    });
    window.onload = function () {
        Backend.Users.windowloadhandler();
    };

</script>
@endsection

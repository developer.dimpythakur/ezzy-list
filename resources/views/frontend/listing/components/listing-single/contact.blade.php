<div class="row">
    <!-- Contact form -->
    <div class="col-md-6 col-sm-12">
        <div class="card card-border-radius shadow mb-5 p-4 border-0">
            <h5 class="card-title mb-4">Contact Business</h5>
            <div class="col-md-6">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                @if (session('warning'))
                <div class="alert alert-warning">
                    {{ session('warning') }}
                </div>
                @endif
            </div>
            {{ Form::open(['route' => 'frontend.contact.save', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'autocomplete'=>'off', 'id' => 'contact-us']) }}
            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                <input type="text" class="form-control" placeholder="Name" name="name">
            </div>
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="text" class="form-control" placeholder="Email" name="email">
            </div>
            <div class="form-group {{ $errors->has('phone_number') ? ' has-error' : '' }}">
                <input type="text" class="form-control" placeholder="Phone" name="phone">
            </div>
            <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                <textarea rows="6" class="form-control resize-none" placeholder="Message" name="message"></textarea>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary border-0 transition-1 text-capitalize shadow"
                       value="Send" name="submit">
            </div>
            {{ Form::close() }}
        </div>
    </div>
    <!-- /Contact form -->
    <!-- Contact info -->
    <div class="col-md-6 et-listing-map-2 col-sm-12">
        <div class="card card-border-radius shadow mb-5 p-4 border-0">
            <h5 class="card-title mb-4">Contact Info</h5>
            @if(null !=($listing->location->first()))
            <p><i class="las la-map text-primary "></i>&nbsp; {{ $listing->location->first()->address }} </p>
            @endif
            @if(null !=$listing->phone)
            <p><i class="las la-phone text-primary"></i>&nbsp; {{ $listing->phone }}</p>
            @endif
            @if(null !=$listing->email)
            <p><i class="las la-envelope text-primary"></i>&nbsp; {{ $listing->email }}</p>
            @endif
            @if(null !=$listing->website)
            <p><i class="las la-link text-primary"></i>&nbsp; {{ $listing->website }}</p>
            @endif
            <div data-lat="{{ $listing->location->first()->latitude }}" data-long="{{ $listing->location->first()->longitude }}" id="contact-map" style="position: relative; overflow: hidden;">
                <div class = "row">
                    <div class = "col-md-12 mb-5">
                        <div id ="listing-map"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Contact info -->
</div>
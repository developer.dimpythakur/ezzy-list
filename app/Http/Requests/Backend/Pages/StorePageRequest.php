<?php

namespace App\Http\Requests\Backend\Pages;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class StorePageRequest.
 */
class StorePageRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return access()->allow('create-page');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'required|max:191',
            'description' => 'required',
            'featured_image' => 'required',
//            'page_slug' => 'unique:' . config('module.pages.table') . ',page_slug'
            'page_slug' => Rule::unique(config('module.pages.table'))->where(function ($query) {
                        return $query->where('page_slug', 1);
                    })
        ];
    }

}

@extends('frontend.layouts.app')
@section('content')
<!-- Pricing Plans -->
<section class="et-package-plans">
    <div class="section-heading text-center d-block full-width">
        <h2 class="text-capitalize">Pricing Plans</h2>
        <p>Choose a package that best suit your needs</p>
    </div>
    <div class="container">
        <div class="row">
            <!-- Package -->
            <div class="col-md-4 col-sm-12 mt-5 mt-xs">
                <div class="package-table rounded-bottom">
                    <div class="package-header rounded-top shadow text-white overflow-hidden position-relative">
                        <div class="package-title midblock rounded-pill position-absolute d-flex align-items-center justify-content-center">
                            Basic
                        </div>
                        <div class="price float-right pr-3 pb-3 pt-3">
                            <p class="mb-2 text-white">Per month</p>
                            <p class="mb-0 text-white"><span class="text-primary h5">$</span>
                                <span class="h1">0</span>
                            </p>
                        </div>
                    </div>
                    <div class="package-body p-4">
                        <ul class="nostyle">
                            <li><i class="las la-check text-primary"></i>  Business Listing (1)</li>
                            <li><i class="las la-times text-primary"></i>  Shop/Product URL (0)</li>
                            <li><i class="las la-times text-primary"></i>  Product per row(0)</li>
                            <li><i class="las la-check text-primary"></i>  Unlimited tags</li>
                            <li><i class="las la-check text-primary"></i>  Business logo</li>
                            <li><i class="las la-check text-primary"></i>  Business Description</li>
                            <li><i class="las la-times text-primary"></i>  Location</li>
                            <li><i class="las la-check text-primary"></i>  Category</li>
                            <li><i class="las la-check text-primary"></i>  Gallary (3)</li>
                            <li><i class="las la-check text-primary"></i>  Social Media integration(1)</li>
                            <li><i class="las la-times text-primary"></i>  Audio/Video (0)</li>
                        </ul>
                        <a href="javascript:void(0)"
                           class="btn btn-primary border-0 transition-1 text-capitalize shadow m-auto d-block w-75">Get
                            started</a>
                    </div>
                </div>
            </div>
            <!-- /Package -->
            <!-- Package -->
            <div class="col-md-4 col-sm-12 mt-5 mt-xs">
                <div class="package-table rounded-bottom">
                    <div class="package-header bg-primary rounded-top shadow text-white overflow-hidden position-relative">
                        <div class="package-title midblock rounded-pill position-absolute d-flex align-items-center justify-content-center">
                            Standard
                        </div>
                        <div class="price float-right pr-3 pb-3 pt-3">
                            <p class="mb-2 text-white">Per month</p>
                            <p class="mb-0 text-white">
                                <span class="text-white h5">$</span>
                                <span class="h1">75</span>
                            </p>
                        </div>
                    </div>
                    <div class="package-body p-4">
                        <ul class="nostyle">
                            <li><i class="las la-check text-primary"></i>  Business Listing (2)</li>
                            <li><i class="las la-check text-primary"></i>  Shop/Product URL (4)</li>
                            <li><i class="las la-check text-primary"></i>  Product per row(4)</li>
                            <li><i class="las la-check text-primary"></i>  Unlimited tags</li>
                            <li><i class="las la-check text-primary"></i>  Business logo</li>
                            <li><i class="las la-check text-primary"></i>  Business Description</li>
                            <li><i class="las la-check text-primary"></i>  Location</li>
                            <li><i class="las la-check text-primary"></i>  Category</li>
                            <li><i class="las la-check text-primary"></i>  Gallary (6)</li>
                            <li><i class="las la-check text-primary"></i>  Social Media integration(3)</li>
                            <li><i class="las la-check text-primary"></i>  Audio/Video (1)</li>
                        </ul>
                        <a href="javascript:void(0)"
                           class="btn btn-primary border-0 transition-1 text-capitalize shadow m-auto d-block w-75">Get
                            started</a>
                    </div>
                </div>
            </div>
            <!-- /Package -->
            <!-- Package -->
            <div class="col-md-4 col-sm-12 mt-5 mt-xs">
                <div class="package-table rounded-bottom">
                    <div class="package-header rounded-top shadow text-white overflow-hidden position-relative">
                        <div class="package-title midblock rounded-pill position-absolute d-flex align-items-center justify-content-center">
                            Premium
                        </div>
                        <div class="price float-right pr-3 pb-3 pt-3">
                            <p class="mb-2 text-white">Per month</p>
                            <p class="mb-0 text-white"><span class="text-primary h5">$</span>
                                <span class="h1">125</span>
                            </p>
                        </div>
                    </div>
                    <div class="package-body p-4">
                        <ul class="nostyle">
                            <li><i class="las la-check text-primary"></i>  Business Listing (3)</li>
                            <li><i class="las la-check text-primary"></i>  Shop/Product URL (12)</li>
                            <li><i class="las la-check text-primary"></i>  Product per row(12)</li>
                            <li><i class="las la-check text-primary"></i>  Unlimited tags</li>
                            <li><i class="las la-check text-primary"></i>  Business logo</li>
                            <li><i class="las la-check text-primary"></i>  Business Description</li>
                            <li><i class="las la-check text-primary"></i>  Location</li>
                            <li><i class="las la-check text-primary"></i>  Category</li>
                            <li><i class="las la-check text-primary"></i>  Gallary (12)</li>
                            <li><i class="las la-check text-primary"></i>  Social Media integration</li>
                            <li><i class="las la-check text-primary"></i>  Audio/Video (1)</li>

                        </ul>
                        <a href="javascript:void(0)"
                           class="btn btn-primary border-0 transition-1 text-capitalize shadow m-auto d-block w-75">Get
                            started</a>
                    </div>
                </div>
            </div>
            <!-- /Package -->
        </div>
    </div>
</section>
<!-- / Pricing Plans -->

</section>

<!-- Newsletter Subscription -->
@include('frontend.listing.components.listing-newsletter.newsletter')
<!-- /Newsletter Subscription -->
@endsection








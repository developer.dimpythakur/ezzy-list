@if($listing->gallery && ($listing->gallery->count()))
<div class="card card-border-radius shadow mb-5 p-4 border-0">
    <h5 class="card-title mb-4">Gallery</h5>
    <div class="owl-carousel post-slider-tab owl-loaded owl-drag">
        @foreach($listing->gallery as $gallery)
        <div class="item">
            <img src="{{ $gallery->url }}" alt="{{ $listing->name}}">
        </div>
        @endforeach
    </div>
    <div class="nav-container-post-slider-tab nav-container text-center full-width mt-4 mb-4">
    </div>
</div>
@else
<div class="card card-border-radius general-questions shadow mb-4 p-4 border-0">
    <h5 class="card-title mb-4">Gallery</h5>
    <div class="card-body">
        <div class="w-100 p-4">
            <div class="p-3 alert alert-danger mb-0">
                <p class="m-0"><i class="la la-images"></i> No gallery</p>
            </div>
        </div>
    </div>
</div>
@endif
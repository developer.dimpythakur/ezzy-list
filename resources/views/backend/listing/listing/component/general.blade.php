<?php
$options = [
    'file_picker_callback' => 'elFinderBrowser',
    'selector' => 'textarea.tinymce',
    'plugins' => 'image,link,media,anchor',
];
$selectedCategories = isset($listing) ? ($listing->categories->pluck('id')->toArray()) : null;
$selectedTags = isset($listing) ? ($listing->tags->pluck('id')->toArray()) : null;
?>
<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-file-text"></i> Basic Informations</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row">
            <div class="form-group col-12 required">
                {{ Form::label('name', trans('validation.attributes.backend.pages.title'), ['class' => 'control-label required']) }}
                {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.pages.title'), 'required' => 'required']) }}
                <p class="help-block">
                    @if(isset($listing))
                    <a target="_blank" href="{{ route('frontend.listing.show',$listing->slug) }}">
                        <cite>
                            {{ route('frontend.index')}}/listing/<span id="listing-slug">{{ isset($listing)? $listing->slug:''}}                    </span>

                        </cite> 
                    </a>
                    @endif
                </p>
            </div>
            <div class="form-group col-12">
                {{ Form::label('tagline', trans('validation.attributes.backend.listings.tagline'), ['class' => 'control-label required']) }}
                {{ Form::text('tagline', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.listings.tagline'), 'required' => 'required']) }}
            </div>
            <div class="form-group col-12">
                {{ Form::label('description', trans('validation.attributes.backend.listings.description'), ['class' => 'control-label required']) }}
                {{ Form::textarea('description', null,[
                    'class' => 'form-control box-size tinymce',
                     'data-options'=> trim(json_encode($options)),
                            'placeholder' => trans('validation.attributes.backend.listings.description')]) }}
            </div>
        </div>
        <div class="row">
            <div class="form-group col-6">
                {{ Form::label('categories', 'Select Category', ['class' => 'control-label']) }}
                @if(!empty($selectedCategories))
                {{ Form::select('categories[]', $categories, $selectedCategories, ['class' => 'form-control box-size select2_multiple', 'placeholder' => 'Select Category', 'required' => 'required'])}}
                @else
                {{ Form::select('categories[]', $categories, null, ['class' => 'form-control box-size select2_multiple', 'placeholder' => 'Select Category', 'required' => 'required'])}}
                @endif
            </div><!--form control-->
            <div class="form-group col-6">
                {{ Form::label('tags', 'Select Tag', ['class' => 'control-label']) }}
                @if(!empty($selectedTags))
                {{ Form::select('tags[]', $tags, $selectedTags, ['class' => 'form-control box-size select2_multiple','multiple'=>'multiple', 'placeholder' => 'Select Tag'])}}
                @else
                {{ Form::select('tags[]', $tags, null, ['class' => 'form-control box-size select2_multiple',       'placeholder' => 'Select Tag','multiple'=>'multiple'])}}
                @endif
            </div><!--form group-->
        </div>
    </div>
</div>
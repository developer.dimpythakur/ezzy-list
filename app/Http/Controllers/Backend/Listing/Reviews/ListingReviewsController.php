<?php

namespace App\Http\Controllers\Backend\Listings\ListingReviews;

use App\Models\ListingReviews\ListingReview;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\ListingReviews\CreateResponse;
use App\Http\Responses\Backend\ListingReviews\EditResponse;
use App\Repositories\Backend\ListingReviews\ListingReviewRepository;
use App\Http\Requests\Backend\ListingReviews\ManageListingReviewRequest;
use App\Http\Requests\Backend\ListingReviews\CreateListingReviewRequest;
use App\Http\Requests\Backend\ListingReviews\StoreListingReviewRequest;
use App\Http\Requests\Backend\ListingReviews\EditListingReviewRequest;
use App\Http\Requests\Backend\ListingReviews\UpdateListingReviewRequest;
use App\Http\Requests\Backend\ListingReviews\DeleteListingReviewRequest;

/**
 * ListingReviewsController
 */
class ListingReviewsController extends Controller
{
    /**
     * variable to store the repository object
     * @var ListingReviewRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param ListingReviewRepository $repository;
     */
    public function __construct(ListingReviewRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\ListingReviews\ManageListingReviewRequest  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(ManageListingReviewRequest $request)
    {
        return new ViewResponse('backend.listingreviews.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateListingReviewRequestNamespace  $request
     * @return \App\Http\Responses\Backend\ListingReviews\CreateResponse
     */
    public function create(CreateListingReviewRequest $request)
    {
        return new CreateResponse('backend.listingreviews.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreListingReviewRequestNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(StoreListingReviewRequest $request)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.listingreviews.index'), ['flash_success' => trans('alerts.backend.listingreviews.created')]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\ListingReviews\ListingReview  $listingreview
     * @param  EditListingReviewRequestNamespace  $request
     * @return \App\Http\Responses\Backend\ListingReviews\EditResponse
     */
    public function edit(ListingReview $listingreview, EditListingReviewRequest $request)
    {
        return new EditResponse($listingreview);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateListingReviewRequestNamespace  $request
     * @param  App\Models\ListingReviews\ListingReview  $listingreview
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(UpdateListingReviewRequest $request, ListingReview $listingreview)
    {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update( $listingreview, $input );
        //return with successfull message
        return new RedirectResponse(route('admin.listingreviews.index'), ['flash_success' => trans('alerts.backend.listingreviews.updated')]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteListingReviewRequestNamespace  $request
     * @param  App\Models\ListingReviews\ListingReview  $listingreview
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(ListingReview $listingreview, DeleteListingReviewRequest $request)
    {
        //Calling the delete method on repository
        $this->repository->delete($listingreview);
        //returning with successfull message
        return new RedirectResponse(route('admin.listingreviews.index'), ['flash_success' => trans('alerts.backend.listingreviews.deleted')]);
    }
    
}

@extends ('frontend.layouts.user')
@section ('title', trans('labels.frontend.listings.management'))
@section('page-header')
<h1>{{ trans('labels.frontend.listings.management') }}</h1>
@endsection
@section('after_styles')
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/backend/datatables.css') }}">
@endsection
@section('page-header')
<h2>My Listings</h2>
@endsection
@section('content')

<section class="row py-2">
    <section class="col-md-9">
        @if(count($listings))
        @foreach($listings as $listing)
        @include('frontend.user.listing.listing.style-one')
        @endforeach
        <nav class="navigation pagination m-auto pb-5">
            {{ $listings->links('vendor.pagination.user-listing') }}
        </nav>
        @else
        <section class="row">
            <section class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <h6 class="my-2"><i class="la la-list-alt"></i> My listings</h6>
                    </div>
                    <div class="card-body bold-labels">
                        <div class="w-100 p-4">
                            <div class="p-3 alert alert-danger">
                                <p class="m-0"><i class="la la-list"></i> No listings found.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        @endif
    </section>
</section>
@endsection
@section('after_scripts')
@endsection
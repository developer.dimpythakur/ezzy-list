<?php

use Database\DisableForeignKeys;
use Database\TruncateTable;
use Illuminate\Database\Seeder;

class ListingTableSeeder extends Seeder {

    use DisableForeignKeys,
        TruncateTable;

    public function run() {
        $seeder = new App\Seeder\Yelp();
        $seeder->getBusinesses();
    }

}

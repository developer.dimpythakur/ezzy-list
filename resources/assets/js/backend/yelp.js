/**
 * similar to PHP's empty function
 */
function empty(data) {
    if (typeof (
            data
            ) == 'number' || typeof (
            data
            ) == 'boolean') {
        return false;
    }
    if (typeof (
            data
            ) == 'undefined' || data === null) {
        return true;
    }
    if (typeof (
            data.length
            ) != 'undefined') {
        return data.length === 0;
    }
    var count = 0;
    for (var i in data) {
        // if(data.hasOwnProperty(i))
        //
        // This doesn't work in ie8/ie9 due the fact that hasOwnProperty works only on native objects.
        // http://stackoverflow.com/questions/8157700/object-has-no-hasownproperty-method-i-e-its-undefined-ie8
        //
        // for hosts objects we do this
        if (Object.prototype.hasOwnProperty.call(data, i)) {
            count++;
        }
    }
    return count === 0;
}
if (!String.prototype.splice) {
    /**
     * {JSDoc}
     *
     * The splice() method changes the content of a string by removing a range of
     * characters and/or adding new characters.
     *
     * @this {String}
     * @param {number} start Index at which to start changing the string.
     * @param {number} delCount An integer indicating the number of old chars to remove.
     * @param {string} newSubStr The String that is spliced in.
     * @return {string} A new string with the spliced substring.
     */
    String.prototype.splice = function (start, delCount, newSubStr) {
        return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
    };
}
String.prototype.splice = function (idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};
/**
 * 
 * @param {type} mode
 * @returns {undefined}Loader
 */
function loader(mode = 'loading') {
    let $button = jQuery("#yelp-search");
    var text_loading = $button.data('loading');
    var text_show = $button.data('show');
    if (mode === 'loading') {
        $button.text(text_loading);
        $button.attr('disabled', true);
    } else {
        $button.text(text_show);
        $button.attr('disabled', false);
}
}
/**
 * Notification 
 * @param {type} $type
 * @param {type} $message
 * @returns {undefined}
 */
function notifyMe($type, $message) {
    Noty.overrideDefaults({
        layout: 'topRight',
        timeout: 2500,
        closeWith: ['click', 'button'],
        theme: 'ezzy'
    });
    new Noty({
        type: $type,
        text: $message
    }).show();
}

/**
 * -----------------------
 * Save Listing || Bookmark
 * ------------------------
 */
jQuery(document).ready(function () {
    jQuery(document).on('click', '#yelp-search', function (event) {
        event.preventDefault();
        event.stopPropagation();
        let $form = jQuery(".listing-form-yelp"),
                $button = jQuery(this);
        //get registed phone number
        let phoneNumber = jQuery("#yelp-phone").val();
        if (empty(phoneNumber)) {
            $form.addClass('was-validated');
            notifyMe('error', 'Please enter a valid/registered phone number.');
            return false;
        }

        loader('loading');
        var $thisObject = jQuery(this);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: $thisObject.data('url'),
            method: 'POST',
            data: {
                phone: phoneNumber,
                location: jQuery("#location").val()
            },
            success: function (result) {
                if (result.type === 'success') {
                    let business = result.message;
                    populate(business);
                    loader('hide');
                    notifyMe('success', 'listing  imported');
                    window.location.href = $button.data('redirect');
                } else {
                    loader('hide');
                    notifyMe(result.type, result.message);
                    $form.removeClass('was-validated');
                }
            },
            error: function (result) {},
        });
    });
});
/**
 * Update form with yelp data
 * @param {type} listing
 * @returns {undefined}
 */
function populate(listing) {
    for (const [key, value] of Object.entries(listing)) {
        if (key === 'banner' && null != (listing.banner)) {
            uploadBanner(listing.banner);
        } else
        if (key === 'gallery' && null != (listing.gallery)) {
            uploadGallery(listing.gallery);
        } else
        if (key === 'description') {
            tinyMCE.get('description').setContent(listing.description);
        } else
        if (key === 'category') {
            selectOption(listing.category, 'categories');
        } else
        if (key === 'region') {
            selectOption(listing.region, 'region');
        } else
        if (key === 'timings') {
            timings(listing.timings);
        } else {
            let $element = jQuery('.form-group #' + key);
            if ($element.length) {
                $element.val(value);
            }
        }

    }
}



/**
 * Update timings
 * @type timings
 */
function timings(timings) {
    console.log(console.log(Object.keys(timings).length));
    console.log(timings);
    for (let i = 0; i <= 6; i++) {
        // Set selected 
        let days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
        //open
        let $elementOpen = jQuery('#' + days[i] + "-open");
        if ($elementOpen.length) {
            let $timeOpen = (timings[days[i]]['opening']);
            $elementOpen.val($timeOpen.splice(2, 0, ":"));
            $elementOpen.select2().trigger('change');
        }
        //close
        let $elementClose = jQuery('#' + days[i] + "-close");
        if ($elementClose.length) {
            let $timeClose = (timings[days[i]]['closing']);
            $elementClose.val($timeClose.splice(2, 0, ":"));
            $elementClose.select2().trigger('change');
        }
    }
}
function selectOption(option, $el) {
    let $element = jQuery('#' + $el + ' option:contains(' + option + ')');
    if ($element.length) {
        $element.attr('selected', true);
        $element.select2().trigger('change');
    }
}
/**
 * Add banner
 * @param {type} tmppath
 * @returns {undefined}
 */
function uploadBanner(tmppath) {
    jQuery("#uploaded_feat_img").append(`<li class="mt-2 col-12" id="gallery-img-1">
                              <input type="hidden" name="banner" value="${tmppath}" />
                                <img class="img-fluid" src="${tmppath}">
                               <i data-targetimg="1" class="remove-gallery-img p-1 shadow pointer btn btn-outline-danger border-0 las las-trash lar la-trash-alt"></i>
                             </li>`);
}

/**
 * Gallery Images
 * @param {type} gallery
 * @returns {undefined}
 */
function uploadGallery(gallery) {
    //-------------------------------
// Gallery Images Upload
//-------------------------------
    var imageHTML = jQuery("#gallery_images_list").html();
    let col = 1;
    gallery.forEach(function (tmppath, index) {
        imageHTML += `<li class="mt-2 col-3" id="gallery-img-${index}">
                <input type="hidden" name="listing_gallery[]" value="${tmppath}" />
                          <img class="img-fluid" src="${tmppath}">
                           <i data-targetimg="${index}" class="remove-gallery-img p-1 shadow pointer btn btn-outline-danger border-0 las las-trash lar la-trash-alt"></i>
                           </li>`;
        col++;
    });
    jQuery('#gallery_images_list').html(imageHTML);
}
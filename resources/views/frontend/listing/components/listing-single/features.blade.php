<section class="row">
    {{--    <!-- Additional Details -->--}}
    <div class="col-md-6 col-sm-12">
        <!-- Services Offered -->
        @include('frontend.listing.components.listing-single.services')

    </div>
    <!-- /Services Offered -->
    <div class="col-md-6 col-sm-12">
        <!-- Additional Details -->
        @include('frontend.listing.components.listing-single.addition-details')
        <!-- /Additional Details -->
    </div>
</section>
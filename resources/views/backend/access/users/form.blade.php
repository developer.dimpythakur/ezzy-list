<div class="card mt-2">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-user-edit"></i> {{ trans('labels.backend.access.users.create') }}</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row my-3">
            {{-- First Name --}}
            <div class="form-group col-12">
                {{ Form::label('First Name', trans('validation.attributes.backend.access.users.firstName'), ['class' => 'control-label required']) }}
                {{ Form::text('first_name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.users.firstName'), 'required' => 'required']) }}
            </div><!--form control-->

            {{-- Last Name --}}
            <div class="form-group col-12">
                {{ Form::label('Last Name', trans('validation.attributes.backend.access.users.lastName'), ['class' => 'control-label required']) }}
                {{ Form::text('last_name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.users.lastName'), 'required' => 'required']) }}
            </div><!--form control-->

            {{-- Email --}}
            <div class="form-group col-12">
                {{ Form::label('email', trans('validation.attributes.backend.access.users.email'), ['class' => 'control-label required']) }}
                {{ Form::text('email', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.users.email'), 'required' => 'required']) }}
            </div><!--form control-->

            {{-- Password --}}
            <div class="form-group col-12">
                {{ Form::label('password', trans('validation.attributes.backend.access.users.password'), ['class' => 'control-label required']) }}
                {{ Form::password('password', ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.users.password'), 'required' => 'required']) }}
            </div><!--form control-->

            {{-- Password Confirmation --}}
            <div class="form-group col-12">
                {{ Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation'), ['class' => 'control-label required']) }}
                {{ Form::password('password_confirmation', ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.users.password_confirmation'), 'required' => 'required']) }}
            </div><!--form control-->

            {{-- Status --}}
            <div class="form-group col-12">
                {{ Form::label('status', trans('validation.attributes.backend.access.users.active'), ['class' => 'control-label']) }}
                <div class="col-lg-1">
                    <div class="control-group">
                        <label class="control control--checkbox">
                            {{ Form::checkbox('status', '1', true) }}
                            <div class="control__indicator"></div>
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->

            {{-- Confirmed --}}
            <div class="form-group col-12">
                {{ Form::label('confirmed', trans('validation.attributes.backend.access.users.confirmed'), ['class' => 'control-label']) }}
                <div class="col-lg-1">
                    <div class="control-group">
                        <label class="control control--checkbox">
                            {{ Form::checkbox('confirmed', '1', true) }}
                            <div class="control__indicator"></div>
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->

            {{-- Confirmation Email --}}
            <div class="form-group col-12">
                <label class="control-label">{{ trans('validation.attributes.backend.access.users.send_confirmation_email') }}<br/>
                    <small>{{ trans('strings.backend.access.users.if_confirmed_off') }}</small>
                </label>

                <div class="col-lg-1">
                    <div class="control-group">
                        <label class="control control--checkbox">
                            {{ Form::checkbox('confirmation_email', '1') }}
                            <div class="control__indicator"></div>
                        </label>
                    </div>
                </div><!--col-lg-1-->
            </div><!--form control-->

            {{-- Associated Roles --}}
            <div class="form-group col-12">
                {{ Form::label('status', trans('validation.attributes.backend.access.users.associated_roles'), ['class' => 'control-label']) }}

                <div class="col-lg-8">
                    @if (count($roles) > 0)
                    @foreach($roles as $role)
                    <div>
                        <label for="role-{{$role->id}}" class="control control--radio">
                            <input type="radio" value="{{$role->id}}" name="assignees_roles[]" id="role-{{$role->id}}" class="get-role-for-permissions" {{ $role->id == 3 ? 'checked' : '' }} />  &nbsp;&nbsp;{!! $role->name !!}
                            <div class="control__indicator"></div>
                            <a href="#" data-role="role_{{ $role->id }}" class="show-permissions small">
                                (
                                <span class="show-text">{{ trans('labels.general.show') }}</span>
                                <span class="hide-text hidden">{{ trans('labels.general.hide') }}</span>
                                {{ trans('labels.backend.access.users.permissions') }}
                                )
                            </a>
                        </label>
                    </div>
                    <div class="permission-list hidden" data-role="role_{{ $role->id }}">
                        @if ($role->all)
                        {{ trans('labels.backend.access.users.all_permissions') }}<br/><br/>
                        @else
                        @if (count($role->permissions) > 0)
                        <blockquote class="small">{{--
                                        --}}@foreach ($role->permissions as $perm){{--
                                            --}}{{$perm->display_name}}<br/>
                            @endforeach
                        </blockquote>
                        @else
                        {{ trans('labels.backend.access.users.no_permissions') }}<br/><br/>
                        @endif
                        @endif
                    </div><!--permission list-->
                    @endforeach
                    @else
                    {{ trans('labels.backend.access.users.no_roles') }}
                    @endif
                </div><!--col-lg-3-->
            </div><!--form control-->

            {{-- Associated Permissions --}}
            <div class="form-group col-12">
                {{ Form::label('associated-permissions', trans('validation.attributes.backend.access.roles.associated_permissions'), ['class' => 'control-label']) }}
                <div class="col-lg-10">
                    <div id="available-permissions" class="hidden mt-20" style="width: 700px; height: 200px; overflow-x: hidden; overflow-y: scroll;">
                        <div class="row">
                            <div class="col-xs-12 get-available-permissions">

                            </div><!--col-lg-6-->
                        </div><!--row-->
                    </div><!--available permissions-->
                </div><!--col-lg-3-->
            </div><!--form control-->

        </div><!-- /.box-body -->
    </div><!--box-->
</div><!--box-->
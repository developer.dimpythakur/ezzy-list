<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-user"></i>Select from Avtars or upload your photo</h6>
    </div>
    <div class="card-body profile-form bold-labels">
        <div class="form-group row">
            <div class="form-group col-4 required">
                <label for="profile_pic">
                    <img src="{{url('/').'/img/frontend/profile-picture/pic-1.png'}}" height=80 width=80>
                    {!! Form::radio('profile_pic') !!}
                </label>
            </div>
            <div class="form-group col-4">
                <label for="profile_pic">
                    <img src="{{url('/').'/img/frontend/profile-picture/pic-2.png'}}" height=80 width=80>
                    {!! Form::radio('profile_pic') !!}
                </label>
            </div>
            <div class="form-group col-4">
                <label for="profile_pic">
                    <img src="{{url('/').'/img/frontend/profile-picture/pic-3.png'}}" height=80 width=80>
                    {!! Form::radio('profile_pic') !!}
                </label>
            </div>
        </div>
        <div class="form-group col-sm-12">
            <div class="fake-file-upload h6 rounded-pill transition-1 d-flex align-items-center justify-content-center p-3 text-center text-muted position-relative">
                <input type="file" id="profile_picture" name="profile_picture" class="position-absolute pointer" >
                <i class="la la-cloud-upload-alt h3 m-0"></i>&nbsp; Choose image
            </div>
        </div>
    </div>
</div>

@include('frontend.user.components.profile.update-button')
@push('after_scripts')
<script>
    jQuery(document).ready(function () {

    });
</script>
@endpush
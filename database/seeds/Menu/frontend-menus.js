[
    {
        "view_permission_id": "",
        "icon": "l",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "frontend.index",
        "name": "Home",
        "id": 1,
        "content": "Homepage"
    },
    {
        "view_permission_id": "",
        "icon": "l",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "frontend.listing.all",
        "name": "Listings",
        "id": 1,
        "content": "Listings"
    },
    {
        "view_permission_id": "",
        "icon": "l",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "frontend.pricing",
        "name": "Pricing",
        "id": 1,
        "content": "Pricing"
    }
]
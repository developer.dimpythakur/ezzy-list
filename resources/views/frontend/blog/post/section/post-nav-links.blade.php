<div class="post-navigator d-flex justify-content-between pt-2 pb-2">
    @if (isset($previous))
    <div class="post-prev post-nav d-inline-block">
        <a href="{{ URL::to('post').'/'. $previous->slug }}"
           class="text-body text-decoration-none transition-1 d-flex align-items-center">
            <i class="las la-angle-left shadow rounded-pill bg-white d-inline-block mr-3 nav-icon"></i>
            <div class="d-inline-block">
                <h6>
                    {{  mb_strlen($previous->name) > 30 ? mb_substr($previous->name, 0, 30) . "..." : $previous->name }}
                </h6>
                <span class="text-muted">Previous post</span>
            </div>
        </a>
    </div>
    @endif
    @if (isset($next))
    <div class="post-next float-right post-nav d-inline-block text-right">
        <a href="{{ URL::to('post').'/'. $next->slug }}"
           class="text-body text-decoration-none transition-1 d-flex align-items-center">
            <div class="d-inline-block">
                <h6>
                    {{  mb_strlen($next->name) > 30 ? mb_substr($next->name, 0, 30) . "..." : $next->name }}
                </h6>

                <span class="text-muted">Next post</span>
            </div>
            <i class="las la-angle-right shadow rounded-pill bg-white d-inline-block ml-3 nav-icon"></i>
        </a>
    </div>
    @endif

</div>

<?php

namespace App\Http\Controllers\Frontend\Listing;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Responses\RedirectResponse;
//Models
use App\Models\Listing\Listing\Listing;
use App\Models\Listing\Categories\Category;
use App\Models\Listing\Region\Region;
use App\Models\Listing\Tags\Tag;
use App\Repositories\Frontend\Listing\ListingRepository;
use Illuminate\Http\Request as Req;

/**
 * Class ListingController.
 */
class ListingController extends Controller {

    public $listing;
    public $categories;

    public function __construct() {
//        $this->categories = Category::getSelectedData();
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index() {
        $title = 'Ezzy Listings';
        $listings = Listing::inRandomOrder()->where('status', 'publish')->paginate(10);
        $categories = Category::withCount('listing')->orderBy('listing_count', 'desc')->limit(6)->get();
        $regions = Region::withCount('locations')->orderBy('locations_count', 'desc')->limit(6)->get();
        $tags = Tag::withCount('listing')->orderBy('listing_count', 'desc')->limit(6)->get();
        return view('frontend.listing.index', compact('title', 'listings', 'categories', 'regions', 'tags'));
    }

    /**
     * show Listing
     * @param type $slug
     * @param ListingRepository $listing
     * @return type
     */
    public function show($slug, ListingRepository $listing) {
        $listing = $listing->findBySlug($slug);
        $related = $this->related($listing);
        $categories = $this->categories;
        $title = $listing->name;
        views($listing)->record();
        $viewCount = views($listing)->count();
        return view('frontend.listing.single', compact('listing', 'title', 'related', 'categories', 'viewCount'));
    }

    /**
     * Related
     * @param type $listing
     * @return type
     */
    public function related($listing) {
        return Listing::with('categories')->where('id', '!=', $listing->id)->get();
    }

    /**
     * Bookmark the listing
     * @param Req $request
     * @return type
     */
    public function bookmark(Req $request) {
        if (!$request->user && $request->user == '0') :
            return response()->json([
                        'type' => 'error',
                        'message' => 'Can not bookmark the listing'
            ]);
        endif;

        $listing = Listing::find($request->listing);
        if ($listing):
            $listing->owner->bookmark($listing); // bookmark or unbookmark
        endif;
        return response()->json([
                    'type' => 'success',
                    'message' => 'Listing bookmark updated.'
        ]);
    }

}

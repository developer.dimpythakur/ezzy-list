<?php

namespace App\Models\Listing\Locations\Traits;

/**
 * Class LocationAttribute.
 */
trait Attribute {

    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/6.x/eloquent-mutators#defining-an-accessor


    public function getActionButtonsAttribute() {

        return '<div class="btn-group action-btn">
                    ' . $this->getEditButtonAttribute('edit-location', 'admin.listing-locations.edit') . '
                    ' . $this->getViewButtonAttribute() . '                    
                    ' . $this->getDeleteButtonAttribute('delete-location', 'admin.listing-locations.destroy') . '
                </div>';
    }

    /**
     * @return string
     */
    public function getViewButtonAttribute() {
        return '<a target="_blank" href="' . route('frontend.listing.locations.show', $this->slug) . '" class="btn btn-flat btn-default">
                    <i data-toggle="tooltip" data-placement="top" title="View Locatoin" class="las la-eye"></i>
                </a>';
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute() {
        if ($this->isActive()) {
            return "<label class='label label-success'>" . trans('labels.general.active') . '</label>';
        }
        return "<label class='label label-danger'>" . trans('labels.general.inactive') . '</label>';
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

}

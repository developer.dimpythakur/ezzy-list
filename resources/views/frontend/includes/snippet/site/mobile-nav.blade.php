<!-- Mobile Menu -->
<button id="nav-toggle">
    <i class="fas fa-bars"></i>
</button>
<nav id="mobile-nav">
    <ul class="text-capitalize">
        <li>
            <a href="#">Home</a>
            <ul>
                <li><a href="index.html">Home</a>
                </li>
                <li><a href="index-banner.html">Home Banner</a>
                </li>
            </ul>
        </li>
        <li><a href="about.html">about</a>
        </li>
        <li>
            <a href="#">Blog</a>
            <ul class="submenu">
                <li><a href="blog-grid.html">Blog Grid</a></li>
                <li><a href="blog-full.html">Blog Full</a></li>
                <li class="dropdown">
                    <a href="#">Blog Detail</a>
                    <ul class="submenu">
                        <li><a href="blog-detail.html">Blog Detail</a></li>
                        <li><a href="blog-detail-sidebar-left.html">Blog Left Sidebar</a></li>
                        <li><a href="blog-detail-sidebar-right.html">Blog Right Sidebar</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">Listings</a>
            <ul>
                <li>
                    <a href="#">Listings Grid</a>
                    <ul class="submenu">
                        <li><a href="search-right-sidebar-grid.html">Right Sidebar</a>
                        </li>
                        <li><a href="search-left-sidebar-grid.html">Left Sidebar</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Listings List</a>
                    <ul class="submenu">
                        <li><a href="search-right-sidebar-list.html">Right Sidebar</a>
                        </li>
                        <li><a href="search-left-sidebar-list.html">Left Sidebar</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Listings Map</a>
                    <ul class="submenu">
                        <li><a href="search-right-sidebar-map.html">Right Sidebar</a>
                        </li>
                        <li><a href="search-left-sidebar-map.html">Left Sidebar</a>
                        </li>
                    </ul>
                </li>
                <li>	<a href="listing-detail.html">Single Listing</a>
                </li>
            </ul>
        </li>
        <li class="pb-1">
            <a href="#">pages</a>
            <ul>
                <li>
                    <a href="#">Events</a>
                    <ul class="submenu">
                        <li><a href="event.html">Events</a>
                        </li>
                        <li><a href="event-detail.html">Event Detail</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Shop</a>
                    <ul class="submenu">
                        <li><a href="shop.html">Shop</a>
                        </li>
                        <li><a href="shop-detail.html">Shop Detail</a>
                        </li>
                    </ul>
                </li>
                <li><a href="add-listing.html">Add Listing</a>
                </li>
                <li><a href="about.html">About Us</a>
                </li>
                <li><a href="404.html">404</a>
                </li>
                <li><a href="faqs.html">FAQs</a>
                </li>
                <li><a href="pricing.html">Pricing</a>
                </li>
                <li><a href="contact.html">Contact Us</a>
                </li>
            </ul>
        </li>
        <li class="et-login-btn pb-3">	<a class="btn bg-primary transition-1 border-0 text-capitalize text-white" href="add-listing.html"><i class="fa fa-plus"></i> Add Listing</a>
        </li>
        <li class="et-add-list-btn pb-3">	<a href="#" class="et-add-listing-btn bg-primary btn transition-1 border-0 text-capitalize text-white" data-toggle="modal" data-target="#loginSignup"> Login/Signup</a>
        </li>
    </ul>
</nav>
<!-- /Mobile Menu -->
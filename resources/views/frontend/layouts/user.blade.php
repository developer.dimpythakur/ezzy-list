<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ config('backend.html_direction') }}">
    <head>
        @include(('frontend.layouts.sections.head'))
        <link rel="stylesheet" type="text/css" href="{{ asset('css/backend/user-dashboard.css')}}">
        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token() ]) !!}
        </script>
    </head>
    <body class="{{ config('backend.body_class') }} user-dashboard sidebar-fixed header-fixed">
        @include(('frontend.layouts.user.header'))
        <div class="app-body" id="app">
            @include('frontend.layouts.user.sidebar')
            <main class="main pt-2">
                <section class="container-fluid animated fadeIn">
                    <section class="row my-3">
                        <div class="col-md-7 title-wrapper">
                            @yield('page-header')
                        </div>
                        <!-- Breadcrumbs -->
                        <div class="col-md-5">
                            @if(Breadcrumbs::exists())
                            {!! Breadcrumbs::render() !!}
                            @endif
                        </div>
                    </section>
                </section>
                <section class="container-fluid animated fadeIn">
                    @yield('content')
                </section>
            </main>
        </div><!-- ./app-body -->

        <footer class="{{ config('backend.footer_class') }} footer-dark d-none">
            @include(('frontend.layouts.user.footer'))
        </footer>
        <script type='text/javascript'>
            var EZZY_MAP = {
                "submitCenterPoint": "40.757662,-73.974741",
                "centerPoint": "40.757662,-73.974741",
                "map_provider": "mapbox",
                "mapbox_access_token": "p",
                "mapbox_retina": "on",
                "start_point": "New York, NY, USA",
                "start_geo_lat": "40.7127753",
                "start_geo_long": "-74.0059728",
                "enable_map": "1",
                "user_location": [],
                "l10n": {
                    "locked": "Lock",
                    "unlocked": "Unlock"
                },
                "country": "USA",
            };
        </script>
        @yield('before_scripts')
        @stack('before_scripts')
        @include(('frontend.layouts.user.scripts'))
        @yield('after_scripts')
        @stack('after_scripts')
        <script type="text/javascript" id="subscription" src="{{ asset('js/backend/yelp.js') }}"></script>
    </body>
</html>
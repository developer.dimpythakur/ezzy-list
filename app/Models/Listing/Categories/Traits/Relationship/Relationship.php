<?php

namespace App\Models\Listing\Categories\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\Listing\Listing\Listing;

/**
 * Class Category Relationship.
 */
trait Relationship {

    /**
     * Category belongs to relationship with state.
     */
    public function creator() {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Category belongs to relationship with state.
     */
    public function listing() {
        return $this->belongsTo(\App\Models\ListingMap\MapCategory::class, 'id', 'ezzy_categories_id');
    }

}

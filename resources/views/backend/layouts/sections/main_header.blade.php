<header class="{{ config('backend.header_class') }}">
    <!-- Logo -->
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto ml-3" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{route('frontend.index')}}">
        @if(isset($setting) && $setting->logo)
        <img height="48" width="226" class="navbar-brand" src="{{route('frontend.index')}}/img/site_logo/{{$setting->logo}}">
        @else
        {!! config('backend.project_logo') !!}
        @endif

    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav ml-auto ">
        <li class="nav-item dropdown pr-4">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="{{ access()->user()->picture }}" class="user-image" alt="{{ access()->user()->first_name }}"/>
            </a>
            <div class="dropdown-menu dropdown-menu-right mr-4 pb-1 pt-1">
                <a class="dropdown-item" href="{{ route('admin.access.user.account')}}"><i class="la la-user"></i> My Account</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{!! route('frontend.auth.logout') !!}"><i class="la la-lock"></i> Logout</a>
            </div>
        </li>
    </ul>
</header>

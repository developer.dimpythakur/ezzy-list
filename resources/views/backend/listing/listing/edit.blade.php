@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.listing.management') . ' | ' . trans('labels.backend.listing.create'))

@section('page-header')
<h1>{{ trans('labels.backend.listing.management') }}</h1>
@endsection
@section('after_styles')
<style>
 
</style>
@endsection
@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::model($listing, ['route' => ['admin.listings.update', $listing], 'class' => 'form-horizontal','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-listing']) }}
        @include("backend.listing.listing.form")

        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=update_and_back">Update</span>
                </button>
            </div>
            <a href="{{ route('admin.listings.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>

@endsection
@section("after_scripts")
@endsection

<section>
    <div class="container">
        <div class="booking-confirm padd-top-30 padd-bot-30">
            <i class="fa fa-check" aria-hidden="true"></i>
            <h2 class="mrg-top-15">Thanks for your booking!</h2>
            <p>You'll receive a confirmation email at mail@yourgmail.com</p>
            <a href="#" class="btn theme-btn-trans mrg-top-20">Check Invoice</a>
        </div>
    </div>
</section>
https://codeminifier.com/listing-hub-2.1/listing-hub/thank-you.html
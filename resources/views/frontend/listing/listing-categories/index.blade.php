@extends('frontend.layouts.app')
@section('content')
<section class="et-featured-categories bg-light events map-home position-relative">
    <div class="container">
        <div class="section-heading mb-3 text-center d-block full-width">
            <h2 class="text-capitalize">Explore Listings By Category
            </h2>
            <p>Discover best things to do around the world by categories.</p>
        </div>
        <div class="row">
            @if($listingsCategories)
            @foreach($listingsCategories as $category)
            @include('frontend.listing.listing-categories.section.category')
            @endforeach
            @else
            <p>No Category Found</p>
            @endif
        </div>
    </div>
</section>
<!-- Newsletter Subscription -->
@include('frontend.includes.section.newsletter.newsletter')
<!-- /Newsletter Subscription -->
@endsection

@section('after-scripts')
<script type="text/javascript">
    jQuery(document).ready(function () {
        // Switch List and Grid View for Listings
        //---------------------------------------
        jQuery(".switch_grid").on("click", function () {
            jQuery(this).addClass("text-primary");
            jQuery(".switch_list").removeClass("text-primary");
            jQuery(".listing-grid-view").fadeIn("slow");
            jQuery(".listing-list-view").fadeOut("slow");
        })
        jQuery(".switch_list").on("click", function () {
            jQuery(this).addClass("text-primary");
            jQuery(".switch_grid").removeClass("text-primary");
            jQuery(".listing-grid-view").fadeOut("slow");
            jQuery(".listing-list-view").fadeIn("slow");
        })
    })
</script>
@endsection

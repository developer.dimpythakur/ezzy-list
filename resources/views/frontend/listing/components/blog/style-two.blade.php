<!-- News and tips -->
<section class="et-blog-home position-relative">
    <div class="container">
        <div class="row">
            <div class="section-heading text-center d-block full-width">
                <h2 class="text-capitalize">News &amp; Tips</h2>
                <p>Search thousands of listings and directories</p>
            </div>
        @if($posts && !is_null($posts))
            @foreach($posts as $post)
                <!-- Post -->
                    <div class="col-md-4 col-sm-12 mt-5 mt-xs">
                        <a href="{{ URL::to('post').'/'. $post->slug }}"
                           class="single-post d-block overflow-hidden shadow card-border-radius img-overlay position-relative transition-1">

                            @if(!empty($post->featured_image))
                                <img src="{{ Storage::url('images/blog/' . $post->featured_image) }}"
                                     alt="img"
                                     class="img-fluid rounded-top">
                            @else
                                <img src="{{ Storage::url('images/blog/' . $post->featured_image) }}" alt="img"
                                     class="img-fluid rounded-top">
                            @endif
                            <div class="post-content position-absolute rounded-pill fixed-top h-100">
                                @if(!empty($post->categories))
                                    @foreach($post->categories  as $category)
                                        <span class="badge badge-primary mt-3 ml-3 p-2 position-absolute post-category sticky-top p-1">
                                              {{$category->name }}
                                           </span>
                                    @endforeach
                                @else
                                    <span class="badge badge-primary mt-3 ml-3 p-2 position-absolute post-category sticky-top p-1">
                                    General
                                    </span>
                                @endif

                                <div class="fixed-bottom p-3 position-absolute text-white bottom-content transition-1">
                                    <i class="icon-speaker h3 mb-2 d-block"></i>
                                    <p class="mb-2 text-white">
                                        {{\Carbon\Carbon::parse($post->publish_datetime)->format('M, d, Y ')}}
                                        - By {{ $post->owner->first_name}}
                                    </p>
                                    <h6 class="text-white">
                                        {{ $post->name }}

                                    </h6>
                                    <p class="description transition-1 mb-0 text-white">
                                        {{ substr($post->content,0,100) }}
                                    </p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- /Post -->
            @endforeach
        @else

            <!-- Post -->
                <div class="col-md-4 col-sm-12 mt-5 mt-xs">
                    <a href="blog-detail.html"
                       class="single-post d-block overflow-hidden shadow card-border-radius img-overlay position-relative transition-1">
                        <img src="img/blog/blog-7.jpg" alt="img" class="img-fluid">
                        <div class="post-content position-absolute rounded-pill fixed-top h-100">
                            <span class="badge badge-primary mt-3 ml-3 p-2 position-absolute post-category sticky-top p-1">Primary</span>
                            <div class="fixed-bottom p-3 position-absolute text-white bottom-content transition-1">
                                <i class="icon-play-button1 h3 mb-2 d-block"></i>
                                <p class="mb-2 text-white">Oct, 19, 2019 - By Admin</p>
                                <h6 class="text-white">Travel guide for you!</h6>
                                <p class="description transition-1 mb-0 text-white">Lorem ipsum dolor sit amet,
                                    consectetur
                                    adipisicing elit, sed...</p>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- /Post -->
            @endif
            <div class="btn-center">
                <a href="blog-grid.html" class="btn btn-primary border-0 m-auto">See more</a>
            </div>
        </div>
    </div>
</section>
<!-- /News and tips -->

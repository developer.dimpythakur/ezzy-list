<?php

namespace App\Repositories\Frontend\Listing;

use Stevenmaguire\Yelp\ClientFactory;
use Stevenmaguire\Yelp\Version;
// use 
use App\Models\Listing\Categories\Category;
//regions
use App\Models\Listing\Region\Region;
// request
use Illuminate\Http\Request;

class YelpRepository {

    /**
     * Required, unless apiKey is provided
     * @var type 
     */
    private $accessToken;

    /**
     * Required, unless apiKey is provided
     * @var type 
     */
    private $apiHost;

    /**
     * API Key
     * @var type 
     */
    private $apiKey;

    /**
     * Client
     * @var type 
     */
    protected $client;

    /**
     * Options
     * @var type 
     */
    protected $options;

    public function __construct() {
        $this->options = [
            'accessToken' => config('yelp.v3.accessToken'),
            'apiHost' => config('yelp.v3.apiHost'),
            'apiKey' => config('yelp.v3.apiKey')
        ];
        $this->client = ClientFactory::makeWith($this->options, Version::THREE);
    }

    /**
     * Get Yelp Business 
     * @param Request $request
     * @return type
     */
    public function searchBusiness($params=null) {
        return $this->client->getBusinessesSearchResults($params);
    }

    /**
     * Get Yelp Business 
     * @param Request $phone
     * @return type
     */
    public function searchPhone($phone) {
        return $this->client->getBusinessesSearchResultsByPhone($phone);
    }

    /**
     * Get Reviews
     * @param type $businessId
     * @return type
     */
    public function businessReviews($businessId) {
        return $this->client->getBusinessReviews($businessId);
    }

    /**
     * Get Reviews
     * @param type $businessId
     * @return type
     */
    public function business($businessId) {
        return $this->client->getBusiness($businessId);
    }

}

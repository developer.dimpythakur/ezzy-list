/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/backend/user-dashboard.js":
/*!*******************************************************!*\
  !*** ./resources/assets/js/backend/user-dashboard.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Star Output
 * @param firstStar
 * @param secondStar
 * @param thirdStar
 * @param fourthStar
 * @param fifthStar
 * @returns {string}
 */
function outPutStar(firstStar, secondStar, thirdStar, fourthStar, fifthStar) {
  return '' + '<span class="las ' + firstStar + '"></span>' + '<span class="las ' + secondStar + '"></span>' + '<span class="las ' + thirdStar + '"></span>' + '<span class="las ' + fourthStar + '"></span>' + '<span class="las ' + fifthStar + '"></span>';
}

function starRating(ratingElem) {
  $(ratingElem).each(function () {
    var dataRating = $(this).attr('data-rating');

    if (jQuery(this).attr('data-class')) {
      var classReview = jQuery(this).attr('data-class');
    }

    var fiveStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star', 'la-star');
    var fourHalfStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star', 'la-star half');
    var fourStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star', 'la-star empty');
    var threeHalfStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star half', 'la-star empty');
    var threeStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star empty', 'la-star empty');
    var twoHalfStars = outPutStar('la-star', 'la-star', 'la-star half', 'la-star empty', 'la-star empty');
    var twoStars = outPutStar('la-star', 'la-star', 'la-star empty', 'la-star empty', 'la-star empty');
    var oneHalfStar = outPutStar('la-star', 'la-star half', 'la-star empty', 'la-star empty', 'la-star empty');
    var oneStar = outPutStar('la-star', 'la-star empty', 'la-star empty', 'la-star empty', 'la-star empty');
    var empty = outPutStar('la-star empty', 'la-star empty', 'la-star empty', 'la-star empty', 'la-star empty');

    if (!dataRating) {
      $(this).append(empty);
      return;
    }

    if (dataRating >= 4.75) {
      $(this).append(fiveStars);
    } else if (dataRating >= 4.25) {
      $(this).append(fourHalfStars);
    } else if (dataRating >= 3.75) {
      $(this).append(fourStars);
    } else if (dataRating >= 3.25) {
      $(this).append(threeHalfStars);
    } else if (dataRating >= 2.75) {
      $(this).append(threeStars);
    } else if (dataRating >= 2.25) {
      $(this).append(twoHalfStars);
    } else if (dataRating >= 1.75) {
      $(this).append(twoStars);
    } else if (dataRating >= 1.25) {
      $(this).append(oneHalfStar);
    } else if (dataRating < 1.25) {
      $(this).append(oneStar);
    } else {
      $(this).append(empty);
    }
  });
}

jQuery(document).ready(function () {
  /*----------------------------------------------------*/

  /*  Ratings Script
   /*----------------------------------------------------*/
  starRating('.star-rating');
});

/***/ }),

/***/ 4:
/*!*************************************************************!*\
  !*** multi ./resources/assets/js/backend/user-dashboard.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/ezzy-app/resources/assets/js/backend/user-dashboard.js */"./resources/assets/js/backend/user-dashboard.js");


/***/ })

/******/ });
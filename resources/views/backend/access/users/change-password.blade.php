@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.change_password'))
@section('page-header')
<h1>
    {{ trans('labels.backend.access.users.management') }}
    <small>{{ trans('labels.backend.access.users.change_password') }}</small>
</h1>
@endsection

@section('content')

<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.pages.section.grouped_errors'))
        {{ Form::open(['route' => ['admin.access.user.change-password', $user], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'patch']) }}
        <div class="card mt-2 p-2">
            <div class="card-header">
                <h6 class="my-2"><i class="la la-user-secret"></i> Update Password</h6>
            </div>
            <div class="card-body bold-labels">
                <div class="row my-3">
                    <div class="form-group col-12">
                        {{ Form::label('old password', trans('validation.attributes.backend.access.users.old_password'), ['class' => 'control-label required', 'placeholder' => trans('validation.attributes.backend.access.users.password')]) }}
                        {{ Form::password('old_password', ['class' => 'form-control  box-size']) }}
                    </div><!--form control-->
                    <div class="form-group col-12">
                        {{ Form::label('password', trans('validation.attributes.backend.access.users.password'), ['class' => 'control-label required', 'placeholder' => trans('validation.attributes.backend.access.users.password')]) }}
                        {{ Form::password('password', ['class' => 'form-control  box-size']) }}
                    </div><!--form control-->

                    <div class="form-group col-12">
                        {{ Form::label('password_confirmation', trans('validation.attributes.backend.access.users.password_confirmation'), ['class' => 'control-label', 'placeholder' => trans('validation.attributes.backend.access.users.password_confirmation')]) }}
                        {{ Form::password('password_confirmation', ['class' => 'form-control  box-size']) }}
                    </div><!--col-lg-10-->
                </div><!--form control-->
            </div><!-- /.box-body -->
        </div><!--box-->

        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="update">Update</span>
                </button>
            </div>
            <a href="" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection
<?php

namespace App\Models\Listing\Media\SocialMedia\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\Listing\Listing\Listing;

/**
 * Class Relationship.
 */
trait Relationship {

    /**
     * BlogCategories belongs to relationship with state.
     */
    public function creator() {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * ListingFaq Belong to Listing
     * @return mixed
     */
    public function listing() {
        return $this->belongsTo(Listing::class);
    }

}

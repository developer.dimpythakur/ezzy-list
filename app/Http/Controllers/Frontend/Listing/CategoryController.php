<?php

namespace App\Http\Controllers\Frontend\Listing;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Models\Listing\Listing\Listing;
use App\Models\Listing\Categories\Category;
use App\Models\Listing\Locations\Location;
use App\Models\Listing\Region\Region;
use App\Repositories\Frontend\Listings\CategoryRepository;
use App\Repositories\Frontend\Listings\ListingsRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class ListingController.
 */
class CategoryController extends Controller {

    public $googlePlaces;

    public function __construct() {
        $this->googlePlaces = '';
    }

    public function index() {
        $listingsCategories = Category::with('listing')->get();
        return view('frontend.listing.listing-categories.index', compact('listingsCategories'));
    }

    /**
     * Category 
     * @param type $slug
     * @param CategoryRepository $category
     * @return type
     */
    public function show($slug, CategoryRepository $category) {
        $category = $category->findBySlug($slug);
        $listings = Listing::where('status', 'publish')->whereHas('categories', function ($q) use ($category) {
                    $q->where('name', 'like', "%{$category->name}%");
                })->paginate();
        $title = $category->name;
        return view('frontend.listing.listing-categories.single', compact('category', 'title', 'listings'));
    }

}

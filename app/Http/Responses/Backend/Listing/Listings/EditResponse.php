<?php

namespace App\Http\Responses\Backend\Listings\Listings;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable
{
    /**
     * @var App\Models\Listings\Listing
     */
    protected $listings;

    /**
     * @param App\Models\Listings\Listing $listings
     */
    public function __construct($listings)
    {
        $this->listings = $listings;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request)
    {
        return view('backend.listings.edit')->with([
            'listings' => $this->listings
        ]);
    }
}
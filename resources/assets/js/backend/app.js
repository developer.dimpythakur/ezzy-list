/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

//require('../bootstrap');


window._ = require('lodash');
global.$ = global.jQuery =
        window.$ = window.jQuery = require('jquery'); // jQuery

window.Popper = require('popper.js').default; // popper.js for tooltips

window.Noty = require('noty'); // Unobtrusive alert or notification bubbles

require('bootstrap'); // Bootstrap for tabs, dropdowns, carousels etc
require('@coreui/coreui'); // CoreUI for sidemenu
require('pace-js'); // Pace JS for  AJAX calls
require('animate.css');

import swal from 'sweetalert';

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};
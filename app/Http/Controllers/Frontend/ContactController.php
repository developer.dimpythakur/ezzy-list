<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\Contact\Contact;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Prologue\Alerts\Facades\Alert;

class ContactController extends Controller {

    /**
     * Show contact us form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function getContact() {

        return view('contact_us');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveContact(Request $request) {
        $validator = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|email',
//            'phone_number' => 'required',
                    'message' => 'required'
        ]);
        if ($validator->fails()):
            Alert::error('Validation Error', 'Please fill the required fields');
            return redirect()->back()
                            ->withInput($request->all())
                            ->withErrors($validator->errors());
        endif;

        //new contact
        $contact = new Contact;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->phone_number = $request->phone_number;
        $contact->message = $request->message;

        $contact->save();

        return back()->with('success', 'Thank you for contact us!');
    }

    /**
     * Send mail to admin
     * @param $request
     */
    public function sendMail($request) {
        \Mail::send('contact_email',
                array(
                    'name' => $request->name,
                    'email' => $request->email,
                    'subject' => $request->subject,
                    'phone_number' => $request->phone_number,
                    'user_message' => $request->message,
                ), function ($message) use ($request) {
            $message->from($request->email);
            $message->to('codingdriver15@gmail.com');
        });
    }

}

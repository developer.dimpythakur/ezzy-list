<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEzzyReviewsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ezzy_reviews';

    /**
     * Run the migrations.
     * @table ezzy_reviews
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('review_id', 100)->nullable()->default(null);
            $table->string('review_type', 100)->nullable()->default(null);
            $table->integer('user_id');
            $table->string('stars', 100)->nullable()->default(null);
            $table->string('useful', 100)->nullable()->default(null);
            $table->string('funny', 100)->nullable()->default(null);
            $table->string('cool', 100)->nullable()->default(null);
            $table->string('rating', 100)->nullable()->default(null);
            $table->text('text')->nullable()->default(null);
            $table->string('ip', 30)->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

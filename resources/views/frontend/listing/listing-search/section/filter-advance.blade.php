<?php
$cat = [];
if ($categories && count($categories)):
    foreach ($categories as $category):
        $cat[$category['id']] = $category['name'];
    endforeach;
endif;
$searchText = isset($listing) ? $listing : '';
?>
{{ Form::open(['route' => 'frontend.search.result', 'class' => 'home-search form-horizontal','autocomplete'=>'off', 'method' => 'GET']) }}
<div class="input-wrap d-flex p-1">
    <div class="near-field position-relative d-flex align-items-center">
        {{ Form::text('search_keywords',$searchText,['placeholder'=>"What You Are Looking For",'class'=>'form-control ml-2 pl-2 text-white']) }}
    </div>
    <div class="find-field position-relative mx-2">
        @if($categories && count($categories))
        @if($categories)
        <select name="category" id="search_categories" class="form-control form-control wide">
            <option value="0" data-display="Any Category">Any Category</option>
            @foreach($categories as $category)
            <option {{ (isset($searchTermCategory) && ($searchTermCategory) ===strtolower($category->name))?'selected':'' }} value="{{ strtolower($category->name)}}">{{$category->name}}</option>
            @endforeach
        </select>
        @endif
        @endif
    </div>
    <div class="find-field position-relative mx-2">
        <select name="location" class="form-control form-control wide">
            @if($regions)
            <option value="0" data-display="Any Location">Any Category</option>
            @foreach($regions as $region)
            <option {{ (isset($location) && ($location) === strtolower($region->name))?'selected':'' }}  value="{{strtolower($region->name)}}">{{$region->name}}</option>
            @endforeach
            @endif
        </select>
    </div>
</div>


<div class="input-wrap d-flex p-1">
    <div class="find-field position-relative mx-2">
        @if($tags && count($tags))
        @if($categories)
        <select name="tags" id="tags" class="form-control form-control wide">
            <option value="0" data-display="Select Tags">Select Tags</option>
            @foreach($tags as $tag)
            <option {{ (isset($searchTermTag) && ($searchTermTag) ===strtolower($tag->name))?'selected':'' }} value="{{ strtolower($tag->name)}}">{{$tag->name}}</option>
            @endforeach
        </select>
        @endif
        @endif
    </div>
    <div class="input-wrap d-flex p-1">
        <div class="submit-btn  btn btn-primary border-0 transition-1 ml-auto position-relative pointer-event d-flex justify-content-center align-items-center">
            <i class="las la-search" aria-hidden="true"></i>
            <input type="submit" class="btn transition-1 field-btn" value="Search">
        </div>
    </div>
</div>
{{ Form::close() }}

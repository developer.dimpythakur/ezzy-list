<nav id="sidebar">
    <div class="shadow-bottom"></div>
    <ul class="list-unstyled menu-categories ps ps--active-y" id="accordionExample">
        <li class="menu">
            <a href="{{route('frontend.user.dashboard')}}" class="dropdown-toggle">
                <span>Dashboard</span>
            </a>
        </li>

        <li class="menu">
            <a href="{{route('frontend.user.listings')}}" data-toggle="collapse" aria-expanded="false"
               class="dropdown-toggle">
                <span>Listings</span>
            </a>
            <ul class="collapse submenu list-unstyled" id="listings" data-parent="#accordionExample">
                <li>
                    <a href="{{ URL::to('user/listings/') }}"> Active</a>
                </li>
                <li>
                    <a href="{{ URL::to('user/listings/') }}"> Pending </a>
                </li>
                <li>
                    <a href="{{ URL::to('user/listings/') }}"> Expired </a>
                </li>
            </ul>
        </li>
        <li class="menu">
            <a href="#components" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-box">
                        <path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                        <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                        <line x1="12" y1="22.08" x2="12" y2="12"></line>
                    </svg>
                    <span>Reviews</span>
                </div>
            </a>
        </li>
        <li class="menu">
            <a href="{{route('frontend.user.account')}}" class="dropdown-toggle">
                <div class="">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                         stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                         class="feather feather-zap">
                        <polygon points="13 2 3 14 12 14 11 22 21 10 12 10 13 2"></polygon>
                    </svg>
                    <span>My Profile</span>
                </div>
            </a>
        </li>
        <li class="menu">
            <a href="{{route('frontend.auth.logout')}}" class="dropdown-toggle">
                <span>Log Out</span>
            </a>
        </li>
    </ul>
</nav>


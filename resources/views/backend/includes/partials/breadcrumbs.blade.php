@if (($breadcrumbs))
<nav aria-label="breadcrumb" class="d-none d-lg-block">
    <ol class="breadcrumb bg-transparent p-0  justify-content-end">
        @foreach ($breadcrumbs as $breadcrumb)
        @if ($breadcrumb->url && !$loop->last)
        <li class="breadcrumb-item text-capitalize"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
        @else
        <li class="breadcrumb-item text-capitalize active" aria-current="page">{{ $breadcrumb->title }}</li>
        @endif
        @endforeach
    </ol>
</nav>
@endif

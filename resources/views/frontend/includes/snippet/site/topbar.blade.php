<!-- Back To Top -->
<div id="backTop"><i aria-hidden="true" class="las la-arrow-up"></i></div>
<!-- /Back To Top -->
<!-- Topbar -->
<div class="et-topbar">
    <div class="container">
        <div class="row align-items-center">
            <div class="align-items-center col-md-6 justify-content-center justify-md-start pb-2 pt-2 pt-md-0 pb-md-0 text-center text-lg-left text-md-left text-sm-center">
                <div class="social list-inline">
                    <a href="#"> <i class="lab la-facebook color-fb" aria-hidden="true"></i>
                    </a>
                    <a href="#"> <i class="lab la-google color-google" aria-hidden="true"></i>
                    </a>
                    <a href="#"> <i class="lab la-instagram color-instagram" aria-hidden="true"></i>
                    </a>
                    <a href="#"> <i class="lab la-youtube color-youtube" aria-hidden="true"></i>
                    </a>
                    <a href="#"> <i class="lab la-linkedin" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="align-items-center d-flex col-lg-6 col-md-6 col-sm-12 justify-content-center justify-content-lg-end text-center text-lg-right text-md-right text-sm-center text-right text-nowrap">
                <!-- Language Switcher -->
                @if (config('locale.status') && count(config('locale.languages')) > 1)
                @include('includes.partials.lang')
                @endif
                <!-- /Language Switcher -->
            </div>
        </div>
    </div>
</div>
<!-- /Topbar -->
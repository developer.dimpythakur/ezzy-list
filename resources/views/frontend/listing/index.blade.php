@extends('frontend.layouts.app')
@section('after-styles')
{{ Html::style(('css/frontend/map/listing-map-manager.css')) }}
@endsection
@section('content')
<section class="et-search-result-wrapper search-map">
    @if($listings && count($listings))
    <div class="container-fluid pt-2 pt-xs">
        <div class="row mx-0">
            <div class="col-lg-7 vh px-0">
                @include('frontend.listing.components.listing-search.search-listing')
            </div>
        </div>

        <div class="row mx-0 listing-all-wrapper">
            <div class="col-lg-7 pt-5 listing-grid-view et-explore-by-city-wrap">
                <div class="ezzy-listings listing-all style-one pt-1">
                    @foreach($listings as  $listing)
                    @include('frontend.listing.components.listing-style.style-one')
                    @endforeach
                </div>
                <!-- Pagination -->
                <div class="row pt-3 mt-4">
                    {{ $listings->links() }}
                </div>
                <!-- /Pagination -->
            </div>
            <div class="col-lg-5 pt-5 map-right hide-map-mobile">
                @include('frontend.listing.components.listing-map.map')
            </div>
        </div>
    </div>
    @else
    <div class="row justify-content-md-center mt-xs mt-md-4">
        <div class="col-lg-10 ezzy-listings ">
            <div class="text-center no-listings-found p-4 mx-auto">
                <h3 class="text-primary">No Listing Found</h3>
            </div>
        </div>
    </div>
    @endif
</section>
@endsection

@section('after-scripts')
<!-- Map API -->
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAmrxCFuWyNUlMh6wLitVLI3pF7j_HqAFY&#038;libraries=places&#038;ver=1.1'></script>
<!-- Custom Js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="{{ asset('listing/mapify.js') }}"></script>
<script src="{{ asset('listing/geo-tag-text.js') }}"></script>
<script src="{{ asset('listing/map/leaflet.js') }}"></script>
<script src="{{ asset('listing/listing-manager.js') }}"></script>
<script>
//grid switch
jQuery(document).ready(function () {
    // Switch List and Grid View for Listings
    //---------------------------------------
    jQuery(".switch_grid").on("click", function () {
        jQuery(this).addClass("text-primary");
        jQuery(".switch_list").removeClass("text-primary");
        jQuery(".listing-grid-view").fadeIn("slow");
        jQuery(".listing-list-view").fadeOut("slow");
    })
    jQuery(".switch_list").on("click", function () {
        jQuery(this).addClass("text-primary");
        jQuery(".switch_grid").removeClass("text-primary");
        jQuery(".listing-grid-view").fadeOut("slow");
        jQuery(".listing-list-view").fadeIn("slow");
    });
});
</script>
@endsection
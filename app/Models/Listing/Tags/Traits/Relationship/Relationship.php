<?php

namespace App\Models\Listing\Tags\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\ListingMap\MapTag;

/**
 * Class Relationship.
 */
trait Relationship {

    /**
     * BlogTags belongs to relationship with state.
     */
    public
            function creator() {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * Tag belongs to relationship with state.
     */
    public function listing() {
        return $this->belongsTo(MapTag::class, 'id', 'ezzy_tags_id');
    }

}

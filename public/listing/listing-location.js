"use strict";
jQuery(function ($) {

    if ('1' === EZZY_MAP.enable_map) {

        var geo_lat = (typeof EZZY_MAP.list_geo_lat !== 'undefined' && '' !== EZZY_MAP.list_geo_lat) ? EZZY_MAP.list_geo_lat : EZZY_MAP.start_geo_lat;
        var geo_long = (typeof EZZY_MAP.list_geo_long !== 'undefined' && '' !== EZZY_MAP.list_geo_lat) ? EZZY_MAP.list_geo_long : EZZY_MAP.start_geo_long;

        var look = [jQuery('#location'), jQuery('#contact-map'), jQuery('#event_location')];

        jQuery.each(look, function (key, $input) {
            if ($input.length) {
                $input.geo_tag_text({latOutput: 'geolocation_lat', lngOutput: 'geolocation_long'});
                $input.mapify({mapify: "#listing-map", mapHeight: "300px", startGeoLat: geo_lat, startGeoLng: geo_long});
            }
        });

    }

    var field_id_array = [jQuery('#search_location')];

    jQuery.each(field_id_array, function (key, $input) {
        if ($input.length) {
            $input.geo_tag_text({latOutput: 'geolocation_lat', lngOutput: 'geolocation_long'});
            var autoComplete = this;
            var autoCompleteField = new google.maps.places.Autocomplete(jQuery(autoComplete)[0]);
            // jQuery( autoComplete ).on( 'change', jQuery( autoComplete ), function() {});
        }
    });


    // Don't set the user location when it already has a value
    if ($('#search_location').val() != '') {
        return;
    }
    var user_location = EZZY_MAP.user_location;

    if (user_location) {

        var input_location = user_location.city ? user_location.city : '';
        if ('' === input_location) {
            input_location = user_location.regionName ? user_location.regionName : '';
        }

        if ('' === input_location) {
            input_location = user_location.countryCode ? user_location.countryCode : '';
        }

        $('.search_jobs').each(function (i, el) {
            var $location = $(this).find('#search_location');
            $location.val(input_location);
        });
    }
});
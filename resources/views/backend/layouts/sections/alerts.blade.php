<script type="text/javascript">
    Noty.overrideDefaults({
    layout   : 'topRight',
            theme    : 'ezzy',
            timeout  : 2500,
            closeWith: ['click', 'button'],
    });
    @foreach (\Alert::getMessages() as $type => $messages)
            @foreach ($messages as $message)
            new Noty({
            type: "{{ $type }}",
                    text: "{!! str_replace('"', "'", $message) !!}"
            }).show();
    @endforeach
            @endforeach
</script>
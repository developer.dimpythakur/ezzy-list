<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEzzyListingEzzyRegionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ezzy_listing_regions';

    /**
     * Run the migrations.
     * @table ezzy_listing_ezzy_locations
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ezzy_listing_id');
            $table->unsignedBigInteger('ezzy_regions_id');
            $table->foreign('ezzy_listing_id')->references('id')->on('ezzy_listing');
            $table->foreign('ezzy_regions_id')->references('id')->on('ezzy_regions');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}

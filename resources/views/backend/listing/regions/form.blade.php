@push('after_styles')
<style>
    .normal-labels label{
        font-weight: 400
    }
    /*
     * File
     */
    .file input:focus ~ .file-custom {
        box-shadow: 0 0 0 .075rem #fff, 0 0 0 .2rem #0074d9;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu .collapse:not(.show){
        display: block !important
    }
    .select2-container .select2-selection--single,
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        height: 40px;
        line-height: 40px;
    }


    /*FAKE FILE*/
    .fake-file-upload {
        border: 2px dashed #ccc;
    }
    .fake-file-upload input {
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
    }
    .fake-file-upload:hover{
        border-color: #e53935 !important;
    }
    .image-preview {
        padding-left: 0;
    }
    .image-preview img{
        height: 150px;
        max-height: 200px;
    }
</style>
@endpush
<div class="row my-3">
    <div class="form-group col-12">
        {{ Form::label('name', 'Location Title', ['class' => ' control-label required']) }}
        {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => 'Location Title', 'required' => 'required']) }}
    </div><!--form-group-->
    <div class="form-group col-12">
        {{ Form::label('country', 'Country', ['class' => ' control-label required']) }}
        {{ Form::text('country', 'USA', ['class' => 'form-control box-size', 'readonly']) }}
    </div><!--form-group-->

    <div class="form-group col-12">
        {{ Form::label('region', 'Region', ['class' => ' control-label required']) }}
        <select name="region" class="form-control box-size select2">
            @foreach(getStates() as $state)
            <option {{  (isset($listing_region)? getSelectedLocation($listing_region,$state['abbreviation']):'')  }} value="{{$state['abbreviation']}}">{{ $state['name'] }}</option>
            @endforeach
        </select>
    </div><!--form-group-->

    <div class="form-group col-12">
        {{ Form::label('category_image', 'Image', ['class' => ' control-label']) }}
        <div class="fake-file-upload h6 rounded-pill transition-1 d-flex align-items-center justify-content-center p-3 text-center text-muted position-relative">
            <input type="file" id="image" name="image" class="position-absolute pointer"
                   > <i  class="icon-cloud-computing h3 m-0"></i>&nbsp; Choose or drag image here
        </div>
    </div>

    <div class="form-group col-12">
        <div class="col-lg-4">
            <div class="image-preview">
                @if(isset($listing_region) && !empty($listing_region->image))
                <img src="{{ Storage::url('images/listing/regions/' . $listing_region->image) }}" height="100"
                     class="img-thumbnail img-preview" width="100%">
                @endif
            </div>
        </div>
    </div>
</div>

@push("after_scripts")

<!-- include select2 js-->
<script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
@if (app()->getLocale() !== 'en')
<script src="{{ asset('packages/select2/dist/js/i18n/' . app()->getLocale() . '.js') }}"></script>
@endif
<script>

$(document).ready(function () {
//        select2
    $('.select2').select2();
// images preview with JavaScript
    var imgPreview = function (input, imgPreviewPlaceholder) {
        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function (event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                }

                reader.readAsDataURL(input.files[i]);
            }
        } else {
            console.log(input);
        }
    };

    $('#image').on('change', function () {
        let $el = 'div.image-preview';
        jQuery($el).html(' ');
        imgPreview(this, $el);
    });

});

</script>
@endpush

@extends('frontend.layouts.app')
@section('after-styles')
{{ Html::style(('listing/map/leaflet.css')) }}
@endsection

@section('content')
@if($locations && count($locations))
<section class="et-search-result-wrapper search-map">
    <div class="container-fluid pt-xs">
        <div class="row">
            <div class="col-lg-6 pt-1">
                <div class="row justify-content-md-center p-4">
                    <div class="col-lg-12 mb-3">
                        @include('frontend.listing.listing-locations.section.search-filter')
                    </div>
                </div>

                <div class="row p-4">
                    @foreach($locations as $location)
                    @php $listing = $location->listing @endphp
                    <div class="col-md-6 px-4 py-1">
                        <div class="et-list-grid-main ezzy-listings listing-location style-two">
                            @include('frontend.listing.components.listing-style.style-two')
                        </div>
                    </div>
                    @endforeach
                </div>
                <!-- Pagination -->
                <div class="w-100 py-3">
                    {{ $locations->links() }}
                </div>
                <!-- /Pagination -->
            </div>
            <!-- Map -->
            <div class="col-lg-6 position-relative map-right">
                @include('frontend.listing.components.listing-map.map')
            </div>
            <!-- Map -->
        </div>
    </div>
</section>
@else
<section class="et-search-result-wrapper search-map">
    <div class="container-fluid px-0">
        @include('frontend.listing.listing-locations.section.location-header')
        <div class="w-100 pt-4 mt-4">
            <div class="row py-4 my-4">
                <div class="col-md-5 mx-auto">
                    <div class="section-heading text-center d-block full-width">
                        <h3 class="text-capitalize text-dark">No listing found under <span class="text-primary">{{ $region->name }}</span></h3>
                        <p>Listings are in reviews before they appear on the website.</p>
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>
</section>
@endsection
@section('after-scripts')
<!-- Map API -->
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAmrxCFuWyNUlMh6wLitVLI3pF7j_HqAFY&#038;libraries=places&#038;ver=1.1'></script>
<!-- Custom Js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="{{ asset('listing/mapify.js') }}"></script>
<script src="{{ asset('listing/geo-tag-text.js') }}"></script>
<script src="{{ asset('listing/map/leaflet.js') }}"></script>
<script src="{{ asset('listing/listing-manager.js') }}"></script>
@endsection
@php
use Illuminate\Support\Facades\Route;
@endphp
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ isset($title) ? $title.' :: '.config('backend.project_name') : config('backend.project_name') }}</title>
        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'Eazzy List')">
        <meta name="author" content="@yield('meta_author', 'Eazzy List')">
        <meta name="keywords" content="@yield('meta_keywords', 'Eazzy List')">
        <!--Fevicon-->
        <link rel='shortcut icon' type='image/x-icon' href='{{ Storage::url('images/logo/favicon.ico')}}' />
        <link rel="icon" type="image/png" href="{{ Storage::url('images/logo/favicon-16x16.png')}}"/>
        <script type='application/ld+json'> 
            {
            "@context": "http://www.schema.org",
            "@type": "Restaurant",
            "name": "Ezzy List",
            "url": "http://ezzylist.com",
            "sameAs": [
            "http://ezzylist.com"
            ],
            "logo": "http://ezzylist.com/storage/images/frontend/logo.png",
            "image": "http://ezzylist.com/storage/images/frontend/logo.png"
            }
        </script>

        @yield('meta')
        <!-- Styles -->
        @yield('before-styles')
        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        @langrtl
        {{ Html::style(getRtlCss(('css/frontend/app.css'))) }}
        @else
        {{ Html::style(('css/frontend/app.css')) }}
        {{ Html::style(('css/frontend/frontend-styes.css')) }}
        {{ Html::style(('packages/line-awesome/css/line-awesome.min.css')) }}
        @endlangrtl
        @yield('after-styles')
        <!-- Scripts -->
        <script>
            window.Laravel = @php echo json_encode([ 'csrfToken' => csrf_token(), ]) @endphp
        </script>
        <?php
        if (!empty($google_analytics)) {
            echo $google_analytics;
        }
        ?>
    </head>
    <body id="app-layout">
        <div id="app">
            <div class="container">
                <div class="col-6 mx-auto mt-3">
                    @include('includes.partials.messages')
                </div>
                </section>
                @yield('content')
            </div><!--#app-->
        </div><!--#app-->
        <script type="application/javascript" src="{{ url('js/frontend/app.js')}}"></script>
    </body>
</html>
<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Prologue\Alerts\Facades\Alert;
use Spatie\Newsletter\Newsletter;

class NewsletterController extends Controller {

    public function create() {
        return view('frontend.newsletter.index');
    }

    public function store(Request $request) {
        $v = Validator::make($request->all(), [
                    'email' => 'required|email',
        ]);
        if ($v->fails()) {
            Alert::error('Validation Error', 'Please provide the accurate details.');
            return redirect()->back()->withErrors($v->errors());
        }
        if (!Newsletter::isSubscribed($request->email)) {
            Newsletter::subscribePending($request->email);
            return redirect()->back()->with('success', 'Thanks For Subscribe');
        }
        Alert::error('Failure', 'Sorry! You have already subscribed ');
        return redirect()->back();
    }

}

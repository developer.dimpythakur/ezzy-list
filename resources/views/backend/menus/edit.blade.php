@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.menus.management') . ' | ' . trans('labels.backend.menus.edit'))

@section('page-header')
<h1>{{ trans('labels.backend.menus.management') }}</h1>
@endsection

@php
$breadcrumbs = [
'Dashboard' => url(config('backend.route_prefix'), 'dashboard'),
trans('menus.backend.menus.all') => route('admin.menus.index'),
trans('menus.backend.menus.create') => false,
];
@endphp


@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.menus.management') }}</span>
    </h2>
</div>
@endsection
@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::model($menu, ['route' => ['admin.menus.update', $menu], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role', 'files' => true]) }}
        <div class="card">
            <div class="card-body row">
                @include("backend.menus.form")
            </div><!-- /.card-body -->
        </div><!--card-->

        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=update_and_back">Update</span>
                </button>
            </div>
            <a href="{{ route('admin.menus.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection

@include("backend.menus.partials.modal")









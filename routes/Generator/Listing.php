<?php

/**
 * Listings
 *
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {

    Route::group(['namespace' => 'Listing'], function () {
        Route::resource('listings', 'ListingController');
        //For Datatable
        Route::post('listings/get', 'ListingTableController')->name('listings.get');
    });
});

<!-- Copyright bar -->
<div class="et-copyright-bar text-white pt-3 pb-3">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-md-0 mb-2">
                <p class="mb-0 text-white">© 2020 Ezzy Listing. All rights reserved. </p>
            </div>
            <div class="col-md-6 text-lg-right">
                <ul class="nostyle list-inline text-capitalize">
                    <li class="pl-2 pr-2">
                        <a href="{{ route('frontend.blog')}}" class="text-decoration-none text-white">Blog</a>
                    </li>
                    <li class="pl-2 pr-2">
                        <a href="javascript:void(0)" class="text-decoration-none text-white">privacy policy</a>
                    </li>
                    <li class="pl-2 pr-2">
                        <a href="javascript:void(0)" class="text-decoration-none text-white">terms of use</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- /Copyright bar -->
@if ($paginator->hasPages())
<nav class="navigation pagination m-auto pb-5">
    <div class="nav-links">
        <!-- Previous Listing Link -->
        @if ($paginator->onFirstPage())
        <span class="page-numbers disabled">&laquo;</span>
        @else
        <a class="prev page-numbers" href="{{ $paginator->previousPageUrl() }}">
            <i class="las la-chevron-left"></i>
            <span class="nav-prev-text">Newer posts</span>
        </a>
        @endif

        <!-- Pagination Elements -->
        @foreach ($elements as $element)
        <!-- "Three Dots" Separator -->
        @if (is_string($element))
        <span class="page-numbers disabled">{{ $element }}</span>
        @endif
        <!-- Array Of Links -->
        @if (is_array($element))
        @foreach ($element as $page => $url)
        @if ($page == $paginator->currentPage())
        <span aria-current="page" class="page-numbers current">{{ $page }}</span>
        @else
        <a class="page-numbers" href="{{ $url }}">{{ $page }}</a>
        @endif
        @endforeach
        @endif
        @endforeach

        <!-- Next Listing Link -->
        @if ($paginator->hasMorePages())
        <a class="next page-numbers" href="{{ $paginator->nextPageUrl() }}">
            <span class="nav-next-text">Older posts</span>
            <i class="las la-chevron-right"></i>
        </a>
        @else
        <span class="page-numbers disabled">&raquo;</span>
        @endif
    </div>
</nav>
@endif

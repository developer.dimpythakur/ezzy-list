@extends('frontend.layouts.app')
@section('content')
@include('frontend.blog.section.header')
<section class="et-content-body">
    <div class="container">
        <div class="row">
            <!-- Page Content -->
            <div class="col-lg-8">
                <div class="blog-content">
                    <div class="blog-thumbnail position-relative">
                        @include('frontend.blog.post.section.post-featured-image')
                        @include('frontend.blog.post.section.post-categories')
                    </div>
                    @include('frontend.blog.post.section.post-meta')
                    <h5>  {{ $post->name }}</h5>
                    <p> {{ strip_tags($post->content) }}</p>
                    <!-- Post Slider -->
                    <!-- /Post Slider -->
                    <hr>
                    <!-- Tags and Share -->
                    @include('frontend.blog.post.section.post-tags')
                    <!-- /Tags and Share -->
                    <hr>
                    <!-- About Author -->
                    @include('frontend.blog.post.section.post-author')
                    <!-- /About Author -->
                    <hr>
                    <!-- Next Previous Post -->
                    @include('frontend.blog.post.section.post-nav-links')
                    <!-- /Next Previous Post -->
                    <hr>
                    <!-- Related Posts -->
                    @include('frontend.blog.post.related.posts')
                    <!-- /Related Posts -->
                    <hr>
                    <!-- Comments -->
                    <!--include('frontend.blog.post.section.comment.comments')-->
                    <!-- /Comments -->
                    <!-- Comment Form -->
                    @include('frontend.blog.post.section.comment.form')
                    <!-- /Comment Form -->
                </div>
            </div>
            <!-- /Page Content -->
            <!-- Sidebar -->
            <aside class="col-lg-4">
                @include('frontend.blog.post.section.sidebar')
            </aside>
            <!-- /Sidebar -->
        </div>
</section>
<!-- Newsletter Subscription -->
@include('frontend.listing.components.listing-newsletter.newsletter')
<!-- /Newsletter Subscription -->
@endsection



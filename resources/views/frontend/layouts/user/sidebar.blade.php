<div id="sidebar-user" class="{{ config('backend.sidebar_class') }}">
    <nav class="sidebar-nav overflow-hidden">
        <div class="sidebar sidebar-pills bg-light">
            <nav class="sidebar-nav overflow-hidden ps">
                <div class="user-profile media align-items-center mb-6">
                    <div class="image mr-3">
                        <img src="{{ access()->user()->picture}}" alt="{{ access()->user()->name}}" class="rounded-circle">
                    </div>
                    <div class="media-body lh-14">
                        <span class="d-block font-size-md title">Howdy,</span>
                        <span class="mb-0 h5 title-user">
                            {{ substr(access()->user()->first_name,0,10)}} 
                        </span>
                    </div>
                </div>
                <ul class="nav">
                    <li class="nav-title">Dashboard</li> 
                    <li class="nav-item  ">
                        <a class="nav-link" href="{{ route('frontend.user.dashboard')}}">
                            <i class="la la-home"></i> Dashboard
                        </a>
                    </li>
                    <li class="nav-title">My Listings</li> 
                    <li data-url="listings" class="nav-item parent nav-dropdown">                
                        <a class="nav-link nav-dropdown-toggle" href="javascript:void(0)">
                            <i class="la la-commenting"></i> Listings
                        </a>
                        <ul class="nav-dropdown-items">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('frontend.user.listings-active')}}">
                                    <i class="las la-check-circle"></i> Active
                                </a>
                            </li>
                            <li class="nav-item open">
                                <a class="nav-link" href="{{ route('frontend.user.listings-pending')}}">
                                    <i class="las la-yin-yang"></i> Pending
                                </a>
                            </li>
                            <li class="nav-item open">
                                <a class="nav-link" href="{{ route('frontend.user.listings-expired')}}">
                                    <i class="las la-deaf"></i> Expired
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item  ">
                        <a class="nav-link" href="{{ route('frontend.user.listings.index')}}">
                            <i class="la la-list-alt"></i> My Listings
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a class="nav-link" href="{{ route('frontend.user.listings.create')}}">
                            <i class="la la-plus-circle"></i> Add New
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a class="nav-link" href="{{ route('frontend.user.listings-yelp')}}">
                            <i class="la la-yelp"></i> Yelp
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a class="nav-link" href="{{ route('frontend.user.bookmark')}}">
                            <i class="la la-bookmark-o"></i> Saved
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a class="nav-link" href="{{ route('frontend.user.reviews')}}">
                            <i class="la la-star-o"></i> Reviews
                        </a>
                    </li>
                    <li class="nav-title">Account</li> 
                    <li class="nav-item  ">
                        <a class="nav-link" href="{{ route('frontend.user.account')}}">
                            <i class="la la-user-alt"></i> Profile
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a class="nav-link" href="{{route('frontend.auth.logout')}}">
                            <i class="la la-power-off"></i> Logout
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a class="nav-link" id="subscription" href="javascript:void(0)">
                            <i class="la la-power-off"></i> Subscription
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- /.sidebar -->
        </div>
    </nav>
    <!-- /.sidebar -->
</div>

@push('before_scripts')
<script type="text/javascript">
    /* Recover sidebar state */
    if (Boolean(sessionStorage.getItem('sidebar-collapsed'))) {
        var body = document.getElementsByTagName('body')[0];
        body.className = body.className.replace('sidebar-lg-show', '');
    }

    /* Store sidebar state */
    var navbarToggler = document.getElementsByClassName("navbar-toggler");
    for (var i = 0; i < navbarToggler.length; i++) {
        navbarToggler[i].addEventListener('click', function (event) {
            event.preventDefault();
            if (Boolean(sessionStorage.getItem('sidebar-collapsed'))) {
                sessionStorage.setItem('sidebar-collapsed', '');
            } else {
                sessionStorage.setItem('sidebar-collapsed', '1');
            }
        });
    }
</script>
@endpush

@push('after_scripts')
<script>
    // Set active state on menu element
    var full_url = "{{ Request::fullUrl() }}";
    var $navLinks = $(".sidebar-nav li a");

    // First look for an exact match including the search string
    var $curentPageLink = $navLinks.filter(
            function () {
                return $(this).attr('href') === full_url;
            }
    );

    // If not found, look for the link that starts with the url
    if (!$curentPageLink.length > 0) {
        $curentPageLink = $navLinks.filter(function () {
            if ($(this).attr('href').startsWith(full_url)) {
                return true;
            }

            if (full_url.startsWith($(this).attr('href'))) {
                return true;
            }

            return false;
        });
    }

    // for the found links that can be considered current, make sure 
    // - the parent item is open
    $curentPageLink.parents('li').addClass('open');
    // - the actual element is active
    $curentPageLink.each(function () {
        $(this).addClass('active');
    });
</script>


<script>
//    check if url contains the string
    function checkUri(urlPrefix) {
        return (window.location.href.indexOf(urlPrefix) > -1) ? true : false;
    }
    jQuery(document).ready(function (e) {
        $("#bottom_buttons").insertBefore($('#crudTable_wrapper .row:last-child'));
    });
</script>
@endpush

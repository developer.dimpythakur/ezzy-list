@extends('frontend.layouts.app')
@section('after-styles')
{{ Html::style(('packages/owl.carousel/dist/assets/owl.carousel.min.css')) }}
{{ Html::style(('packages/owl.carousel/dist/assets/owl.theme.default.min.css')) }}
{{ Html::style(('css/frontend/map/listing-map-manager.css')) }}
<style>
    .swal2-container .swal2-content .nice-select{ display: none;visibility: hidden}
</style>
@endsection

@section('content')
<!-- Explore by city -->
@include('frontend.listing.components.listing-single.banner')
<!-- Explore by city -->
<section class="w-100 float-left">
    <!-- Listing Intro -->
    <div class="et-intro style-2 pt-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-sm-12 text-center text-md-left">
                    <div class="listing-logo bg-white rounded-pill d-inline-block overflow-hidden position-relative shadow ">
                        @if($listing->banner)
                        <img src="{{$listing->banner }}"
                             alt="{{ $listing->name }}"
                             class="img-fluid w-100 h-100">
                        @else
                        @endif
                    </div>
                    <div class="d-inline-block pl-3">
                        @if(count($listing->categories))
                        <p class="mt-1 mb-2">
                            @foreach($listing->categories as $category)
                            {{ $category->name }}
                            @endforeach
                        </p>
                        @endif
                        <h3 class="listing-name">
                            {{ $listing->name }}
                            <span class="varified-listing icon-checked1 text-white" data-toggle="tooltip"
                                  data-placement="right" title="Verified Business">
                            </span>
                        </h3>
                        <p> {{ $listing->tagline }}</p>
                    </div>
                </div>
                <!-- Listing Meta -->
                @include('frontend.listing.components.listing-single.listing-meta')
                <!-- Listing Meta -->
            </div>
        </div>
    </div>
    <!-- /Listing Intro -->

    <!-- Tabs -->
    <div class="et-content-tabs">
        <div class="container">

            <ul class="nav nav-pills mb-5 mt-3 bg-light text-center" id="pills-tab-2" role="tablist">
                <li class="nav-item">
                    <a class="nav-link pl-3 pr-3 active" id="pills-all-tab" data-toggle="pill" href="#pills-all"
                       role="tab" aria-controls="pills-all" aria-selected="true">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link pl-3 pr-3" id="pills-description-tab" data-toggle="pill"
                       href="#pills-description" role="tab" aria-controls="pills-description" aria-selected="false">
                        Description
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link pl-3 pr-3" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                       role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link pl-3 pr-3" id="pills-features-tab" data-toggle="pill" href="#pills-features"
                       role="tab" aria-controls="pills-features" aria-selected="false">Features</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link pl-3 pr-3" id="pills-gallery-tab" data-toggle="pill" href="#pills-gallery"
                       role="tab" aria-controls="pills-gallery" aria-selected="false">Gallery
                        <span class="badge text-white position-absolute rounded-pill ml-2">
                            @if($listing->gallery && count($listing->gallery))
                            {{ ( $listing->gallery->count()) }}
                            @else
                            0
                            @endif
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link pl-3 pr-3" id="pills-faqs-tab" data-toggle="pill" href="#pills-faqs"
                       role="tab" aria-controls="pills-faqs" aria-selected="false">FAQs
                        <span class="badge text-white position-absolute rounded-pill ml-2">
                            @if($listing->faqs && count($listing->faqs))
                            {{ ( $listing->faqs->count()) }}
                            @else
                            0
                            @endif
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link pl-3 pr-3" id="pills-reviews-tab" data-toggle="pill" href="#pills-reviews"
                       role="tab" aria-controls="pills-reviews" aria-selected="false">Reviews
                        <span class="badge text-white position-absolute rounded-pill ml-2">
                            {{ $listing->countRating() }}
                        </span>
                    </a>
                </li>
                @if($listing->store && count($listing->store))
                <li class="nav-item">
                    <a class="nav-link pl-3 pr-3" id="pills-store-tab" data-toggle="pill"
                       href="#pills-store" role="tab" aria-controls="pills-store"
                       aria-selected="false">
                        Store <span class="badge text-white position-absolute rounded-pill ml-2">8</span>
                    </a>
                </li>
                @endif
            </ul>
            @if ($errors->any())
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger mb-2 mt-2">{{$error}}</div>
            @endforeach
            @endif
            <div class="tab-content mb-5" id="pills-tabContent-2">
                <!-- All -->
                <div class="tab-pane fade show active" id="pills-all" role="tabpanel"
                     aria-labelledby="pills-all-tab">
                    @include('frontend.listing.components.listing-single.all')
                </div>
                <!-- Description Tab -->
                <div class="tab-pane fade" id="pills-description" role="tabpanel"
                     aria-labelledby="pills-description-tab">
                    <!-- Description -->
                    @include('frontend.listing.components.listing-single.description')
                </div>
                <!-- /Description Tab-->
                <!-- Contact tab -->
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    @include('frontend.listing.components.listing-single.contact')
                </div>
                <!--/ Contact tab -->
                <!-- Features tab -->
                <div class="tab-pane fade" id="pills-features" role="tabpanel" aria-labelledby="pills-features-tab">
                    @include('frontend.listing.components.listing-single.features')
                </div>
                <!-- / Features tab -->
                <!--  Gallery tab -->
                <div class="tab-pane fade" id="pills-gallery" role="tabpanel" aria-labelledby="pills-gallery-tab">
                    @include('frontend.listing.components.listing-single.gallery')
                </div>
                <!-- / Gallery tab -->
                <!--  Faq tab -->
                <div class="tab-pane fade" id="pills-faqs" role="tabpanel" aria-labelledby="pills-faqs-tab">
                    @include('frontend.listing.components.listing-single.faqs')
                </div>
                <!-- / Faq tab -->
                <!-- Reviews tab -->
                <div class="tab-pane fade" id="pills-reviews" role="tabpanel" aria-labelledby="pills-reviews-tab">
                    @include('frontend.listing.components.listing-single.reviews')
                </div>
                <!-- / Reviews tab -->
            </div>
        </div>
    </div>
</section>

<!-- Newsletter Subscription -->
@include('frontend.listing.components.listing-newsletter.newsletter')
<!-- /Newsletter Subscription -->
@endsection

@section('after-scripts')
{{ Html::script(('packages/owl.carousel/dist/owl.carousel.min.js')) }}
<!-- Map API -->
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAmrxCFuWyNUlMh6wLitVLI3pF7j_HqAFY&#038;libraries=places&#038;ver=1.1'></script>
<!-- Custom Js -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="{{ asset('listing/mapify.js') }}"></script>
<script>
jQuery(function ($) {

    if ('1' === EZZY_MAP.enable_map) {
        var $input = jQuery('#contact-map');
        if ($input.length) {
            $input.mapify({mapify: "#listing-map", mapHeight: "300px", startGeoLat: $input.data("lat"), startGeoLng: $input.data("long")});
        }
    }
});
</script>

<script>
    jQuery(document).ready(function () {
        //-------------------------------
        // Listing Detail Slider
        //-------------------------------
        if (jQuery(".owl-carousel.listing-slider").length) {
            jQuery(".owl-carousel.listing-slider").owlCarousel({
                loop: true,
                autoplay: true,
                items: 4,
                margin: 0,
                nav: false,
                dots: false,
                navContainer: '.nav-container-listing-slider',
                navText: ["<i class='las la-angle-left'>", "<i class='las la-angle-right'>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    576: {
                        items: 2
                    },
                    1000: {
                        items: 4
                    }
                }
            });
        }


        //-------------------------------
        // Blog Detail Slider Home
        //-------------------------------

        if (jQuery(".owl-carousel.post-slider-tab").length) {
            jQuery(".owl-carousel.post-slider-tab").owlCarousel({
                loop: true,
                autoplay: true,
                items: 4,
                margin: 10,
                nav: true,
                dots: false,
                navContainer: '.nav-container-post-slider',
                navText: ["<i class='las la-angle-left'>", "<i class='las la-angle-right'>"],
                responsive: {
                    0: {
                        items: 1
                    },
                    576: {
                        items: 2
                    },
                    1000: {
                        items: 4,
                    }
                }
            });
        }

        jQuery('.collapse').collapse()

    });
</script>

<script>
    $(document).ready(function () {
        /* 1. Visualizing things on Hover - See next part for action on click */
        jQuery(document).on('mouseover', '.listing-review .star-rating li', function () {


            var onStar = parseInt(jQuery(this).attr('data-value'), 10); // The star currently mouse on
            console.log(onStar);

            // Now highlight all the stars that's not after the current hovered star
            $(this).parent().children('li.star').each(function (e) {
                if (e < onStar) {
                    $(this).addClass('hover');
                } else {
                    $(this).removeClass('hover');
                }
            });

            jQuery(this).parents('.listing-review').prev().html(onStar);
            jQuery(this).parent().prev().val(onStar);

        }).on('mouseout', function () {
            $(this).parent().children('li.star').each(function (e) {
                $(this).removeClass('hover');
            });
        });


        /* 2. Action to perform on click */
        $('.listing-review .star-rating li').on('click', function () {
            var onStar = parseInt($(this).data('value'), 10); // The star currently selected
            var stars = $(this).parent().children('li.star');

            for (i = 0; i < stars.length; i++) {
                $(stars[i]).removeClass('selected');
            }

            for (i = 0; i < onStar; i++) {
                $(stars[i]).addClass('selected');
            }

            // JUST RESPONSE (Not needed)
            var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
            var msg = "";
            if (ratingValue > 1) {
                msg = "Thanks! You rated this " + ratingValue + " stars.";
            } else {
                msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
            }
            responseMessage(msg);

        });


    });

    function responseMessage(msg) {
        $('.success-box').fadeIn(200);
        $('.success-box div.text-message').html("<span>" + msg + "</span>");
    }

    function addStars($el, limit = 5) {
        const titles = ['Poor', 'Fair', 'Good', 'Excellent', 'Wow'];
        for (var i = 1; i <= 5; i++) {
            jQuery($el).append(`<li class='star' title='${titles[i + 1]}' data-value='${i}'><i class="las la-star align-middle"></i></li>`);
    }
    }

    jQuery(document).ready(function () {
        addStars('.listing-review .star-rating', 5);
    });

</script>
@endsection
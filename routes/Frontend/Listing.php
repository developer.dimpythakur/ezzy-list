<?php

/**
 * Listings Controller
 * All route names are prefixed with 'frontend.'.
 */
/*
 * Show pages
 */
Route::get('listings', 'Listing\ListingController@index')->name('listing.all');
Route::get('listing/{slug}', 'Listing\ListingController@show')->name('listing.show');
//bookmark
Route::post('listing-bookmark', 'Listing\ListingController@bookmark')->name('listing-bookmark');
//Listing Categories
Route::get('listing-categories', 'Listing\CategoryController@index')->name('listing-categories');
Route::get('listing-category/{slug}', 'Listing\CategoryController@show')->name('listing-category');
//Listing Locations
Route::get('locations', 'Listing\LocationController@index')->name('locations');
Route::get('search-results', 'SearchController@search')->name('search.result');
//REVIEWS
Route::get('listing/reviews/{listingId}', 'Listing\ReviewController@index')->name('reviews');
Route::post('reviews', 'Listing\ReviewController@store')->name('add-review');
Route::post('reviews/{reviewId}/{type}', 'Listing\ReviewController@update');
// Contact Us
Route::get('contact-us', 'ContactController@getContact')->name('contact.show');
Route::post('contact-us', 'ContactController@saveContact')->name('contact.save');


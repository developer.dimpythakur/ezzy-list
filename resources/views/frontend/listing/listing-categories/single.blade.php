@extends('frontend.layouts.app')
@section('content')

@include('frontend.listing.listing-categories.section.category-header')
<section class="et-categrory-single bg-light events map-home position-relative">
    <div class="container">
        @if(isset($listings) && count($listings))
        <div class="heading mb-3 text-center d-block full-width">
            <h3 class="text-capitalize mb-5">
                {{isset($listings)?$listings->total():0  }} listings found in
                <span class="badge-primary rounded p-1"> {{$category->name}}</span>
                category
            </h3>
        </div>
        <div class="row">
            @foreach($listings as $listing)
            <div class="col-md-4 my-4 listing-category-single style-two">
                @include('frontend.listing.components.listing-style.style-two')
            </div>
            @endforeach
        </div>
        <!-- Pagination -->
        {{ $listings->links() }}
        <!-- /Pagination -->
        @else
        <div class="row py-4 my-4">
            <div class="col-md-7 mx-auto">
                <div class="section-heading text-center d-block full-width">
                    <h3 class="text-capitalize text-dark">
                        No new listing found under  <span class="text-uppercase text-primary">{{$category->name}}</span> category
                    </h3>
                    <p>Listings are in reviews before they appear on the website.</p>
                </div>
            </div>
        </div>
        @endif
    </div>
</section>
<!-- Newsletter Subscription -->
@include('frontend.listing.components.listing-newsletter.newsletter')
<!-- /Newsletter Subscription -->
@endsection

@section('after-scripts')

@endsection

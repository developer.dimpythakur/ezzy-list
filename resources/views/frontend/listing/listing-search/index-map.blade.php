@extends('frontend.layouts.app')
@section('after-styles')
{{ Html::style(('css/frontend/map/listing-map-manager.css')) }}
@endsection
@section('content')
<section class="et-search-result-wrapper search-result-with-map list search-map listing-search">
    <div class="container-fluid pt-xs">
        <div class="row">
            <div class="col-lg-6">
                <!-- List Results -->
                <div class="listing-list-view et-top-rated-business list et-top-rated list">
                    <div class="row justify-content-md-center p-2">
                        <div class="col-lg-12 mb-3">
                            @include('frontend.listing.components.listing-search.search-listing')
                        </div>
                        <div class="col-lg-12">
                            @if(isset($searchResults))
                            @if ($searchResults-> isEmpty())
                            <section id="listings-not-found" class="margin-bottom-50">
                                <h2>Nothing found</h2>
                                <p>We’re sorry but we do not have any listings matching your search, try to
                                    change you search settings</p>
                            </section>
                            @else
                            <?php
                            $searchterm = (collect($searchterm)->filter(function($term) {
                                        return $term;
                                    }));
                            if (count($searchterm) && $searchResults->total()):
                                ?>
                                <h6>We found  <span class="text-capitalize text-primary">
                                        {{ $searchResults->total() }} 
                                    </span>
                                    listings 
                                    @if($searchterm['location'])
                                    <b> <small>location</small> 
                                        <span  class="text-capitalize text-primary">
                                            "{{ $searchterm['location'] }}"
                                        </span>
                                    </b>
                                    @endif
                                    @if(isset($searchterm['category']))
                                    <b>  <small>category</small>   
                                        <span  class="text-capitalize text-primary">
                                            "{{ $searchterm['category'] }}"
                                        </span>
                                    </b>
                                    @endif
                                </h6>
                            <?php else:
                                ?>
                                <h6>We found <span class="text-capitalize text-primary">
                                        {{ $searchResults->total() }} 
                                    </span> listings.</h6>
                            <?php endif; ?>
                            <hr/>
                            <section class="ezzy-listings listing-search style-one pt-1 pb-1">
                                @foreach($searchResults as $type => $listing)
                                @include('frontend.listing.components.listing-style.style-one')
                                @endforeach
                            </section>
                            @endif
                            <section class="w-100 mt-3 py-1">
                                <!-- Pagination -->
                                {{ $searchResults->links() }}
                                <!-- /Pagination -->
                            </section>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /List Results -->
            </div>
            <div class="col-lg-6 position-relative map-right" style="display: none">
                @include('frontend.listing.components.listing-map.map')
            </div>
        </div>
    </div>
</section>
@endsection

@section('after-scripts')
<!-- Map API -->
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAmrxCFuWyNUlMh6wLitVLI3pF7j_HqAFY&#038;libraries=places&#038;ver=1.1'></script>
<!-- Custom Js -->
<script src="{{ asset('listing/easy-listing-maps.js') }}"></script>
@endsection

<!-- Listing Detail Header -->
<section class="et-listing-detail-header style-2 text-center text-capitalize position-relative">
    <!-- Slides -->
    <div class="owl-carousel listing-slider owl-loaded owl-drag">
        @if($listing->gallery)
            @foreach($listing->gallery as $gallery)
                <div class="item">
                <img src="{{ $gallery->url }}" alt="{{ $listing->name}}">
                </div>
            @endforeach
        @endif
    </div>
    <!-- /Slides -->
    <div class="container">
        <div class="row position-absolute fixed-bottom m-0 align-items-center pb-3">
            <!-- Action buttons -->
            <div class="actions-2 col-md-9 col-sm-12 text-center text-md-left">
                <a href="#"
                   class="transition-1 text-capitalize text-white"><i
                            class="icon-bookmark-black-shape text-primary"></i>&nbsp; Save</a>
                <a href="#" class="transition-1 text-capitalize text-white"><i class="icon-flag text-primary"></i>&nbsp;
                    Report</a>
                <a href="#" data-toggle="modal" data-target="#claimListing"
                   class="transition-1 text-capitalize text-white"><i class="icon-call-to-action text-primary"></i>&nbsp;
                    Claim</a>
                <span class="transition-1 text-capitalize text-white position-relative social-share">
						<i class="icon-share text-primary"></i>&nbsp; Share
						<span class="social-icons transition-1 shadow p-2 rounded-pill position-absolute bg-white">
						<a href="{{ $listing->getShareUrl('facebook') }}">
						<i class="lab la-facebook" aria-hidden="true"></i>
						</a>
						<a href="{{ $listing->getShareUrl('google') }}">
						<i class="lab la-google" aria-hidden="true"></i>
						</a>
						<a href="{{ $listing->getShareUrl('instagram') }}">
						<i class="lab la-instagram" aria-hidden="true"></i>
						</a>
						<a href="{{ $listing->getShareUrl('pinterest') }}">
						<i class="lab la-pinterest" aria-hidden="true"></i>
						</a>
						<a href="{{ $listing->getShareUrl('linkedin') }}">
						<i class="lab la-linkedin" aria-hidden="true"></i>
						</a>
						</span>
				</span>
            </div>
            <!-- /Action buttons -->


        @if($listing->timings && count($listing->timings))
            <!-- Listing Meta -->
                <div class="listing-meta mt-3 mt-md-0 mb-3 mb-md-0 col-md-3 col-sm-12 text-center text-md-right">
                    @if ($listing->timings && !is_null($listing->timings))
                        @if(timingStatus($listing->timings) && timingStatus($listing->timings) == 'open')
                            <span class="meta-info business-time rounded-pill bg-primary text-white pl-2 pr-2"
                                  data-toggle="tooltip"
                                  data-placement="top"
                                  title="Open Now"> Open Now
                    </span>
                        @else
                            <span class="meta-info business-time rounded-pill bg-primary text-white pl-2 pr-2"
                                  data-toggle="tooltip"
                                  data-placement="top"
                                  title="Currently Closed"> Closed
                    </span>
                        @endif
                    @else
                        <span class="meta-info business-time rounded-pill bg-primary text-white pl-2 pr-2"
                              data-toggle="tooltip"
                              data-placement="top"
                              title="Currently Closed"> Closed
                    </span>
                    @endif


                    <span class="timing-dropdown position-relative">
						<span class="text-white pl-3 pointer toggle-timing">
							<i class="icon-wall-clock"></i> Timing
							<i class="las la-caret-down"></i>
						</span>
						<span class="dropdown-data p-3 text-white text-left position-absolute">
                          @foreach($listing->timings as $timing)
                                <label class="text-white w-100">{{ ucfirst($timing->name) }}
									<span class="float-right">{{ $timing->open_time }} - {{ $timing->close_time  }}</span>
					        	</label>
                            @endforeach
						</span>
						</span>
                </div>
        @endif
        <!-- /Listing Meta -->
        </div>
    </div>
</section>
<!-- /Listing Detail Header -->
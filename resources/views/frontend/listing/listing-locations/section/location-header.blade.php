<section class="et-page-header position-relative text-center"
         style="background-image: url({{ Storage::url('images/listing/regions/'.$region->image) }})">
    <div class="header-content position-relative text-white">
        <ul class="nostyle list-inline breadcrumb-links mb-1">
            <li><a href="{{ URL::to('/') }}">Home</a>
            </li>
            <li class="current">
                <a href="{{ route('frontend.locations') }}">
                    Locations
                </a>
            </li>
        </ul>
        <h1 class="page-title d-inline-flex align-items-center m-0 position-relative">{{ $region->name }}</h1>
        <p class="mb-0 mt-1 text-white">{{ $region->name}}</p>
    </div>
</section>
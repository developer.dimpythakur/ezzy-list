
@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.listings.management') . ' | ' . trans('labels.backend.blogs.create'))

@section('page-header')
<h1>{{ trans('labels.backend.listings.management') }}</h1>
@endsection

@php
$breadcrumbs = [
'Dashboard' => url(config('backend.route_prefix'), 'dashboard'),
trans('listings.backend.listings.all') => route('admin.listings.index'),
trans('listings.backend.listings.create') => false,
];
@endphp


@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.listings.management') }}</span>
    </h2>
</div>
@endsection

@section("after_styles")
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
<style>
    .normal-labels label{
        font-weight: 400
    }
    /*
     * File
     */
    .file input:focus ~ .file-custom {
        box-shadow: 0 0 0 .075rem #fff, 0 0 0 .2rem #0074d9;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu .collapse:not(.show){
        display: block !important
    }
    .select2-container .select2-selection--single,
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        height: 40px;
        line-height: 40px;
    }


    /*FAKE FILE*/
    .fake-file-upload {
        border: 2px dashed #ccc;
    }
    .fake-file-upload input {
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
    }
    .fake-file-upload:hover{
        border-color: #e53935 !important;
    }

</style>
@endsection
@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::model($listing, ['route' => ['admin.listings.update', $listing], 'class' => 'form-horizontal','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-listing']) }}
        @include("backend.listing.listing.form")

        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=update_and_back">Update</span>
                </button>
            </div>
            <a href="{{ route('admin.listings.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>

@endsection
@section("after_scripts")


<script>
    jQuery(document).ready(function () {

        //-------------------------------
        // Featured Images Upload
        //-------------------------------
        $('#single_img_upload').change(function (event) {
            var imageHTML = '';

            $(event.target.files).each(function (index) {
                var tmppath = URL.createObjectURL(event.target.files[index]);
                imageHTML += '<li class="m-1" id="featured-img-' + index + '"><img class="img-fluid" src="' + tmppath + '"><i data-targetimg="' + index + '" class="remove-featured-img p-1 shadow pointer btn btn-primary border-0 la la-trash"></i></li>';
            });
            $('#uploaded_feat_img').html(imageHTML);
            event.preventDefault();

        });
        //-------------------------------
        // Remove Featured Image
        //-------------------------------
        $(document).on('click', '.remove-featured-img', function (e) {
            e.preventDefault();
            $(this).closest('li').remove();
        });


//-------------------------------
// Gallery Images Upload
//-------------------------------
        jQuery('#gallery_img_upload').change(function (event) {
            var imageHTML = jQuery("#gallery_images_list").html();
            let col = 1;
            jQuery(event.target.files).each(function (index) {
                let className = 'col-3';
                var tmppath = URL.createObjectURL(event.target.files[index]);
                imageHTML += `<li class="mt-2 ${className}" id="gallery-img-${index}">
                          <img class="img-fluid" src="${tmppath}">
                           <i data-targetimg="${index}" class="remove-gallery-img p-1 shadow pointer btn btn-outline-danger border-0 las las-trash lar la-trash-alt"></i>
                           </li>`;
                col++;
            });
            jQuery('#gallery_images_list').html(imageHTML);
            event.preventDefault();
        });

//-------------------------------
// Remove Gallery Images
//-------------------------------
        $(document).on('click', '.remove-gallery-img', function (e) {
            e.preventDefault();
            $(this).closest('li').remove();
        });
//-------------------------------
// Add FAQs
//-------------------------------

        if (jQuery("#faqs").length) {
            jQuery(document).on('click', ".add-faq", function () {
                var $faqQuestionInput = jQuery("input[id='faq_question']");
                var faqQuestion = $faqQuestionInput.val();

                var $faqAnswerInput = jQuery("textarea[id='faq_answer']");
                var faqAnswer = $faqAnswerInput.val();

                if ((typeof faqQuestion !== "undefined") && faqQuestion !== '' &&
                        (typeof faqAnswer !== "undefined") && faqAnswer !== '') {
                    let $targetTable = jQuery(".selected-faqs");
                    let $question = `<input type="hidden" value="${faqQuestion}" name="faq[question][]">`;
                    let $answer = `<input type="hidden" value="${faqAnswer}" name="faq[answer][]">`;
                    let $tableRow = `<tr><td>${$question} ${faqQuestion}</td><td>${$answer} ${faqAnswer}</td><td class="text-center"><i class='las la-trash remove pointer text-danger'></i></td></tr>`;
                    $targetTable.show();
                    jQuery(".selected-faqs table tbody").append($tableRow);
                } else {
                    alert("Please fill both the fields");
                }
                $faqQuestionInput.val('');
                $faqAnswerInput.val('');

            });

            jQuery(document).on("click", ".selected-faqs .remove", function (u) {
                jQuery(this).closest("tr").remove();
                if (jQuery(".selected-faqs tbody > tr").length < 1) {
                    jQuery(".selected-faqs").hide();
                }
            });
        }
    });

</script>


<!-- include select2 js-->
<script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
@if (app()->getLocale() !== 'en')
<script src="{{ asset('packages/select2/dist/js/i18n/' . app()->getLocale() . '.js') }}"></script>
@endif
<script src="{{ asset('packages/tinymce/tinymce.min.js') }}"></script>
<script>

    $(document).ready(function () {
//        select2
        $('.select2_multiple').select2();
        //tinymce
        var $element = "textarea.form-control.tinymce";
        tinymce.init(jQuery($element).data('options'));

    });

</script>
@endsection

<?php

return array(
    array('id' => '1', 'name' => 'Auto Listings', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:49:22', 'updated_at' => '2020-08-09 05:49:22', 'deleted_at' => NULL),
    array('id' => '2', 'name' => 'Hotels', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:49:32', 'updated_at' => '2020-08-09 05:49:32', 'deleted_at' => NULL),
    array('id' => '3', 'name' => 'Restaurants', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:49:38', 'updated_at' => '2020-08-09 05:49:38', 'deleted_at' => NULL),
    array('id' => '4', 'name' => 'Food', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:49:45', 'updated_at' => '2020-08-09 05:49:45', 'deleted_at' => NULL),
    array('id' => '5', 'name' => 'Holidays', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:49:53', 'updated_at' => '2020-08-09 05:49:53', 'deleted_at' => NULL),
    array('id' => '6', 'name' => 'Fun', 'status' => '1', 'created_by' => '1', 'updated_by' => NULL, 'created_at' => '2020-08-09 05:50:00', 'updated_at' => '2020-08-09 05:50:00', 'deleted_at' => NULL)
);

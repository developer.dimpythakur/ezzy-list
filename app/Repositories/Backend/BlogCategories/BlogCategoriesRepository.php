<?php

namespace App\Repositories\Backend\BlogCategories;

use App\Events\Backend\BlogCategories\BlogCategoryCreated;
use App\Events\Backend\BlogCategories\BlogCategoryDeleted;
use App\Events\Backend\BlogCategories\BlogCategoryUpdated;
use App\Exceptions\GeneralException;
use App\Models\BlogCategories\BlogCategory;
use App\Repositories\BaseRepository;
use DB;
use Illuminate\Support\Facades\Storage;

/**
 * Class BlogCategoriesRepository.
 */
class BlogCategoriesRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = BlogCategory::class;

    /**
     * Upload path
     * @var type 
     */
    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = 'images' . DIRECTORY_SEPARATOR . 'blog-category' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.blog_categories.table') . '.created_by')
                        ->select([
                            config('module.blog_categories.table') . '.id',
                            config('module.blog_categories.table') . '.name',
                            config('module.blog_categories.table') . '.status',
                            config('module.blog_categories.table') . '.created_by',
                            config('module.blog_categories.table') . '.created_at',
                            config('access.users_table') . '.first_name as user_name',
        ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create(array $input) {
        if ($this->query()->where('name', $input['name'])->first()) {
            throw new GeneralException(trans('exceptions.backend.blogcategories.already_exists'));
        }

        DB::transaction(function () use ($input) {
            $input['status'] = isset($input['status']) ? 1 : 0;
            $input['image'] = $this->uploadImage($input);
            $input['created_by'] = access()->user()->id;
            if ($blogcategory = BlogCategory::create($input)) {
                event(new BlogCategoryCreated($blogcategory));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.blogcategories.create_error'));
        });
    }

    /**
     * @param \App\Models\BlogCategories\BlogCategory $blogcategory
     * @param  $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * return bool
     */
    public function update(BlogCategory $blogcategory, array $input) {
        if ($this->query()->where('name', $input['name'])->where('id', '!=', $blogcategory->id)->first()) {
            throw new GeneralException(trans('exceptions.backend.blogcategories.already_exists'));
        }

        DB::transaction(function () use ($blogcategory, $input) {
            $input['status'] = isset($input['status']) ? 1 : 0;
            $input['image'] = $this->uploadImage($input);
            $input['updated_by'] = access()->user()->id;
            if ($blogcategory->update($input)) {
                event(new BlogCategoryUpdated($blogcategory));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.blogcategories.update_error'));
        });
    }

    /**
     * @param \App\Models\BlogCategories\BlogCategory $blogcategory
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function delete(BlogCategory $blogcategory) {
        DB::transaction(function () use ($blogcategory) {
            if ($blogcategory->delete()) {
                event(new BlogCategoryDeleted($blogcategory));

                return true;
            }

            throw new GeneralException(trans('exceptions.backend.blogcategories.delete_error'));
        });
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        if (isset($input['image']) && !empty($input['image'])) {
            $avatar = $input['image'];
            $fileName = time() . $avatar->getClientOriginalName();
            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()));
            return $fileName;
        }
        return 'default-category.jpeg';
    }

}

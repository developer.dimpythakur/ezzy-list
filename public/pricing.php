Basic:

-1 listing

- 0 Shop/Product URL

- Price (free)

- Unlimited tags

- Business Description

- Location

- Category

- Social Media integration (1)

- Audio/Video (no)

- Business logo

- Gallary (3)


Standard:

-1 listing

- 4 Shop/Product URL

--- 4 product per row


- Price ($75)

- Unlimited tags

- Business Description

- Location

- Category

- Social Media integration (3)

- Audio/Video (1)

- Business logo

- Gallary (6)





Premium

- 3 listing (Business)

- 12 Shop/Product URL

---- 12 (4 per row)


- Price ($125)

- Unlimited tags

- Business Description

- Location

- Category

- Social Media integration (unlimited)

- Audio/Video (yes/1)

- Business logo

- Gallary (12)

- Blog premium adds 
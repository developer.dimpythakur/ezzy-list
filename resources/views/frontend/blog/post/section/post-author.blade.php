<div class="about-author pt-2 pb-2 d-flex align-items-center">
    <div class="author-image mr-3">
        @if(!empty($post->owner->picture))
        <img height="60" width="60" src="{{ $post->owner->picture }}"
             alt="{{ $post->owner->first_name }}"
             class="img-fluid rounded-pill shadow">
        @else
        <img height="60" width="60" src="{{ Storage::url('images/user/profile/user-avatar-default.png') }}" alt="{{ $post->owner->first_name }}"
             class="img-fluid rounded-pill shadow">
        @endif
    </div>
    <div class="author-data">
        <h6>{{ $post->owner->first_name }}  {{ $post->owner->last_name }}</h6>
        <p class="mb-0">{{ $post->owner->profile->bio }}</p>
    </div>
</div>
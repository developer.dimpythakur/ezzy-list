<?php

/**
 * Categories
 *
 */
Route::group(['namespace' => 'Backend\Listing', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {

    Route::group(['namespace' => 'Categories'], function () {
        Route::resource('listing-categories', 'CategoryController');
        //For Datatable
        Route::post('listing-categories/get', 'CategoryTableController')->name('listing-categories.get');
    });
});

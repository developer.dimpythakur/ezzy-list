<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-file-text"></i> Contact Information</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row my-3">
            <!-- Phone -->
            <div class="form-group col-4">
                {{ Form::label('phone', 'Phone', ['class' => 'control-label']) }}
                {{ Form::text('phone',null,['class' => ' border form-control', 'placeholder' => '123-456-789', 'required' => 'required']) }}
            </div>
            <!-- /Phone -->

            <!-- Email -->
            <div class="form-group col-4">
                {{ Form::label('email', 'Email', ['class' => 'control-label']) }}
                {{ Form::text('email',null,['class' => ' border form-control', 'placeholder' => 'business@example.email', 'required' => 'required']) }}
            </div>
            <!-- /Email -->

            <!-- Website -->
            <div class="form-group col-4">
                {{ Form::label('website', 'Website', ['class' => 'control-label']) }}
                {{ Form::text('website',null,['class' => ' border form-control', 'placeholder' => 'http://', 'required' => 'required']) }}
            </div>
            <!-- /Website -->
        </div>
    </div>
</div>

<?php
if (($post->featured_image) && ($post->featured_image) != 'default-image.jpg' && $post->type == 'feed'):
    $imageUrl = $post->featured_image;
else:
    $imageUrl = Storage::url('images/blog/' . $post->featured_image);
endif;
?>
<section class="et-page-header position-relative text-center"
         style="background-image: url({{ $imageUrl }})">
    <div class="header-content position-relative text-white">
        <ul class="nostyle list-inline breadcrumb-links mb-1">
            <li><a href="{{ URL::to('/') }}">Home</a>
            </li>
            <li class="current">
                {{ $post->name }}
            </li>
        </ul>
        <h1 class="page-title d-inline-flex align-items-center m-0 position-relative">{{ $post->name }}</h1>
        <p class="mb-0 mt-1 text-white">{{ strip_tags(substr($post->content,0,50)) }}</p>
    </div>
</section>
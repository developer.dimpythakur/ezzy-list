<?php

namespace App\Models\Listing\Timings\Traits\Attribute;

/**
 * Class ListingTimings.
 */
trait Attribute {

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
        return '<div class="btn-group action-btn">
                    ' . $this->getEditButtonAttribute('edit-page', 'admin.listings.edit') . '
                    ' . $this->getViewButtonAttribute() . '                    
                    ' . $this->getDeleteButtonAttribute('delete-page', 'admin.listings.destroy') . '
                </div>';
    }

    /**
     * @return string
     */
    public function getViewButtonAttribute() {
        return '<a target="_blank" href="' . route('frontend.listin.show', $this->id) . '" class="btn btn-flat btn-default">
                    <i data-toggle="tooltip" data-placement="top" title="View Listing" class="las la-eye"></i>
                </a>';
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute() {
        if ($this->isActive()) {
            return "<label class='label label-success'>" . trans('labels.general.active') . '</label>';
        }

        return "<label class='label label-danger'>" . trans('labels.general.inactive') . '</label>';
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

}

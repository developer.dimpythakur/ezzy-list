<?php

namespace App\Http\Controllers\Backend\Listing\Tags;

use App\Models\Listing\Tags\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Responses
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Listing\Tags\CreateResponse;
use App\Http\Responses\Backend\Listing\Tags\EditResponse;
use App\Repositories\Backend\Listing\Tags\TagRepository;
//Requests
use App\Http\Requests\Backend\Listing\Tags\Manage;
use App\Http\Requests\Backend\Listing\Tags\Create;
use App\Http\Requests\Backend\Listing\Tags\Store;
use App\Http\Requests\Backend\Listing\Tags\Edit;
use App\Http\Requests\Backend\Listing\Tags\Update;
use App\Http\Requests\Backend\Listing\Tags\Delete;

/**
 * TagsController
 */
class TagController extends Controller {

    /**
     * variable to store the repository object
     * @var TagRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param TagRepository $repository;
     */
    public function __construct(TagRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Tag\Manage  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(Manage $request) {
        return new ViewResponse('backend.listing.tags.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateNamespace  $request
     * @return \App\Http\Responses\Backend\Listings\Tag\CreateResponse
     */
    public function create(Create $request) {
        return new CreateResponse('backend.listing.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(Store $request) {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.listing-tags.index'), ['flash_success' => trans('alerts.backend.tags.created')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Tag\Tag  $listing_tag
     * @param  EditNamespace  $request
     * @return \App\Http\Responses\Backend\Listings\Tag\EditResponse
     */
    public function edit(Tag $listing_tag, Edit $request) {
        return new EditResponse($listing_tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNamespace  $request
     * @param  App\Models\Tag\Tag  $listing_tag
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Update $request, Tag $listing_tag) {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update($listing_tag, $input);
        //return with successfull message
        return new RedirectResponse(route('admin.tags.index'), ['flash_success' => trans('alerts.backend.tags.updated')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteNamespace  $request
     * @param  App\Models\Tag\Tag  $listing_tag
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Tag $listing_tag, Delete $request) {
        //Calling the delete method on repository
        $this->repository->delete($listing_tag);
        //returning with successfull message
        return new RedirectResponse(route('admin.tags.index'), ['flash_success' => trans('alerts.backend.tags.deleted')]);
    }

}

<?php

namespace App\Models\Listing\Listing\Traits;

use App\Models\Access\User\User;
use App\Models\Listing\Categories\Category;
use App\Models\Listing\Tags\Tag;
use App\Models\Listing\Faqs\Faq;
use App\Models\Listing\Media\SocialMedia\SocialMedia;
use App\Models\Listing\Media\Gallery\MediaGallery;
use App\Models\Listing\Media\Audio\MediaAudio;
use App\Models\Listing\Media\Video\MediaVideo;
use App\Models\Listing\Timings\Timing;
use App\Models\Listing\Locations\Location;
use App\Models\Listing\Reviews\Review;
use App\Models\Listing\Region\Region;

trait Relationship {

    /**
     * Listing has many relationship with categories.
     */
    public function categories() {
        $categoryTable = (new Category)->getMapTable();
        return $this->belongsToMany(Category::class, $categoryTable, 'ezzy_listing_id', 'ezzy_categories_id');
    }

    /**
     * Listing has many relationship with tags.
     */
    public function tags() {
        $tagsTable = (new Tag)->getMapTable();
        return $this->belongsToMany(Tag::class, $tagsTable, 'ezzy_listing_id', 'ezzy_tags_id');
    }

    /**
     * Listing has many relationship with tags.
     */
    public function faqs() {
        $faqsTable = (new Faq)->getMapTable();
        return $this->belongsToMany(Faq::class, $faqsTable, 'ezzy_listing_id', 'ezzy_faqs_id');
    }

    /**
     * Listing has many relationship with tags.
     */
    public function social_network() {
        $socialNetworkTable = (new SocialMedia)->getMapTable();

        return $this->belongsToMany(SocialMedia::class, $socialNetworkTable, 'ezzy_listing_id', 'ezzy_social_networks_id');
    }

    /**
     * Media has many relationship with tags.
     */
    public function gallery() {
        $mediaGalleryTable = (new MediaGallery)->getMapTable();
        return $this->belongsToMany(MediaGallery::class, $mediaGalleryTable, 'ezzy_listing_id', 'ezzy_media_gallery_id');
    }

    /**
     * Audio has many relationship with tags.
     */
    public function audios() {
        $mediaAudioTable = (new MediaAudio())->getMapTable();
        return $this->belongsToMany(MediaAudio::class, $mediaAudioTable, 'ezzy_listing_id', 'ezzy_media_audio_id');
    }

    /**
     * Videos has many relationship with tags.
     */
    public function videos() {
        $mediaVideoTable = (new MediaVideo())->getMapTable();
        return $this->belongsToMany(MediaVideo::class, $mediaVideoTable, 'ezzy_listing_id', 'ezzy_media_video_id');
    }

    /**
     * Blogs has many relationship with tags.
     */
    public function timings() {
        $timingTable = (new Timing)->getMapTable();
        return $this->belongsToMany(Timing::class, $timingTable, 'ezzy_listing_id', 'ezzy_timings_id');
    }

    /**
     * Listing Locations
     * @return mixed
     */
    public function location() {
        $locationsTable = (new Location)->getMapTable();
        return $this->belongsToMany(Location::class, $locationsTable, 'ezzy_listing_id', 'ezzy_locations_id');
    }

    /**
     * Listing Locations
     * @return mixed
     */
    public function region() {
        return $this->belongsTo(Region::class, 'id');
    }

    /**
     * Get all reviews of this model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function reviews() {
        return $this->belongsToMany(Review::class, 'ezzy_listing_id', 'listing_reviews');
    }

    /**
     * Get all reviews of this model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */

    /**
     * Created by 
     * @return type
     */
    public function owner() {
        return $this->belongsTo(User::class, 'created_by');
    }

}

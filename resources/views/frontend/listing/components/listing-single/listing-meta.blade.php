<!-- Listing Meta -->
<div class="listing-meta col-md-6 col-sm-12 text-center text-md-right">
    <div class="star-rating d-inline-block mb-1 listing-rating single-listing"
         data-toggle="tooltip"
         data-placement="top"
         title="Overall Rating"
         data-rating="{{ $listing->averageRating(2) }}">Overall rating:
    </div>
    <span class="meta-info reviews pl-2 pr-2" data-toggle="tooltip" data-placement="top"
          title="{{ $listing->countRating() }} Customer Reviews">
        {{ $listing->countRating() }} Reviews <i class="icon-quote text-primary"></i>
    </span>
    <br>
    <?php
    if ($listing->location->pluck('address')):
        $locationURL = "https://www.google.com/maps/dir//" . ($listing->location->pluck('address')->first());
        ?>
        <hr>
        <span class="meta-info reviews pl-2 pr-2" data-toggle="tooltip" data-placement="left"
              title="See Directions">
            <a target="_blank"
               href="{{ $locationURL }}"
               class="transition-1 text-capitalize text-body">
                <i class="las la-map-marker-alt text-primary"></i>&nbsp;
                {{ $listing->location->pluck('address')->first() }}
            </a>
        </span>
    <?php endif; ?>
</div>
<!-- /Listing Meta -->
<?php

use Carbon\Carbon as Carbon;
use Database\DisableForeignKeys;
use Database\TruncateTable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Access\User\User;
use App\Models\Access\User\Profile;
use App\Repositories\Frontend\Access\User\UserRepository;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder {

    use DisableForeignKeys,
        TruncateTable;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run() {
        $this->disableForeignKeys();
        $this->truncate(config('access.users_table'));

        //Add the master administrator, user id of 1
        $users = [
            [
                'first_name' => 'Ezzy',
                'last_name' => 'Listing',
                'email' => 'admin@admin.com',
                'password' => bcrypt('1234'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'created_by' => 1,
                'updated_by' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => null,
            ],
//            [
//                'first_name' => 'User 2',
//                'last_name' => 'Listing',
//                'email' => 'executive@executive.com',
//                'password' => bcrypt('1234'),
//                'confirmation_code' => md5(uniqid(mt_rand(), true)),
//                'confirmed' => true,
//                'created_by' => 1,
//                'updated_by' => null,
//                'created_at' => Carbon::now(),
//                'updated_at' => Carbon::now(),
//                'deleted_at' => null,
//            ],
            [
                'first_name' => 'User',
                'last_name' => 'Test',
                'email' => 'user@user.com',
                'password' => bcrypt('1234'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed' => true,
                'created_by' => 1,
                'updated_by' => null,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => null,
            ],
        ];
        $path = 'images' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR . 'profile' . DIRECTORY_SEPARATOR;
        foreach ($users as $user):
            $repo = User::create($user);
            $repo->profile()->create([
                "phone" => isset($user['phone']) ? $user['phone'] : '',
                "website" => isset($user['website']) ? $user['website'] : '',
                "bio" => isset($user['bio']) ? $user['bio'] : '',
                "social_media" => isset($user['social_media']) ? json_encode($user['social_media']) : '',
                'avatar' => $path . config('backend.default_avatar')
            ]);
        endforeach;
//        DB::table(config('access.users_table'))->insert($users);
        $this->enableForeignKeys();
    }

}

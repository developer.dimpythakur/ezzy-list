@php
$options = [
'file_picker_callback' => 'elFinderBrowser',
'selector' => 'textarea.tinymce',
'plugins' => 'image,link,media,anchor',
];
@endphp


<div class="form-group col-12">
    {{ Form::label('name', trans('validation.attributes.backend.blogs.title'), ['class' => 'control-label required']) }}
    {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.title'), 'required' => 'required']) }}
    @if(isset($blog))
    <p class="help-block mt-2">
        <a target="_blank" href="{{ route('frontend.post.show',$blog->slug) }}">
            <cite>
                {{ route('frontend.index')}}/listing/<span id="listing-slug">{{ isset($blog)? $blog->slug:''}}</span>
            </cite> 
        </a>
    </p>
    @endif
</div><!--form control-->
<div class="form-group col-12">
    {{ Form::label('categories', trans('validation.attributes.backend.blogs.category'), ['class' => 'control-label required']) }}
    @if(!empty($selectedCategories))
    {{ Form::select('categories[]', $blogCategories, $selectedCategories, ['class' => 'form-control select2_multiple tags box-size', 'required' => 'required', 'multiple' => 'multiple']) }}
    @else
    {{ Form::select('categories[]', $blogCategories, null, ['class' => 'form-control tags box-size select2_multiple', 'required' => 'required', 'multiple' => 'multiple']) }}
    @endif
</div><!--col-lg-10-->
<div class="form-group col-12">
    {{ Form::label('publish_datetime', trans('validation.attributes.backend.blogs.publish'), ['class' => 'control-label required']) }}
    @if(!empty($blog->publish_datetime))
    {{ Form::text('publish_datetime', \Carbon\Carbon::parse($blog->publish_datetime)->format('m/d/Y h:i a'), ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.blogs.publish'), 'required' => 'required', 'id' => 'datetimepicker1']) }}
    @else
    {{ Form::text('publish_datetime', null, ['class' => 'form-control datetimepicker1 box-size', 'placeholder' => trans('validation.attributes.backend.blogs.publish'), 'required' => 'required', 'id' => 'datetimepicker1']) }}
    @endif
</div><!--form control-->
<div class="form-group col-12">
    {{ Form::label('featured_image', trans('validation.attributes.backend.blogs.image'), ['class' => 'control-label required']) }}
    @if(!empty($blog->featured_image))
    <div class="col-lg-12">
        <img src="{{ Storage::disk('public')->url('images/blog/' . $blog->featured_image) }}" height="80" width="80">
        <label class="form-control file">
            <input type="file" id="file" name="featured_image" 
                   data-multiple-caption="{count} files selected" 
                   aria-label="Upload Featured Image">
            <span class="file-custom"></span>
        </label>
    </div>
    @else
    <label class="form-control file">
        <input type="file" id="file" name="featured_image" 
               data-multiple-caption="{count} files selected" 
               aria-label="Upload Featured Image">
        <span class="file-custom"></span>
    </label>
    @endif
</div><!--form control-->

<div class="form-group col-12">
    {{ Form::label('content', trans('validation.attributes.backend.blogs.content'), ['class' => 'control-label required']) }}
    {{ Form::textarea('content', null, [
                 'class' => 'form-control box-size tinymce',
                     'data-options'=> trim(json_encode($options)), 'placeholder' => trans('validation.attributes.backend.blogs.content')]) }}
</div><!--form control-->

<div class="form-group col-12">
    {{ Form::label('tags', trans('validation.attributes.backend.blogs.tags'), ['class' => 'control-label required']) }}
    @if(!empty($selectedtags))
    {{ Form::select('tags[]', $blogTags, $selectedtags, ['class' => 'form-control select2_multiple tags box-size', 'required' => 'required', 'multiple' => 'multiple']) }}
    @else
    {{ Form::select('tags[]', $blogTags, null, ['class' => 'form-control tags select2_multiple box-size', 'required' => 'required', 'multiple' => 'multiple']) }}
    @endif
</div><!--form control-->

<div class="form-group col-12">
    {{ Form::label('meta_title', trans('validation.attributes.backend.blogs.meta-title'), ['class' => 'control-label']) }}
    {{ Form::text('meta_title', null, ['class' => 'form-control box-size ', 'placeholder' => trans('validation.attributes.backend.blogs.meta-title')]) }}
</div><!--form control-->

<div class="form-group col-12">
    {{ Form::label('slug', trans('validation.attributes.backend.blogs.slug'), ['class' => 'control-label']) }}
    {{ Form::text('slug', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.slug'), 'disabled' => 'disabled']) }}
</div><!--form control-->

<div class="form-group col-12">
    {{ Form::label('cannonical_link', trans('validation.attributes.backend.blogs.cannonical_link'), ['class' => 'control-label']) }}
    {{ Form::text('cannonical_link', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.cannonical_link')]) }}
</div><!--form control-->


<div class="form-group col-12">
    {{ Form::label('meta_keywords', trans('validation.attributes.backend.blogs.meta_keyword'), ['class' => 'control-label']) }}
    {{ Form::text('meta_keywords', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogs.meta_keyword')]) }}
</div><!--form control-->

<div class="form-group col-12">



    {{ Form::label('meta_description', trans('validation.attributes.backend.blogs.meta_description'), ['class' => 'control-label']) }}

    {{ Form::textarea('meta_description', null,[
                 'class' => 'form-control box-size tinymce',
                     'data-options'=> trim(json_encode($options)),
                'placeholder' => trans('validation.attributes.backend.blogs.meta_description')]) }}
</div><!--form control-->

<div class="form-group col-12">
    {{ Form::label('status', trans('validation.attributes.backend.blogs.status'), ['class' => 'control-label required']) }}
    {{ Form::select('status', $status, null, ['class' => 'form-control select2 status box-size', 'placeholder' => trans('validation.attributes.backend.blogs.status'), 'required' => 'required']) }}
</div><!--form control-->

@section("after_scriptss")
<script type="text/javascript">

    Backend.Blog.selectors.GenerateSlugUrl = "{{route('admin.generate.slug')}}";
    Backend.Blog.selectors.SlugUrl = "{{url('/')}}";
    Backend.Blog.init('{{ config('locale.languages.' . app()->getLocale())[1] }}');
</script>
@endsection
/**
 * Allows you to add data-method="METHOD to links to automatically inject a form
 * with the method on click
 *
 * Example: <a href="{{route('customers.destroy', $customer->id)}}"
 * data-method="delete" name="delete_item">Delete</a>
 *
 * Injects a form with that's fired on click of the link with a DELETE request.
 * Good because you don't have to dirty your HTML with delete forms everywhere.
 */
function addDeleteForms() {
    $('[data-method]').append(function () {
        if (!$(this).find('form').length > 0)
            return "\n" +
                    "<form action='" + $(this).attr('href') + "' method='POST' name='delete_item' style='display:none'>\n" +
                    "   <input type='hidden' name='_method' value='" + $(this).attr('data-method') + "'>\n" +
                    "   <input type='hidden' name='_token' value='" + $('meta[name="csrf-token"]').attr('content') + "'>\n" +
                    "</form>\n";
        else
            return "";
    })
            .removeAttr('href')
            .attr('style', 'cursor:pointer;')
            .attr('onclick', '$(this).find("form").submit();');
}

/**
 * Place any jQuery/helper plugins in here.
 */
jQuery(document).ready(function () {

    /**
     * Bind all bootstrap tooltips
     */
//    jQuery("[data-toggle=\"tooltip\"]").tooltip();

    /**
     * Bind all bootstrap popovers
     */
    // $("[data-toggle=\"popover\"]").popover();

    /**
     * This closes the popover when its clicked away from
     */
    jQuery('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
    //-------------------------------
    // jQuery Nice Select
    //-------------------------------
    if ($("select").length) {
        $('select').niceSelect();
    }



    //-------------------------------
    // Bootstrap DateTimePicker
    //-------------------------------
    if ($(".datetimepicker").length) {
        $(function () {
            $('.datetimepicker').datetimepicker({
                pickDate: false
            });
        });
    }

    //-------------------------------
    // Background Image Scroll Effect
    //-------------------------------

    var velocity = 0.6; // Scroll speed
    function update() {
        var pos = $(window).scrollTop();
        $('.et-add-listing').each(function () {
            var $element = $(this);
            // subtract some from the height b/c of the padding
            var height = $element.height() - 18;
            $(this).css('backgroundPosition', '50% ' + Math.round((height - pos) * velocity) + 'px');
        });
    }
    ;
    $(window).bind('scroll', update);



    //-------------------------------
    // Mobile Nav Menu
    //-------------------------------
    $('#mobile-nav').hcOffcanvasNav({
        maxWidth: 768,
        customToggle: $('#nav-toggle')
    });




    /*
     =======================================================================
     Back to top
     =======================================================================
     */
    $(window).scroll(function () {
        if ($(this).scrollTop()) {
            $('#backTop').fadeIn();
        } else {
            $('#backTop').fadeOut();
        }
    });
    $("#backTop").on('click', function () {
        $("html, body").animate({
            scrollTop: 0
        }, 980);
    });


    /*----------------------------------------------------*/
    /*  Sticky Header
     /*----------------------------------------------------*/
    if ($('#site-header').hasClass('sticky-header')) {
        $("#site-header")
                .not("#site-header.not-sticky")
                .clone(true)
                .addClass('cloned unsticky')
                .insertAfter("#site-header");

        var reg_logo = $("#site-header.cloned #logo")
                .data('logo');
        $("#site-header.cloned #logo img").attr('src', reg_logo);
        // sticky header script
        var headerOffset = 100;	 // height on which the sticky header will shows
        $(window).scroll(function () {
            if ($(window).scrollTop() > headerOffset) {
                $("#site-header.cloned").addClass('sticky').removeClass("unsticky");
                $("#navigation.style-2.cloned").addClass('sticky').removeClass("unsticky");
            } else {
                $("#site-header.cloned").addClass('unsticky').removeClass("sticky");
                $("#navigation.style-2.cloned").addClass('unsticky').removeClass("sticky");
            }
        });
    }


});


jQuery(document).ready(function () {
    let $buttons = [
        jQuery('button[value="Register"'),
        jQuery('button[value="Login"')
    ];
    $buttons.forEach(function ($item, $index) {
        $item.on('click', function () {
            jQuery(this).attr('disabled', true);
            jQuery(this).toggleClass('disabled');
        });
    });
});
/**
 * -----------------------
 * Save Listing || Bookmark
 * ------------------------
 */
jQuery(document).ready(function () {
    jQuery(document).on('click', '.et-save-icon', function (e) {
        var $thisObject = jQuery(this);
        if (user === '0') {
            notifyMe("error", "Please login to bookmark the listing");
            return;
        }
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        let ID = jQuery(this).data('id');
        let URL = jQuery(this).data('url');
        jQuery.ajax({
            url: URL,
            method: 'POST',
            data: {
                user: user,
                listing: ID,
            },
            success: function (result) {
                notifyMe(result.type, result.message);
                $thisObject.toggleClass('active');
            }});
    });
});



/**
 * Notification message
 * @param {type} $type
 * @param {type} $message
 * @returns {undefined}
 */
function notifyMe($type, $message) {
    Noty.overrideDefaults({
        layout: 'topRight',
        timeout: 2500,
        closeWith: ['click', 'button'],
        theme: 'ezzy',
    });
    new Noty({
        type: $type,
        text: $message
    }).show();
}

/**
 * Generate Ratings
 * @param firstStar
 * @param secondStar
 * @param thirdStar
 * @param fourthStar
 * @param fifthStar
 * @returns {string}
 */
function outPutStar(firstStar, secondStar, thirdStar, fourthStar, fifthStar) {
    return ('' +
            '<span class="las ' + firstStar + '"></span>' +
            '<span class="las ' + secondStar + '"></span>' +
            '<span class="las ' + thirdStar + '"></span>' +
            '<span class="las ' + fourthStar + '"></span>' +
            '<span class="las ' + fifthStar + '"></span>');
}

function starRating(ratingElem) {
    $(ratingElem).each(function () {
        var dataRating = $(this).attr('data-rating');


        if (jQuery(this).attr('data-class')) {
            var classReview = jQuery(this).attr('data-class');
        }
        var fiveStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star', 'la-star');
        var fourHalfStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star', 'la-star half');
        var fourStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star', 'la-star empty');
        var threeHalfStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star half', 'la-star empty');
        var threeStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star empty', 'la-star empty');
        var twoHalfStars = outPutStar('la-star', 'la-star', 'la-star half', 'la-star empty', 'la-star empty');
        var twoStars = outPutStar('la-star', 'la-star', 'la-star empty', 'la-star empty', 'la-star empty');
        var oneHalfStar = outPutStar('la-star', 'la-star half', 'la-star empty', 'la-star empty', 'la-star empty');
        var oneStar = outPutStar('la-star', 'la-star empty', 'la-star empty', 'la-star empty', 'la-star empty');
        var empty = outPutStar('la-star empty', 'la-star empty', 'la-star empty', 'la-star empty', 'la-star empty');

        if (dataRating == 0) {
            $(this).append(empty);
            return;
        }

        if (dataRating >= 4.75) {
            $(this).append(fiveStars);
        } else if (dataRating >= 4.25) {
            $(this).append(fourHalfStars);
        } else if (dataRating >= 3.75) {
            $(this).append(fourStars);
        } else if (dataRating >= 3.25) {
            $(this).append(threeHalfStars);
        } else if (dataRating >= 2.75) {
            $(this).append(threeStars);
        } else if (dataRating >= 2.25) {
            $(this).append(twoHalfStars);
        } else if (dataRating >= 1.75) {
            $(this).append(twoStars);
        } else if (dataRating >= 1.25) {
            $(this).append(oneHalfStar);
        } else if (dataRating < 1.25) {
            $(this).append(oneStar);
        } else {
            $(this).append(empty);
        }

    });
}

jQuery(document).ready(function () {
    /*----------------------------------------------------*/
    /*  Ratings Script
     /*----------------------------------------------------*/
    starRating('.listing-rating');
//    starRating('.star-rating');


    jQuery('#show-map-button').on('click', function (event) {
        event.preventDefault();
        jQuery(".hide-map-mobile").toggleClass("map-show");
        var text_enabled = jQuery(this).data('enabled');
        var text_disabled = jQuery(this).data('disabled');
        if (jQuery(".hide-map-mobile").hasClass('map-show')) {
            jQuery(this).text(text_disabled);
        } else {
            jQuery(this).text(text_enabled);
        }
    });
});
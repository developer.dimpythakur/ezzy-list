@push('after_styles')

<style>
    /*FAKE FILE*/
    .fake-file-upload {
        border: 2px dashed #ccc;
    }
    .fake-file-upload input {
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
    }
    .fake-file-upload:hover{
        border-color: #e53935 !important;
    }
</style>
@endpush
<?php 


?>

<div class="form-group col-sm-12">
    {{ Form::label('name', trans('validation.attributes.backend.blogcategories.title'), ['class' => 'control-label required']) }}
    {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.blogcategories.title'), 'required' => 'required']) }}
</div><!--form control-->



<div class="form-group col-sm-12">
    {{ Form::label('image', 'Category Image', ['class' => 'control-label']) }}
    <div class="fake-file-upload h6 rounded-pill transition-1 d-flex align-items-center justify-content-center p-3 text-center text-muted position-relative">
        <input type="file" id="single_img_upload" name="image" class="position-absolute pointer" >
        <i class="la la-cloud-upload-alt h3 m-0"></i>&nbsp; Choose image
    </div>
</div>
@if(isset($blogcategory) && !empty($blogcategory->image))
<div class="form-group col-sm-12">
    <div class="col-lg-3">
        <img src="{{ Storage::url('images/blog-category/' . $blogcategory->image) }}" height="150"
             class="img-thumbnail img-preview" width="150">
    </div>
</div>
@endif
<div class="form-group col-sm-12">
    {{ Form::label('icon', 'Icon', ['class' => 'control-label']) }}
    @if(!empty($selectedIcon))
    {{ Form::select('icon', $icon, $blogcategory->icon, ['class' => 'form-control box-size', 'placeholder' => 'Select Icon', 'required' => 'required'])}}
    @else
    {{ Form::select('icon', blogCategoriesIcons(), null, ['class' => 'form-control box-size', 'placeholder' => 'Select Icon', 'required' => 'required'])}}
    @endif
</div><!--form control-->
<div class="form-group col-sm-12" element="div">
    <div>
        <label>Status</label>
    </div>

    <div class="form-check form-check-inline">
        <input type="radio" {{ (isset($blogcategory)&& ($blogcategory->status == 1) ? 'checked' : '' ) }} class="form-check-input" checked="" value="1" name="status" id="status_1">
        <label class="radio-inline form-check-label font-weight-normal" for="status_1">Published</label>
    </div>
    <div class="form-check form-check-inline">
        <input type="radio" {{ (isset($blogcategory)&& ($blogcategory->status == 0) ? 'checked' : '' ) }} class="form-check-input" value="0" name="status" id="status_0">
        <label class="radio-inline form-check-label font-weight-normal" for="status_0">Draft</label>
    </div>
</div><!--form group-->

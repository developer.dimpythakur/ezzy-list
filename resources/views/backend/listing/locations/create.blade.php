
@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.listings.locations.management') . ' | ' . trans('labels.backend.blogs.create'))

@section('page-header')
<h1>{{ trans('labels.backend.listings.locations.management') }}</h1>
@endsection

@php
$breadcrumbs = [
'Dashboard' => url(config('backend.route_prefix'), 'dashboard'),
trans('listings.backend.listings.all') => route('admin.listings.index'),
trans('listings.backend.listings.create') => false,
];
@endphp


@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.listings.locations.management') }}</span>
    </h2>
</div>
@endsection

@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::open(['route' => 'admin.listing-locations.store', 'class' => 'form-horizontal add-listing','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'post', 'id' => 'create-listing']) }}
        @include("backend.listing.locations.form")
        <div id="saveForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back">Save and back</span>
                </button>
            </div>
            <a href="{{ route('admin.listings.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>

@endsection
@section("after_scripts")

@endsection

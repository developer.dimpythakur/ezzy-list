@section("after_styles")
{!! Html::style('css/nestable2/jquery.nestable.css') !!}
@endsection

<div class="form-group col-sm-4">
    {{ Form::label('categories', trans('labels.backend.menus.field.type'), ['class' => 'control-label required']) }}
    {{ Form::select('type', $types, null, ['class' => 'form-control tags box-size', 'required' => 'required']) }}
</div>
<div class="form-group col-sm-8">
    {{ Form::label('name', trans('labels.backend.menus.field.name'), ['class' => 'control-label required']) }}
    {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('labels.backend.menus.field.name'), 'required' => 'required']) }}
</div>
<hr/>
<div class="form-group col-sm-4">
    <div class="row">
        <div class="col-lg-12">
            @foreach ($modules as $module)
            <div class="row modules-list-item">
                <div class="col-lg-10">
                    <span >{{ $module->name }}&nbsp;&nbsp;</span>
                </div>
                <div class="col-lg-2">
                    <a href="javascript:void(0);"><i class="la la-plus add-module-to-menu" data-id="{{ $module->id }}" data-name="{{ $module->name }}" data-url="{{ $module->url }}" data-url_type="route" data-open_in_new_tab="0" data-view_permission_id="{{ $module->view_permission_id }}" ></i></a>
                </div>
            </div>
            @endforeach
            <br/>
            <button type="button" class="btn btn-info show-modal" data-form="_add_custom_url_form" data-header="Add Custom URL"><i class="la la-plus" ></i>&nbsp;&nbsp;Add Custom URL</button>
        </div>
    </div>
</div>
<div class="form-group col-sm-8">
    {{ Form::hidden('items', empty($menu->items) ? '{}' : $menu->items, ['class' => 'menu-items-field']) }}
    <div class="card">
        <div class="card-body">
            <div id="menu-items" class="dd">
            </div>
        </div>
    </div>
</div>




@section('before_scripts')
{{ Html::script('js/nestable2/jquery.nestable.js') }}
@endsection

@section("after_scripts")
<script type="text/javascript">
    Backend.Menu.selectors.formUrl = "{{route('admin.menus.getform')}}";
    Backend.Menu.init();
</script>
@endsection

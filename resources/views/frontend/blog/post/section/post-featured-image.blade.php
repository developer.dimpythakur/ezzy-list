@if(($post->featured_image) && ($post->featured_image) != 'default-image.jpg' && $post->type =='feed')
<img src="{{ $post->featured_image }}"
     alt="{{ $post->name }}"
     class="img-fluid rounded-top">
@else
<img src="{{ Storage::url('images/blog/' . $post->featured_image) }}" alt="{{ $post->name }}"
     class="img-fluid rounded-top">
@endif
<!-- FAQs -->

<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-question-circle-o"></i> FAQs</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row mb-3">
            <div class="form-group col-12">
                <div id="faqs" class="appender">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Question</label>
                            <input type="text" class="form-control" placeholder="Enter question" id="faq_question">
                        </div>
                        <div class="col-sm-6 mt-md-0 mt-3 mb-md-0 mb-3">
                            <label>Answer</label>
                            <textarea class="form-control" placeholder="Enter answer" id="faq_answer"></textarea>
                        </div>
                        <div class="col-sm-2 text-right">
                            <button type="button" class="add-faq btn btn-secondary border-transparent border-0">
                                <span class="icon"><i class="la la-plus-square-o"></i></span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive selected-faqs p-2" style="display: {{ (!empty($listing)&& count($listing->faqs))?'block':'none' }}">
                        <table class="mt-3 table table-centered table-bordered">
                            <thead>
                                <tr>
                                    <th>Question</th>
                                    <th>Answer</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if( !empty($listing) && count($listing->faqs))
                                @foreach ($listing->faqs as $faq)
                                <tr>
                                    <td><input type="hidden" value="{{$faq->question}}"
                                               name="faq[question][]"> {{$faq->question}}</td>
                                    <td><input type="hidden" value="{{$faq->answer}}" name="faq[answer][]"> {{$faq->answer}}
                                    </td>
                                    <td class="text-center"><i class="las la-trash remove pointer text-danger"></i></td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /FAQs -->

@push("after_scripts")

<script>
    jQuery(document).ready(function () {
//-------------------------------
// Add FAQs
//-------------------------------
        jQuery(document).on('click', ".add-faq", function () {
            var $faqQuestionInput = jQuery("input[id='faq_question']");
            var faqQuestion = $faqQuestionInput.val();

            var $faqAnswerInput = jQuery("textarea[id='faq_answer']");
            var faqAnswer = $faqAnswerInput.val();

            if ((typeof faqQuestion !== "undefined") && faqQuestion !== '' &&
                    (typeof faqAnswer !== "undefined") && faqAnswer !== '') {
                let $targetTable = jQuery(".selected-faqs");
                let $question = `<input type="hidden" value="${faqQuestion}" name="faq[question][]">`;
                let $answer = `<input type="hidden" value="${faqAnswer}" name="faq[answer][]">`;
                let $tableRow = `<tr><td>${$question} ${faqQuestion}</td><td>${$answer} ${faqAnswer}</td><td class="text-center"><i class='las la-trash remove pointer text-danger'></i></td></tr>`;
                $targetTable.show();
                jQuery(".selected-faqs table tbody").append($tableRow);
            } else {
                alert("Please fill both the fields");
            }
            $faqQuestionInput.val('');
            $faqAnswerInput.val('');

        });

        jQuery(document).on("click", ".selected-faqs .remove", function (u) {
            jQuery(this).closest("tr").remove();
            if (jQuery(".selected-faqs tbody > tr").length < 1) {
                jQuery(".selected-faqs").hide();
            }
        });
    });
</script>
@endpush
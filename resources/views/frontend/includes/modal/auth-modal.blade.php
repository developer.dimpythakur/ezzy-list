<!-- Modal for Login/Signup -->
<div class="et-modal modal animated slideInUp" id="loginSignup" tabindex="-1" role="dialog"
     aria-labelledby="loginSignup" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content border-0">
            <div class="modal-header text-center d-block border-0 p-0">
                <button type="button" class="close p-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="icon-cancel-music"></span>
                </button>
            </div>
            <ul class="nav login-tabs-pill nav-pills mb-3 align-items-center justify-content-center mb-4" id="pills-tab"
                role="tablist">
                <li class="nav-item">
                    <a class="nav-link active p-3" id="pills-login-tab" data-toggle="pill" href="#pills-login"
                       role="tab" aria-controls="pills-login" aria-selected="false">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link p-3" id="pills-signup-tab" data-toggle="pill"
                       href="#pills-signup" role="tab" aria-controls="pills-signup"
                       aria-selected="true">Signup
                    </a>
                </li>

            </ul>
            <div class="modal-body pt-0 pb-0">
                <div class="tab-content" id="pills-tabContent">
                    <!-- Login -->
                    <div class="tab-pane fade in active show" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
                        <h5 class="modal-title text-center">Login</h5>
                        <p class="text-muted text-center mb-4">Login to see what's happeing with your listings</p>


                        {{ Form::open(['route' => 'frontend.auth.login', 'class' => 'form-horizontal']) }}
                        <div class="form-group">
                            {{ Form::label('email', trans('validation.attributes.frontend.register-user.email'), ['class' => 'col-md-8 control-label']) }}
                            <div class="col-md-12">
                                {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.email')]) }}
                            </div><!--col-md-12-->
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::label('password', trans('validation.attributes.frontend.register-user.password'), ['class' => 'col-md-8 control-label']) }}
                            <div class="col-md-12">
                                {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password')]) }}
                            </div><!--col-md-12-->
                        </div><!--form-group-->

                        <div class="form-group">
                            <div class="col-md-12 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        {{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}
                                    </label>
                                </div>
                            </div><!--col-md-12-->
                        </div><!--form-group-->


                        <div class="modal-footer border-0 mb-3 pl-0 w-100">
                            <div class="form-group row w-100">
                                <div class="col-md-8">
                                    {{ link_to_route('frontend.auth.password.reset', trans('labels.frontend.passwords.forgot_password')) }}
                                </div><!--col-md-6-->
                                <div class="col-md-4 text-right">
                                    {{ Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn btn-primary']) }}
                                </div>
                            </div><!--form-group-->
                        </div>
                        {{ Form::close() }}
                    </div>
                    <!-- /Login -->
                    <!-- Signup -->
                    <div class="tab-pane fade" id="pills-signup" role="tabpanel"
                         aria-labelledby="pills-signup-tab">
                        <h5 class="modal-title text-center">Create Account</h5>
                        <p class="text-muted text-center mb-4">Create account to add your favourite listings</p>

                        {{ Form::open(['route' => 'frontend.auth.register', 'class' => 'form-horizontal']) }}
                        <div class="form-group">
                            {{ Form::label('first_name', trans('validation.attributes.frontend.register-user.firstName').'*', ['class' => 'col-md-8 control-label']) }}
                            <div class="col-md-12">
                                {{ Form::input('name', 'first_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.firstName')]) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->
                        <div class="form-group">
                            {{ Form::label('last_name', trans('validation.attributes.frontend.register-user.lastName').'*', ['class' => 'col-md-8 control-label']) }}
                            <div class="col-md-12">
                                {{ Form::input('name', 'last_name', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.lastName')]) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::label('email', trans('validation.attributes.frontend.register-user.email').'*', ['class' => 'col-md-8 control-label']) }}
                            <div class="col-md-12">
                                {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.email')]) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::label('password', trans('validation.attributes.frontend.register-user.password').'*', ['class' => 'col-md-8 control-label']) }}
                            <div class="col-md-12">
                                {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password')]) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            {{ Form::label('password_confirmation', trans('validation.attributes.frontend.register-user.password_confirmation').'*', ['class' => 'col-md-8 control-label']) }}
                            <div class="col-md-12">
                                {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password_confirmation')]) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->

                        <div class="form-group">
                            <div class="col-xs-7">

                            </div><!--form-group-->
                        </div><!--col-md-6-->

                        @if (config('access.captcha.registration'))
                        <div class="form-group">
                            {{ Form::label('captcha', trans('validation.attributes.frontend.register-user.password_confirmation').'*', ['class' => 'col-md-8 control-label']) }}
                            <div class="col-md-12">
                                {!! captcha_img() !!}
                                {{ Form::input('captcha', 'captcha', null,      ['class' => 'form-control mt-1', 'placeholder' =>'Captcha']) }}
                            </div><!--col-md-6-->
                        </div><!--form-group-->
                        @endif
                        <div class="form-group">
                            <div class="check-types col-12">
                                <label for="cbk3" class="w-100">
                                    <input name="is_term_accept" type="checkbox" id="cbk3">
                                    <span class="cbx rounded-pill">
                                        <svg width="10px" height="10px" viewBox="0 0 12 11">
                                        <polyline points="1 6.29411765 4.5 10 11 1"></polyline>
                                        </svg>
                                    </span>
                                    <span>I agree to the <a href="#" class="text-primary">Terms &amp; Conditions</a> and <a
                                            href="#" class="text-primary">privacy policy</a>.</span>
                                </label>
                            </div>
                        </div>
                        <div class="modal-footer border-0 mb-3 pt-0">
                            {{ Form::submit(trans('labels.frontend.auth.register_button'), ['class' => 'btn btn-primary']) }}
                        </div>
                        {{ Form::close()  }}</div>
                    <!-- /Signup -->

                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Modal for Login/Signup -->

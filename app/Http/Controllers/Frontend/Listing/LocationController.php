<?php

namespace App\Http\Controllers\Frontend\Listing;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Models\Listing\Listing\Listing;
use App\Models\Listing\Categories\Category;
use App\Models\Listing\Region\Region;
use App\Repositories\Frontend\Listing\LocationRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;
//use SKAgarwal\GoogleApi\PlacesApi;
use App\Models\Listing\Locations\Location;

/**
 * Class ListingController.
 */
class LocationController extends Controller {

    public $googlePlaces;
    public $categories;
    public $regions;

    public function __construct() {
//        $this->googlePlaces = new PlacesApi(env('GOOGLE_PLACES_KEY'));
        $this->categories = Category::select('name')->where('status', 1)->get()->toArray();
        $this->regions = Region::limit(6)->get();
    }

    public function index() {
        
    }

    /**
     * @param $slug
     * @param LocationRepository $location
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \App\Exceptions\GeneralException
     */
    public function show($slug, LocationRepository $location) {
        $region = Region::where('slug', $slug)->first();
        if (!$region) {
            Alert::error('Error', 'Something went wrong');
            return redirect('/');
        }
        $locations = Location::with('listing')->where('region_id', $region->id)->paginate(8);
        $categories = Category::select('name')->where('status', 1)->get();
        $regions = $this->regions;
        $title = $region->name;
        return view('frontend.listing.listing-locations.index',
                compact('locations',
                        'region',
                        'title',
                        'regions',
                        'categories')
        );
    }

}

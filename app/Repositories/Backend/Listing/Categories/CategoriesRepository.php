<?php

namespace App\Repositories\Backend\Listing\Categories;

use DB;
use Carbon\Carbon;
use App\Models\Listing\Categories\Category;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
//Events
use App\Events\Backend\Listing\Categories\Created;
use App\Events\Backend\Listing\Categories\Updated;
use App\Events\Backend\Listing\Categories\Deleted;
//Storage
use Illuminate\Support\Facades\Storage;
use Prologue\Alerts\Facades\Alert;
//Validator
use Illuminate\Support\Facades\Validator;

/**
 * Class CategoryRepository.
 */
class CategoryRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Category::class;

    public function __construct() {
        $this->upload_path = 'images' . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.ezzy_categories.table') . '.id',
                            config('module.ezzy_categories.table') . '.name',
                            config('module.ezzy_categories.table') . '.slug',
                            config('module.ezzy_categories.table') . '.image',
                            config('module.ezzy_categories.table') . '.description',
                            config('module.ezzy_categories.table') . '.parent_id',
                            config('module.ezzy_categories.table') . '.status',
                            config('module.ezzy_categories.table') . '.created_at',
                            config('module.categories.table') . '.updated_at',
        ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        if ($this->query()->where('name', $input['name'])->first()) {
            throw new GeneralException(trans('exceptions.backend.categories.already_exists'));
        }
        DB::transaction(function () use ($input) {
            $input['status'] = isset($input['status']) ? 1 : 0;
            $input['slug'] = Str::slug($input['name']);
            $input['image'] = $this->uploadImage($input);
            $input['icon'] = $input['icon'];
            $input['created_by'] = access()->user()->id;
            if ($category = Category::create($input)) {
                event(new Created($category));
                return true;
            }

            throw new GeneralException(trans('exceptions.backend.listingcategories.create_error'));
        });
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        if (isset($input['category_image']) && !empty($input['category_image'])) {
            $avatar = $input['category_image'];
            // Build the input for validation
            $fileArray = array('image' => $avatar);
            // Tell the validator that this file should be an image
            $rules = array(
                'image' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
            );
            // Now pass the input and rules into the validator
            $validator = Validator::make($fileArray, $rules);
            // Check to see if validation fails or passes
            if ($validator->fails()) {
                // that the provided file was not an adequate type
                return response()->json(['error' => $validator->errors()->getMessages()], 400);


//                Alert::error(trans('alerts.backend.categories.update_error'))->flash();
//                return redirect()->back();
            } else {
                // Store the File Now
                // read image from temporary file
//                Image::make($file)->resize(300, 200)->save('foo.jpg');
                $fileName = time() . $avatar->getClientOriginalName();
                $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()));
            };
            return $fileName;
        }
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Category $listing_category
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Category $listing_category, array $input) {
        if ($this->query()->where('name', $input['name'])->where('id', '!=', $listing_category->id)->first()) {
            Alert::error(trans('alerts.backend.categories.already_exists'))->flash();
            return redirect()->back();
        }

        DB::transaction(function () use ($listing_category, $input) {
            // Uploading Image
//            if (isset($input['category_image']) && $input->hasFile('category_image')) {
            if (isset($input['category_image'])) {
//                $this->deleteOldFile($listingcategory);
                $input['image'] = $this->uploadImage($input);
            }
            $input['status'] = $input['status'];
            $input['slug'] = Str::slug($input['name']);
            $input['icon'] = $input['icon'];
            $input['updated_by'] = access()->user()->id;
            if ($listing_category->update($input)) {
                event(new Updated($listing_category));
                return true;
            }
            Alert::error(trans('alerts.backend.categories.update_error'))->flash();
            return redirect()->back();
//            throw new GeneralException(trans('exceptions.backend.listingcategories.update_error'));
        });
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Category $category
     * @throws GeneralException
     * @return bool
     */
    public function delete(Category $listing_category) {
        DB::transaction(function () use ($listing_category) {
            if ($listing_category->delete()) {
                event(new Deleted($listing_category));
                return true;
            }
            throw new GeneralException(trans('exceptions.backend.listingcategories.delete_error'));
        });
    }

}

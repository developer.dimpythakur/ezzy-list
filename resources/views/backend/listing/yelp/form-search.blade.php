@push("after_styles")
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
@endpush
<div class="form-group col-7 px-0">
    <div class="card">
        <div class="card-header">
            <h6 class="my-2"><i class="la la-search-location"></i> Search Business</h6>
        </div>
        <div class="card-body listing-form bold-labels">
            <div class="row">
                <div class="form-group col-12 required">
                    {{ Form::label('term', 'Find', ['class' => 'control-label']) }}
                    {{ Form::text('term', null, ['class' => 'form-control box-size',
                    'placeholder' => 'burgers, barbers, spas, handymen…', 'required' => 'required']) }}
                </div>
            </div>
            <div class="row" style="display:none">
                <div class="form-group col-12">
                    {{ Form::label('categories', 'Select Category', ['class' => 'control-label']) }}
                    {{ Form::select('categories', $categories, null, ['class' => 'form-control box-size select2', 'placeholder' => 'Select Category'])}}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12">
                    {{ Form::label('location', 'Select Location', ['class' => 'control-label']) }}
                    {{ Form::select('location', $locations, null, ['class' => 'form-control box-size select2', 'placeholder' => 'Select Location'])}}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-12">
                    <button class="btn btn-primary float-right">
                        <i class="la la-search-location"></i> Search
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@push('after_scripts')
<!-- include select2 js-->
<script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
@if (app()->getLocale() !== 'en')
<script src="{{ asset('packages/select2/dist/js/i18n/' . app()->getLocale() . '.js') }}"></script>
@endif
<script src="{{ asset('packages/tinymce/tinymce.min.js') }}"></script>
<script>

$(document).ready(function () {
//        select2
    $('.select2').select2();
    //tinymce
    var $element = "textarea.form-control.tinymce";
    tinymce.init(jQuery($element).data('options'));

});

</script>
@endpush
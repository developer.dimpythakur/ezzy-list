<?php

namespace App\Models\Listing\Faqs;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use App\Models\Listing\Faqs\Traits\Attribute\Attribute;
use App\Models\Listing\Faqs\Traits\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends BaseModel {

    use ModelTrait,
        SoftDeletes,
        Relationship,
        Attribute {
        // PageAttribute::getEditButtonAttribute insteadof ModelTrait;
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];
    protected $fillable = ['question', 'answer', 'status', 'created_by'];

    /**
     * The default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'created_by' => 1,
    ];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.ezzy_faqs.table');
    }

    /**
     * Return Faq map Table
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getMapTable() {
        return config('module.listing_faqs.table');
    }

    public function addFAQs($question, $answer) {
        return Faq::create([
                    'question' => $question,
                    'answer' => $answer,
                    'status' => 1,
                    'created_by' => 1
                ])->id;
    }

    /**
     * Delete FAQs
     * @param $id
     */
    public function deleteFaqs($id) {
        $faq = Faq::find($id);
        if ($faq):
            $faq->delete();
        endif;
        return;
    }

}

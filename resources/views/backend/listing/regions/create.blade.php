@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.listing.regions.management') . ' | ' . trans('labels.backend.listing.regions.create'))
@section('page-header')
<h1>{{ trans('labels.backend.listing.regions.create') }}</h1>
@endsection

@section("after_styles")
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
@endsection
@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::open(['route' => 'admin.listing-regions.store', 'class' => 'form-horizontal add-region','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'post', 'id' => 'create-listing']) }}
        <div class="card">
            <div class="card-header">
                <h6 class="my-2"><i class="la la-location-arrow"></i> Add New Region</h6>
            </div>
            <div class="card-body listing-form bold-labels">
                @include("backend.listing.regions.form")
            </div>
        </div>
        <div id="saveForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back">Save and back</span>
                </button>
            </div>
            <a href="{{ route('admin.listing-regions.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>

@endsection

<div class="card video-frame card-border-radius shadow mb-5 p-4 border-0">
    <h5 class="card-title mb-4">Video</h5>
    @if($listing)
        @foreach($listing->videos as $video)
            @if($video->url && $video->url !='null' )
                <p>{{ $video->url }}</p>
            @else
                <iframe src="https://www.youtube.com/embed/Ykid7UTXxYI?controls=0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen="">
                </iframe>
            @endif
        @endforeach
    @endif
</div>

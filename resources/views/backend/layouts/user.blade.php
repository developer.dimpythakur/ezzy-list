<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}" dir="{{ config('backend.html_direction') }}">
    <head>
        @include(('backend.layouts.sections.head'))
        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token() ]) !!}
            ;
        </script>
        <style>
            .dataTables_wrapper{
                overflow-x:  hidden
            }
            .form-group.required label:not(:empty)::after {
                content: " *";
                color: rgb(255, 0, 0);
            }
            .user-profile{
                margin-bottom: 1rem;
            }
            .user-profile .image {
                width: 60px;
                height: 60px;
                padding: 0.5rem 0.25rem 0.5rem 0.75rem;
                /*                min-width: 80px;
                                min-height: 80px;
                                max-width: 80px;
                                max-height: 80px;*/
            }
            .user-profile .image img{
                border-radius: 50%!important;
                vertical-align: middle;
                border-style: none;
                max-width: 100%;
                height: 100%;
                width: 100%;
            }

            .profile-box{
                border: 1px solid #f3f6f9;
                padding: 1rem;
                margin-bottom: 1rem;
            }
            .profile-box img.my-profile-img,
            .profile-box img {
                width: 120px;
                height: 120px;
                border-radius: 50%;
            }
            .user-profile  .media-body {
                -webkit-box-flex: 1;
                -ms-flex: 1;
                flex: 1;
            }
            .user-profile .title{
                font-size: 1rem!important;
                color: #000!important;
                display: block!important;
            }
            .user-profile .title-user{
                margin-bottom: 0!important;
                /*font-size: 1.25rem;*/
            }

            /*FAKE FILE*/
            .fake-file-upload {
                border: 2px dashed #ccc;
            }
            .fake-file-upload input {
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                opacity: 0;
            }
            .fake-file-upload:hover{
                border-color: #e53935 !important;
            }



            .card{
                background-color: #fff;
                box-shadow: 1px 0px 4px 0 rgba(68,102,242,.05);
                border-radius: 5px;
                border: none;
            }
        </style>
    </head>

    <body class="{{ config('backend.body_class') }}">
        @include(('backend.layouts.sections.main_header'))
        <div class="app-body" id="app">
            @include('backend.layouts.sections.sidebar')
            <main class="main pt-2">
                @includeWhen(isset($breadcrumbs), ('backend.layouts.sections.breadcrumbs'))
                @yield('header')
                <div class="container-fluid animated fadeIn">
                    @yield('content')
                </div>
            </main>
        </div><!-- ./app-body -->

        <footer class="{{ config('backend.footer_class') }}">
            @include(('backend.layouts.sections.footer'))
        </footer>
        @yield('before_scripts')
        @stack('before_scripts')

        @include(('backend.layouts.sections.scripts'))

        @yield('after_scripts')
        @stack('after_scripts')

    </body>
</html>

<!--   @yield('page-header')
                     Breadcrumbs would render from routes/breadcrumb.php 
                    @if(Breadcrumbs::exists())
                        {!! Breadcrumbs::render() !!}
                    @endif-->
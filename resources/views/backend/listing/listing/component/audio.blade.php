<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-music"></i> Audio</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <!-- Audio -->
        <div class="row mb-3">
            {{ Form::label('audio_url', 'Audio', ['class' => 'control-label col-12 required']) }}
            <div class="form-group col-sm-8">
                <input type="text" class="form-control" placeholder="Audio URL" id="audio_url">
            </div>
            <div class="form-group col-sm-4 text-left">
                <button type="button" class="add-audio btn btn-primary border-transparent border-0">
                    <span class="icon"><i class="la la-plus-square-o"></i></span>
                </button>
            </div>
        </div>
        <div class="row mb-3">
            <div class="selected-audios mt-4">
                <ul class="nostyle d-flex flex-wrap">
                    @if( !empty($listing) && !empty($listing->audios))
                    @foreach ($listing->audios as $audio)
                    <li>
                        <iframe width="100%" scrolling="no" frameborder="no" allow="autoplay"
                                src="{{$audio->url}}"></iframe>
                        <i class="p-1 shadow pointer btn btn-primary border-0 las la-trash-alt remove"></i></li>
                    <input type="hidden" value="{{$audio->url}}"
                           name="audio[{{$audio->filename}}][{{$audio->url}}]">
                    </li>
                    @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <!--/ Audio -->
    </div>
</div>

@push('after_scripts')
<script>
    jQuery(document).ready(function () {
        //-------------------------------
        // Add Audio
        //-------------------------------

        jQuery(document).on('click', ".add-audio", function () {
            var $audioUrl = jQuery("input[id='audio_url']");
            $audioUrl.removeClass('is-invalid');
            if ((typeof $audioUrl.val() === "undefined") || $audioUrl.val() === '') {
                $audioUrl.addClass('is-invalid');
                return false;

            }
            var ifrmaeSRC = '';
            var $src = $audioUrl.val();
            var ifrmaeSRC = `<iframe width="100%" 
                                 scrolling="no" frameborder="no"
                                  allow="autoplay"
                                   src="${$src}"></iframe>`;
            var $input = `<input type="hidden" value="${$audioUrl.val()}" name="audio[custom_url][${$audioUrl.val()}]"/>`
            var $audioDOM = `<li> ${$input} ${ifrmaeSRC}<i class="p-1 remove shadow pointer btn btn-primary border-0 las la-trash-alt"></i></li>`;

            jQuery(".selected-audios").show();
            jQuery(".selected-audios ul").append($audioDOM);
            $audioUrl.val("");
        });
        jQuery(document).on("click", ".selected-audios .remove", function (u) {
            jQuery(this).closest("li").remove();
        });
    });
</script>
@endpush
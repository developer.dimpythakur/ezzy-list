<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-user-secret"></i> Update Password</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="form-group col-sm-12 required">
            {{ Form::label('old_password', trans('validation.attributes.frontend.register-user.old_password'), ['class' => 'control-label']) }}
            {{ Form::input('password', 'old_password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.old_password')]) }}
        </div>
        <div class="form-group col-sm-12 required">
            {{ Form::label('password', trans('validation.attributes.frontend.register-user.new_password'), ['class' => 'control-label']) }}
            {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.new_password')]) }}
        </div>

        <div class="form-group col-sm-12 required">
            {{ Form::label('password_confirmation', trans('validation.attributes.frontend.register-user.new_password_confirmation'), ['class' => 'control-label']) }}
            {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.new_password_confirmation')]) }}
        </div>
    </div>
</div>
@include('frontend.user.components.update-button')
@push("after_scripts")

<script>
    jQuery(document).ready(function () {
    });
</script>
@endpush
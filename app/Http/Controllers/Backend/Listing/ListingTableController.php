<?php

namespace App\Http\Controllers\Backend\Listing;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Listing\Manage;
use App\Repositories\Backend\Listings\ListingsRepository;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use App\Models\Listing\Listing\Listing;

/**
 * Class ListingTableController.
 */
class ListingTableController extends Controller {

    protected $listings;

    /**
     * @param ListingsRepository $listings
     */
    public function __construct(ListingsRepository $listings) {
        $this->listings = $listings;
    }

    /**
     * @param ManageListingRequest $request
     * @return mixed
     * @throws \Exception
     */
    public function __invoke(Manage $request) {
        return Datatables::of($this->listings->getForDataTable($request))
                        ->escapeColumns(['title'])
                        ->addColumn('name', function ($listing) {
                            if ($listing->banner && !is_null($listing->banner)):
                                $banner = '<img style="border-radius: 5px; height: 39px; display: inline-block; margin-right: 10px;" src="' .  $listing->banner . '" class="img-fluid"/>';
                            else:
                                $banner = '<img style="border-radius: 5px; height: 39px; display: inline-block; margin-right: 10px;" src="' . route('frontend.index') . '/img/frontend/listings/coffee-1.jpg" class="img-fluid"/>';
                            endif;
                            return '<a target="_blank" href="' . URL::to('listing/' . $listing->slug) . '">' . $banner . ' ' . $listing->name . '</a>';
                        })
                        ->addColumn('status', function ($listing) {
                            return $listing->status_label;
                        })
                        ->addColumn('category', function ($listing) {
                            if (null == $listing->categories->first()):
                                return 'Name';
                            endif;
                            return '<a target="_blank" href="' . URL::to('listing-category/' . $listing->categories->first()->slug) . '">' . $listing->categories->first()->name . '</a>';
                        })
                        ->addColumn('created_at', function ($listing) {
                            return ($listing->created_at) ? $listing->created_at->toDateString() : null;
                        })
                        ->addColumn('created_by', function ($listing) {
                            return '<a target="_blank" href="' . URL::to('listing-category/' . $listing->created_by) . '">' . $listing->created_by . '</a>';
                        })
                        ->addColumn('actions', function ($listing) {
                            return $listing->action_buttons;
                        })
                        ->filterColumn('name', function($query, $keyword) {
                            $sql = "name like ?";
                            $query->whereRaw($sql, ["%{$keyword}%"]);
                        })
                        ->orderColumn('id', function($query, $keyword) {
                            $query->orderBy('id', "$keyword");
                        })
                        ->make(true);
    }
}

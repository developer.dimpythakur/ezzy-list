<?php

Breadcrumbs::register('admin.listing-tags.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.tags.management'), route('admin.listing-tags.index'));
});

Breadcrumbs::register('admin.listing-tags.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.listing-tags.index');
    $breadcrumbs->push(trans('menus.backend.tags.create'), route('admin.listing-tags.create'));
});

Breadcrumbs::register('admin.listing-tags.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.listing-tags.index');
    $breadcrumbs->push(trans('menus.backend.tags.edit'), route('admin.listing-tags.edit', $id));
});

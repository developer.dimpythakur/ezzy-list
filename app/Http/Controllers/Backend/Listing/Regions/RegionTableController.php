<?php

namespace App\Http\Controllers\Backend\Listing\Regions;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Listing\Regions\RegionRepository;
use App\Http\Requests\Backend\Listing\Regions\Manage;

/**
 * Class RegionTableController.
 */
class RegionTableController extends Controller {

    /**
     * variable to store the repository object
     * @var RegionRepository
     */
    protected $region;

    /**
     * contructor to initialize repository object
     * @param RegionRepository $region ;
     */
    public function __construct(RegionRepository $region) {
        $this->region = $region;
    }

    /**
     * This method return the data of the model
     * @param ManageRegionRequest $request
     *
     * @return mixed
     */
    public function __invoke(Manage $request) {
        return Datatables::of($this->region->getForDataTable())
                        ->escapeColumns(['id'])
                        ->addColumn('image', function ($region) {
                            return $region->image;
                        })
                        ->addColumn('created_at', function ($region) {
                            return Carbon::parse($region->created_at)->toDateString();
                        })
                        ->addColumn('listings', function ($region) {
                            if ($region->locations->count()):
                                return $region->locations->count();
                            else:
                                return 0;
                            endif;
                        })
                        ->addColumn('actions', function ($region) {
                            return $region->action_buttons;
                        })
                        ->filterColumn('name', function($query, $keyword) {
                            $sql = "name like ?";
                            $query->whereRaw($sql, ["%{$keyword}%"]);
                        })
                        ->orderColumn('id', function($query, $keyword) {
                            $query->orderBy('id', "$keyword");
                        })
                        ->make(true);
    }

}

<?php

namespace App\Http\Responses\Backend\Listing\Tags;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable {

    /**
     * @var App\Models\Tag\Tag
     */
    protected $tags;

    /**
     * @param App\Models\Tag\Tag $tags
     */
    public function __construct($tags) {
        $this->tags = $tags;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request) {
        return view('backend.listing.tags.edit')->with([
                    'tags' => $this->tags
        ]);
    }

}

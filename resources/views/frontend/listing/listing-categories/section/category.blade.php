<div class="col-md-4 mt-5 mt-xs">
    <div class="category-boxes text-white text-decoration-none">
        <div class="c-card-f transition-1 img-overlay transition-1 card-border-radius position-relative">
            @if($category->image)
            <img src="{{ Storage::url('images/category/'.$category->image) }}" class="img-fluid"
                 alt="listing">
            @endif
            <div class="slide-data text-center midblock position-absolute">
                <div class="cat-icon">
                    @if($category->icon && $category->icon !='null')
                    <i class="{{ $category->icon }}"></i>
                    @else
                    <i class="las la-shopping-cart"></i>
                    @endif
                </div>
                <h4 class="content-text text-capitalize text-white">
                    <a href="{{ route('frontend.listing-category',$category->slug) }}"
                       class="text-white text-decoration-none">
                        {{$category->name}} </a>
                </h4>
                <span class="content-count badge badge-pill badge-success">
                    {{ categoryCount($category->id) }} Listings
                </span>
            </div>
        </div>
    </div>
</div>
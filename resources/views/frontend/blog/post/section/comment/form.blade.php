<div class="post-comment-form">
    <h5 class="mt-4 mb-3">Add a comment</h5>
    <form autocomplete="off">
        <div class="row">
            <div class="form-group col-md-6 col-sm-12">
                <input type="text" class="form-control rounded-pill" placeholder="Name"
                       name="name">
            </div>
            <div class="form-group col-md-6 col-sm-12">
                <input type="text" class="form-control rounded-pill" placeholder="Email address"
                       name="email">
            </div>
            <div class="form-group col-sm-12">
                                <textarea rows="6" class="form-control resize-none" placeholder="Type your comment"
                                          name="comment"></textarea>
            </div>
            <div class="col-sm-12">
                <input type="submit"
                       class="btn btn-primary border-0 transition-1 text-capitalize shadow"
                       value="Submit Comment" name="submit">
            </div>
        </div>
    </form>
</div>

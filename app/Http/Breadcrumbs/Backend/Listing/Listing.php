<?php

Breadcrumbs::register('admin.listings.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.listings.management'), route('admin.listings.index'));
});

Breadcrumbs::register('admin.listings.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.listings.index');
    $breadcrumbs->push(trans('menus.backend.listings.create'), route('admin.listings.create'));
});

Breadcrumbs::register('admin.listings.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.listings.index');
    $breadcrumbs->push(trans('menus.backend.listings.edit'), route('admin.listings.edit', $id));
});

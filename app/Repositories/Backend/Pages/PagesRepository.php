<?php

namespace App\Repositories\Backend\Pages;

use App\Events\Backend\Pages\PageCreated;
use App\Events\Backend\Pages\PageDeleted;
use App\Events\Backend\Pages\PageUpdated;
use App\Exceptions\GeneralException;
use App\Models\Page\Page;
use App\Repositories\BaseRepository;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Models\Page\Image;
use Cviebrock\EloquentSluggable\Services\SlugService;

/**
 * Class PagesRepository.
 */
class PagesRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Page::class;

    /**
     * File Upload Folder Path
     * @var type 
     */
    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    /**
     * ListingsRepository constructor.
     */
    public function __construct() {
        $this->upload_path = 'images' . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->leftjoin(config('access.users_table'), config('access.users_table') . '.id', '=', config('module.pages.table') . '.created_by')
                        ->select([
                            config('module.pages.table') . '.id',
                            config('module.pages.table') . '.title',
                            config('module.pages.table') . '.page_slug',
                            config('module.pages.table') . '.status',
                            config('module.pages.table') . '.created_at',
                            config('module.pages.table') . '.updated_at',
                            config('access.users_table') . '.first_name as created_by',
        ]);
    }

    /**
     * @param array $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function create($request) {
        $input = $request->except(['_token']);
//        check if name(title) already exists
        if ($this->query()->where('title', $input['title'])->first()) {
            throw new GeneralException(trans('exceptions.backend.pages.already_exists'));
        }
        // Making extra fields
        $input['status'] = isset($input['status']) ? 1 : 0;
        $input['created_by'] = auth()->id();
        $input['featured_image'] = $this->updateImage('featured_image');
        if ($page = Page::create($input)) {
            event(new PageCreated($page));

            return $page;
        }

        throw new GeneralException(trans('exceptions.backend.pages.create_error'));
    }

    /**
     * @param \App\Models\Page\Page $page
     * @param array                 $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function update($page, array $input) {
        if ($this->query()->where('title', $input['title'])->where('id', '!=', $page->id)->first()) {
            throw new GeneralException(trans('exceptions.backend.pages.already_exists'));
        }
        // Making extra fields
        $input['status'] = isset($input['status']) ? 1 : 0;
        $input['updated_by'] = access()->user()->id;
        if (null != $input['featured_image']):
            $input['featured_image'] = $this->updateImage('featured_image', $page->featured_image);
        else:
            unset($input['featured_image']);
        endif;

        if ($page->update($input)) {
            event(new PageUpdated($page));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.pages.update_error'));
    }

    /**
     * @param \App\Models\Page\Page $page
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    public function delete($page) {
        if ($page->delete()) {
            event(new PageDeleted($page));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.pages.delete_error'));
    }

    /**
     * 
     * @param type $image
     * @return type
     */
    public function updateImage($image = 'featured_image', $old = null) {
        if (request()->file($image)) {
            if ($this->storage->exists($old)):
                $this->storage->delete($this->upload_path . $old);
            endif;

            $uploadedFile = request()->file($image);
            $filename = time() . $uploadedFile->getClientOriginalName();
            if ($this->storage->put($this->upload_path . $filename, file_get_contents(request()->file($image)->getRealPath()))):
                return ($this->upload_path) . $filename;
            endif;
        }

        return null;
    }

}

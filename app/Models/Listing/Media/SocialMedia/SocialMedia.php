<?php

namespace App\Models\Listing\Media\SocialMedia;

use App\Models\BaseModel;
use App\Models\Listing\Media\SocialMedia\Traits\Attribute\Attribute;
use App\Models\Listing\Media\SocialMedia\Traits\Relationship\Relationship;
use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialMedia extends BaseModel {

    use ModelTrait,
        SoftDeletes,
        Attribute,
        Relationship {
        
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;
    protected $fillable = ['name', 'network_type', 'network_url', 'status', 'created_by'];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.ezzy_social_networks.table');
    }

    /**
     * Return ListingSocialMedia map Table
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getMapTable() {
        return config('module.listing_social_networks.table');
    }

}

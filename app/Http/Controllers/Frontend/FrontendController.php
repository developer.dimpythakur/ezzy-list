<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
/*
  |--------------------------------------------------------------------------
  | Model
  |--------------------------------------------------------------------------
  |
 */
use App\Models\Blogs\Blog;
use App\Models\Listing\Listing\Listing;
use App\Models\Listing\Categories\Category;
use App\Models\Listing\Region\Region;
use App\Models\Settings\Setting;
/*
  |--------------------------------------------------------------------------
  | Repositories
  |--------------------------------------------------------------------------
  |
 */
use App\Repositories\Frontend\Pages\PagesRepository;
use Illuminate\Http\Request;
use App\Repositories\Frontend\Listing\YelpRepository;
use App\Repositories\Backend\Listings\ListingsRepository;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller {

    /**
     * Get locations
     * @var type 
     */
    public $locations = null;

    /**
     * Posts
     * @var type 
     */
    public $posts = null;

    /**
     * Blog Categories
     * @var type 
     */
    public $categories = null;

    /**
     * Listing Regions
     * @var type 
     */
    public $regions = null;

    /**
     * Settings
     * @var type 
     */
    public $setting = null;

    /**
     * Listing
     * @var type 
     */
    public $listing_top_rated = null;

    /**
      Yelp Repository
     * */
    public $yelp;

    /**
     * Listing Categories
     * @var type 
     */
    public $listing_categories = null;

    public function __construct() {
        $this->listingRepository = new ListingsRepository;
        $this->yelp = new YelpRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {

//        $seeder = new \App\Seeder\Yelp();
//        $seeder->getBusinesses();
//        dd('yp');


        $this->setting = Setting::first();
        $this->categories = Category::withCount('listing')->orderBy('listing_count', 'desc')->limit(6)->get();
        $this->regions = Region::withCount('locations')->orderBy('locations_count', 'desc')->limit(6)->get();
        $this->posts = Blog::inRandomOrder()->limit(6)->where('status', 'Published')->get();
        $this->listing_top_rated = Listing::inRandomOrder()->where('status', 'publish')->limit(5)->get();
        $this->listing_categories = Category::withCount('listing')->orderBy('listing_count', 'desc')->limit(6)->get();
        return view('frontend.index',
                with([
            'setting' => $this->setting,
            'listing_top_rated' => $this->listing_top_rated,
            'listing_categories' => $this->listing_categories,
            'categories' => $this->categories,
            'posts' => $this->posts,
            'regions' => $this->regions,
            'title' => 'Home'
                ])
        );
    }

    /**
     * @param $slug
     * @param PagesRepository $pages
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function showPage($slug, PagesRepository $pages) {
        $result = $pages->findBySlug($slug);

        return view('frontend.pages.index')
                        ->withpage($result);
    }

    /**
     * @param $slug
     * @param PagesRepository $pages
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function pricing() {
        $title = 'Ezzy Pricing';
        return view('frontend.listing.pricing', compact('title'));
    }

}

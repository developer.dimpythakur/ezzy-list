<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
@if (config('backend.meta_robots_content'))<meta name="robots" content="{{ config('backend.meta_robots_content', 'noindex, nofollow') }}"> @endif

<meta name="csrf-token" content="{{ csrf_token() }}" />
<title> @yield('title') {{ isset($title) ? $title.' :: '.config('backend.project_name') : config('backend.project_name') }}</title>
<!--Fevicon-->
<link rel='shortcut icon' type='image/x-icon' href='{{ Storage::url('images/logo/favicon.ico')}}' />
<link rel="icon" type="image/png" href="{{ Storage::url('images/logo/favicon-16x16.png')}}"/>
<script type='application/ld+json'> 
    {
    "@context": "http://www.schema.org",
    "@type": "Restaurant",
    "name": "Ezzy List",
    "url": "http://ezzylist.com",
    "sameAs": [
    "http://ezzylist.com"
    ],
    "logo": "http://ezzylist.com/storage/images/frontend/logo.png",
    "image": "http://ezzylist.com/storage/images/frontend/logo.png"
    }
</script>
@yield('before_styles')
@stack('before_styles')

@if (config('backend.styles') && count(config('backend.styles')))
@foreach (config('backend.styles') as $path)
<link rel="stylesheet" type="text/css" href="{{ asset($path)}}">
@endforeach
@endif

@if (config('backend.mix_styles') && count(config('backend.mix_styles')))
@foreach (config('backend.mix_styles') as $path => $manifest)
<link rel="stylesheet" type="text/css" href="{{ mix($path, $manifest) }}">
@endforeach
@endif

@yield('after_styles')
@stack('after_styles')
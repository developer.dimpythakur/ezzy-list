<div data-id="{{ $listing->id }}"
     class="mt-xs ezzy-single-listing pb-3 listing-single-page"
     data-action="{{ URL::to('listing/'.$listing->slug)  }}"
     data-color="#ee4124"
     data-title="{{ $listing->name }}"
     data-website="{{ $listing->website  }}"
     data-phone="{{ $listing->phone  }}"
     data-friendly-address="{{$listing->location->pluck('address')->first()}}"
     data-address="{{$listing->location->pluck('address')->first()}}"
     data-image="{{ $listing->banner }}"
     data-longitude="{{$listing->location->pluck('longitude')->first()}}"
     data-latitude="{{$listing->location->pluck('latitude')->first()}}"
     data-rating="{{ $listing->averageRating(2) }}"
     data-url="{{ URL::to('listing/'.$listing->slug)  }}"
     data-reviews="{{ $listing->averageRating(2) }}"
     data-categories="Listing,Amazing"
     data-icon="<i class=&quot;{{ $listing->categories->first()->icon  }}&quot;></i>">
    <div class="slide mb-3">
        <div class="et-list-grid-main">
            <div class="listing-thumbnail max-height-thumbnail-300 img-overlay transition-1 overflow-hidden rounded-top position-relative">
                <span class="p-1 position-absolute small business-time right bg-primary text-white pl-2 pr-2">
                    <i class="las la-clock"></i> Closed</span>
                <a href="#" class="et-author-img-box">
                    <img src="{{$listing->owner->picture}}" class="et-author-img"
                         alt="author-img">
                </a>
                @if ($listing->banner && !is_null($listing->banner))
                <img src="{{ $listing->banner }}"
                     class="img-fluid h-100" alt="{{ $listing->name }}">
                @else
                <img src="{{  Storage::url('images/frontend/listings/coffee-1.jpg')}}"
                     class="img-fluid h-100" alt="listing">
                @endif

                <div class="grid-rating-section p-3 pt-0 pb-4">
                    <?php
                    $rating = $listing->averageRating(2);
                    $filterRatings = collect($rating)->filter();
                    ?>
                    <span class="badge listing-rating bg-transparent ml-2"
                          data-rating="{{ (count($filterRatings))?$listing->averageRating(2):'No ratings' }}">
                    </span>
                    <div class="star-rating">
                        <span class="et-view-count">
                            <i class="las la-eye"></i>  {{  views($listing)->count() }}
                        </span>
                        <span class="et-location-icon"><i class="las la-map-marker-alt"></i></span>
                        <span 
                            title="Save listing" 
                            data-title="Save listing" 
                            data-id="{{ $listing->id}}"
                            data-url="{{ route('frontend.listing-bookmark')}}"
                            class="et-save-icon {{ is_bookmarked($listing)}}"><i class="las la-heart"></i></span>
                    </div>
                </div>
            </div>
            <div class="et-list-wrapper shadow rounded-bottom p-3 bg-white">
                <a href="{{  route('frontend.listing.show',$listing->slug) }}"
                   class="text-decoration-none d-block mt-2 text-reset text-capitalize">
                    <h3>{{ $listing->name }}
                        <i class="las la-check-circle"
                           data-toggle="tooltip"
                           data-placement="top"
                           title="{{ $listing->name }}"
                           data-original-title="Claimed"></i>
                    </h3>
                </a>
                <p class="mb-3">{{ substr($listing->description,0,50)  }}</p>
                <ul class="et-grid-info">
                    @if('' != $listing->location->pluck('address')->first())
                    <li>
                        <i class="las la-map-marker-alt primary-icon"></i>
                        <p>{{$listing->location->pluck('address')->first()}}&nbsp;</p>
                    </li>
                    @endif
                    @if('' != ($listing->phone))
                    <li>
                        <i class="las la-phone primary-icon"></i>
                        <p>{{$listing->phone}}&nbsp;</p>
                    </li>
                    @endif
                    @if('' != ($listing->website))
                    <li>
                        <i class="las la-link primary-icon"></i>
                        <p>{{ ($listing->website)  }}</p>
                    </li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
</div>
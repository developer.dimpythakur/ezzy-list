<div class="form-group">
    <input type="hidden" name="save_action" value="save_and_back">
    <div class="btn-group" role="group">
        <button id="update-profile" type="submit" class="btn btn-success">
            <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
            <span data-value="update">Update</span>
        </button>
    </div>
</div>
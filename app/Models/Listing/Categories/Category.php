<?php

namespace App\Models\Listing\Categories;

use App\Models\BaseModel;
use App\Models\Listing\Categories\Traits\Attribute\Attribute;
use App\Models\Listing\Categories\Traits\Relationship\Relationship;
use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
//Slugable
use Cviebrock\EloquentSluggable\Sluggable;

//Category
class Category extends BaseModel {

    use ModelTrait,
        SoftDeletes,
        Attribute,
        Relationship {
        // BlogCategoryAttribute::getEditButtonAttribute insteadof ModelTrait;
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * Fillable Arrray
     * @var string[]
     */
    protected $fillable = ['name', 'slug', 'icon', 'image', 'description', 'status', 'created_by'];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.ezzy_categories.table');
    }

    /**
     * Return Category map Table
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getMapTable() {
        return config('module.listing_categories.table');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function scopeActive($query) {
        return $query->where('status', 1);
    }

}

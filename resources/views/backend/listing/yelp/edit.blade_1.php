@extends ('backend.layouts.listing')
@section ('title', trans('labels.backend.pages.management') . ' | ' . trans('labels.backend.pages.create'))
@section('page-header')
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<section class="content-header">
    <div class="container-fluid">
        <div class="row page-titles mb-2">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Listings</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Listing New</li>
                    </ol>
                    @permission('create-page')
                    <a href="{{route('admin.listings.index')}}"
                       class="btn btn-info d-none d-lg-block ml-2 px-2 py-1">
                        <i class="las la-eye"></i> View All
                    </a>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

@endsection
@section('content')

<section class="container-fluid">
    <div class="row">
        <section class="col-10" id="messages">
            @include('includes.partials.messages')
        </section>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card-body pad table-responsive p-2">
                {{ Form::model($listing, ['route' => ['admin.listings.update', $listing], 'class' => 'form-horizontal','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-listing']) }}
                <div class="row">
                    <div class="col-10">
                        <div class="card -basic-info pt-4 pr-4 pb-2 mb-3">
                            <div class="form-group card p-3 mb-2">
                                {{ Form::label('name', trans('validation.attributes.backend.pages.title'), ['class' => 'col-lg-2 control-label required']) }}
                                <div class="col-lg-10">
                                    {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.pages.title'), 'required' => 'required']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->

                            <div class="form-group card p-3 mb-2">
                                {{ Form::label('tagline', trans('validation.attributes.backend.listings.tagline'), ['class' => 'col-lg-2 control-label required']) }}
                                <div class="col-lg-10">
                                    {{ Form::text('tagline', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.listings.tagline'), 'required' => 'required']) }}
                                </div><!--col-lg-10-->
                            </div><!--form control-->
                            <div class="form-group card p-3 mb-2">
                                {{ Form::label('description', trans('validation.attributes.backend.listings.description'), ['class' => 'col-lg-2 control-label required']) }}
                                <div class="col-lg-10">
                                    {{ Form::textarea('description', null,['class' => 'form-control box-size d-block', 'placeholder' => trans('validation.attributes.backend.pages.description')]) }}
                                </div><!--col-lg-3-->
                            </div><!--form control-->
                        </div>

                        <div class="add-listing-section row  basic_info ">
                            <div class="col-6">
                                @include('backend.listings.listing.section.categories')
                            </div>
                            <div class="col-6">
                                @include('backend.listings.listing.section.tags')
                            </div>
                        </div>
                        <div class="add-listing-section row  basic_info ">
                            <div class="col-4">
                                @include('backend.listings.listing.section.website')
                            </div>
                            <div class="col-4">
                                @include('backend.listings.listing.section.phone')
                            </div>
                            <div class="col-4">
                                @include('backend.listings.listing.section.email')
                            </div>
                        </div>

                        @include('backend.listings.listing.section.social-media')
                        @include('backend.listings.listing.section.locations')
                        @include('backend.listings.listing.section.faq')
                        @include('backend.listings.listing.section.timings')
                        @include('backend.listings.listing.section.audio')
                        @include('backend.listings.listing.section.video')
                        @include('backend.listings.listing.section.featured-image')
                        @include('backend.listings.listing.section.gallery')
                    </div>
                </div>
                <div class="form-group mt-3">
                    {{ Form::label('status', trans('validation.attributes.backend.pages.is_active'), ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        <div class="control-group">
                            <label class="control control--checkbox">
                                {{ Form::checkbox('status', 1, true) }}
                                <div class="control__indicator"></div>
                            </label>
                        </div>
                    </div><!--col-lg-3-->
                </div><!--form control-->
                <div class="form-group mt-3">
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.listings.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(trans('buttons.general.crud.update'), ['class' => 'btn btn-primary btn-md']) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@endsection
@section("after-scripts")
{{ Html::script(('js/dashboard/listing/tags-input.js')) }}
{{ Html::script(('js/dashboard/listing/add-listing.js')) }}
{{ Html::script(('js/dashboard/listing/listing-location.js')) }}
<script type="text/javascript">
    Backend.Pages.init('{{ config('locale.languages.' . app()->getLocale())[1] }}');</script>
@if ($tags)
<script>
    var tags = [
            @foreach ($tags as $tag)
    {
    tag: "{{$tag}}"
    },
            @endforeach
    ];</script>
@endif


<script type="text/javascript">
    jQuery(document).ready(function () {
    var map = new GMaps({
    el: '#map',
            lat: - 12.043333,
            lng: - 77.028333
    });
    jQuery(document).on('change', '#location-address input', function (e) {
    var address = jQuery(this).val();
    GMaps.geocode({
    address: address,
            callback: function (results, status) {
            if (status == 'OK') {
            var latlng = results[0].geometry.location;
            map.setCenter(latlng.lat(), latlng.lng());
            map.addMarker({
            lat: latlng.lat(),
                    lng: latlng.lng()
            });
            }
            }
    });
    });
    $.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
    jQuery(document).on('change', '#location-address inputssss', function (e) {
    e.preventDefault();
    console.log(jQuery(this).val());
    var URL = "{{ route('admin.locations.search') }}"
            $.ajax({
            type: 'POST',
                    url: URL,
                    data: {address: jQuery(this).val()},
                    success: function (data) {
                    alert(data.success);
                    }
            });
    });
    });</script>


<script type="text/javascript">
    $(function () {
    jQuery('.open-time,.close-time').datetimepicker({
    format: 'HH:mm',
            debug: true
    });
    });
</script>

@endsection
<?php

namespace App\Http\Controllers\Backend\Listing\Categories;

use App\Models\Listing\Categories\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
//Response
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Listing\Categories\Create as CreateResponse;
use App\Http\Responses\Backend\Listing\Categories\Edit as EditResponse;
//Repositories
use App\Repositories\Backend\Listing\Categories\CategoryRepository;
//Requests
use App\Http\Requests\Backend\Listing\Categories\Manage;
use App\Http\Requests\Backend\Listing\Categories\Create;
use App\Http\Requests\Backend\Listing\Categories\Store;
use App\Http\Requests\Backend\Listing\Categories\Edit;
use App\Http\Requests\Backend\Listing\Categories\Update;
use App\Http\Requests\Backend\Listing\Categories\Delete;
//Alert
use Prologue\Alerts\Facades\Alert;

/**
 * CategoryController
 */
class CategoryController extends Controller {

    /**
     * variable to store the repository object
     * @var CategoryRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param CategoryRepository $repository;
     */
    public function __construct(CategoryRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Listing\Categories\Manage  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(Manage $request) {
        return new ViewResponse('backend.listing.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateNamespace  $request
     * @return \App\Http\Responses\Backend\Listings\Categories\CreateResponse
     */
    public function create(Create $request) {
        return new CreateResponse('backend.listing.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(Store $request) {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.listing-categories.index'), trans('alerts.backend.categories.created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Listing\Categories\Category  $category
     * @param  EditNamespace  $request
     * @return \App\Http\Responses\Backend\Listings\Categories\EditResponse
     */
    public function edit(Category $listing_category, Edit $request) {
        return new EditResponse($listing_category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNamespace  $request
     * @param  App\Models\Categories\Category  $category
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Update $request, Category $listing_category) {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update($listing_category, $input);
        //return with successfull message
        return new RedirectResponse(route('admin.listing-categories.index'), trans('alerts.backend.categories.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteNamespace  $request
     * @param  App\Models\Categories\Category  $category
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Category $listing_category, Delete $request) {
        //Calling the delete method on repository
        $this->repository->delete($listing_category);
        //returning with successfull message
        return new RedirectResponse(route('admin.listing-categories.index'), trans('alerts.backend.categories.deleted'));
    }

}

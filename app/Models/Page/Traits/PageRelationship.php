<?php

namespace App\Models\Page\Traits;

use App\Models\Access\User\User;
use App\Models\Page\File;

trait PageRelationship {

    public function owner() {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function featured_image() {
        return $this->hasOne(File::class);
    }

}

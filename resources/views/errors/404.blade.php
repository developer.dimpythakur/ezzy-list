@extends('frontend.layouts.404')
@section('content')
<section class="et-not-found text-capitalize p-5">
    <div class="container pt-5 pb-5">
        <div class="row">
            <div class="col-md-12 col-sm-6">
                <h1 class="text-primary mb-3">{{ trans('http.404.title') }}</h1>
                <h1 class="display-1 mb-3">404</h1>
                <p>Unfortunately the item you were looking for could not be found.</p>
                <form style="display: none">
                    <div class="input-wrap search mb-3 mt-2 position-relative d-inline-block">
                        <input type="text" class="form-control thick-border" placeholder="Search" name="email">
                        <div class="submit-btn rounded-right bg-primary transition-1 text-body position-absolute ml-auto  pointer-event d-flex justify-content-center align-items-center">
                            <i class="las la-angle-right text-white" aria-hidden="true"></i>
                            <input type="submit" class="btn transition-1 field-btn position-absolute" value=""
                                   name="search">
                        </div>
                    </div>
                </form>
                <a href="{{ URL::to('/') }}" class="btn btn-primary border-transparent border-0">
                    <i class="las la-arrow-left" aria-hidden="true"></i>
                    Home
                </a>
            </div>
        </div>
    </div>
</section>
@endsection

<?php

namespace App\Models\ListingMap;

use App\Models\BaseModel;

class MapGallery extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.listing_media_gallery.table');
    }

}

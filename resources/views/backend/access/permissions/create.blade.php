@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.listings.management') . ' | ' . trans('labels.backend.blogs.create'))

@section('page-header')
<h1>
    {{ trans('labels.backend.access.permissions.management') }}
    <small>{{ trans('labels.backend.access.permissions.create') }}</small>
</h1>
@endsection

@php
$breadcrumbs = [
'Dashboard' => url(config('backend.route_prefix'), 'dashboard'),
trans('menus.backend.access.permissions.all') => route('admin.access.permission.index'),
trans('menus.backend.access.permissions.create') => false,
];
@endphp


@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.access.permissions.management') }}</span>
    </h2>
</div>
@endsection

@section('content')
<section class="row">

    <div class="col-md-8 bold-labels">
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::open(['route' => 'admin.access.permission.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission']) }}
        @include("backend.access.permissions.form")
        <div id="saveForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back">Save and back</span>
                </button>
            </div>
            <a href="{{ route('admin.access.permission.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection
@section("after_scripts")
@endsection

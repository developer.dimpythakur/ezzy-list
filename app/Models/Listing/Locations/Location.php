<?php

namespace App\Models\Listing\Locations;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Listing\Locations\Traits\Attribute;
use App\Models\Listing\Locations\Traits\Relationship;
//basemodel
use App\Models\BaseModel;

class Location extends BaseModel {

    use ModelTrait,
        Attribute,
        Relationship {
        // LocationAttribute::getEditButtonAttribute insteadof ModelTrait;
    }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table;

    /**
     * Mass Assignable fields of model
     * @var array
     */

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [
    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.ezzy_locations.table');
    }

    /**
     * Listing Locations
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getMapTable() {
        return config('module.listing_locations.table');
    }

}

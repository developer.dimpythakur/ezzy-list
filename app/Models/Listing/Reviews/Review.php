<?php

namespace App\Models\Listing\Reviews;

use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Listing\Reviews\Traits\Attribute;
use App\Models\Listing\Reviews\Traits\Relationship;
use App\Models\Listing\Reviews\Generator\ReviewFactory;

class Review extends Model { 

    use ModelTrait,
        Attribute,
        Relationship {
        // ListingReviewAttribute::getEditButtonAttribute insteadof ModelTrait;
    }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table;

    /**
     * Mass Assignable fields of model
     * @var array
     */
    protected $fillable = ['review_id', 'user_id', 'review_type', 'stars', 'useful', 'funny', 'text', 'cool', 'rating', 'ip', 'status'];

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [
    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [
        'id'
    ];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.ezzy_reviews.table');
    }

    /**
     * Return ListingMedia map Table
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getMapTable() {
        return config('module.listing_reviews.table');
    }

    /**
     * Writes a review to the database.
     * @param Model $model
     * @param null $reviewArray
     * @param Model|null $author
     */
    public static function write(Model $model, $data = null, Model $author = null) {
        ReviewFactory::create($model, $data, $author);
    }

    
    public function getRatings($business_id){
        
    }
}

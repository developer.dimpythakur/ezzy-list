<li data-url="{{ collect(request()->segments())->last() }}"
    class="nav-item-frontend">  
    <a href="{{ getRouteUrl($item->url, $item->url_type) }}">
        <i class="{{ @$item->icon }}"></i> {{ $item->name }}
    </a>
</li>
<div class="et-list-grid-main">
    <div class="listing-thumbnail img-overlay transition-1 overflow-hidden rounded-top position-relative">
        @if(timingStatus($listing->timings))
        @if(timingStatus($listing->timings) && timingStatus($listing->timings) == 'open')
        <span class="p-1 position-absolute small business-time right bg-success text-white pl-2 pr-2">
            <i class="las la-clock"></i> Open Now
        </span>
        @else
        <span class="p-1 position-absolute small business-time right bg-primary text-white pl-2 pr-2"><i
                class="las la-clock"></i> Closed</span>
        @endif
        @else
        <span class="p-1 position-absolute small business-time right bg-primary text-white pl-2 pr-2">
            <i class="las la-clock"></i> Closed</span>
        @endif

        @if($listing->owner->picture)
        <a href="{{ route('frontend.listing.show',$listing->slug) }}" class="et-author-img-box">
            <img src="{{ $listing->owner->picture}}" class="et-author-img"
                 alt="{{ $listing->name }}">
        </a>
        @endif

        @if ($listing->banner && !is_null($listing->banner))
        <img src="{{ Storage::url('images/listing/'.$listing->banner) }}"
             class="img-fluid" alt="{{ $listing->name }}">
        @else
        <img src="{{route('frontend.index')}}/img/frontend/listings/coffee-1.jpg"
             class="img-fluid" alt="listing">
        @endif
        <div class="grid-rating-section p-3 pt-0 pb-4">
            <div class="star-rating pt-1">
                <span class="et-view-count">
                    <i class="las la-eye"></i>
                    {{  views($listing)->count() }}
                </span>
                <span class="et-location-icon"><i class="las la-map-marker-alt"></i></span>
                <span class="et-save-icon"><i class="las la-heart"></i></span>


                <span class="badge listing-rating category-listing-single badge-secondary ml-2"
                      data-rating="{{ $listing->averageRating(2) }}">
                </span>
            </div>
        </div>
    </div>
    <div class="et-list-wrapper shadow rounded-bottom p-3 bg-white">
        <a href="{{ route('frontend.listing.show',$listing->slug) }}"
           class="text-decoration-none d-block mt-2 text-reset text-capitalize">
            <h3>{{ $listing->name  }}
                <i class="las la-check-circle"
                   data-toggle="tooltip"
                   data-placement="top"
                   title=""
                   data-original-title="Claimed"></i>
            </h3>

        </a>
        @if ($listing->categories && count($listing->categories))
        @if($listing->categories)
        @foreach($listing->categories as $categorySingle)
        @if(strtolower($category->name) == strtolower($categorySingle->name))
        <a href="{{ route('frontend.listing-category',$categorySingle->slug) }}"
           class="p-2 small business-category-city pl-2 pr-2">
            {{--                            <i class="primary-icon {{ $categorySingle->icon  }}"></i>--}}
            {{--                            {{$categorySingle->name}}--}}
        </a>
        @endif
        @endforeach
        @endif
        @endif

        <p class="mb-3 mt-3">{{ substr($listing->description,0,100)  }}</p>
        <ul class="et-list-info">
            @if($listing->location)
            <li>
                <i class="las la-map-marker-alt primary-icon"></i>
                <p>{{$listing->location->pluck('address')->first()}}</p>
            </li>
            @endif
            <li>
                <i class="las la-phone primary-icon"></i>
                <p>{{ ($listing->phone)  }}</p>
            </li>
            <li>
                <i class="las la-link primary-icon"></i>
                <p>{{ ($listing->website)  }}</p>
            </li>
        </ul>
    </div>
</div>
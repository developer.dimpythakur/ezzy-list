<?php

namespace App\Seeder;

use App\Http\Requests\Request;
use App\Models\Access\User\User;
use App\Repositories\Backend\Listings\ListingRelationshipManager as Relations;
use Illuminate\Support\Facades\DB;
use App\Models\Listing\Categories\Category;
use Illuminate\Support\Facades\Storage;
use App\Models\Listing\Listing\Listing;
use App\Models\Listing\Tags\Tag;
use App\Models\Listing\Region\Region;

class Repository {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $storage;
    protected $categories = null;
    protected $tags = null;
    protected $social_networks = null;
    protected $faqs = null;
    protected $timings = null;
    protected $audio = null;
    protected $video = null;
    protected $gallery = null;
    protected $locations = null;
    protected $type = 'listing';
    protected $dumPath = '';
    protected $locationsArray;
    protected $categoriesArray;
    protected $tagsArray;

    public function __construct() {
        $this->upload_path = 'images' . DIRECTORY_SEPARATOR . 'listing' . DIRECTORY_SEPARATOR;
        $this->gallery_path = 'images' . DIRECTORY_SEPARATOR . 'listing/gallery' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
        $this->relation = new Relations($this->storage, $this->upload_path, $this->gallery_path);
        $this->runBefore();
    }

    public function runBefore() {

        if (Category::count() <= 0) {
            DB::table('ezzy_categories')->insert(config('listing.categories'));
        }
        if (Tag::count() <= 0) {
            DB::table('ezzy_tags')->insert(config('listing.tags'));
        }
        if (Region::count() <= 0) {
            DB::table('ezzy_regions')->insert(config('listing.regions'));
        }
    }

    /**
     * Create New Listing
     * @param type $request
     * @return type
     */
    public function make($request) {
        $request = (object) $request;
        if (isset($request->id) && $request->id !== '') {
            $this->type = 'yelp';
            $this->business_id = (isset($request->id) && $request->id !== '') ? $request->id : null;
        }
        if (Listing::where('name', $request->name)->where('phone', $request->phone)->first()) {
            return [
                'status' => 0,
                'type' => 'error',
                'message' => 'Listing already registered with ' . $request->name . ' and ' . $request->phone
            ];
        }
        if (null != ($request->tags)) {
            $this->tags = (array_filter($request->tags));
        }
        if (null != $request->categories) {
            $this->categories = (array_filter($request->categories));
        }

        $this->social_networks = $this->relation->addSocialMedia($request->social_media);
        //check if faqs
        if ($request->faq):
            $this->faqs = $this->relation->createFaqs($request->faq);
        endif;
        //check if timings
        if ($request->timings):
            $this->timings = $this->relation->createTimings($request->timings);
        endif;

        //check if audio
        if ($request->audio):
            $this->audio = $this->relation->createMediaAudio($request->audio);
        endif;
        //check if video
        if ($request->video):
            $this->video = $this->relation->createMediaVideo($request->video);
        endif;
        //check if location
        if ($request->location):
            $this->location = $this->relation->manageLocations($request->location);
        endif;
        //Listing Media Gallery Yelp
        if ($this->type === 'yelp'):
            $this->gallery = $this->relation->mediaGalleryYelp($request->listing_gallery);
        else:
            $this->gallery = $this->relation->mediaGallery($request, 'listing_gallery');
        endif;

        DB::transaction(function () use ($request) {
            // upload listing banner
            if ($this->type == 'yelp'):
                $banner = $request->banner;
            else:
                $banner = $this->relation->uploadImage($request, 'banner');
            endif;
            //create new listing
            $listing = Listing::create([
                        'name' => $request->name,
                        'tagline' => $request->tagline,
                        'business_id' => $this->business_id,
                        'description' => $request->description,
                        'phone' => $request->phone,
                        'website' => $request->website,
                        'email' => $request->email,
                        'type' => $this->type,
                        'banner' => $banner,
                        'status' => 'publish',
                        'created_by' => (access()->user() !== null) ? access()->user()->id : 1
            ]);
            if ($listing) {
                $this->updateRelations($request, $listing);
                if (isset($request->reviews)):
                    $this->reviews($listing, $request->reviews);
                endif;
            }
        });
    }

    /**
     * Add all related items to the listing
     * @param Request $request
     * @param Listing $listing
     */
    public function updateRelations($request, Listing $listing) {
        // Inserting associated category's id in mapper table
        if (!is_null($this->categories) && count($this->categories)) {
            $listing->categories()->sync($this->categories);
        }

        // Inserting associated tag's id in mapper table
        if (!is_null($this->tags) && count($this->tags)) {
            $listing->tags()->sync($this->tags);
        }
        // Inserting FAQs
        if (!is_null($this->faqs) && count($this->faqs)) {
            $listing->faqs()->sync($this->faqs);
        }

        // Inserting FAQs
        if (!is_null($this->social_networks) && count($this->social_networks)) {
            $listing->social_network()->sync($this->social_networks);
        }
        // Inserting Timings
        if (!is_null($this->timings) && count($this->timings)) {
            $listing->timings()->sync($this->timings);
        }

        // Inserting Audio
        if (!is_null($this->audio) && count($this->audio)) {
            $listing->audios()->sync($this->audio);
        }
        // Inserting Video
        if (!is_null($this->video) && count($this->video)) {
            $listing->videos()->sync($this->video);
        }
        // Insert Gallery
        if (!is_null($this->gallery) && count($this->gallery)) {
            $listing->gallery()->sync($this->gallery);
        }
        // Insert Gallery
        if (!is_null($this->location)) {
            $listing->location()->sync($this->location);
        }
    }

    /**
     * 
     * @param type $listing
     * @param type $reviews
     * @return boolean
     */
    public function reviews($listing, $reviews) {
        $user = User::first();
        foreach ($reviews as $key => $review):
            $listing->rating($review, $user);
        endforeach;
    }

}

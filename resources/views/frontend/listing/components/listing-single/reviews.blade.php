<?php
$ratings = $listing->getAllRatings($listing->id);
?>
<div class="card card-border-radius customer-reviews shadow mb-5 p-4 border-0">
    @if($ratings->count())
    <h5 data-review="{{ route('frontend.reviews',$listing->id) }}"
        data-review="{{ route('frontend.reviews',$listing->id) }}"
        id="review-title"
        class="card-title mb-4">Customer Reviews</h5>
    <!-- Comments -->
    <div class="post-comments">
        @foreach ($ratings as $single)
        <!-- Comment -->
        <div class="user-review border-bottom pb-3">
            <div class="d-md-flex d-sm-block mb-2">
                <div class="commenter-thumbnail">
                    <img src="{{$single->author->picture}}" alt="comment"
                         class="img-fluid rounded-pill shadow">
                </div>
                <div class="comment-content mb-0">
                    <h6 style="display: none">{{ $single->author->first_name }} {{$single->author->last_name}} -
                        <span class="text-muted review-date">
                            {{ \Carbon\Carbon::parse($single->created_at)->format('d M Y H:i')}}
                        </span>
                    </h6>
                    <div class="d-inline-block mb-1 listing-rating"
                         data-rating="{{ (0<($single->rating))?$single->rating:'No ratings' }}">
                    </div>
                    <div class="w-100">
                        <h4 class="my-2">{{ $single->title }}</h4>
                        <p>{{ $single->body }}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Comment -->
        @endforeach
    </div>
    @else
    <h5 class="card-title mb-4">Listing Reviews</h5>
    <div class="card-body">
        <div class="w-100 p-4">
            <div class="p-3 alert alert-danger mb-0">
                <p class="m-0"><i class="la la-comment"></i> No Reviews</p>
            </div>
        </div>
    </div>
    @endif
    @include('frontend.listing.components.listing-single.review-add')
</div>
@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.pages.management') . ' | ' . trans('labels.backend.pages.create'))

@section('page-header')
<h1>{{ trans('labels.backend.pages.management') }}</h1>
@endsection

@php
$defaultBreadcrumbs = [
'Dashboard' => url(config('backend.route_prefix'), 'dashboard'),
trans('menus.backend.pages.all') => route('admin.pages.index'),
trans('menus.backend.pages.create') => false,
];

// if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
$breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp


@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.pages.management') }}</span>
    </h2>
</div>
@endsection


@section("after_styles")
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
@endsection

@section('content')


<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.pages.section.grouped_errors'))
        {{ Form::open(['route' => 'admin.pages.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-permission', 'enctype'=>'multipart/form-data']) }}
        @include("backend.pages.form")
        <div id="saveForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back">Save and back</span>
                </button>
            </div>
            <a href="" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>



@endsection
@section("after_scripts")
<script type="text/javascript">
//    Backend.Pages.init('{{ config('locale.languages.' . app()->getLocale())[1] }}');
</script>
<!--<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>-->
<script>
//  tinymce.init({selector: 'textarea'});
</script>


<script src="{{ asset('js/backend/ckeditor/ckeditor.js') }}"></script>
<script language="JavaScript">
    function bpFieldInitCKEditorElement(element) {
// remove any previous CKEditors from right next to the textarea
// element.siblings("[id^='cke_editor']").remove();
// trigger a new CKEditor
        element.ckeditor(element.data('options'));
    }


    CKEDITOR.editorConfig = function (config) {
        config.uiColor = '#AADC6E';
        config.language = 'zh';
        config.skin = 'kama';
        config.width = 700;
        config.height = 300;

        config.toolbar = 'MyBasic';

        config.toolbar_MyFull = [
            {name: 'document', items: ['Source', '-', 'Save', 'NewPage', 'DocProps', 'Preview', 'Print', '-', 'Templates']},
            {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
            {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'SpellChecker', 'Scayt']},
            {name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField']},
            {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
            {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
            {name: 'colors', items: ['TextColor', 'BGColor']},
            '/',
            {name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']},
            {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
            {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
        ];

        config.toolbar_MyBasic = [
            {name: 'document', items: ['Source', 'NewPage', 'Preview']},
            {name: 'basicstyles', items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat']},
            {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
            {name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt']},
            '/',
            {name: 'styles', items: ['Styles', 'Format']},
            {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote']},
            {name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']},
            {name: 'links', items: ['Link', 'Unlink', 'Anchor']},
            {name: 'tools', items: ['Maximize']}
        ];
    };





    CKEDITOR.replace('descriptiosn', {
        toolbar: 'ToolbarA'
    });

//    CKEDITOR.replace('description');

    console.log(CKEDITOR);
//
//    ClassicEditor
//            .create(document.querySelector('#description'))
//            .then(editor => {
//                console.log(editor);
//            })
//            .catch(error => {
//                console.error(error);
//            });
</script>

@endsection

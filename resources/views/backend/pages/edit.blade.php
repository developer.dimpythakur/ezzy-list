@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.pages.management') . ' | ' . trans('labels.backend.pages.edit'))

@section('page-header')
<h1>{{ trans('labels.backend.menus.management') }}</h1>
@endsection

@php
$defaultBreadcrumbs = [
'Dashboard' => url(config('backend.route_prefix'), 'dashboard'),
trans('menus.backend.pages.all') => route('admin.pages.index'),
trans('menus.backend.pages.edit') => false,
];

// if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
$breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
@endphp


@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.pages.management') }}</span>
        <small id="datatable_info_stack">
            <a href="{{route('admin.pages.create')}}" class="btn btn-primary" data-style="zoom-in">
                <span class="ladda-label"><i class="la la-plus"></i> {{trans('menus.backend.pages.create')}}</span>
            </a>
        </small>
    </h2>
</div>
@endsection


@section('content')

<!-- THE ACTUAL CONTENT -->
<section class="row">
    <div class="col-md-8 bold-labels">
        @include(('backend.pages.section.grouped_errors'))
        {{ Form::model($page, ['route' => ['admin.pages.update', $page], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role']) }}
        @include("backend.pages.form")
        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span>{{ trans('buttons.general.crud.update') }}</span>
                </button>
            </div>
            <a href="" class="btn btn-default"><span class="la la-ban"></span> &nbsp;{{ trans('buttons.general.cancel') }}</a>
        </div>
        {{ Form::close() }}
    </div><!-- /. -->
</section>
@endsection
@section("after-scripts")
<script type="text/javascript">
    Backend.Pages.init('{{ config('locale.languages.' . app()->getLocale())[1] }}');
</script>
@endsection
<!-- CKeditor -->
@php
$extra_plugins = "embed,widget";

$defaultOptions = [
"filebrowserBrowseUrl" => url('elfinder/ckeditor'),
"extraPlugins" => $extra_plugins,
"embed_provider" => "//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}",
];

$options = array_merge($defaultOptions, $field['options'] ?? []);
@endphp
<textarea
    name="name"
    data-init-function="bpFieldInitCKEditorElement"
    data-options="{{ trim(json_encode($options)) }}"
    ></textarea>




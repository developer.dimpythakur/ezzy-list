@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.faqs.management'))
@section('page-header')
    <h1>{{ trans('labels.backend.faqs.management') }}</h1>
@endsection

@section('content')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.faqs.management') }}</h3>

            <div class="box-tools pull-right">
                @include('backend.faqs.partials.faqs-header-buttons')
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive data-table-wrapper">
                <table id="faqs-table" class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>{{ trans('labels.backend.faqs.table.question') }}</th>
                            <th>{{ trans('labels.backend.faqs.table.answer') }}</th>
                            <th>{{ trans('labels.backend.faqs.table.status') }}</th>
                            <th>{{ trans('labels.backend.faqs.table.updatedat') }}</th>
                            <th>{{ trans('labels.general.actions') }}</th>
                        </tr>
                    </thead>
                    <thead class="transparent-bg"></thead>
                </table>
            </div><!--table-responsive-->
        </div><!-- /.box-body -->
    </div><!--box-->
@endsection
@section('after_scripts')
<script type="text/javascript" src="{{ asset('js/backend/datatables.js') }}"></script>
<script>
$(function() {
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});
      var dataTable = $('#faqs-table').dataTable({
processing: true,
        serverSide: true,
        ajax: {
         url: '{{ route("admin.faqs.get") }}',
                type: 'post'
        },
      columns: [
                    {data: 'question', name: '{{config('module.faqs.table')}}.question'},
                    {data: 'answer', name: '{{config('module.faqs.table')}}.answer'},
                    {data: 'status', name: '{{config('module.faqs.table')}}.status'},
                    {data: 'updated_at', name: '{{config('module.faqs.table')}}.updated_at'},
                    {data: 'actions', name: 'actions', searchable: false, sortable: false}
                ],
        order: [[1, "asc"]],
        searchDelay: 500,
        dom: "<'row hidden'<'col-sm-6 hidden-xs'i><'col-sm-6 hidden-print'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row mt-2 '<'col-sm-6 col-md-4'l><'col-sm-2 col-md-4 text-center'B><'col-sm-6 col-md-4 hidden-print'p>>",
        buttons: {
        buttons: [
        { extend: 'copy', className: 'copyButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
        { extend: 'csv', className: 'csvButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
        { extend: 'excel', className: 'excelButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
        { extend: 'pdf', className: 'pdfButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
        { extend: 'print', className: 'printButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }}
        ]
        },
        language: {
        @lang('datatable.strings')
        }
});
Backend.DataTableSearch.init(dataTable);
});
</script>

@endsection
@extends ('frontend.layouts.user')

@section ('title', trans('labels.frontend.listing.management') . ' | ' . trans('labels.frontend.listing.create'))

@section('page-header')
<h1>{{ trans('labels.frontend.listing.edit') }}</h1>
@endsection
@section('after_styles')
<style>
    #updateForm{
        /*background: #fff;*/
        position: fixed;
        bottom: 0;
        padding: 1rem 1rem 1rem 0;
        margin: 0;
    }
</style>
@endsection
@section('content')

<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::model($listing, ['route' => ['frontend.user.listings.update', $listing], 'class' => 'form-horizontal','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-listing']) }}
        @include("frontend.user.listing.listing.form")
        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=update_and_back">Update</span>
                </button>
            </div>
            <a href="{{ route('frontend.user.listings.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>

@endsection
@section("after_scripts")
@endsection

<header id="site-header">
    @include('includes.partials.logged-in-as')
    @include('frontend.includes.snippet.site.topbar')
    @include('frontend.includes.snippet.site.navbar-ezzy')
</header>
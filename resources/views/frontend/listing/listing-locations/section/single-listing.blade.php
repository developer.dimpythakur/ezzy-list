<div class="col-md-6 px-4 py-1">
    <div class="et-list-grid-main">
        <div class="row mt-5 mt-xs ezzy-single-listing pb-3"
             data-action="{{$location->listing->banner }}"
             data-title="{{ $location->listing->name  }}"
             data-friendly-address="{{$location->address}}"
             data-address="{{$location->address}}"
             data-image="{{ Storage::url('images/listing/'.$location->listing->banner) }}"
             data-longitude="{{$location->longitude}}"
             data-latitude="{{$location->latitude}}"
             data-rating="{{ $location->listing->averageRating(2) }}"
             data-url="{{ URL::to('listing/'.$location->listing->slug)  }}"
             data-reviews="{{ $location->listing->averageRating(2) }}"
             data-icon="<i class=&quot;{{ $location->listing->categories->first()->icon  }}&quot;></i>">

            <div class="listing-thumbnail img-overlay transition-1 overflow-hidden rounded-top position-relative">
              <span class="p-1 position-absolute small business-time right bg-primary text-white pl-2 pr-2">
                  <i class="las la-clock"></i> Closed</span>
                <a href="#" class="et-author-img-box">
                    <img src="{{$location->listing->owner->picture}}" class="et-author-img"
                         alt="author-img">
                </a>
                @if($location->listing->banner)
                    <img src="{{Storage::url('images/listing/'.$location->listing->banner)  }}"
                         alt="{{ $location->listing->name }}"
                         class="img-fluid h-100">
                @endif

                <div class="grid-rating-section p-3 pt-0 pb-4">

                     <span class="badge listing-single-listings listing-rating badge-secondary ml-2 bg-transparent"
                           data-rating="{{ $location->listing->averageRating(2) }}">
                                                </span>

                    <div class="star-rating">
                        <span class="et-view-count">
                                 <i class="las la-eye"></i>  {{  views($location->listing)->count() }}
                        </span>
                        <span class="et-location-icon"><i class="las la-map-marker-alt"></i></span>
                        <span class="et-save-icon"><i class="lar la-heart"></i></span>
                    </div>
                </div>
            </div>
            <div class="et-list-wrapper shadow rounded-bottom p-3 bg-white">
                <a href="{{  route('frontend.listing.show',$location->listing->slug) }}"
                   class="text-decoration-none d-block mt-2 text-reset text-capitalize">
                    <h3>{{ $location->listing->name }}
                        <i class="las la-check-circle"
                           data-toggle="tooltip"
                           data-placement="top"
                           title="{{ $location->listing->name }}"
                           data-original-title="Claimed"></i>
                    </h3>
                </a>
                <p class="mb-3">{{ substr($location->listing->description,0,50)  }}</p>
                <ul class="et-grid-info">
                    @if($location)
                        <li>
                            <i class="las la-map-marker-alt primary-icon"></i>
                            <p>{{$location->address}}</p>
                        </li>
                    @else
                        <li>
                            <i class="las la-map-marker-alt primary-icon"></i>
                            <p>7803 19th Dr, East Elmhurst</p>
                        </li>
                    @endif


                    <li>
                        <i class="las la-phone primary-icon"></i>
                        <p>{{ ($location->listing->phone)  }}</p>
                    </li>
                    <li>
                        <i class="las la-link primary-icon"></i>
                        <p>{{ ($location->listing->website)  }}</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

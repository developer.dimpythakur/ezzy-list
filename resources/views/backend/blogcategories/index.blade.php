@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.blogcategories.management'))
@section('page-header')
<h1>
    <span class="text-capitalize">{{ trans('labels.backend.blogcategories.management') }}</span>
</h1>
@endsection

@section('after_styles')
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/backend/datatables.css') }}">
@endsection

@section('content')
<!-- Default box -->
<div class="row">

    <!-- THE ACTUAL CONTENT -->
    <div class="col-12 mb-4">
        <div class="row mb-0">
            <div class="col-sm-6">
                <div class="hidden-print with-border">
                    <a href="{{route('admin.blogCategories.create')}}" class="btn btn-primary" data-style="zoom-in">
                        <span class="ladda-label"><i class="la la-plus"></i> {{trans('menus.backend.blogcategories.create')}}</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="datatable_search_stack" class="mt-sm-0 mt-2"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive data-table-wrapper">
        <table id="blogcategories-table"
               class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2 dataTable dtr-inline">
            <thead>
                <tr>
                    <th>{{ trans('labels.backend.blogcategories.table.title') }}</th>
                    <th>{{ trans('labels.backend.blogcategories.table.status') }}</th>
                    <th>{{ trans('labels.backend.blogcategories.table.createdby') }}</th>
                    <th>{{ trans('labels.general.actions') }}</th>
                </tr>
            </thead>
            <thead class="transparent-bg">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div><!--table-responsive-->
</div>
@endsection

@section('after_scripts')
<!-- DATA TABLES SCRIPT -->
<script type="text/javascript" src="{{ asset('js/backend/datatables.js') }}"></script>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(function () {
    var dataTable = $('#blogcategories-table').dataTable({
    processing: true,
            serverSide: true,
            ajax: {
            url: '{{ route("admin.blogCategories.get") }}',
                    type: 'post'
            },
            columns: [
            {data: 'name', name: 'name'},
            {data: 'status', name: 'status'},
            {data: 'created_by', name: 'created_by'},
            {data: 'actions', name: 'actions', searchable: false, sortable: false}
            ],
            order: [[3, "asc"]],
            searchDelay: 500,
            dom: "<'row hidden'<'col-sm-6 hidden-xs'i><'col-sm-6 hidden-print'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row mt-2 '<'col-sm-6 col-md-4'l><'col-sm-2 col-md-4 text-center'B><'col-sm-6 col-md-4 hidden-print'p>>",
            buttons: {
            buttons: [
            { extend: 'copy', className: 'copyButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
            { extend: 'csv', className: 'csvButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
            { extend: 'excel', className: 'excelButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
            { extend: 'pdf', className: 'pdfButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
            { extend: 'print', className: 'printButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }}
            ]
            },
            language: {
            @lang('datatable.strings')
            }
    });
            Backend.DataTableSearch.init(dataTable);
});
</script>

@endsection
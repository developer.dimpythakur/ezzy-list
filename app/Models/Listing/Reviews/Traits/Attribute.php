<?php

namespace App\Models\Listing\Reviews\Traits;

/**
 * Class Attribute.
 */
trait Attribute {

    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute() {
        return '<div class="btn-group action-btn"> {$this->getEditButtonAttribute("edit-listing.review", "admin.listing.reviews.edit")}
                {$this->getDeleteButtonAttribute("delete-listing-review", "admin.listing.reviews.destroy")}
                </div>';
    }

}

<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Models\Access\Permission\Permission;
use App\Models\Access\User\User;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Http\Requests\Backend\Access\User\UpdateUserProfileRequest;
use Prologue\Alerts\Facades\Alert;

/**
 * Class UserProfileController.
 */
class UserProfileController extends Controller {

    /**
     * @var \App\Repositories\Backend\Access\User\UserRepository
     */
    protected $user;

    /**
     * @var \App\Repositories\Backend\Access\Role\RoleRepository
     */
    protected $roles;

    /**
     * @param \App\Repositories\Backend\Access\User\UserRepository $users
     * @param \App\Repositories\Backend\Access\Role\RoleRepository $roles
     */
    public function __construct(UserRepository $user) {
        $this->user = $user;
    }

    /**
     * @param \App\Http\Requests\Backend\Access\User\ManageUserRequest $request
     *
     * @return \App\Http\Responses\ViewResponse
     */
    public function index() {
        return new ViewResponse('backend.access.users.account');
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     */
    public function update(UpdateUserProfileRequest $request) {
//        $this->updateProfile(access()->id(), $request->all());
        $this->user->updateProfile(access()->id(), $request->all());
        // E-mail address was updated, user has to reconfirm
        Alert::success('Profile update success-fully')->flash();
        return redirect()->route('admin.access.user.account')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }

}

<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Repositories\Frontend\Access\User\UserRepository;
use Prologue\Alerts\Facades\Alert;
use Illuminate\Support\Facades\Request;
use App\Models\Access\User\Profile;

/**
 * Class ProfileController.
 */
class ProfileController extends Controller {

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * ProfileController constructor.
     *
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user) {
        $this->user = $user;
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     */
    public function update(UpdateProfileRequest $request) {
        $output = $this->user->updateProfile(access()->id(), $request->all());
        // E-mail address was updated, user has to reconfirm
        if (is_array($output) && $output['email_changed']) {
            access()->logout();
            Alert::success('E-mail address was updated, user has to reconfirm')->flash();
            return redirect()->route('frontend.auth.login')->withFlashInfo(trans('strings.frontend.user.email_changed_notice'));
        }
        Alert::success('Profile update success-fully')->flash();
        return redirect()->route('frontend.user.account')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }



}

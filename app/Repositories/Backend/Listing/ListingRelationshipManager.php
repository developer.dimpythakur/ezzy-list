<?php

namespace App\Repositories\Backend\Listings;

use App\Models\Faqs\Faq;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
//Tag
use App\Models\Listing\Tags\Tag;
//ListingCategory
use App\Models\Listing\Timings\Timing;
use App\Models\Listing\Categories\Category;
//Social Media
use App\Models\Listing\Media\SocialMedia\SocialMedia;
//MediaGallery
use App\Models\Listing\Media\Gallery\MediaGallery;
use App\Models\Listing\Media\Audio\MediaAudio;
use App\Models\Listing\Media\Video\MediaVideo;
//Location
use App\Models\Listing\Locations\Location;
use App\Models\Listing\Region\Region;

class ListingRelationshipManager {

    /**
     * Storage Disk path
     * @var type 
     */
    protected $storagePath;

    /**
     * Upload Directory Path
     * @var type 
     */
    protected $uploadPath;

    /**
     * Gallery folder path
     * @var type 
     */
    protected $galleryPath;

    public function __construct($storage, $upload, $gallery_path) {
        $this->storagePath = $storage;
        $this->uploadPath = $upload;
        $this->galleryPath = $gallery_path;
    }

    /**
     * Creating Tags.
     *
     * @param array $tags
     *
     * @return array
     */
    public function createTags($tags = null) {
        $tags_array = [];
        if (!is_null($tags)) {
            //Creating a new array for tags (newly created)
            if (!is_array($tags)) {
                $tags = explode(',', $tags);
            }
            foreach ($tags as $tag) {
                if (is_numeric($tag)) {
                    $tags_array[] = $tag;
                } else {
                    $exist = Tag::where('name', '=', $tag)->first();
                    if (!is_null($tag)) {
                        $newTag = Tag::create(['name' => $tag, 'slug' => Str::slug($tag), 'status' => 1]);
                        $tags_array[] = $newTag->id;
                    }
                }
            }
            return $tags_array;
        }
        return $tags_array;
    }

    /**
     * Creating Categories.
     *
     * @param Array($categories)
     *
     * @return array
     */
    public function createCategories($categories) {
        //Creating a new array for categories (newly created)
        $categories_array = [];
        foreach ($categories as $category) {
            if (is_numeric($category)) {
                $categories_array[] = $category;
            } else {
                $newCategory = Category::create([
                            'name' => $category,
                            'slug' => Str::slug($category),
                            'status' => 1,
                            'created_by' => (access()->user() !== null) ? access()->user()->id : 1
                ]);

                $categories_array[] = $newCategory->id;
            }
        }
        return $categories_array;
    }

    /**
     * Upload Listing Image
     * @param $request
     * @param string $name
     * @return bool
     */
    public function uploadImage($request, $name = 'banner') {
        if ($request->hasFile($name)) {
            $banner = $request->file($name);
            $fileName = time() . $banner->getClientOriginalName();
            if ($this->storagePath->put($this->uploadPath . $fileName, file_get_contents($banner->getRealPath()))):
                return $fileName;
            endif;
        }
        return false;
    }

    /**
     * Url
     * @param type $request
     * @param type $name
     * @return boolean|string
     */
    public function listingBanner($request, $name = 'banner') {
        if ($request->hasFile($name)) {
            $banner = $request->file($name);
            $fileName = time() . $banner->getClientOriginalName();
            if ($this->storagePath->put($this->uploadPath . $fileName, file_get_contents($banner->getRealPath()))):
                return $fileName;
            endif;
        }
        return false;
    }

    /**
     * Upload Listing Images (gallery)
     * @param $request
     * @param string $name
     * @return array
     */
    public function mediaGallery($request, $name = 'listing_gallery') {
        $filesArray = [];
        $files = $request->file($name);
        if ($request->hasFile($name)) :
            foreach ($files as $gallery) :
                $fileName = time() . $gallery->getClientOriginalName();
                if ($this->storagePath->put($this->galleryPath . $fileName, file_get_contents($gallery->getRealPath()))):
                    $filesArray[] = MediaGallery::create([
                                'filename' => $fileName,
                                'type' => 'listing',
                                'path' => $this->galleryPath,
                                'url' => Storage::url('images/listing/gallery/') . $fileName,
                                'media_type' => 'image',
                                'created_by' => (access()->user() !== null) ? access()->user()->id : 1
                            ])->id;
                endif;
            endforeach;
        endif;
        return $filesArray;
    }

    /**
     * Upload Listing Images (gallery)
     * @param $gallery
     * @param string $name
     * @return array
     */
    public function mediaGalleryYelp($gallery) {
        $filesArray = [];
        if (count($gallery)):
            foreach ($gallery as $image) :
                $info = pathinfo($image);
                $name = $info['basename'];
                $filesArray[] = MediaGallery::create([
                            'filename' => $name,
                            'type' => 'yelp',
                            'path' => $this->galleryPath,
                            'url' => $image,
                            'media_type' => 'image',
                            'created_by' => (access()->user() !== null) ? access()->user()->id : 1
                        ])->id;
            endforeach;
        endif;
        return $filesArray;
    }

    /**
     * 
     * @param type $fileName
     */
    function addNewGallery($fileName, $type = 'listing') {
        return MediaGallery::create([
                    'filename' => $fileName,
                    'type' => $type,
                    'path' => $this->galleryPath,
                    'url' => Storage::url('images/listing/gallery/') . $fileName,
                    'media_type' => 'image',
                    'created_by' => (access()->user() !== null) ? access()->user()->id : 1
        ]);
    }

    public function fakeMedia($files) {
        $filesArray = [];
        foreach ($files as $fileName) :
            $mediaGallery = $this->addNewGallery($fileName);
            $filesArray[] = $mediaGallery->id;
        endforeach;
        return $filesArray;
    }

    /**
     * Create New Social Media networks row
     * @param $socialMedia
     * @return array
     */
    public function addSocialMedia($socialMedia, $listing = null) {
        $mediaArray = [];
        if (!is_null($listing) && $listing->social_network) {
            foreach ($listing->social_network as $network) {
                $array = ['network_url' => $socialMedia[$network->network_type]];
                SocialMedia::where('id', $network->id)->
                        update($array);
                $mediaArray[] = $network->id;
            }

            return $mediaArray;
        }
        if ($socialMedia && count($socialMedia)):
            foreach ($socialMedia as $media => $target):
                $mediaArray[] = SocialMedia::create([
                            'name' => $media,
                            'network_type' => $media,
                            'network_url' => $target,
                            'status' => 1,
                            'created_by' => (access()->user() !== null) ? access()->user()->id : 1
                        ])->id;
            endforeach;
        endif;
        return $mediaArray;
    }

    /**
     * Manage FAQs
     * @param $faqs
     * @param null $listing
     * @return array
     */
    public function createFaqs($faqs, $listing = null) {
        $faqsArray = [];
//        update FAQs
        if ($listing && !is_null($listing)):
            $this->deleteFAQs($listing->faqs);
            $faqsArray = $this->addFAQs($faqs);
        else:

            $faqsArray = $this->addFAQs($faqs);
        endif;
        return $faqsArray;
    }

    /**
     * Add New Faq
     * @param type $faqs
     * @return type
     */
    public function addFAQs($faqs) {
        $faqsArray = [];
        $model = new Faq;
        if (isset($faqs['question']) && (count($faqs['question']))):
            for ($i = 0; $i <= (count($faqs['question']) - 1); $i++):
                $faqsArray[] = $model->addFAQs($faqs['question'][$i], $faqs['answer'][$i]);
            endfor;
        endif;
        return $faqsArray;
    }

    /**
     * Delete Faq
     * @param type $faqs
     */
    public function deleteFAQs($faqs) {
        $model = new Faq;
        foreach ($faqs as $faq):
            $model->deleteFaqs($faq->id);
        endforeach;
    }

    /**
     * Listing Timings
     * @param $timings
     * @return array
     */
    public function createTimings($timings, $listing = null) {
        $listingTimings = new Timing;
        $timingsArray = $listingTimings->updateTimings($listing, $timings);
        return $timingsArray;
    }

    /**
     * Listing createMediaVideo
     * @param $media
     * @return array
     */
    public function createMediaVideo($media) {
        $mediaArray = [];
        if ($media && count($media)):
            foreach ($media as $mediaType => $video):
                if ($video && is_array($video)):
                    foreach ($video as $row):
                        $mediaArray[] = MediaVideo::create([
                                    'filename' => $mediaType,
                                    'type' => 'listing',
                                    'url' => $row,
                                    'media_type' => 'video',
                                    'created_by' => (access()->user() !== null) ? access()->user()->id : 1
                                ])->id;
                    endforeach;
                endif;
            endforeach;
        endif;
        return ($mediaArray) ? Arr::flatten($mediaArray) : [];
    }

    /**
     * Add New video
     * @param $media
     * @return array
     */
    public function createMediaAudio($media) {
        $mediaArray = [];
        if ($media && count($media)):
            foreach ($media as $mediaType => $audio):
                if ($audio && is_array($audio)):
                    foreach ($audio as $row):
                        $mediaArray[] = MediaAudio::create([
                                    'filename' => $mediaType,
                                    'type' => 'listing',
                                    'url' => $row,
                                    'media_type' => 'audio',
                                    'created_by' => (access()->user() !== null) ? access()->user()->id : 1
                                ])->id;
                    endforeach;
                endif;
            endforeach;
        endif;
        return ($mediaArray) ? Arr::flatten($mediaArray) : [];
    }

    /**
     * Add Location
     * @param type $locations
     * @param type $listing
     * @return type
     */
    public function manageLocations($locations, $listing = null) {
        $locationsArray = [];
        if (!is_null($listing) && $listing->location) {
            foreach ($listing->location as $location) {
                Location::where('id', $location->id)->
                        update([
                            'name' => $locations['address'],
                            'address' => $locations['address'],
                            'region_id' => isset($locations['region']) ? $locations['region'] : null,
                            'postal_code' => isset($locations['postal_code']) ? $locations['postal_code'] : null,
                            'longitude' => isset($locations['longitude']) ? $locations['longitude'] : null,
                            'latitude' => isset($locations['latitude']) ? $locations['latitude'] : null,
                ]);
                $locationsArray[] = $location->id;
            }
            return $locationsArray;
        }
        if (isset($locations) && !empty($locations) && $listing == null):
            $location = Location::create([
                        'name' => $locations['address'],
                        'address' => $locations['address'],
                        'region_id' => isset($locations['region']) ? $locations['region'] : null,
                        'postal_code' => isset($locations['postal_code']) ? $locations['postal_code'] : null,
                        'longitude' => isset($locations['longitude']) ? $locations['longitude'] : null,
                        'latitude' => isset($locations['latitude']) ? $locations['latitude'] : null,
                    ])->id;
            if ($location):
                return $location;
            endif;
        endif;
        return null;
    }

    /**
     * Add Location
     * @param type $locations
     * @param type $listing
     * @return type
     */
    public function listingRegion($locations, $listing = null) {
        $location = [
            'region_id' => isset($locations['region']) ? $locations['region'] : null,
        ];
        if ($listing->region()->count()):
            $listing->region()->update();
        else:
            $listing->region()->create($this->profileFields($request));
        endif;
        return null;
    }

    /*     * *
     * BUSINESS
     * 
     */

    /**
     * Get Business Listing
     * @param type $listing
     */
    public function businessListing($listing, $reviews = null) {
        $business = [];
        if ($listing):
            $business['id'] = ($listing->id);
            $business['name'] = $listing->name;
            $business['tags'] = $this->tags($listing->categories);
            $business['description'] = $listing->name;
            $business['tagline'] = $listing->alias;
            $business['phone'] = $listing->display_phone;
            $business['website'] = null;
            $business['email'] = null;
            $business['faq'] = null;
            $business['audio'] = null;
            $business['video'] = null;
            $business['social_media'] = null;
            $business['banner'] = $listing->image_url;
            $business['url'] = $listing->url;
            $business['categories'] = $this->categories($listing->categories);
            $business['location'] = $this->filterLocations($listing);
            $business['listing_gallery'] = isset($listing->photos) ? $listing->photos : null;
            if (!$listing->is_closed):
                $business['timings'] = $this->hours($listing);
            endif;
            if ($reviews):
                $business['reviews'] = $this->businessReviews($reviews);
            endif;
        endif;
        return $business;
    }

    /**
     * Location address including city and country
     * @param type $business
     * @return type
     */
    public function filterLocations($business) {
        $address = [];
        $filepath = public_path('storage/images/listing/regions');
        if (isset($business->location)):
            $region = Region::where('name', 'LIKE', "%{$business->location->city}%")
                            ->orWhere('city', 'LIKE', "%{$business->location->city}%")->first();
            if (null == $region):
                $regionID = Region::create([
                            'name' => $business->location->state,
                            'slug' => Str::slug($business->location->state),
                            'city' => ($business->location->city),
                            'state' => ($business->location->state),
                            'region' => ($business->location->state),
                            'country' => ($business->location->country),
                            'image' => $this->getImage('city', $filepath),
                        ])->id;
            else:
                $regionID = $region->id;
            endif;
            $regionID = (null !== $regionID) ? $regionID : 1;
            $address['address'] = collect($business->location->display_address)->implode(' ');
            $address['region'] = $regionID;
            $address['latitude'] = $business->coordinates->latitude;
            $address['longitude'] = $business->coordinates->longitude;
        endif;
        return $address;
    }

    /**
     * Filter Hours
     * @param type $hours
     * @return type
     */
    public function hours($hours = null) {
        $timings = [];
        $timing = (isset($hours->hours) && isset(collect($hours->hours)->first()->open)) ? (collect($hours->hours)->first()->open) : null;
        if (null !== $timing):
            foreach ($timing as $timing) {
                $timings[strtolower(daysArray()[$timing->day])] = [
                    'opening' => substr_replace($timing->start, ':', 2, 0),
                    'closing' => substr_replace($timing->end, ':', 2, 0),
                ];
            }
        endif;
        return $timings;
    }

    /**
     * Categories
     * @param type $categories
     * @return type
     */
    public function categories($categories) {
        $categoriesArray = array('food', 'nightlife', 'people');
        $cat = [];
        $path = public_path('storage/images/category/');
        if (count($categories)):
            foreach ($categories as $category):
                $catAvailable = Category::where('name', 'like', '%' . $category->title . '%')->first();
                if ($catAvailable == null):
                    $cat[] = Category::create([
                                'name' => $category->title,
                                'slug' => $category->alias,
                                'description' => $category->title,
                                'image' => $this->getImage($categoriesArray[array_rand($categoriesArray, 1)], $path),
                                'icon' => 'icon-dish',
                                'status' => 1,
                                'created_by' => (access()->user() !== null) ? access()->user()->id : 1
                            ])->id;
                else:
                    $cat[] = $catAvailable->id;
                endif;
                return $cat;
            endforeach;
        endif;
        return $cat;
    }

    /**
     * Categories
     * @param type $tag
     * @return type
     */
    public function tags($tags = null) {
        return [1, 2, 3];
    }

    public function getImage($cat = 'city', $path = null) {
        $faker = \Faker\Factory::create();
        return $faker->image($path, 400, 300, $cat, false);
    }

    public function businessReviews($reviews) {
        $reviewsArray = [];
        if ($reviews->total > 0):
            foreach ($reviews->reviews as $review):
                $reviewsArray[] = [
                    'title' => $review->user->name . ' said ' . substr($review->text, 0, rand(10, 30)),
                    'body' => $review->text,
                    'customer_service_rating' => $review->rating,
                    'quality_rating' => $review->rating,
                    'friendly_rating' => $review->rating,
                    'pricing_rating' => $review->rating,
                    'rating' => $review->rating,
                    'recommend' => 'Yes',
                    'approved' => true, // This is optional and defaults to false
                ];
            endforeach;
        endif;
        return $reviewsArray;
    }

}

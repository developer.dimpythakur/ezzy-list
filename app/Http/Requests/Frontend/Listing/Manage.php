<?php

namespace App\Http\Requests\Frontend\Listing;

use Illuminate\Foundation\Http\FormRequest;

class Manage extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return access()->allow('manage-listing');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
        ];
    }

    public function messages() {
        return [
        ];
    }

}

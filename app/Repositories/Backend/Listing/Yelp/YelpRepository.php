<?php

namespace App\Repositories\Backend\Listing\Yelp;

use Stevenmaguire\Yelp\ClientFactory;
use Stevenmaguire\Yelp\Version;
// request
use Illuminate\Http\Request;

class YelpRepository {

    /**
     * Required, unless apiKey is provided
     * @var type 
     */
    private $accessToken;

    /**
     * Required, unless apiKey is provided
     * @var type 
     */
    private $apiHost;

    /**
     * API Key
     * @var type 
     */
    private $apiKey;

    /**
     * Client
     * @var type 
     */
    protected $client;

    /**
     * Options
     * @var type 
     */
    protected $options;

    public function __construct() {
        $this->options = [
            'accessToken' => config('yelp.v3.accessToken'),
            'apiHost' => config('yelp.v3.apiHost'),
            'apiKey' => config('yelp.v3.apiKey')
        ];
        $this->client = ClientFactory::makeWith($this->options, Version::THREE);
    }

    /**
     * Get Yelp Business 
     * @param Request $request
     * @return type
     */
    public function searchBusiness($params) {
        $business = [];
//        $params = [
//            'location' => ($request->location) ? Region::find($request->location)->name : 'New York',
//            'term' => ($request->term) ? $request->term : 'Food',
//        ];
        $results = $this->client->getBusinessesSearchResults($params);
        if ($results):
//            $business['total'] = $results->total;
//            $business['region'] = $results->region;
//            $business['businesses'] = $results->businesses;
            return $results->businesses[0];
        endif;
        return $business;
    }

    /**
     * Get Yelp Business 
     * @param Request $phone
     * @return type
     */
    public function searchPhone($phone) {
        return $this->client->getBusinessesSearchResultsByPhone($phone);
    }

    /**
     * Get Reviews
     * @param type $businessId
     * @return type
     */
    public function businessReviews($businessId) {
        return $this->client->getBusinessReviews($businessId);
    }

    /**
     * Get Reviews
     * @param type $businessId
     * @return type
     */
    public function business($businessId) {
        return $this->client->getBusiness($businessId);
    }

}

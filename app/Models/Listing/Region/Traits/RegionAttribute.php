<?php

namespace App\Models\Listing\Region\Traits;

use Illuminate\Support\Facades\Storage;

/**
 * Class RegionAttribute.
 */
trait RegionAttribute {

    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute() {
        return '<div class="btn-group action-btn">
                    ' . $this->getEditButtonAttribute('edit-region', 'admin.listing-regions.edit') . '
                    ' . $this->getViewButtonAttribute() . '                    
                    ' . $this->getDeleteButtonAttribute('delete-region', 'admin.listing-regions.destroy') . '
                </div>';
    }

    /**
     * @return string
     */
    public function getViewButtonAttribute() {
        return '<a target="_blank" href="' . route('frontend.location.show', $this->attributes['slug']) . '" class="btn btn-flat btn-default">
                    <i data-toggle="tooltip" data-placement="top" title="View Region" class="las la-eye"></i>
                </a>';
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute() {
        if ($this->isActive()) {
            return "<label class='label label-success'>" . trans('labels.general.active') . '</label>';
        }
        return "<label class='label label-danger'>" . trans('labels.general.inactive') . '</label>';
    }

    /**
     *
     * @return string
     */
    public function getImageAttribute() {
        return $this->attributes['image'];
        if (isset($this->attributes['image']) && !is_null($this->attributes['image'])) {
            $imageName = $this->attributes['image'];
            $imageSource = Storage::url('images/listing/regions/' . $imageName);
            $image = '';
            $image .= '<img class="border-r rounded" alt="' . $this->attributes['name'] . '" src="' . $imageSource . '">';
            return $image;
        }
        return 'image';
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

}

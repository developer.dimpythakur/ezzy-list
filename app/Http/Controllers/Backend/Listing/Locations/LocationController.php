<?php

namespace App\Http\Controllers\Backend\Listing\Locations;

use App\Models\Listing\Region\Region as Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\ViewResponse;
use App\Http\Responses\Backend\Listings\Locations\CreateResponse;
use App\Http\Responses\Backend\Listings\Locations\EditResponse;
use App\Repositories\Backend\Listing\Locations\LocationRepository;
//Requests
use App\Http\Requests\Backend\Listing\Locations\Manage;
use App\Http\Requests\Backend\Listing\Locations\Create;
use App\Http\Requests\Backend\Listing\Locations\Store;
use App\Http\Requests\Backend\Listing\Locations\Edit;
use App\Http\Requests\Backend\Listing\Locations\Update;
use App\Http\Requests\Backend\Listing\Locations\Delete;

/**
 * LocationController
 */
class LocationController extends Controller {

    /**
     * variable to store the repository object
     * @var LocationRepository
     */
    protected $repository;

    /**
     * contructor to initialize repository object
     * @param LocationRepository $repository;
     */
    public function __construct(LocationRepository $repository) {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  App\Http\Requests\Backend\Listing\Locations\Manage  $request
     * @return \App\Http\Responses\ViewResponse
     */
    public function index(Manage $request) {
        return new ViewResponse('backend.listing.locations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  CreateNamespace  $request
     * @return \App\Http\Responses\Backend\Listings\Locations\CreateResponse
     */
    public function create(Create $request) {
        return new CreateResponse('backend.listing.locations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreNamespace  $request
     * @return \App\Http\Responses\RedirectResponse
     */
    public function store(Store $request) {
        //Input received from the request
        $input = $request->except(['_token']);
        //Create the model using repository create method
        $this->repository->create($input);
        //return with successfull message
        return new RedirectResponse(route('admin.listing-locations.index'), ['flash_success' => trans('alerts.backend.locations.created')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Locations\Location  $location
     * @param  EditNamespace  $request
     * @return \App\Http\Responses\Backend\Listings\Locations\EditResponse
     */
    public function edit(Location $listing_location, Edit $request) {
        return new EditResponse($listing_location);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNamespace  $request
     * @param  App\Models\Locations\Location  $location
     * @return \App\Http\Responses\RedirectResponse
     */
    public function update(Update $request, Location $location) {
        //Input received from the request
        $input = $request->except(['_token']);
        //Update the model using repository update method
        $this->repository->update($location, $input);
        //return with successfull message
        return new RedirectResponse(route('admin.locations.index'), ['flash_success' => trans('alerts.backend.locations.updated')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DeleteNamespace  $request
     * @param  App\Models\Locations\Location  $location
     * @return \App\Http\Responses\RedirectResponse
     */
    public function destroy(Location $location, Delete $request) {
        //Calling the delete method on repository
        $this->repository->delete($location);
        //returning with successfull message
        return new RedirectResponse(route('admin.locations.index'), ['flash_success' => trans('alerts.backend.locations.deleted')]);
    }

}

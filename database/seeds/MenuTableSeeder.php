<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table(config('access.menus_table'))->truncate();
        $menus = [
            [
                'id' => 1,
                'type' => 'backend',
                'name' => 'Backend Sidebar Menu',
                'items' => file_get_contents(__DIR__ . '/Menu/admin-menus.js'),
                'created_by' => 1,
                'created_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'type' => 'user',
                'name' => 'User Sidebar Menu',
                'items' => file_get_contents(__DIR__ . '/Menu/user-menus.js'),
                'created_by' => 1,
                'created_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'type' => 'frontend',
                'name' => 'Frontend Header Menu',
                'items' => file_get_contents(__DIR__ . '/Menu/frontend-menus.js'),
                'created_by' => 1,
                'created_at' => Carbon::now(),
            ]
        ];
        DB::table(config('access.menus_table'))->insert($menus);
    }

}

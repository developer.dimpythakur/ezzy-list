<?php

namespace App\Http\Responses\Backend\Listing\Categories;

use Illuminate\Contracts\Support\Responsable;

class Edit implements Responsable {

    /**
     * @var App\Models\Categories\Category
     */
    protected $listing_category;

    /**
     * @param App\Models\Categories\Category $categories
     */
    public function __construct($category) {
        $this->listing_category = $category;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request) {
        return view('backend.listing.categories.edit')->with([
                    'listing_category' => $this->listing_category
        ]);
    }

}

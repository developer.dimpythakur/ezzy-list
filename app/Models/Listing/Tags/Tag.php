<?php

namespace App\Models\Listing\Tags;

use App\Models\BaseModel;
use App\Models\Listing\Tags\Traits\Attribute\Attribute;
use App\Models\Listing\Tags\Traits\Relationship\Relationship;
use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
//SLUG
use Cviebrock\EloquentSluggable\Sluggable;

//ListingTag
class Tag extends BaseModel {

    use ModelTrait,
        Sluggable,
        SoftDeletes,
        Attribute,
        Relationship {
        // ListingTagAttribute::getEditButtonAttribute insteadof ModelTrait;
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;
    protected $fillable = ['name', 'slug', 'status', 'created_at'];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);

        $this->table = config('module.ezzy_tags.table');
    }

    /**
     * Return Tags map Table
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getMapTable() {
        return config('module.listing_tags.table');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

}

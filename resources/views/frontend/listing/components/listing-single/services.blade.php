<div class="card card-border-radius services-offered shadow mb-5 p-4 border-0">
    <h5 class="card-title mb-4">Services offered</h5>
    <div>
       <span class="service pr-4 d-inline-block mb-4">
        <i class="las la-wifi"></i> Wi-Fi
       </span>
        <span class="service pr-4 d-inline-block mb-4">
            <i class="las la-parking"></i>Parking
        </span>
        <span class="service pr-4 d-inline-block mb-4">
      <i class="las la-people-carry"></i> Family
        </span>
        <span class="service pr-4 d-inline-block mb-4">
       <i class="las la-smoking"></i> Smoking
        </span>
        <span class="service pr-4 d-inline-block mb-4">
        <i class="las la-door-open"></i>Open Area
        </span>
        <span class="service pr-4 d-inline-block mb-4">
            <i class="las la-credit-card"></i> Card Accepted
        </span>
    </div>
</div>
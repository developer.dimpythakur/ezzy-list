@extends('frontend.layouts.user')
@section('content')

@section('page-header')
<h2>My Profile </h2>
@endsection

@if ($errors->any())
<section class="row">
    <section class="col-6">
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible mb-2 mt-2 fade show" role="alert">
        {{$error}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-times-circle-o"></i></span>
            </button>
        </div>
        @endforeach
    </section>
</section>
@endif
<section class="row">
    <section class="col-6">
        {{ Form::model($logged_in_user, ['route' => 'frontend.user.profile.update','files' => true, 'class' => 'form-', 'method' => 'PATCH']) }}
        @include('frontend.user.components.profile.profile')
        {{ Form::close() }}
        <section class="row">
            {{ Form::model($logged_in_user, ['route' => 'frontend.user.profile-picture.update', 'files' => true,'class' => 'form-', 'method' => 'PATCH']) }}
            @method('PATCH')
            @csrf
            <!--include('frontend.user.components.profile.upload-photo')-->
            {{ Form::close() }}
        </section>
    </section>
    <section class="col-6">
        {{ Form::open(['route' => ['frontend.auth.password.change'], 'class' => 'form-', 'method' => 'patch']) }}
        @include('frontend.user.components.profile.password')
        {{ Form::close() }}
    </section>
</section>

@endsection

@section('after-scripts')

<script type="text/javascript">
    $(document).ready(function () {

        // To Use Select2
        Backend.Select2.init();

        if ($.session.get("tab") == "edit")
        {
            $("#li-password").removeClass("active");
            $("#li-profile").removeClass("active");
            $("#li-edit").addClass("active");

            $("#profile").removeClass("active");
            $("#password").removeClass("active");
            $("#edit").addClass("active");
        } else if ($.session.get("tab") == "password")
        {
            $("#li-password").addClass("active");
            $("#li-profile").removeClass("active");
            $("#li-edit").removeClass("active");

            $("#profile").removeClass("active");
            $("#password").addClass("active");
            $("#edit").removeClass("active");
        }

        $(".tabs").click(function () {
            var tab = $(this).attr("aria-controls");
            $.session.set("tab", tab);
        });
    });
</script>
@endsection
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEzzyMediaVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ezzy_media_video', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('filename', 200)->nullable()->default(null);
            $table->string('type', 200)->nullable()->default(null);
            $table->string('media_type', 100)->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default(1);

            $table->string('path', 100)->nullable()->default(null);
            $table->string('url', 100)->nullable()->default(null);

            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ezzy_media_video');
    }
}

<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-file-text"></i> Contact Information</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row my-3">
            <!-- Phone -->
            <div class="form-group col-md-4">
                {!! Html::decode(Form::label('phone', '<i class="la la-phone"></i>'.trans('validation.attributes.backend.listings.phone'), ['class' => 'control-label required'])) !!}
                {{ Form::text('phone',null,['class' => ' border form-control', 'placeholder' => '123-456-789', 'required' => 'required']) }}
            </div>
            <!-- /Phone -->

            <!-- Email -->
            <div class="form-group col-md-4">
                {!! Html::decode(Form::label('email', '<i class="la la-envelope-o"></i>'.trans('validation.attributes.backend.listings.email'), ['class' => 'control-label required'])) !!}
                {{ Form::text('email',null,['class' => ' border form-control', 'placeholder' => 'business@example.email']) }}
            </div>
            <!-- /Email -->

            <!-- Website -->
            <div class="form-group col-md-4">
                {!! Html::decode(Form::label('website', '<i class="la la-html5"></i>'.trans('validation.attributes.backend.listings.website'), ['class' => 'control-label required'])) !!}
                {{ Form::text('website',null,['class' => ' border form-control', 'placeholder' => 'http://']) }}
            </div>
            <!-- /Website -->
        </div>
    </div>
</div>
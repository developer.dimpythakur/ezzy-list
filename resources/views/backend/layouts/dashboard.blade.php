<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}" dir="{{ config('backend.html_direction') }}">
    <head>
        @include(('backend.layouts.sections.head'))
              <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token() ]) !!};
        </script>
    </head>

    <body class="{{ config('backend.body_class') }}">
        @include(('backend.layouts.sections.main_header'))
        <div class="app-body" id="app">
            @include('backend.layouts.sections.sidebar')
            <main class="main pt-2">
                @includeWhen(isset($breadcrumbs), ('backend.layouts.sections.breadcrumbs'))
                @yield('header')
                <div class="container-fluid animated fadeIn">
                    @yield('content')
                </div>
            </main>
        </div><!-- ./app-body -->

        <footer class="{{ config('backend.footer_class') }}">
            @include(('backend.layouts.sections.footer'))
        </footer>
        @yield('before_scripts')
        @stack('before_scripts')

        @include(('backend.layouts.sections.scripts'))

        @yield('after_scripts')
        @stack('after_scripts')
    </body>
</html>
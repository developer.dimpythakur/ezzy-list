<!--Standalone Web Portal Needed To occomodate up to 500 users.

Will need to be linked to client main website.

Need admin access and members access.

Need full security features added to protect user information and at login stage a secure process for loging into portal.

Ability to add contact information, short description and image.


Admin full ability to add or remove profiles


Ability to communicate directly within portal
-->




<!-- News and tips -->
<section class="et-blog-grid map-home bg-light position-relative">
    <div class="container">
        <div class="row">
            <div class="section-heading text-center d-block full-width">
                <h2 class="text-capitalize">News & Tips</h2>
                <p>Read all business related tips from popular blogs</p>
            </div>
            @if($posts && !is_null($posts) && count(($posts)))
            @foreach($posts as $post)
            <div class="col-md-4 col-sm-12 mt-5 mt-xs">
                <div class="blog-post shadow">
                    <div class="blog-thumbnail img-overlay transition-1 position-relative">
                        @if(($post->featured_image) && ($post->featured_image) != 'default-image.jpg' && $post->type =='feed')
                        <img src="{{ $post->featured_image }}"
                             alt="{{ $post->name }}"
                             class="img-fluid rounded-top">
                        @else
                        <img src="{{ Storage::url('images/blog/') . ($post->featured_image) }}" alt="{{ $post->name }}"
                             class="img-fluid rounded-top">
                        @endif
                        <a href="javascript:void(0)"
                           class="badge badge-primary mt-3 ml-3 p-2 position-absolute post-category sticky-top p-1">
                            {{ $post->blog}}
                        </a>
                        <span class="post-type position-absolute h4 text-white bg-dark p-2 card-border-radius">
                            <i class="las la-blog"></i>
                        </span>
                    </div>
                    <div class="pt-0 pb-4 pl-4 pr-4 bg-white">
                        <div class="post-meta pb-3 pt-3">
                            <a class="text-muted pr-2" href="javascript:void(0)">
                                <i class="las la-clock"></i>
                                {{\Carbon\Carbon::parse($post->publish_datetime)->format('M, d, Y ')}}
                            </a>
                            <a class="text-muted pr-2" href="javascript:void(0)">
                                <i class="las la-user-circle"></i>
                                {{ $post->owner->first_name}}
                            </a>
                            @if(!empty($post->categories))
                            @foreach($post->categories  as $category)
                            <a class="text-muted pr-2" href="javascript:void(0)">
                                <i class="las la-save"></i>
                                {{$category->name }}
                            </a>
                            @endforeach
                            @else
                            <a class="text-muted pr-2" href="javascript:void(0)">
                                <i class="icon-icon"></i>
                                Travel & lifestyle
                            </a>
                            @endif
                        </div>
                        <h5>
                            <a href="{{ URL::to('post').'/'. $post->slug }}" class="text-body">
                                {{ $post->name }}
                            </a>
                        </h5>
                        <p> {{ strip_tags(substr($post->content,0,200)) }}</p>
                        <a href="{{ URL::to('post').'/'. $post->slug }}"
                           class="btn btn-primary border-0 transition-1 text-capitalize shadow text-white pointer">Read
                            More</a>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</section>
<!-- /News and tips -->
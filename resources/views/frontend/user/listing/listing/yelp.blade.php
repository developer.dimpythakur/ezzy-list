@extends ('frontend.layouts.user')
@section ('title', trans('labels.frontend.listing.management') . ' | ' . trans('labels.frontend.listing.create'))
@section('page-header')
<h1>{{ trans('labels.backend.listing.create') }}</h1>
@endsection
@section('content')
<section class="row">
    <div class="col-md-5 bold-labels">
        <!-- Default box -->
        @include(('frontend.layouts.sections.grouped_errors'))
        {{ Form::open(['route' => 'frontend.user.listings.store', 'class' => 'form-horizontal add-listing','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'post', 'id' => 'create-listing']) }}

        <div class="card">
            <div class="card-header">
                <h6 class="my-2"><i class="la la-yelp"></i> Import listing from Yelp</h6>
            </div>
            <div class="card-body listing-form-yelp bold-labels">
                <div class="row">
                    <div class="form-group">
                        <input type="hidden" name="id" id="id"/>
                        <input type="hidden" name="url" id="url"/>
                    </div>
                    <div class="form-group col-md-8 required">
                        {{ Form::text('yelp', null, ['id'=>'yelp-phone','required'=>'required', 'class' => 'form-control box-size', 'placeholder' => 'Registered Phone Number']) }}
                        <span class="help text-error d-block py-2"> It must start with + like +123456789</span>
                    </div>
                    <div class="form-group col-md-4 required">
                        <button 
                            data-redirect="{{ route('frontend.user.listings.index')}}"
                            data-loading="Searching...."
                            data-show="Find and Save" id="yelp-search" data-url='{{ route('frontend.user.listings-yelp') }}' type="button" 
                            class="btn btn-success listing-yelp">
                            <i class="la la-yelp"></i>&nbsp;Find and Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection
@section("after_scripts")
@endsection

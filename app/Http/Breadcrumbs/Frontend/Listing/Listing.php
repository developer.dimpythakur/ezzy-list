<?php

Breadcrumbs::register('frontend.user.listings', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('menus.backend.listings.management'), route('frontend.user.listings.index'));
});
Breadcrumbs::register('user.listings.index', function ($breadcrumbs) {
    $breadcrumbs->parent('user.dashboard');
    $breadcrumbs->push(trans('menus.backend.listings.management'), route('user.listings.index'));
});

Breadcrumbs::register('user.listings.create', function ($breadcrumbs) {
    $breadcrumbs->parent('user.listings.index');
    $breadcrumbs->push(trans('menus.backend.listings.create'), route('user.listings.create'));
});

Breadcrumbs::register('user.listings.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('user.listings.index');
    $breadcrumbs->push(trans('menus.backend.listings.edit'), route('user.listings.edit', $id));
});

Breadcrumbs::register('user.listings-active', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('menus.backend.listings.active'), route('user.listings-active'));
});

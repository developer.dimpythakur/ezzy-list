<?php

Breadcrumbs::register('admin.listing-categories.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.categories.management'), route('admin.listing-categories.index'));
});

Breadcrumbs::register('admin.listing-categories.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.listing-categories.index');
    $breadcrumbs->push(trans('menus.backend.categories.create'), route('admin.listing-categories.create'));
});

Breadcrumbs::register('admin.listing-categories.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.listing-categories.index');
    $breadcrumbs->push(trans('menus.backend.categories.edit'), route('admin.listing-categories.edit', $id));
});

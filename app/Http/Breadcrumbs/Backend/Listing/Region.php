<?php

Breadcrumbs::register('admin.listing-regions.index', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.dashboard');
    $breadcrumbs->push(trans('menus.backend.regions.management'), route('admin.listing-regions.index'));
});

Breadcrumbs::register('admin.listing-regions.create', function ($breadcrumbs) {
    $breadcrumbs->parent('admin.listing-regions.index');
    $breadcrumbs->push(trans('menus.backend.regions.create'), route('admin.listing-regions.create'));
});

Breadcrumbs::register('admin.listing-regions.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('admin.listing-regions.index');
    $breadcrumbs->push(trans('menus.backend.regions.edit'), route('admin.listing-regions.edit', $id));
});

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/backend/yelp.js":
/*!*********************************************!*\
  !*** ./resources/assets/js/backend/yelp.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
 * similar to PHP's empty function
 */
function empty(data) {
  if (typeof data == 'number' || typeof data == 'boolean') {
    return false;
  }

  if (typeof data == 'undefined' || data === null) {
    return true;
  }

  if (typeof data.length != 'undefined') {
    return data.length === 0;
  }

  var count = 0;

  for (var i in data) {
    // if(data.hasOwnProperty(i))
    //
    // This doesn't work in ie8/ie9 due the fact that hasOwnProperty works only on native objects.
    // http://stackoverflow.com/questions/8157700/object-has-no-hasownproperty-method-i-e-its-undefined-ie8
    //
    // for hosts objects we do this
    if (Object.prototype.hasOwnProperty.call(data, i)) {
      count++;
    }
  }

  return count === 0;
}

if (!String.prototype.splice) {
  /**
   * {JSDoc}
   *
   * The splice() method changes the content of a string by removing a range of
   * characters and/or adding new characters.
   *
   * @this {String}
   * @param {number} start Index at which to start changing the string.
   * @param {number} delCount An integer indicating the number of old chars to remove.
   * @param {string} newSubStr The String that is spliced in.
   * @return {string} A new string with the spliced substring.
   */
  String.prototype.splice = function (start, delCount, newSubStr) {
    return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
  };
}

String.prototype.splice = function (idx, rem, str) {
  return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};
/**
 * 
 * @param {type} mode
 * @returns {undefined}Loader
 */


function loader() {
  var mode = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'loading';
  var $button = jQuery("#yelp-search");
  var text_loading = $button.data('loading');
  var text_show = $button.data('show');

  if (mode === 'loading') {
    $button.text(text_loading);
    $button.attr('disabled', true);
  } else {
    $button.text(text_show);
    $button.attr('disabled', false);
  }
}
/**
 * Notification 
 * @param {type} $type
 * @param {type} $message
 * @returns {undefined}
 */


function notifyMe($type, $message) {
  Noty.overrideDefaults({
    layout: 'topRight',
    timeout: 2500,
    closeWith: ['click', 'button'],
    theme: 'ezzy'
  });
  new Noty({
    type: $type,
    text: $message
  }).show();
}
/**
 * -----------------------
 * Save Listing || Bookmark
 * ------------------------
 */


jQuery(document).ready(function () {
  jQuery(document).on('click', '#yelp-search', function (event) {
    event.preventDefault();
    event.stopPropagation();
    var $form = jQuery(".listing-form-yelp"),
        $button = jQuery(this); //get registed phone number

    var phoneNumber = jQuery("#yelp-phone").val();

    if (empty(phoneNumber)) {
      $form.addClass('was-validated');
      notifyMe('error', 'Please enter a valid/registered phone number.');
      return false;
    }

    loader('loading');
    var $thisObject = jQuery(this);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
    });
    jQuery.ajax({
      url: $thisObject.data('url'),
      method: 'POST',
      data: {
        phone: phoneNumber,
        location: jQuery("#location").val()
      },
      success: function success(result) {
        if (result.type === 'success') {
          var business = result.message;
          populate(business);
          loader('hide');
          notifyMe('success', 'listing  imported');
          window.location.href = $button.data('redirect');
        } else {
          loader('hide');
          notifyMe(result.type, result.message);
          $form.removeClass('was-validated');
        }
      },
      error: function error(result) {}
    });
  });
});
/**
 * Update form with yelp data
 * @param {type} listing
 * @returns {undefined}
 */

function populate(listing) {
  for (var _i = 0, _Object$entries = Object.entries(listing); _i < _Object$entries.length; _i++) {
    var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
        key = _Object$entries$_i[0],
        value = _Object$entries$_i[1];

    if (key === 'banner' && null != listing.banner) {
      uploadBanner(listing.banner);
    } else if (key === 'gallery' && null != listing.gallery) {
      uploadGallery(listing.gallery);
    } else if (key === 'description') {
      tinyMCE.get('description').setContent(listing.description);
    } else if (key === 'category') {
      selectOption(listing.category, 'categories');
    } else if (key === 'region') {
      selectOption(listing.region, 'region');
    } else if (key === 'timings') {
      timings(listing.timings);
    } else {
      var $element = jQuery('.form-group #' + key);

      if ($element.length) {
        $element.val(value);
      }
    }
  }
}
/**
 * Update timings
 * @type timings
 */


function timings(timings) {
  console.log(console.log(Object.keys(timings).length));
  console.log(timings);

  for (var i = 0; i <= 6; i++) {
    // Set selected 
    var days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']; //open

    var $elementOpen = jQuery('#' + days[i] + "-open");

    if ($elementOpen.length) {
      var $timeOpen = timings[days[i]]['opening'];
      $elementOpen.val($timeOpen.splice(2, 0, ":"));
      $elementOpen.select2().trigger('change');
    } //close


    var $elementClose = jQuery('#' + days[i] + "-close");

    if ($elementClose.length) {
      var $timeClose = timings[days[i]]['closing'];
      $elementClose.val($timeClose.splice(2, 0, ":"));
      $elementClose.select2().trigger('change');
    }
  }
}

function selectOption(option, $el) {
  var $element = jQuery('#' + $el + ' option:contains(' + option + ')');

  if ($element.length) {
    $element.attr('selected', true);
    $element.select2().trigger('change');
  }
}
/**
 * Add banner
 * @param {type} tmppath
 * @returns {undefined}
 */


function uploadBanner(tmppath) {
  jQuery("#uploaded_feat_img").append("<li class=\"mt-2 col-12\" id=\"gallery-img-1\">\n                              <input type=\"hidden\" name=\"banner\" value=\"".concat(tmppath, "\" />\n                                <img class=\"img-fluid\" src=\"").concat(tmppath, "\">\n                               <i data-targetimg=\"1\" class=\"remove-gallery-img p-1 shadow pointer btn btn-outline-danger border-0 las las-trash lar la-trash-alt\"></i>\n                             </li>"));
}
/**
 * Gallery Images
 * @param {type} gallery
 * @returns {undefined}
 */


function uploadGallery(gallery) {
  //-------------------------------
  // Gallery Images Upload
  //-------------------------------
  var imageHTML = jQuery("#gallery_images_list").html();
  var col = 1;
  gallery.forEach(function (tmppath, index) {
    imageHTML += "<li class=\"mt-2 col-3\" id=\"gallery-img-".concat(index, "\">\n                <input type=\"hidden\" name=\"listing_gallery[]\" value=\"").concat(tmppath, "\" />\n                          <img class=\"img-fluid\" src=\"").concat(tmppath, "\">\n                           <i data-targetimg=\"").concat(index, "\" class=\"remove-gallery-img p-1 shadow pointer btn btn-outline-danger border-0 las las-trash lar la-trash-alt\"></i>\n                           </li>");
    col++;
  });
  jQuery('#gallery_images_list').html(imageHTML);
}

/***/ }),

/***/ 5:
/*!***************************************************!*\
  !*** multi ./resources/assets/js/backend/yelp.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/www/html/ezzy-app/resources/assets/js/backend/yelp.js */"./resources/assets/js/backend/yelp.js");


/***/ })

/******/ });
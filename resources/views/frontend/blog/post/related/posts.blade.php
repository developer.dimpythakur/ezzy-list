<div class="related-posts">
    <h5 class="mt-4 mb-3">Related Posts</h5>
    <div class="row">
    @if(isset($relatedPosts))
        @foreach($relatedPosts as $post)
            <!-- Post -->
                <div class="col-md-6 col-sm-12 mb-4">
                    @include('frontend.blog.post.related.single')
                </div>
                <!-- /Post -->
            @endforeach
        @endif
    </div>
</div>
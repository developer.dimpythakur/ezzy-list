<?php

namespace App\Http\Controllers\Frontend\Listing;

use App\Http\Responses\RedirectResponse;
use App\Models\Listing\Listing\Listing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Listing\Reviews\Contracts\Review as ReviewContract;
use App\Models\Listing\Reviews\Review;
use Illuminate\Support\Facades\Auth;
use Prologue\Alerts\Facades\Alert;
use App\Models\Access\User\User;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller {

    public function __construct() {
//        $this->review();
    }

    /**
     * Default index page
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($id) {
        $listing = Listing::findOrFail($id);
        $ratings = $listing->getAllRatings(2);
        return response()->json($ratings);
    }

    /**
     * Store new review
     * @param Request $request
     * @return RedirectResponse|\Illuminate\Http\RedirectResponse
     */
    public function store(Request $request) {

        $v = Validator::make($request->all(), [
                    'title' => 'required',
                    'body' => 'required',
        ]);
        if ($v->fails()) {
            Alert::error('Error', 'Please fill the required fields');
            return redirect()->back()->withErrors($v->errors());
        }

        if ($request->has('listing_id') && auth()->user()->id):
            $listing = Listing::findOrFail($request->listing_id);
            // feedback array
            $feedbackArray = [
                $request->customer_service_rating,
                $request->quality_rating,
                $request->friendly_rating,
                $request->pricing_rating,
            ];
            $average = collect($feedbackArray)->average();
            $listing->rating([
                'title' => $request->title,
                'body' => $request->body,
                'customer_service_rating' => $request->customer_service_rating,
                'quality_rating' => $request->quality_rating,
                'friendly_rating' => $request->friendly_rating,
                'pricing_rating' => $request->pricing_rating,
                'rating' => $average,
                'recommend' => 'Yes',
                'approved' => true, // This is optional and defaults to false
                    ], auth()->user());


            Alert::success('Success', 'Review Added Successfully');
            return redirect()->back();

        endif;
        return new RedirectResponse(route('frontend.index'), ['flash_success' => 'Something went wrong please try again']);
    }

    public function delete(Request $request) {
        
    }

}

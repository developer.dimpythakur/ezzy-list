<?php

namespace App\Models\Listing\Reviews\Traits;

use App\Helpers\Auth\Auth;
use App\Models\Access\User\User;
use App\Models\Listing\Listing\Listing;

/**
 * Class Relationship
 */
trait Relationship {
    /*
     * put you model relationships here
     * Take below example for reference
     */

    public function listing() {
        return $this->belongsTo(Listing::class);
    }

    /**
     * Get the author of the score.
     *
     * return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author() {
        return $this->belongsTo(User::class, 'user_id');
    }

}

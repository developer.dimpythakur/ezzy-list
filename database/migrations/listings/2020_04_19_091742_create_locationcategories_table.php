<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $tableName = 'ezzy_location_categories';


    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100)->nullable()->default(null);
            $table->string('slug')->nullable()->default(null);
            $table->string('description', 100)->nullable()->default(null);
            $table->string('image', 200)->nullable()->default(null);
            $table->string('icon', 200)->nullable()->default(null);
            $table->integer('parent_id')->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default(1);

            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locationcategories');
    }
}

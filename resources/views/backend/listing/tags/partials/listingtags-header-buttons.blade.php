<!--Action Button-->
@if(Active::checkUriPattern('admin/listing-tags'))
    <export-component></export-component>
@endif
<!--Action Button-->
<div class="dropdown">
    <button class="btn btn-info dropdown-toggle" type="button" id="listingsMenuDropdown" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
        Action
    </button>
    <div class="dropdown-menu" aria-labelledby="listingsMenuDropdown">
        <a class="dropdown-item" href="{{route('admin.listing-tags.index')}}">
            <i class="las la-list-ul"></i> {{trans('menus.backend.pages.all')}}
        </a>
        @permission('create-page')
        <a class="dropdown-item rounded" href="{{route('admin.listing-tags.create')}}">
            <i class="las la-plus"></i> {{trans('menus.backend.listings.create')}}
        </a>
        @endauth
    </div>
</div>





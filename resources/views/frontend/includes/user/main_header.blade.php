<header class="{{ config('backend.header_class') }}">
    <!-- Logo -->
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto ml-3" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{route('frontend.index')}}">
        @if(isset($setting) && $setting->logo)
        <img height="48" width="226" class="navbar-brand" src="{{route('frontend.index')}}/img/site_logo/{{$setting->logo}}">
        @else
        {!! config('backend.project_logo') !!}
        @endif

    </a>

    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- ========================================================= -->
    <!-- ========= Top menu right items (ordered right) ========== -->
    <!-- ========================================================= -->
    <ul class="nav navbar-nav ml-auto ">
        <!-- Topbar. Contains the right part -->
        <!-- This file is used to store topbar (right) items -->
        <li class="nav-item dropdown pr-4">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img class="img-avatar" src="https://secure.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61.jpg?s=45&amp;d=https%3A%2F%2Fplacehold.it%2F160x160%2F00a65a%2Fffffff%2F%26text%3DD&amp;r=g" alt="Demo Admin">
            </a>
            <div class="dropdown-menu dropdown-menu-right mr-4 pb-1 pt-1">
                <a class="dropdown-item" href="http://127.0.0.1:8000/admin/edit-account-info"><i class="la la-user"></i> My Account</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="http://127.0.0.1:8000/admin/logout"><i class="la la-lock"></i> Logout</a>
            </div>
        </li>
    </ul>
    <!-- ========== End of top menu right items ========== -->
</header>

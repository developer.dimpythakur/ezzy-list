@extends('frontend.layouts.app')
@section('after-styles')
{{ Html::style(('css/frontend/map/listing-map-manager.css')) }}
@endsection
@section('content')
<section class="et-search-result-wrapper search-result-with-map list search-map listing-search">
    <div class="container-fluid pt-xs">
        <div class="row">
            <div class="col-lg-6">
                <!-- List Results -->
                <div class="listing-list-view et-top-rated-business list et-top-rated list">
                    <div class="row justify-content-md-center p-4">
                        <div class="col-lg-12 mb-3">
                            @include('frontend.listing.listing-search.section.search-filter')
                        </div>
                        <div class="col-lg-12">
                            @if(isset($searchResults))
                            @if ($searchResults-> isEmpty())
                            <section id="listings-not-found" class="margin-bottom-50">
                                <h2>Nothing found</h2>
                                <p>We’re sorry but we do not have any listings matching your search, try to
                                    change you search settings</p>
                            </section>
                            @else
                            <h6>There are {{ $searchResults->total() }} results for the term
                                <b>"{{ implode(', ',$searchterm) }}"</b></h6>
                            <hr/>
                            <section class="ezzy-listings pt-1 pb-1">
                                @foreach($searchResults as $type => $listing)
                                @include('frontend.listing.components.listing-style.style-one')
                                @endforeach
                            </section>
                            @endif
                            <section class="w-100 mt-3 py-1">
                                <!-- Pagination -->
                                {{ $searchResults->links() }}
                                <!-- /Pagination -->
                            </section>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /List Results -->
            </div>
            <div class="col-lg-6 position-relative map-right">
                @include('frontend.listing.components.listing-map.map')
            </div>
        </div>
    </div>
</section>
@endsection

@section('after-scripts')
<script type='text/javascript'>
    var EZZY_MAP = {
        "submitCenterPoint": "40.757662,-73.974741",
        "centerPoint": "40.757662,-73.974741",
        "map_provider": "mapbox",
        "mapbox_access_token": "pk.eyJ1IjoiZmVsb25pIiwiYSI6ImNrOXMzY290cDBxbGIzZW1ybzM2cngzdnYifQ.Adeh0zFoXWDKXwqE6eQQhA",
        "mapbox_retina": "on",
        "start_point": "New York, NY, USA",
        "start_geo_lat": "40.7127753",
        "start_geo_long": "-74.0059728",
        "enable_map": "1",
        "user_location": [],
        "l10n": {
            "locked": "Lock Pin",
            "unlocked": "Unlock Pin"
        },
        "country": "USA",
        "strings": {
            "file-upload": "Add Image",
            "not_found": "No results",
            "results-no": "Results",
            "select_options": "Select Some Options",
            "select_option": "Select an Option",
            "no_results_match": "No results match"
        }
    };
</script>
<!-- Map API -->
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAmrxCFuWyNUlMh6wLitVLI3pF7j_HqAFY&#038;libraries=places&#038;ver=1.1'></script>
<!-- Custom Js -->
<script src="{{ asset('listing/jquery.cookie.min.js') }}"></script>
<script src="{{ asset('listing/mapify.js') }}"></script>
<script src="{{ asset('listing/geo-tag-text.js') }}"></script>
<script src="{{ asset('listing/map/leaflet.js') }}"></script>
<script src="{{ asset('listing/listing-manager.js') }}"></script>
@endsection

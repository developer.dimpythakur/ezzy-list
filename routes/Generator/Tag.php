<?php

/**
 * Tag
 *
 */
Route::group(['namespace' => 'Backend\Listing', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {

    Route::group(['namespace' => 'Tags'], function () {
        Route::resource('listing-tags', 'TagController');
        //For Datatable
        Route::post('listing-tags/get', 'TagTableController')->name('listing-tags.get');
    });
});

<div class="{{ config('backend.sidebar_class') }}">
    <nav class="sidebar-nav overflow-hidden">
        <div class="sidebar sidebar-pills bg-light">
            <nav class="sidebar-nav overflow-hidden ps">
                <ul class="nav">
                    {{ renderMenuItems(getMenuItems()) }}
                </ul>
            </nav>
        </div>
    </nav>
</div>

@push('before_scripts')
<script type="text/javascript">
    /* Recover sidebar state */
    if (Boolean(sessionStorage.getItem('sidebar-collapsed'))) {
        var body = document.getElementsByTagName('body')[0];
        body.className = body.className.replace('sidebar-lg-show', '');
    }

    /* Store sidebar state */
    var navbarToggler = document.getElementsByClassName("navbar-toggler");
    for (var i = 0; i < navbarToggler.length; i++) {
        navbarToggler[i].addEventListener('click', function (event) {
            event.preventDefault();
            if (Boolean(sessionStorage.getItem('sidebar-collapsed'))) {
                sessionStorage.setItem('sidebar-collapsed', '');
            } else {
                sessionStorage.setItem('sidebar-collapsed', '1');
            }
        });
    }
</script>
@endpush

@push('after_scripts')
<script>
    var full_url = "{{ Request::fullUrl() }}";
    var $navLinks = $(".sidebar-nav li a");
    var $curentPageLink = $navLinks.filter(
            function () {
                return $(this).attr('href') === full_url;
            }
    );

    if (!$curentPageLink.length > 0) {
        $curentPageLink = $navLinks.filter(function () {
            if ($(this).attr('href').startsWith(full_url)) {
                return true;
            }

            if (full_url.startsWith($(this).attr('href'))) {
                return true;
            }
            return false;
        });
    }

    // for the found links that can be considered current, make sure 
    // - the parent item is open
    $curentPageLink.parents('li').addClass('open');
    // - the actual element is active
    $curentPageLink.each(function () {
        $(this).addClass('active');
    });
</script>


<script>
    function checkUri(urlPrefix) {
        return (window.location.href.indexOf(urlPrefix) > -1) ? true : false;
    }
</script>
@endpush

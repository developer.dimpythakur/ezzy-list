@extends('frontend.layouts.app')
@section('content')
<!-- Explore by city -->
@include('frontend.includes.section.slider')
<!-- Explore by city -->
<section class="et-blog-grid">
    <div class="container pt-5 pt-xs">
        <div class="row">
            @if($post && !is_null($post))
            @include('frontend.blog.post.section.style-one')
            @endif
        </div>
    </div>
</section>
<!-- Newsletter Subscription -->
@include('frontend.includes.section.newsletter.newsletter')
<!-- /Newsletter Subscription -->
@endsection
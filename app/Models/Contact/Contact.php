<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    public $table = 'contacts';
    public $fillable = ['name', 'email', 'phone_number', 'subject', 'message'];

}

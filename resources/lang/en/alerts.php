<?php

return [
    "backend" => [
        "roles" => [
            "created" => "The role was successfully created.",
            "deleted" => "The role was successfully deleted.",
            "updated" => "The role was successfully updated.",
        ],
        "permissions" => [
            "created" => "The permission was successfully created.",
            "deleted" => "The permission was successfully deleted.",
            "updated" => "The permission was successfully updated.",
        ],
        "users" => [
            "confirmation_email" => "A new confirmation e-mail has been sent to the address on file.",
            "created" => "The user was successfully created.",
            "deleted" => "The user was successfully deleted.",
            "deleted_permanently" => "The user was deleted permanently.",
            "restored" => "The user was successfully restored.",
            "session_cleared" => "The user's session was successfully cleared.",
            "updated" => "The user was successfully updated.",
            "updated_password" => "The user's password was successfully updated.",
        ],
        "pages" => [
            "created" => "The Page was successfully created.",
            "deleted" => "The Page was successfully deleted.",
            "updated" => "The Page was successfully updated.",
        ],
        "blogcategories" => [
            "created" => "The Blog Category was successfully created.",
            "deleted" => "The Blog Category was successfully deleted.",
            "updated" => "The Blog Category was successfully updated.",
        ],
        "blogtags" => [
            "created" => "The Blog Tag was successfully created.",
            "deleted" => "The Blog Tag was successfully deleted.",
            "updated" => "The Blog Tag was successfully updated.",
        ],
        "blogs" => [
            "created" => "The Blog was successfully created.",
            "deleted" => "The Blog was successfully deleted.",
            "updated" => "The Blog was successfully updated.",
        ],
        "settings" => [
            "updated" => "The Setting was successfully updated.",
        ],
        "faqs" => [
            "created" => "The Faq was successfully created.",
            "deleted" => "The Faq was successfully deleted.",
            "updated" => "The Faq was successfully updated.",
        ],
        "menus" => [
            "created" => "The Menu was successfully created.",
            "deleted" => "The Menu was successfully deleted.",
            "updated" => "The Menu was successfully updated.",
        ],
        "lockdowns" => [
            "created" => "The Lockdown was successfully created.",
            "deleted" => "The Lockdown was successfully deleted.",
            "updated" => "The Lockdown was successfully updated.",
        ],
        "listings" => [
            "created" => "The Listing was successfully created.",
            "deleted" => "The Listing was successfully deleted.",
            "updated" => "The Listing was successfully updated.",
        ],
        "categories" => [
            "created" => "The Category was successfully created.",
            "deleted" => "The Category was successfully deleted.",
            "updated" => "The Category was successfully updated.",
        ],
        "locations" => [
            "created" => "The Location was successfully created.",
            "deleted" => "The Location was successfully deleted.",
            "updated" => "The Location was successfully updated.",
        ],
        "regions" => [
            "created" => "The Regions was successfully created.",
            "deleted" => "The Regions was successfully deleted.",
            "updated" => "The Regions was successfully updated.",
        ],
        "tags" => [
            "created" => "The Tag was successfully created.",
            "deleted" => "The Tag was successfully deleted.",
            "updated" => "The Tag was successfully updated.",
        ],
    ],
];

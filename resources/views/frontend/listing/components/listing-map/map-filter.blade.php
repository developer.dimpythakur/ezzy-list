<!-- Slider -->
<div class="et-home-slider et-home-map-banner img-overlay transition-1 no-hover">
    <div id="map"></div>
    @include('frontend.listing.components.listing-search.filter')
</div>
<!-- /Slider -->
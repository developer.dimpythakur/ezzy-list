@php
use Illuminate\Support\Facades\Route;
@endphp
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>
            {{ isset($title) ? $title.' :: '.config('backend.project_name') : 'Page not found '.' :: '.config('backend.project_name') }}</title>
    </title>
    <!-- Meta -->
    <meta name="description" content="@yield('meta_description', 'Eazzy List')">
    <meta name="author" content="@yield('meta_author', 'Eazzy List')">
    <meta name="keywords" content="@yield('meta_keywords', 'Eazzy List')">
    @yield('meta')

    <!-- Styles -->
    @yield('before-styles')
    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    @langrtl
    {{ Html::style(getRtlCss(('css/frontend/frontend.css'))) }}
    @else
    {{ Html::style(('css/frontend/app.css')) }}
    {{ Html::style(('css/frontend/frontend-styes.css')) }}
    @endlangrtl

    @yield('after-styles')
    <!-- Scripts -->
    <script>
        window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
    </script>
</head>
<body id="app-layout">
    <div id="app">
        <!-- Header -->
        @include('frontend.includes.header')
        <!-- /Header -->
        @yield('content')
        <!-- Footer -->
        @include('frontend.includes.footer')
        <!-- /Footer -->
    </div><!--#app-->
    @yield('before-scripts')
    @stack('before-scripts')
    @include(('frontend.layouts.sections.scripts'))
    @include('frontend.includes.modal.auth-modal')

    @yield('after-scripts')
    @stack('after-scripts')
</body>
</html>
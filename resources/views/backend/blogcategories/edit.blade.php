@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.blogcategories.management') . ' | ' . trans('labels.backend.blogcategories.edit'))

@section('page-header')
<h1><span class="text-capitalize">{{ trans('labels.backend.blogcategories.edit') }}</span></h1>
@endsection
@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::model($blogcategory, ['route' => ['admin.blogCategories.update', $blogcategory], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role','files'=>true]) }}
        <div class="card">
            <div class="card-body row">
                @include("backend.blogcategories.form")
            </div><!-- /.card-body -->
        </div><!--card-->

        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=update_and_back">Update</span>
                </button>
            </div>
            <a href="{{ route('admin.blogCategories.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>


@endsection
@section("after_scripts")


@endsection







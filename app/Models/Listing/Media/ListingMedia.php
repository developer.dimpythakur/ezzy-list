<?php

namespace App\Models\Listing\ListingMedia;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use App\Models\Listing\ListingMedia\Traits\Attribute\ListingMediaAttribute;
use App\Models\Listing\ListingMedia\Traits\ListingMediaRelationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListingMedia extends BaseModel {

    use ModelTrait,
        SoftDeletes,
        ListingMediaRelationship,
        ListingMediaAttribute {
        // PageAttribute::getEditButtonAttribute insteadof ModelTrait;
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'created_by' => 1,
    ];
    protected $fillable = [
        'filename',
        'type',
        'media_type',
        'media_count',
        'path',
        'url',
        'status',
        'created_by'
    ];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.ezzy_media.table');
    }

    /**
     * Return allowed media types
     * @param $type
     * @return string[]
     */
    public static function mediaAllowed($type) {
        switch ($type):
            case 'audio':
                return ['soundcloud', 'youtube'];
            case 'video':
                return ['dailymotion', 'youtube', 'vimeo'];
        endswitch;
    }

    /**
     * Return ListingMedia map Table
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getMapTable() {
        return config('module.listing_media.table');
    }

}

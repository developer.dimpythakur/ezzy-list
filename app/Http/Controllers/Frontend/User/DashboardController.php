<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\DashboardViewRequest;
use App\Models\Listing\Listing\Listing;
use App\Models\Listing\Categories\Category;
use Prologue\Alerts\Facades\Alert;
//Yelp Search
use App\Repositories\Frontend\Listing\YelpRepository;
use Illuminate\Http\Request;
//validation
use App\Rules\PhoneNumber;
use Illuminate\Support\Facades\Validator;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller {

    /**
     * Listing Model
     * @var type 
     */
    protected $listing;

    /**
     * Yelp Repository
     * protected $yelp;
     */
    protected $yelp;

    public function __construct() {
        $this->listing = new Listing;
        $this->yelp = new YelpRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(DashboardViewRequest $request) {
        $published = $this->listing->published()->get();
        $pending = $this->listing->pending()->get();
        $review = $this->listing->review()->get();
        $expired = $this->listing->review()->get();
        return view('frontend.user.dashboard')
                        ->with([
                            'published' => $published,
                            'pending' => $pending,
                            'review' => $review,
                            'expired' => $expired,
        ]);
    }

    /**
     * Get All reviews 
     * @param type $param
     */
    public function reviews() {
        $listings = $this->listing->published()->paginate(5);
        return view('frontend.user.reviews')
                        ->with([
                            'listings' => $listings,
        ]);
    }

    /**
     * Return all bookmark listings
     * @param type $param
     * @return type
     */
    public function bookmark() {
        $listings = $this->listing->saved_listings();
        return view('frontend.user.bookmark')
                        ->with([
                            'listings' => $listings,
        ]);
    }

    /**
     * Remove bookmark
     * @param DashboardViewRequest $request
     * @return type
     */
    public function remove(DashboardViewRequest $request) {
        if ($request->user_id && $request->listing_id):
            $listing = Listing::find($request->listing_id);
            if ($listing):
                $listing->owner->bookmark($listing);
            endif;
        endif;
        Alert::success('Listing removed from bookmarks');
        return redirect()->back();
    }

    /**
     * Search yelp
     * @param \App\Http\Controllers\Frontend\User\Request $request
     */
    public function yelpSearchPhone(Request $request) {
        $business = $this->yelp->searchBusiness($request);
        if (count($business)):
            return response()->json([
                        'type' => 'success',
                        'message' => $business
            ]);
        endif;
        return response()->json([
                    'type' => 'error',
                    'message' => 'Something went wrong please try again.'
        ]);
    }

    /**
     * Search yelp
     * @param \App\Http\Controllers\Frontend\User\Request $request
     */
    public function yelpSearch(Request $request) {
        $validator = Validator::make($request->all(), [
                    'phone' => ['required', new PhoneNumber],
        ]);
        if ($validator->fails()) {
            return response()->json([
                        'type' => 'error',
                        'message' => 'Please enter a valid phone number.'
            ]);
        }
        $businesses = $this->yelp->searchPhone($request->phone);
        if (($businesses->total >= 1)):
            foreach ((collect($businesses->businesses)) as $business):
                $saved = (new \App\Seeder\Yelp())->store($business->id, true);
            endforeach;
            if (isset($saved['status']) && $saved['status'] == 0):
                return response()->json([
                            'type' => 'error',
                            'message' => $saved['message']
                ]);
            else:
                return response()->json([
                            'type' => 'success',
                            'message' => 'Listing imported successfully'
                ]);
            endif;
        endif;
        return response()->json([
                    'type' => 'error',
                    'message' => 'Please check your number and try again.'
        ]);
    }

    /**
     * Get Business
     * @param type $businessId
     */
    public function getBusiness($businessId) {
        $listing = ($this->yelp->business($businessId));
        if ($listing):
            $business['id'] = ($listing->id);
            $business['name'] = $listing->name;
            $business['description'] = $listing->name;
            $business['tagline'] = $listing->alias;
            $business['phone'] = $listing->display_phone;
            $business['banner'] = $listing->image_url;
            $business['url'] = $listing->url;
            $business['category'] = $this->categories($listing->categories);
            $business['location'] = $this->filterLocations($listing);
            $business['region'] = $listing->location->city;
            $business['latitude'] = $listing->coordinates->latitude;
            $business['longitude'] = $listing->coordinates->longitude;
            $business['gallery'] = isset($listing->photos) ? $listing->photos : null;
            if (!$listing->is_closed):
                $business['timings'] = $this->hours($listing);
            endif;
        endif;
        return $business;
    }

    /**
     * Location address including city and country
     * @param type $business
     * @return type
     */
    public function filterLocations($business) {
        if (isset($business->location)):
            return collect($business->location->display_address)->implode(' ');
        endif;
        return null;
    }

    /**
     * Filter Hours
     * @param type $hours
     * @return type
     */
    public function hours($hours = null) {
        $timings = [];
        if (isset($hours->hours)):
            $days = array(
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday',
                'Sunday',
            );
            $hours = $hours->hours;
            $timing = isset($hours[0]->open) ? $hours[0]->open : null;
            if (null !== $timing):
                for ($i = 0; $i <= 6; $i++):
                    $timings[strtolower($days[$i])] = [
                        'opening' => $timing[$i]->start,
                        'closing' => $timing[$i]->end,
                    ];
                endfor;
            endif;
            return $timings;
        endif;
        return $timings;
    }

    /**
     * Categories
     * @param type $categories
     * @return type
     */
    public function categories($categories) {
        if (count($categories)):
            foreach ($categories as $category):
                $available = Category::where('name', 'like', '%' . $category->title . '%');
                if ($available->count()):
                    return $category->title;
                endif;
            endforeach;
        endif;
        return ['Ezzy'];
    }

}

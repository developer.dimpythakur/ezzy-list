<div class="card card-border-radius general-questions shadow mb-4 p-4 border-0">
    <h5 class="card-title mb-4">General Questions</h5>
    <div class="et-accordion" id="accordion">
        @if($listing->faqs && count($listing->faqs))
        @php
        $count =1;
        $collapseClass='colapse';
        if($count===1){
        $collapseClass='show';
        }
        @endphp
        @foreach($listing->faqs as $faq)
        <div class="card">
            <div class="card-header bg-transparent border-0 p-0" id="heading-{{$count}}">
                <h5 class="mb-0">
                    <button class="btn w-100 text-white text-left shadow-none"
                            data-toggle="collapse"
                            data-target="#collapse-{{$count}}"
                            aria-expanded="true"
                            aria-controls="collapse-{{$count}}">
                        {{ $faq->question }}
                        <i class="las la-plus-circle float-right mt-1"></i>
                        <i class="las la-minus-circle float-right mt-1"></i>
                    </button>
                </h5>
            </div>
            <div id="collapse-{{$count}}"
                 class="collapse {{$collapseClass}}"
                 aria-labelledby="heading-{{$count}}"
                 data-parent="#accordion">
                <div class="card-body">
                    {{ $faq->answer }}
                </div>
            </div>
        </div>
        @php $count = $count+1 @endphp
        @endforeach
        @else
        <div class="w-100 p-4">
            <div class="p-3 alert alert-danger">
                <p class="m-0"><i class="la la-question"></i> No faq</p>
            </div>
        </div>
        @endif
    </div>
</div>

<!-- Newsletter Subscription -->
<section class="et-subscription-form text-white position-relative">
    <div class="container">
        <div class="row">
            <div class="col-md-6 text-md-left text-center">
                <h4 class="pt-sm-0 pt-5 pt-xs-0 text-white title-newsletter">Join our community</h4>
                <p class="text-white pb-md-0 pb-4 mb-0">Subscribe to get the latest updates and offers</p>
            </div>
            <div class="col-md-6 text-center mb-5 mb-md-0 text-md-right">
                {{ Form::open(['route' => 'frontend.subscribe', 'class' => 'form-horizontal','autocomplete'=>'off', 'method' => 'POST']) }}
                <div class="input-wrap position-relative d-inline-block">
                    <input type="text" class="bg-white border-0 newsletter-input form-control outline"
                           placeholder="Enter your email address" name="email">
                    <div class="submit-btn border-0 btn transition-1 text-white position-absolute white ml-auto pointer-event d-flex justify-content-center align-items-center">
                        Subscribe
                        <input type="submit" class="btn border-0 transition-1 field-btn position-absolute"
                               value="" name="submit">
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
<!-- /Newsletter Subscription -->
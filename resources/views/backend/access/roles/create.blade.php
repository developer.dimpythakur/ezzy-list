@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.roles.management') . ' | ' . trans('labels.backend.access.roles.create'))

@section('page-header')
<h1>
    {{ trans('labels.backend.access.roles.management') }}
    <small>{{ trans('labels.backend.access.roles.create') }}</small>
</h1>
@endsection

@section('content')

<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.pages.section.grouped_errors'))

        {{ Form::open(['route' => 'admin.access.role.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-role']) }}

        <div class="card mt-2 p-2">
            <div class="card-header">
                <h6 class="my-2"><i class="la la-user-secret"></i> {{ trans('labels.backend.access.roles.create') }}</h6>
            </div>
            <div class="card-body bold-labels">
                <div class="row my-3">
                    <div class="form-group col-12">
                        {{ Form::label('name', trans('validation.attributes.backend.access.roles.name'), ['class' => 'control-label required']) }}
                        {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.roles.name'), 'required' => 'required']) }}
                    </div><!--form control-->
                    <div class="form-group col-12">
                        {{ Form::label('associated_permissions', trans('validation.attributes.backend.access.roles.associated_permissions'), ['class' => 'control-label']) }}
                        {{ Form::select('associated_permissions', array('all' => trans('labels.general.all'), 'custom' => trans('labels.general.custom')), 'all', ['class' => 'form-control select2 box-size']) }}
                        <div id="available-permissions" class="hidden mt-20" style="width: 700px; height: 200px; overflow-x: hidden; overflow-y: scroll;">
                            <div class="row">
                                <div class="col-xs-12">
                                    @if ($permissions->count())
                                    @foreach ($permissions as $perm)
                                    <label class="control control--checkbox">
                                        <input type="checkbox" name="permissions[{{ $perm->id }}]" value="{{ $perm->id }}" id="perm_{{ $perm->id }}" {{ is_array(old('permissions')) && in_array($perm->id, old('permissions')) ? 'checked' : '' }} /> <label for="perm_{{ $perm->id }}">{{ $perm->display_name }}</label>
                                        <div class="control__indicator"></div>
                                    </label>
                                    <br/>
                                    @endforeach
                                    @else
                                    <p>There are no available permissions.</p>
                                    @endif
                                </div><!--col-lg-6-->
                            </div><!--row-->
                        </div><!--available permissions-->
                    </div><!--form control-->
                    <div class="form-group col-12">
                        {{ Form::label('sort', trans('validation.attributes.backend.access.roles.sort'), ['class' => 'control-label']) }}
                        {{ Form::text('sort', ($roleCount+1), ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.roles.sort')]) }}
                    </div><!--col-lg-10-->

                    <div class="form-group col-12">
                        {{ Form::label('status', trans('validation.attributes.backend.access.roles.active'), ['class' => 'control-label']) }}

                        <div class="col-lg-10">
                            <div class="control-group">
                                <label class="control control--checkbox">
                                    {{ Form::checkbox('status', 1, true) }}
                                    <div class="control__indicator"></div>
                                </label>
                            </div>
                        </div><!--col-lg-3-->
                    </div><!--form control-->
                    <div class="edit-form-btn">
                        {{ link_to_route('admin.access.role.index', trans('buttons.general.cancel'), [], ['class' => 'btn btn-danger btn-md']) }}
                        {{ Form::submit(trans('buttons.general.crud.create'), ['class' => 'btn btn-primary btn-md']) }}
                    </div>
                </div><!-- /.box-body -->
            </div><!--box-->

            <div id="updateForm" class="form-group">
                <div class="btn-group" role="group">
                    <button type="submit" class="btn btn-success">
                        <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                        <span data-value="update">Update</span>
                    </button>
                </div>
                <a href="" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</section>
@endsection

@section('after-scripts')
{{ Html::script('js/backend/access/roles/script.js') }}
<script type="text/javascript">
    Backend.Utils.documentReady(function () {
        Backend.Roles.init("rolecreate")
    });
</script>
@endsection

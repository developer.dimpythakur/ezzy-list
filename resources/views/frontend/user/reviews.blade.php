@extends('frontend.layouts.user')
@section('page-header')
<h2>{{trans('menus.backend.listings.reviews')}}</h2>
@endsection
@section('content')


<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                <h6 class="my-2"><i class="la la-comment-alt"></i> Visitor Reviews</h6>
            </div>
            <div class="card-body bold-labels">
                <?php
                if ($listings && $listings->count()):
                    foreach ($listings as $key => $listing):
                        $ratings = $listing->getAllRatings($listing->id);
                        foreach ($ratings as $review):
                            ?>
                            <div class="user-review">
                                <div class="row">
                                    <div class="user-image col-md-2">
                                        <img src="{{ ($review->author->picture) }}"
                                             alt="{{ ($review->author->name) }}">
                                    </div>
                                    <div class="comment-by col-md-10">
                                        {{ $review->author->name }}
                                        <div class="comment-by-listing">on 
                                            <a href="javascript:void(0)">{{$listing->slug}}</a>
                                        </div> 
                                        <span class="date">{{$review->created_at}}</span>
                                        <div data-toggle="tooltip" data-placement="top" title="Rating {{ $review->rating}}"
                                             class="star-rating" data-rating="{{ $review->rating }}"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 ml-auto">
                                        <h4>{{ substr($review->title,0,40) }}</h4>
                                        <p>{{ $review->body }}</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 ml-auto">
                                        <div class="btn-box">
                                            <a href="#dialog" class="btn-link btn btn-default review-rating">
                                                <i class="la la-undo"></i> Reply to this review
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    endforeach;
                else:
                    ?>
                    <div class="w-100 p-4">
                        <div class="p-3 alert alert-danger">
                            <p class="m-0"><i class="la la-comment"></i> No review found</p>
                        </div>
                    </div>
                <?php
                endif;
                ?>
            </div>
        </div>
    </div><!-- col-md-12 -->
</div><!-- row -->
@endsection
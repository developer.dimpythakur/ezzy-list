<?php

namespace App\Repositories\Backend\Listing\Regions;

use App\Events\Backend\Listing\Regions\Created;
use App\Events\Backend\Listing\Regions\Updated;
use App\Http\Requests\Request;
use DB;
use App\Models\Listing\Region\Region;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\Storage;
use Prologue\Alerts\Facades\Alert;

/**
 * Class RegionRepository.
 */
class RegionRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Region::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = 'images' . DIRECTORY_SEPARATOR . 'listing/regions' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.ezzy_regions.table') . '.id',
                            config('module.ezzy_regions.table') . '.name',
                            config('module.ezzy_regions.table') . '.slug',
                            config('module.ezzy_regions.table') . '.image',
                            config('module.ezzy_regions.table') . '.created_at',
                            config('module.ezzy_regions.table') . '.updated_at',
                        ])->orderBy('created_at', 'DESC');
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @return bool
     * @throws GeneralException
     */
    public function create(array $request) {
        if ($this->query()->where('name', $request['name'])->first()) {
            throw new GeneralException('Name already exists');
        }
        DB::transaction(function () use ($request) {
            $image = $this->uploadImage($request);
            $request['image'] = $image;
            $request['city'] = isset($request['name']) ? $request['name'] : "Templeton";
            $request['created_by'] = access()->user()->id;
            if ($location = Region::create($request)) {
                event(new Created($location));
                return true;
            }
            throw new GeneralException(trans('exceptions.backend.locations.create_error'));
        });
    }

    /**
     * Upload Image.
     * @param $input
     * @return string|null
     */
    public function uploadImage($input) {
        if (isset($input['image']) && !empty($input['image'])):
            $avatar = $input['image'];
            $fileName = time() . $avatar->getClientOriginalName();
            if ($this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()))):
                return $fileName;
            endif;
        endif;
        return 'location-default.jpeg';
    }

    /**
     *  For updating the respective Model in storage
     * @param Location $listing_region
     * @param array $input
     * @throws GeneralException
     */
    public function update(Region $listing_region, array $input) {
        DB::transaction(function () use ($listing_region, $input) {
            // Uploading Image
            if (isset($input['image'])) {
                $input['image'] = $this->uploadImage($input);
            }
            $input['city'] = isset($input['name']) ? $input['name'] : "Templeton";
            $input['updated_by'] = access()->user()->id;
            $listing_region->slug = null;
            if ($listing_region->update($input)) {
                event(new Updated($listing_region));
                return true;
            }
            throw new GeneralException(trans('exceptions.backend.locations.create_error'), []);
        });
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Location $location
     * @return bool
     * @throws GeneralException
     */
    public function delete(Region $listing_region) {
        if ($listing_region->delete()) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.locations.delete_error'));
    }

}

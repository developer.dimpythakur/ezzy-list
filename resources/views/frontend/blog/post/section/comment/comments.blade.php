<div class="post-comments">
    <h5 class="mt-4 mb-3">Comments (1)</h5>
    <div class="user-comment">
        <div class="d-md-flex mt-3 mb-3 d-sm-block">
            <div class="commenter-thumbnail">
                <img src="{{ $post->owner->picture }}" alt="{{ $post->owner->first_name }}"
                     class="img-fluid rounded-pill shadow">

            </div>
            <div class="comment-content">
                <h6>{{ $post->owner->first_name }} <i
                        class="reply-trigger pointer float-right text-muted fa fa-reply"
                        data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Reply"></i></h6>
                <p class="border-bottom pb-3"></p>
            </div>
        </div>
    </div>
</div>
<?php

namespace App\Repositories\Frontend\Listings;

use App\Exceptions\GeneralException;
use App\Models\Listing\Categories\Category;
use App\Repositories\BaseRepository;

/**
 * Class ListingsRepository.
 */
class CategoryRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Category::class;

    /*
     * Find page by page_slug
     */

    public function findBySlug($category_slug) {
        if (!is_null($this->query()->whereSlug($category_slug)->firstOrFail())) {
            return $this->query()->whereSlug($category_slug)->firstOrFail();
        }
        throw new GeneralException(trans('exceptions.backend.category.not_found'));
    }

}

@extends('backend.layouts.user')
@section('content')
<section class="row my-3">
    @include('backend.access.users.components.title')
</section>
@if ($errors->any())
<section class="row">
    <section class="col-6">
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger mb-2 mt-2">{{$error}}</div>
        @endforeach
    </section>
</section>
@endif
<section class="row">
    <section class="col-6">
        <section class="row">
            {{ Form::model($logged_in_user, ['route' => 'admin.access.user.account.update','files' => true, 'class' => 'form-', 'method' => 'PATCH']) }}
            @include('backend.access.users.components.profile')
            {{ Form::close() }}
        </section>
    </section>
    <section class="col-6">
        {{ Form::open(['route' => ['frontend.auth.password.change'], 'class' => 'form-', 'method' => 'patch']) }}
        @include('frontend.user.components.profile.password')
        {{ Form::close() }}
    </section>
</section>

@endsection

@section('after-scripts')

<script type="text/javascript">
    $(document).ready(function () {

        // To Use Select2
        Backend.Select2.init();

        if ($.session.get("tab") == "edit")
        {
            $("#li-password").removeClass("active");
            $("#li-profile").removeClass("active");
            $("#li-edit").addClass("active");

            $("#profile").removeClass("active");
            $("#password").removeClass("active");
            $("#edit").addClass("active");
        } else if ($.session.get("tab") == "password")
        {
            $("#li-password").addClass("active");
            $("#li-profile").removeClass("active");
            $("#li-edit").removeClass("active");

            $("#profile").removeClass("active");
            $("#password").addClass("active");
            $("#edit").removeClass("active");
        }

        $(".tabs").click(function () {
            var tab = $(this).attr("aria-controls");
            $.session.set("tab", tab);
        });
    });
</script>
@endsection
<?php

return array(
    array('id' => '3', 'name' => 'Restaurants', 'slug' => 'restaurants', 'image' => '1597741932premiumforrestaurants_0.jpg', 'icon' => 'icon-dish', 'status' => '1', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-09 07:23:33', 'updated_at' => '2020-08-18 09:12:12', 'deleted_at' => NULL),
    array('id' => '4', 'name' => 'Hotels', 'slug' => 'hotels', 'image' => '1597741946Nightlife.jpg', 'icon' => 'icon-coffee-cup', 'status' => '1', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-09 07:23:47', 'updated_at' => '2020-08-18 09:12:26', 'deleted_at' => NULL),
    array('id' => '5', 'name' => 'Nightlife', 'slug' => 'nightlife', 'image' => '1597741974Nightlife.jpg', 'icon' => 'icon-video-camera', 'status' => '1', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-09 07:24:00', 'updated_at' => '2020-08-18 09:12:54', 'deleted_at' => NULL),
    array('id' => '6', 'name' => 'Cinema', 'slug' => 'cinema', 'image' => '1597741982cinema.jpg', 'icon' => 'icon-video-camera', 'status' => '1', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-09 07:24:21', 'updated_at' => '2020-08-18 09:13:02', 'deleted_at' => NULL),
    array('id' => '7', 'name' => 'Shopping', 'slug' => 'shopping', 'image' => '1597742003download.jpeg', 'icon' => 'icon-tyre', 'status' => '1', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-09 07:26:02', 'updated_at' => '2020-08-18 09:13:23', 'deleted_at' => NULL)
);

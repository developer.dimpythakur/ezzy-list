@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.listing.tags.management') . ' | ' . trans('labels.backend.listing.tags.create'))
@section('page-header')
<h1>{{ trans('labels.backend.listing.tags.create') }}</h1>
@endsection

@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::open(['route' => 'admin.listing-tags.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id' => 'create-tags']) }}
        <div class="card">
            <div class="card-body row">
                @include("backend.listing.tags.form")
            </div>
        </div>
        <div id="saveForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back">Save and back</span>
                </button>
            </div>
            <a href="{{ route('admin.listing-tags.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>

@endsection
@section("after_scripts")

@endsection

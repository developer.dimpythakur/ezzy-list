<?php

namespace App\Http\Controllers\Frontend\Posts;

use App\Http\Controllers\Controller;
use App\Models\BlogCategories\BlogCategory;
use App\Models\Blogs\Blog;
use App\Models\BlogTags\BlogTag;
use App\Models\Listing\Categories\Category;
use App\Repositories\Frontend\Posts\PostsRepository;
use Illuminate\Support\Facades\Storage;

/**
 * Class PostController.
 */
class PostController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function index() {
        $title = 'Ezzy Blog';
        $posts = Blog::paginate(27);
        $categories = Category::select('name')->where('status', 1)->get();
        $image = Blog::inRandomOrder()->take(1)->where('status', 'Published')->first('featured_image');
        if (filter_var($image->featured_image, FILTER_VALIDATE_URL)):
            $banner = $image->featured_image;
        else:
            $banner = Storage::url('images/blog/' . $image->featured_image);
        endif;
        return view('frontend.blog.index',
                compact(
                        'title',
                        'banner',
                        'posts',
                        'categories'
                )
        );
    }

    /**
     *
     * Show Posts By Slug
     * @param $slug
     * @param PostsRepository $post
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function show($slug, PostsRepository $post) {
        $post = $post->findBySlug($slug);
        $previousPost = Blog::where('id', '<', $post->id)->orderBy('id', 'desc')->first();
        $nextPost = Blog::where('id', '>', $post->id)->orderBy('id')->first();
        $blogCategories = BlogCategory::with('blog')->orderBy('name')->get();
        $tags = BlogTag::where('status', 1)->limit(10)->get();
        $related = $this->related($post);
        return view('frontend.blog.post.post-with-right-sidebar')
                        ->withPost($post)
                        ->withCategories($blogCategories)
                        ->withTags($tags)
                        ->withPrevious($previousPost)
                        ->withNext($nextPost)
                        ->withTitle($post->name)
                        ->withRelatedPosts($related);
    }

    public function related($post) {
        return Blog::with('categories')
                        ->where('id', '!=', $post->id)
                        ->limit(2)
                        ->get();
    }

}

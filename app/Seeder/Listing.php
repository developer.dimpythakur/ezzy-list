<?php

namespace App\Seeder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Listing
 *
 * @author codepluck
 */
use App\Repositories\Frontend\Listing\YelpRepository;
use App\Models\Listing\Region\Region;
use App\Models\Listing\Categories\Category;
use App\Seeder\Repository;
use Illuminate\Support\Arr;

class Listing extends Repository {

    private $yelp;

    public function add() {
        $cat = array(
            'business', 'city', 'food',
            'fashion'
        );
        $faker = \Faker\Factory::create();
        for ($i = 0; $i <= 20; $i++):
            $this->fake([
                'name' => $faker->company,
                'slug' => Str::slug($faker->company),
                'tagline' => $faker->catchPhrase,
                'description' => $faker->realText(500),
                'banner' => banner($cat[$faker->numberBetween(0, 3)]),
                'tags' => tags(),
                'categories' => categories(),
                'type' => 'listing',
                'website' => 'http://' . Str::slug($faker->text(40)) . '.com',
                'phone' => $faker->phoneNumber,
                'email' => $faker->email,
                'social_media' => social_media(),
                'listing_gallery' => gallery(),
                'faq' => faqs(),
                'location' => locations(),
                'timings' => timings(),
                'audio' => audio(),
                'video' => video(),
                'status' => 'publish'
            ]);
            dump('Listing Created ' . $i);
        endfor;
    }

}

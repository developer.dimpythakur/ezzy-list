@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.access.users.management') . ' | ' . trans('labels.backend.access.users.create'))
@section('page-header')
<h1>
    {{ trans('labels.backend.access.users.management') }}
    <small>{{ trans('labels.backend.access.users.create') }}</small>
</h1>
@endsection

@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.pages.section.grouped_errors'))
        {{ Form::open(['route' => 'admin.access.user.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
        @include("backend.access.users.form")
        {{-- Buttons --}}
        <div id="addForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span>Create</span>
                </button>
            </div>
            <a href="{{ route('admin.access.user.index')}}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>

@endsection

@section('after_scripts')
<script type="text/javascript">


    Backend.Utils.documentReady(function () {
        Backend.Users.selectors.getPremissionURL = "{{ route('admin.get.permission') }}";
        Backend.Users.init("create");
    });

    window.onload = function () {
        Backend.Users.windowloadhandler();
    };

</script>
@endsection

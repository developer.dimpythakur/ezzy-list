
@if(isset($listing_top_rated ) && !is_null($listing_top_rated) && count($listing_top_rated))
<div class="pin_map_mapker_svg">
    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="65" viewBox="0 0 50 65">
        <g transform="translate(0.000000,65.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none">
            <path d="M166 636 c-142 -53 -200 -201 -132 -339 29 -61 157 -238 194 -271 22 -19 22 -19 44 0 37 33 165 210 194 271 65 131 21 262 -109 330 -43 22 -143 27 -191 9z"/>
        </g>
    </svg>
</div>
<section class="et-top-rated-business list position-relative bg-light">
    <div class="container">
        <div class="section-heading text-center d-block full-width">
            <h2 class="text-capitalize">Best Listings You can Find</h2>
            <p>Explore your Favourite Listings Around the World</p>
        </div>
        <div class="et-top-rated list" id=""main_content>
            <div class="row justify-content-md-center">
                <div class="col-lg-10 ezzy-listings top-rated style-one">
                    @foreach($listing_top_rated as $listing)
                    @include('frontend.listing.components.listing-style.style-one')
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@else

<section class="et-top-rated-business list position-relative bg-light">
    <div class="container">
        <div class="et-top-rated list" id="main_content">
            <div class="row justify-content-md-center mt-xs">
                <div class="col-lg-10 ezzy-listings ">
                    <div class="col-lg-6 text-center no-listings-found p-4 mx-auto">
                        <h3 class="text-primary">No Listing Found</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!-- /Top Rated Businesses -->

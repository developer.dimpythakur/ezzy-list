<header class="app-header bg-light border-0 navbar">
    <!-- Logo -->
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto ml-3" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="http://127.0.0.1:8000">
        <b>Backpack</b>
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>

    <!-- =================================================== -->
    <!-- ========== Top menu items (ordered left) ========== -->
    <!-- =================================================== -->
    <ul class="nav navbar-nav d-md-down-none">

        <!-- Topbar. Contains the left part -->
        <!-- This file is used to store topbar (left) items -->



        <ul class="nav navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> 
                    4.1 
                    <span class="badge badge-pill badge-warning ml-2">New</span>
                </a>
                <div class="dropdown-menu">
                    <div class="dropdown-header"><strong>Documentation</strong></div>
                    <a class="dropdown-item" href="https://backpackforlaravel.com/docs/4.1/release-notes">Release Notes</a>
                    <a class="dropdown-item" href="https://backpackforlaravel.com/docs/4.1/upgrade-guide">Upgrade Guide</a>
                    <a class="dropdown-item" href="https://backpackforlaravel.com/docs/4.1/installation">Installation</a>

                    <div class="dropdown-header"><strong>PRs</strong></div>
                    <a class="dropdown-item" href="https://github.com/Laravel-Backpack/CRUD/pull/2508">Backpack\CRUD</a>
                    <a class="dropdown-item mb-2" href="https://github.com/Laravel-Backpack/demo/pull/134">Backpack\Demo</a>
                </div>
            </li>
        </ul>

    </ul>
    <!-- ========== End of top menu left items ========== -->



    <!-- ========================================================= -->
    <!-- ========= Top menu right items (ordered right) ========== -->
    <!-- ========================================================= -->
    <ul class="nav navbar-nav ml-auto ">
        <!-- Topbar. Contains the right part -->
        <!-- This file is used to store topbar (right) items -->


        <li class="nav-item dropdown pr-4">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img class="img-avatar" src="https://secure.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61.jpg?s=45&amp;d=https%3A%2F%2Fplacehold.it%2F160x160%2F00a65a%2Fffffff%2F%26text%3DD&amp;r=g" alt="Demo Admin">
            </a>
            <div class="dropdown-menu dropdown-menu-right mr-4 pb-1 pt-1">
                <a class="dropdown-item" href="http://127.0.0.1:8000/admin/edit-account-info"><i class="la la-user"></i> My Account</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="http://127.0.0.1:8000/admin/logout"><i class="la la-lock"></i> Logout</a>
            </div>
        </li>
    </ul>
    <!-- ========== End of top menu right items ========== -->
</header>
<!-- Left side column. contains the sidebar -->
<div class="{{ config('backend.sidebar_class') }}">
    <!-- sidebar: style can be found in sidebar.less -->
    <nav class="sidebar-nav overflow-hidden">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <div class="sidebar sidebar-pills bg-light">
            <!-- sidebar: style can be found in sidebar.less -->
            <nav class="sidebar-nav overflow-hidden ps">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="nav">
                    <!-- <li class="nav-title">ADMINISTRATION</li> -->
                    <!-- ================================================ -->
                    <!-- ==== Recommended place for admin menu items ==== -->
                    <!-- ================================================ -->
                    {{ renderMenuItems(getMenuItems()) }}
                    <?php
                    renderMenuItems(getMenuItems('frontend'))
                    ?>
                    <!-- ======================================= -->
                    <!-- <li class="nav-title">Entries</li> -->
                </ul>
            </nav>
            <!-- /.sidebar -->
        </div>
    </nav>
    <!-- /.sidebar -->
</div>

@push('before_scripts')
<script type="text/javascript">
    /* Recover sidebar state */
    if (Boolean(sessionStorage.getItem('sidebar-collapsed'))) {
        var body = document.getElementsByTagName('body')[0];
        body.className = body.className.replace('sidebar-lg-show', '');
    }

    /* Store sidebar state */
    var navbarToggler = document.getElementsByClassName("navbar-toggler");
    for (var i = 0; i < navbarToggler.length; i++) {
        navbarToggler[i].addEventListener('click', function (event) {
            event.preventDefault();
            if (Boolean(sessionStorage.getItem('sidebar-collapsed'))) {
                sessionStorage.setItem('sidebar-collapsed', '');
            } else {
                sessionStorage.setItem('sidebar-collapsed', '1');
            }
        });
    }
</script>
@endpush

@push('after_scripts')
<script>
    // Set active state on menu element
    var full_url = "{{ Request::fullUrl() }}";
    var $navLinks = $(".sidebar-nav li a");

    // First look for an exact match including the search string
    var $curentPageLink = $navLinks.filter(
            function () {
                return $(this).attr('href') === full_url;
            }
    );

    // If not found, look for the link that starts with the url
    if (!$curentPageLink.length > 0) {
        $curentPageLink = $navLinks.filter(function () {
            if ($(this).attr('href').startsWith(full_url)) {
                return true;
            }

            if (full_url.startsWith($(this).attr('href'))) {
                return true;
            }

            return false;
        });
    }

    // for the found links that can be considered current, make sure 
    // - the parent item is open
    $curentPageLink.parents('li').addClass('open');
    // - the actual element is active
    $curentPageLink.each(function () {
        $(this).addClass('active');
    });
</script>


<script>

//    check if url contains the string
    function checkUri(urlPrefix) {
//        let prefixes = 
        console.log(urlPrefix);
        return (window.location.href.indexOf(urlPrefix) > -1) ? true : false;
    }
    jQuery(document).ready(function (e) {
        // move the bottom buttons before pagination
        $("#bottom_buttons").insertBefore($('#crudTable_wrapper .row:last-child'));
//        jQuery(".sidebar-nav ul.nav li").each(function (r) {
//            let parent = jQuery(this).data('children');
//            console.log('parent   ' + parent);
////             && checkUri('listing')
//            if (parent == '1') {
//                jQuery(this).addClass(' sddddddddddddddddddddddd');
//            } else {
//                jQuery(this).removeClass('open');
//            }
//        });

    });


    window.setTimeout(function (e) {
        jQuery(".sidebar-nav ul.nav li.parent").each(function (r) {
            let $row = jQuery(this);
            ['listing', 'blog', 'user'].forEach(function (item, index) {
                $row.removeClass('open');
                if (window.location.href.indexOf(item) > -1) {
                    console.log($row);
                    $row.addClass('open');
                } else {
                    $row.removeClass('open');
                }
            });
        });
    }, 5000);
</script>
@endpush

@extends('frontend.layouts.app')
@section('content')

@if($posts && !is_null($posts) && count($posts))
<!-- Header -->
<section class="et-page-header position-relative text-center"
         style="background-image:url({{ ($banner)}})">
    <div class="header-content position-relative text-white">
        <ul class="nostyle list-inline breadcrumb-links mb-1">
            <li><a href="{{ URL::to('/') }}">Home</a></li>
            <li class="current">Blog</li>
        </ul>
        <h1 class="page-title d-inline-flex align-items-center m-0 position-relative">Blog</h1>
    </div>
</section>
<!-- Display Posts -->
<section class="et-blog-grid">
    <div class="container pt-5 pt-xs">
        <div class="row">
            @foreach($posts as $post)
            <!-- Single Post -->
            <div class="col-lg-4 col-sm-12">
                @include('frontend.blog.section.single-post-view')
            </div>
            @endforeach
            <!-- Single Post -->
        </div>
        <div class="row">
            <!-- Pagination -->
            {{ $posts->links() }}
            <!-- /Pagination -->
        </div>
    </div>
</section>
@else
<div class="container pt-5 pb-5">
    <div class="row">
        <div class="col-md-12 col-sm-6">
            <div class="w-100 p-2">
                <h1 class="text-primary mb-3 not-found-title">We don't have any post to show</h1>
                <p>No post found.</p>
                <a href="{{ URL::to('/') }}" class="btn btn-primary border-transparent border-0">
                    <i class="las la-arrow-left" aria-hidden="true"></i>
                    Home
                </a>
            </div>
        </div>
    </div>
</div>
@endif
<!-- Newsletter Subscription -->
@include('frontend.listing.components.listing-newsletter.newsletter')
<!-- /Newsletter Subscription -->
@endsection
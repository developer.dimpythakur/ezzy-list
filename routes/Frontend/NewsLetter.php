<?php

/**
 * Newsletter Controller
 * All route names are prefixed with 'frontend.'.
 */

/*
* Show pages
*/

Route::get('newsletter', 'NewsletterController@create');
Route::post('newsletter', 'NewsletterController@store')->name('subscribe');
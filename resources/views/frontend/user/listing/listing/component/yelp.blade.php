<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-yelp"></i> Import listing from Yelp</h6>
    </div>
    <div class="card-body listing-form-yelp bold-labels">
        <div class="row">
            <div class="form-group">
                <input type="hidden" name="id" id="id"/>
                <input type="hidden" name="url" id="url"/>
            </div>
            <div class="form-group col-md-5 required">
                {{ Form::text('yelp', null, ['id'=>'yelp-phone', 'required'=>'required', 'class' => 'form-control box-size', 'placeholder' => 'Registered Phone Number']) }}
            </div>
            <div class="form-group col-md-5 required">
                <button id="yelp-search" data-url='{{ route('frontend.user.listings-yelp') }}' type="button" class="btn btn-primary listing-yelp">
                    <i class="la la-yelp"></i>&nbsp;Search
                </button>
            </div>
        </div>
    </div>
</div>
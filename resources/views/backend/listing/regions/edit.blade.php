@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.listing.regions.management') . ' | ' . trans('labels.backend.listing.regions.edit'))
@section('page-header')
<h1>{{ trans('labels.backend.listing.regions.edit') }}</h1>
@endsection

@section("after_styles")
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
@endsection
@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::model($listing_region, ['route' => ['admin.listing-regions.update', $listing_region], 'class' => 'form-horizontal','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-location']) }}
        <div class="card">
            <div class="card-header">
                <h6 class="my-2"><i class="la la-location-arrow"></i> Update Region</h6>
            </div>
            <div class="card-body listing-form bold-labels">
                @include("backend.listing.regions.form")
            </div>
        </div>
        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=update_and_back">Update</span>
                </button>
            </div>
            <a href="{{ route('admin.listing-regions.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>

@endsection
@section("after_scripts")
@endsection

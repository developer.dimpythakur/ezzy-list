<?php

namespace App\Http\Controllers\Backend\Listing;

use App\Http\Controllers\Controller;
use Prologue\Alerts\Facades\Alert;
/*
  |--------------------------------------------------------------------------
  | Requests
  |--------------------------------------------------------------------------
  |
  | Used to handle the requests
 */
use App\Http\Requests\Backend\Listing\Create;
use App\Http\Requests\Backend\Listing\Delete;
use App\Http\Requests\Backend\Listing\Edit;
use App\Http\Requests\Backend\Listing\Manage;
use App\Http\Requests\Backend\Listing\Store;
use App\Http\Requests\Backend\Listing\Update;

/*
  |--------------------------------------------------------------------------
  | Model
  |--------------------------------------------------------------------------
  |
 */
use App\Models\Listing\Listing\Listing;
use App\Http\Responses\RedirectResponse;
use App\Http\Responses\Backend\Listing\EditResponse;
use App\Http\Responses\ViewResponse;
use App\Repositories\Backend\Listings\ListingsRepository;
use Illuminate\Http\Request;
use Carbon\Carbon;
// Helper Models
use App\Models\Listing\Categories\Category;
use App\Models\Listing\Tags\Tag;
use App\Models\Listing\Region\Region;
//Slugable
use Cviebrock\EloquentSluggable\Services\SlugService;

class ListingController extends Controller {

    /**
     * Modal Instance
     * @var type 
     */
    protected $listings;

    /**
     * Tags
     * @var type 
     */
    protected $tags = null;

    /**
     * Categories
     * @var type 
     */
    protected $categories = null;

    /**
     * Regions
     * @var type 
     */
    protected $regions = null;

    /**
     * Listing Status.
     */
    protected $status = [
        'publish' => 'Published',
        'review' => 'In Review',
        'pending' => 'Pending',
        'expired' => 'Expired',
    ];

    /**
     * ListingsController constructor.
     * @param ListingsRepository $listings
     */
    public function __construct(ListingsRepository $listings) {
        $this->listings = $listings;
//        $this->categories = Category::active()->get();
        $this->categories = Category::getSelectData();
        $this->tags = Tag::getSelectData();
        $this->regions = Region::getSelectData();
    }

    /**
     * Display listings
     * @param Manage $request
     * @return ViewResponse
     */
    public function index(Manage $request) {
        $this->data['title'] = trans('labels.backend.blogcategories.management'); // set the page title
        $this->data['breadcrumbs'] = [
            'admin' => route('admin.dashboard'),
            'dashboard' => false,
        ];
        return new ViewResponse('backend.listing.listing.index', $this->data);
    }

    /**
     * @param Create $request
     * @return ViewResponse
     */
    public function create(Create $request) {
        return new ViewResponse('backend.listing.listing.create', [
            'tags' => $this->tags,
            'categories' => $this->categories,
            'regions' => $this->regions,
        ]);
    }

    /**
     * @param Store $request
     * @return RedirectResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function store(Store $request) {
//    public function store(Request $request) {
        $this->listings->create($request);
        return new RedirectResponse(route('admin.listings.index'), trans('alerts.backend.listings.created'));
    }

    /**
     * @param Listing $listing
     * @param Edit $request
     * @return EditResponse
     */
    public function edit(Listing $listing, Edit $request) {

        return new EditResponse([
            'listing' => $listing,
            'status' => $this->status,
            'categories' => $this->categories,
            'tags' => $this->tags,
            'regions' => $this->regions,
        ]);
    }

    /**
     * 
     * @param Listing $listing
     * @param Update $request
     * @return RedirectResponse
     */
    public function update(Listing $listing, Update $request) {
        $this->listings->update($listing, $request);
        return new RedirectResponse(route('admin.listings.index'), trans('alerts.backend.listings.updated'));
    }

    /**
     * @param Listing $listing
     * @param Delete $request
     * @return RedirectResponse
     * @throws \App\Exceptions\GeneralException
     */
    public function destroy(Listing $listing, Delete $request) {
        $this->listings->delete($listing);
        return new RedirectResponse(route('admin.listings.index'), trans('alerts.backend.listings.deleted'));
    }

    /**
     * Check if slug already exist
     * @param Request $request
     * @return type
     */
    public function check_slug(Request $request) {
        // New version: to generate unique slugs
        $slug = SlugService::createSlug(Listing::class, 'slug', $request->name);
        return response()->json(['slug' => $slug]);
    }

}

<!-- Social Media -->
<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-user-astronaut"></i> Social Information</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row my-3 normal-labels">
            <div class="form-group col-md-4">
                {!! Html::decode(Form::label('social_media[facebook]', '<i class="lab la-facebook"></i>Facebook', ['class' => 'control-label required'])) !!}
                {{ Form::text('social_media[facebook]', ((isset($listing))? getMedia($listing,'facebook'):null), ['class' => 'form-control box-size', 'placeholder' =>'https://www.facebook.com/']) }}
            </div><!--form group-->

            <div class="form-group col-md-4">
                {!! Html::decode(Form::label('social_media[twitter]', '<i class="lab la-twitter"></i>Twitter', ['class' => 'control-label required'])) !!}
                {{ Form::text('social_media[twitter]', ((isset($listing))? getMedia($listing,'twitter'):null), ['class' => 'form-control box-size', 'placeholder' => 'https://www.twitter.com/']) }}
            </div><!--form group-->
            <div class="form-group col-md-4">
                {!! Html::decode(Form::label('social_media[instagram]', '<i class="lab la-instagram"></i>Instagram', ['class' => 'control-label required'])) !!}
                {{ Form::text('social_media[instagram]', ((isset($listing))? getMedia($listing,'instagram'):null), ['class' => 'form-control box-size', 'placeholder' => 'Instagram']) }}
            </div><!--form group-->
            <div class="form-group col-md-4">
                {!! Html::decode(Form::label('social_media[youtube]', '<i class="lab la-youtube"></i>Youtube', ['class' => 'control-label required'])) !!}
                {{ Form::text('social_media[youtube]', ((isset($listing))? getMedia($listing,'youtube'):null), ['class' => 'form-control box-size', 'placeholder' => 'Youtube']) }}
            </div><!--form group-->

            <div class="form-group col-md-4">
                {!! Html::decode(Form::label('social_media[whatsapp]', '<i class="lab la-whatsapp"></i>WhatsApp', ['class' => 'control-label required'])) !!}
                {{ Form::text('social_media[whatsapp]', ((isset($listing))? getMedia($listing,'whatsapp'):null), ['class' => 'form-control box-size', 'placeholder' => 'WhatsApp']) }}
            </div><!--form group-->
            <div class="form-group col-md-4">
                {!! Html::decode(Form::label('social_media[skype]', '<i class="lab la-skype"></i>Skype', ['class' => 'control-label required'])) !!}
                {{ Form::text('social_media[skype]', ((isset($listing))? getMedia($listing,'skype'):null), ['class' => 'form-control box-size', 'placeholder' => 'Skype']) }}
            </div><!--form group-->
        </div>
    </div>
</div>
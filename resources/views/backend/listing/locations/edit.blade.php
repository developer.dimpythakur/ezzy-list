



@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.locations.management') . ' | ' . trans('labels.backend.locations.create'))

@section('page-header')
<h1>{{ trans('labels.backend.locations.management') }}</h1>
@endsection

@php
$breadcrumbs = [
'Dashboard' => url(config('backend.route_prefix'), 'dashboard'),
trans('labels.backend.locations.all') => route('admin.listings.index'),
trans('labels.backend.locations.create') => false,
];
@endphp


@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.locations.management') }}</span>
    </h2>
</div>
@endsection
@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::model($listing_location, ['route' => ['admin.listing-locations.update', $listing_location], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-location']) }}
        @include("backend.listing.locations.form")
        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=update_and_back">Update</span>
                </button>
            </div>
            <a href="{{ route('admin.listing-locations.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection
@section("after_scripts")
@endsection

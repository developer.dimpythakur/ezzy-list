<?php

namespace App\Repositories\Frontend\Posts;

use App\Exceptions\GeneralException;
use App\Models\Blogs\Blog;
use App\Repositories\BaseRepository;

/**
 * Class PagesRepository.
 */
class PostsRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Blog::class;

    /*
    * Find page by page_slug
    */
    public function findBySlug($slug)
    {
        if (!is_null($this->query()->whereSlug($slug)->firstOrFail())) {
            return $this->query()->whereSlug($slug)->firstOrFail();
        }

        throw new GeneralException(trans('exceptions.backend.access.pages.not_found'));
    }

    public function nextPostLink($post)
    {
        $nextPost = Blog::where('id', '>', $post->id)->orderBy('id')->first();
        return $nextPost;
    }

    public function previousPostLink($post)
    {
        $previousPost = Blog::where('id', '<', $post->id)->orderBy('id', 'desc')->first();
        return $previousPost;
    }
}

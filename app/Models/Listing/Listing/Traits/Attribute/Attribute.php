<?php

namespace App\Models\Listing\Listing\Traits\Attribute;

/**
 * Class PageAttribute.
 */
trait Attribute {

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {

        return '<div class="btn-group action-btn">
                    ' . $this->getEditButtonAttribute('edit-page', 'admin.listings.edit') . '
                    ' . $this->getViewButtonAttribute() . '                    
                    ' . $this->getDeleteButtonAttribute('delete-page', 'admin.listings.destroy') . '
                </div>';
    }

    /**
     * @return string
     */
    public function getViewButtonAttribute() {
        return '<a target="_blank" href="' . route('frontend.listing.show', $this->slug) . '" class="btn btn-flat btn-default">
                    <i data-toggle="tooltip" data-placement="top" title="View Listing" class="las la-eye"></i>
                </a>';
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute() {
        if ($this->isActive()) {
            return "<label class='badge badge-success'>" . trans('labels.general.active') . '</label>';
        }
        if ($this->status == 'review'):
            return "<label class='badge badge-warning text-white'>In Review</label>";
        endif;
        if ($this->status == 'pending'):
            return "<label class='badge badge-primary'>Pending</label>";
        endif;
        if ($this->status == 'expired'):
            return "<label class='badge badge-danger'>Expired</label>";
        endif;
        return "<label class='badge badge-danger'>" . trans('labels.general.inactive') . '</label>';
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 'publish';
    }

    /**
     * Get the summerized score value.
     *
     * @return int
     */
    public function getScoreAttribute() {
        return $this->reviews->sum('score');
    }

    /**
     * Get the average score value.
     *
     * @return int
     */
    public function getAvgScoreAttribute() {
        return $this->reviews()->avg('score');
    }

    /**
     * Create a review for this model.
     *
     * @param int $score
     * @param string $body
     * @param model $author
     *
     * @return Review
     */
    public function createReview($review = null, $author = null) {
        return ReviewFactory::create($this, $review, $author);
    }

    /**
     * URL attribute
     * @return type
     */
    public function getUrlAttribute() {
        return route('frontend.listing.show', $this->slug);
    }

    /**
     * Published Listings 
     * @return type
     */
    public function scopePublished($query) {
        return $query
                        ->where('created_by', access()->id())
                        ->where('status', 'publish');
    }

    /**
     * Pending Query 
     * @return type
     */
    public function scopePending($query) {
        return $query
                        ->where('created_by', access()->id())
                        ->where('status', 'pending');
    }

    /**
     * Review Query 
     * @return type
     */
    public function scopeReview($query) {
        return $query
                        ->where('created_by', access()->id())
                        ->where('status', 'review');
    }

    /**
     * Expired Query 
     * @return type
     */
    public function scopeExpired($query) {
        return $query
                        ->where('created_by', access()->id())
                        ->where('status', 'expired');
    }

    /**
     * Expired Query 
     * @return type
     */
    public function scopeUserRating($query) {
        return $query
                        ->where('created_by', access()->id());
    }

    /**
     * 
     * @param type $review
     * @param type $author
     * @return type
     */
    public function scopeTimezone() {
//        if ($this->attributes['type'] == 'yelp') {
//            $timigs = ( new YelpRepository)->business($this->business_id);
//            dd($timigs);
//        }
    }

    /**
     * Published Status attribute
     * @return type
     */
    public function getPublishAttribute() {
        return $this->status == 'publish';
    }

    /**
     * Pending Status attribute
     * @return type
     */
    public function getPendingAttribute() {
        return $this->status == 'pending';
    }

    /**
     * Published Status attribute
     * @return type
     */
    public function getExpiredAttribute() {
        return $this->status == 'publish';
    }

    /**
     * Categories Count Status attribute
     * @return type
     */
    public function getCategoriesCountAttribute() {
        return count($this->categories);
    }

    /**
     * Listing Banner attribute
     * @return type
     */
    public function getBannerAttribute() {
        if ($this->attributes['type'] === 'yelp'):
            return ($this->attributes['banner']);
        endif;
        return ($this->upload_path . $this->attributes['banner']);
    }

}

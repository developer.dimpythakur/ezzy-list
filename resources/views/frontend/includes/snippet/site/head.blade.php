<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>@yield('title', app_name())</title>
<!-- Meta -->
<meta name="description" content="@yield('meta_description', 'Eazzy App')">
<meta name="author" content="@yield('meta_author', 'Eazzy App')">
<meta name="keywords" content="@yield('meta_keywords', 'Eazzy App')">
@yield('meta')

<!-- Styles -->
@yield('before-styles')

<!-- Check if the language is set to RTL, so apply the RTL layouts -->
<!-- Otherwise apply the normal LTR layouts -->
@langrtl
{{ Html::style(getRtlCss(('frontend/css/frontend.css'))) }}
@else
{{ Html::style(('frontend/css/frontend.css')) }}
{{ Html::style(('frontend/css/all.css')) }}
@endlangrtl

<!--{!! Html::style('js/select2/select2.min.css') !!}-->
@yield('after-styles')

<!-- Scripts -->
<script>
    window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
</script>
<?php
if (!empty($google_analytics)) {
    echo $google_analytics;
}
?>
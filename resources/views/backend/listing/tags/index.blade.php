@extends ('backend.layouts.app')
@section ('title', trans('labels.listing.tags.management'))
@section('page-header')
<h1>{{ trans('labels.listing.tags.management') }}</h1>
@endsection
 
@section('after_styles')
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/backend/datatables.css') }}">
@endsection
@section('content')
<div class="row">
    <div class="col-12 mb-4">
        <div class="row mb-0">
            <div class="col-sm-6">
                <div class="hidden-print with-border">
                    <a href="{{route('admin.listing-tags.create')}}" class="btn btn-primary" data-style="zoom-in">
                        <span class="ladda-label"><i class="la la-plus"></i> {{trans('labels.backend.listing.tags.create')}}</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="datatable_search_stack" class="mt-sm-0 mt-2"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive data-table-wrapper">
        <table id="listing-tags-table"
               class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2 dataTable dtr-inline">
            <thead>
                <tr>
                    <th>{{ trans('labels.backend.listing.tags.table.title') }}</th>
                    <th>{{ trans('labels.backend.listing.tags.table.status') }}</th>
                    <th>{{ trans('labels.backend.listing.tags.table.created_at') }}</th>
                    <th>{{ trans('labels.general.actions') }}</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>{{ trans('labels.backend.listing.tags.table.title') }}</th>
                    <th>{{ trans('labels.backend.listing.tags.table.status') }}</th>
                    <th>{{ trans('labels.backend.listing.tags.table.created_at') }}</th>
                    <th>{{ trans('labels.general.actions') }}</th>
                </tr>
            </tfoot>
        </table>
    </div><!--table-responsive-->
</div>
@endsection


@section('after_scripts')
<!-- DATA TABLES SCRIPT -->
<script type="text/javascript" src="{{ asset('js/backend/datatables.js') }}"></script>

<script>
//Below written line is short form of writing $(document).ready(function() { })
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
$(function () {
var dataTable = $('#listing-tags-table').dataTable({
processing: true,
        serverSide: true,
        ajax: {
        url: '{{ route("admin.listing-tags.get") }}',
                type: 'post'
        },
        columns: [
        {data: 'name', name: '{{config('module.listing_listing.tags.table')}}.name'},
        {data: 'status', name: '{{config('module.listing_listing.tags.table')}}.status'},
        {
        data: 'created_at', name: '{{config('module.listing_listing.tags.table')}}.created_at'
        },
        {data: 'actions', name: 'actions', searchable: false, sortable: false}
        ],
        order: [[3, "asc"]],
        searchDelay: 500,
        dom: "<'row hidden'<'col-sm-6 hidden-xs'i><'col-sm-6 hidden-print'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row mt-2 '<'col-sm-6 col-md-4'l><'col-sm-2 col-md-4 text-center'B><'col-sm-6 col-md-4 hidden-print'p>>",
        buttons: {
        },
        language: {
        @lang('datatable.strings')
        }
});
Backend.DataTableSearch.init(dataTable);
});</script>
@endsection
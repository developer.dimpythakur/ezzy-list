<?php
$options = [
    'file_picker_callback' => 'elFinderBrowser',
    'selector' => 'textarea.tinymce',
    'plugins' => 'image,link,media,anchor',
];
$selectedCategories = isset($listing) ? ($listing->categories->pluck('id')->toArray()) : null;
$selectedTags = isset($listing) ? ($listing->tags->pluck('id')->toArray()) : null;
?>
@push('after_scripts')
<style>
    .cd-example label {
        display: inline-block;
        margin-bottom: .5rem;
    }
    .c-switch {
        display: inline-block;
        width: 40px;
        height: 26px;
    }

</style>
@endpush
@push('after_scripts')
<script type="text/javascript" id="subscription" src="{{ asset('js/backend/user-dashboard.js') }}"></script>

@endpush
<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-file-text"></i> Basic Informations</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row" id="section-listing">
            <div class="form-group col-12 required">
                {!! Html::decode(Form::label('name', '<i class="la la-info"></i> '.trans('validation.attributes.frontend.listing.title'), ['class' => 'control-label required'])) !!}
                {{ Form::text('name', null, ['id'=>'name', 'class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.frontend.listing.title'), 'required' => 'required']) }}
                <p class="help-block">
                    @if(isset($listing))
                    <a target="_blank" href="{{ route('frontend.listing.show',$listing->slug) }}">
                        <cite>
                            {{ route('frontend.index')}}/listing/<span id="listing-slug">{{ isset($listing)? $listing->slug:''}}                    </span>

                        </cite> 
                    </a>
                    @endif
                </p>
            </div>
            <div class="form-group col-12">
                {!! Html::decode(Form::label('tagline', '<i class="la la-pencil-alt"></i> '.trans('validation.attributes.frontend.listing.tagline'), ['class' => 'control-label required'])) !!}
                {{ Form::text('tagline', null, ['id'=>'tagline', 'class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.frontend.listing.tagline'), 'required' => 'required']) }}
            </div>
            <div class="form-group col-12">
                {!! Html::decode(Form::label('description', '<i class="la la-list-alt"></i> '.trans('validation.attributes.frontend.listing.description'), ['class' => 'control-label required'])) !!}
                {{ Form::textarea('description', null,[
                    'class' => 'form-control box-size tinymce',
                    'id' => 'description',
                     'data-options'=> trim(json_encode($options)),
                            'placeholder' => trans('validation.attributes.backend.pages.description')]) }}
            </div>
            <div class="form-group col-md-6">
                {!! Html::decode(Form::label('categories', '<i class="la la-list-ul"></i> '.trans('validation.attributes.frontend.listing.category'), ['class' => 'control-label required'])) !!}
                @if(!empty($selectedCategories))
                {{ Form::select('categories[]', $categories, $selectedCategories, ['id'=>'categories', 'class' => 'form-control box-size select2_multiple', 'placeholder' => 'Select Category', 'required' => 'required'])}}
                @else
                {{ Form::select('categories[]', $categories, null, ['id'=>'categories','class' => 'form-control box-size select2_multiple', 'placeholder' => 'Select Category', 'required' => 'required'])}}
                @endif
            </div><!--form control-->
            <div class="form-group col-md-6">
                {!! Html::decode(Form::label('tags', '<i class="la la-tags"></i> '.trans('validation.attributes.frontend.listing.tag'), ['class' => 'control-label required'])) !!}
                @if(!empty($selectedTags))
                {{ Form::select('tags[]', $tags, $selectedTags, ['id'=>'tags','class' => 'form-control box-size select2_multiple','multiple'=>'multiple', 'placeholder' => 'Select Tag'])}}
                @else
                {{ Form::select('tags[]', $tags, null, ['id'=>'tags','class' => 'form-control box-size select2_multiple',       'placeholder' => 'Select Tag','multiple'=>'multiple'])}}
                @endif
            </div><!--form group-->
        </div>
    </div>
</div>
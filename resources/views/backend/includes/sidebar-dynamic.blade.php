<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>
            <li class="{{ active_class(Active::checkUriPattern('admin/dashboard')) }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>
            <li class="header">{{ trans('menus.backend.sidebar.system') }}</li>
            {{ renderMenuItems(getMenuItems()) }}
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>
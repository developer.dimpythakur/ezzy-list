<?php

namespace App\Models\Faqs;

use App\Models\BaseModel;
use App\Models\Faqs\Traits\Attribute\FaqAttribute;
use App\Models\ModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faq extends BaseModel {

    use ModelTrait,
        SoftDeletes,
        FaqAttribute {
        // FaqAttribute::getEditButtonAttribute insteadof ModelTrait;
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['question', 'answer', 'status'];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.ezzy_faqs.table');
    }

    /**
     * Add new Faq
     * @param type $question
     * @param type $answer
     * @return type
     */
    public function addFAQs($question, $answer) {
        return Faq::create([
                    'question' => $question,
                    'answer' => $answer,
                    'status' => 1,
                    'created_by' => isset(access()->user()->id) ? access()->user()->id : 1
                ])->id;
    }

    /**
     * Delete FAQs
     * @param $id
     */
    public function deleteFaqs($id) {
        $faq = Faq::find($id);
        if ($faq):
            $faq->delete();
        endif;
        return;
    }

}

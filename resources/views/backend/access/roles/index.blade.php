@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.roles.management'))

@section('page-header')
<h1>{{ trans('labels.backend.access.roles.management') }}</h1>
@endsection
@section('after_styles')
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/backend/datatables.css') }}">
@endsection
@section('content')
<div class="box box-info">
    <div class="box-header with-border">
        <div class="box-tools pull-right">
            @include('backend.access.includes.partials.role-header-buttons')
        </div>
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive data-table-wrapper">
            <table id="roles-table" class="table table-condensed table-hover table-bordered">
                <thead>
                    <tr>
                        <th>{{ trans('labels.backend.access.roles.table.role') }}</th>
                        <th>{{ trans('labels.backend.access.roles.table.permissions') }}</th>
                        <th>{{ trans('labels.backend.access.roles.table.number_of_users') }}</th>
                        <th>{{ trans('labels.backend.access.roles.table.sort') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <thead class="transparent-bg">
                </thead>
            </table>
        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
@endsection

@section('after-scripts')
<script type="text/javascript" src="{{ asset('js/backend/datatables.js') }}"></script>

<script>
$(function() {
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
var dataTable = $('#roles-table').dataTable({
processing: true,
    serverSide: true,
    ajax: {
    url: '{{ route("admin.access.role.get") }}',
            type: 'post',
    },
    columns: [
    {data: 'name', name: '{{config('access.roles_table')}}.name'},
    {data: 'permissions', name: '{{config('access.permissions_table')}}.display_name', sortable: false},
    {data: 'users', name: 'users', searchable: false, sortable: false},
    {data: 'sort', name: '{{config('access.roles_table')}}.sort'},
    {data: 'actions', name: 'actions', searchable: false, sortable: false}
    ],
    order: [[3, "asc"]],
    searchDelay: 500,
    dom: 'lBfrtip',
    buttons: {
    buttons: [
    { extend: 'copy', className: 'copyButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
    { extend: 'csv', className: 'csvButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
    { extend: 'excel', className: 'excelButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
    { extend: 'pdf', className: 'pdfButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
    { extend: 'print', className: 'printButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }}
    ]
    },
    language: {
    @lang('datatable.strings')
    }
});
Backend.DataTableSearch.init(dataTable);
});
</script>
@endsection
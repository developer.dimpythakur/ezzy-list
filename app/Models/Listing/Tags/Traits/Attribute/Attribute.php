<?php

namespace App\Models\Listing\Tags\Traits\Attribute;

/**
 * Class Attribute.
 */
trait Attribute {

    /**
     * @return string
     */
    public function getActionButtonsAttribute() {
        return '<div class="btn-group action-btn">
                    ' . $this->getEditButtonAttribute('edit-blog-tag', 'admin.listing-tags.edit') . '
                    ' . $this->getDeleteButtonAttribute('delete-blog-tag', 'admin.listing-tags.destroy') . '
                </div>';
    }

    /**
     * @return string
     */
    public function getStatusLabelAttribute() {
        if ($this->isActive()) {
            return "<label class='badge badge-success'>" . trans('labels.general.active') . '</label>';
        }
        return "<label class='badge badge-danger'>" . trans('labels.general.inactive') . '</label>';
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->status == 1;
    }

}

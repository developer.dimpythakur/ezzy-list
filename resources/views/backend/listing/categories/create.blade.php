@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.listing.categories.management') . ' | ' . trans('labels.backend.listing.categories.create'))
@section('page-header')
<h1>{{ trans('labels.backend.listing.categories.create') }}</h1>
@endsection

@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::open(['route' => 'admin.listing-categories.store', 'class' => 'form-horizontal add-listing-category','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'post', 'id' => 'create-category']) }}
        <div class="card">
            <div class="card-body row">
                @include("backend.listing.categories.form")
            </div>
        </div>
        <div id="saveForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value="save_and_back">Save and back</span>
                </button>
            </div>
            <a href="{{ route('admin.listing-categories.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection
@section("after_scripts")

@endsection

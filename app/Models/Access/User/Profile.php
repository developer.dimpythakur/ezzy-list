<?php

namespace App\Models\Access\User;

use App\Models\BaseModel;
use App\Models\Access\User\User;

/**
 * Class Profile.
 */
class Profile extends BaseModel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_profile';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['website', 'phone', 'bio', 'social_media', 'avatar'];

//    protected $gaurded = ['*'];
//
    public function user() {
        return $this->belongsTo(User::class);
    }

    public function profileFields($request) {
        return [
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            "phone" => $request['phone'],
            "website" => $request['website'],
            "bio" => $request['bio'],
            'social_media' => json_encode($request['social_media']),
            'profile_pic' => $this->updateProfilePicture($request),
        ];
    }

    public function updateProfilePicture($request) {
        if (isset($request->profile_pic)):
            return $request->profile_pic;
        endif;
        return 'default.jpeg';
    }

}

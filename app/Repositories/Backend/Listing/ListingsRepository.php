<?php

namespace App\Repositories\Backend\Listings;

//Exception
use App\Exceptions\GeneralException;
use App\Http\Requests\Request;
//Events
use App\Events\Backend\Listing\Created;
use App\Events\Backend\Listing\Deleted;
use App\Events\Backend\Listing\Updated;
//Modal
use App\Models\Listing\Listing\Listing;
//Map Tables
use App\Models\ListingMap\MapCategory;
use App\Models\ListingMap\MapTag;
use App\Models\ListingMap\MapFaq;
use App\Models\ListingMap\MapTiming;
use App\Models\ListingMap\MapMedia;
use App\Models\ListingMap\MapGallery;
use App\Models\ListingMap\MapAudio;
use App\Models\ListingMap\MapVideo;
use App\Models\ListingMap\MapSocialMedia;
use App\Models\ListingMap\MapLocation;
use App\Repositories\Backend\Listings\ListingRelationshipManager as Relations;
use App\Repositories\BaseRepository;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
//Tag
use App\Models\Listing\Tags\Tag;
use App\Repositories\Backend\Listing\Yelp\YelpRepository;

/**
 * Class BlogsRepository.
 */
class ListingsRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Listing::class;

    /**
     * Upload path
     * @var type 
     */
    protected $upload_path;

    /**
     * Gallery folder
     * @var type 
     */
    protected $gallery_path;

    /**
     * Holds the relations
     * @var type 
     */
    protected $relation;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    /**
     * Categories
     * @var type 
     */
    protected $categories = null;

    /**
     * Tags
     * @var type 
     */
    protected $tags = null;

    /**
     * Social Networks
     * @var type 
     */
    protected $social_networks = null;

    /**
     * Faqs
     * @var type 
     */
    protected $faqs = null;

    /**
     * Timing
     * @var type 
     */
    protected $timings = null;

    /**
     * Audio 
     * @var type 
     */
    protected $audio = null;

    /**
     * Videos
     * @var type 
     */
    protected $video = null;

    /**
     * Gallery 
     * @var type 
     */
    protected $gallery = null;

    /**
     * Listing location
     * @var type 
     */
    protected $location = null;

    /**
     * Listing region
     * @var type 
     */
    protected $region = null;

    /**
     * Default listing type
     * @var type 
     */
    protected $type = 'listing';

    /**
     * Business ID
     * @var type 
     */
    protected $yelp;
    protected $ratings;
    protected $business_id = null;

    /**
     * Listing Status.
     */
    protected $status = [
        'publish' => 'publish',
        'review' => 'review',
        'pending' => 'pending',
        'expired' => 'expired',
    ];

    /**
     * ListingsRepository constructor.
     */
    public function __construct() {
        $this->upload_path = 'images' . DIRECTORY_SEPARATOR . 'listing' . DIRECTORY_SEPARATOR;
        $this->gallery_path = 'images' . DIRECTORY_SEPARATOR . 'listing/gallery' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
        $this->relation = new Relations($this->storage, $this->upload_path, $this->gallery_path);
        $this->yelp = new YelpRepository;
    }

    /**
     * @return mixed
     */
    public function getForDataTable($request = null) {
//        return Listing::query();
        return $this->query()
                        ->leftjoin(
                                config('access.users_table'),
                                config('access.users_table') . '.id', '=',
                                config('module.listings.table') . '.created_by')
                        ->select([
                            config('module.listings.table') . '.id',
                            config('module.listings.table') . '.name',
                            config('module.listings.table') . '.slug',
                            config('module.listings.table') . '.type',
                            config('module.listings.table') . '.banner',
                            config('module.listings.table') . '.tagline',
                            config('module.listings.table') . '.status',
                            config('module.listings.table') . '.created_at',
                            config('module.listings.table') . '.updated_at',
                            config('access.users_table') . '.first_name as created_by',
        ]);
//                ->orderBy(config('module.listings.table') . '.created_at', 'DESC');
    }

    /**
     * Create New Listing
     * @param array $request
     */
    public function create($request) {
        if (isset($request->id) && $request->id !== '') {
            $this->type = 'yelp';
            $this->business_id = (isset($request->id) && $request->id !== '') ? $request->id : null;
        }

        if ($this->query()->where('name', $request->name)->first()) {
            throw new GeneralException(trans('exceptions.backend.listings.already_exists'));
        }
        if (null != $request->tags && count($request->tags)) {
            $this->tags = (array_filter($request->tags));
        }
        if (null != $request->categories && count($request->categories)) {
            $this->categories = (array_filter($request->categories));
        }

        $this->social_networks = $this->relation->addSocialMedia($request->social_media);
        //check if faqs
        if ($request->faq):
            $this->faqs = $this->relation->createFaqs($request->faq);
        endif;
        //check if timings
        if ($request->timings):
            $this->timings = $this->relation->createTimings($request->timings);
        endif;
        //check if audio
        if ($request->audio):
            $this->audio = $this->relation->createMediaAudio($request->audio);
        endif;
        //check if video
        if ($request->video):
            $this->video = $this->relation->createMediaVideo($request->video);
        endif;
        //check if location
//        if ($request->region):
//            $this->region = $this->relation->manageLocations($request->location);
//        endif;
        //check if location
        if ($request->location):
            $this->location = $this->relation->manageLocations($request->location);
        endif;
        //Listing Media Gallery
        if ($request->hasFile('listing_gallery')):
            $this->gallery = $this->relation->mediaGallery($request, 'listing_gallery');
        endif;
        //Listing Media Gallery Yelp
        if ($this->type === 'yelp'):
            $this->gallery = $this->relation->mediaGalleryYelp($request->listing_gallery);
        endif;
        DB::transaction(function () use ($request) {
            // upload listing banner
            if ($this->type == 'yelp'):
                $banner = $request->banner;
            else:
                $banner = $this->relation->uploadImage($request, 'banner');
            endif;
            //create new listing
            $listing = Listing::create([
                        'name' => $request->name,
                        'tagline' => $request->tagline,
                        'business_id' => $this->business_id,
                        'description' => $request->description,
                        'phone' => $request->phone,
                        'website' => $request->website,
                        'email' => $request->email,
                        'type' => $this->type,
                        'banner' => $banner,
                        'status' => $this->status['publish'],
                        'created_by' => access()->user()->id
            ]);
            if ($listing) {
                $this->updateRelations($request, $listing);
                event(new Created($listing));
                if ($this->type === 'yelp'):
                    $this->reviews($listing);
                endif;
                return true;
            }
            throw new GeneralException(trans('exceptions.backend.listings.create_error'));
        });
    }

    /**
     * Get business reviews 
     * @param type $listing
     */
    public function reviews($listing) {
        $ratings = $this->yelp->businessReviews($this->business_id);
        if ($ratings->total > 0):
            foreach ($ratings->reviews as $review):
                $listing->rating([
                    'title' => $review->user->name,
                    'body' => $review->text,
                    'customer_service_rating' => $review->rating,
                    'quality_rating' => $review->rating,
                    'friendly_rating' => $review->rating,
                    'pricing_rating' => $review->rating,
                    'rating' => $review->rating,
                    'recommend' => 'Yes',
                    'approved' => true, // This is optional and defaults to false
                        ], auth()->user());
            endforeach;
        endif;
    }

    /**
     * Update Listing.
     *
     * @param \App\Models\Blogs\Blog $blog
     * @param array $input
     */
    public function update(Listing $listing, $request) {
        if (isset($request->id) && $request->id !== '') {
            $this->type = 'yelp';
        }
        //Update Relational Tables
        $this->tags = $this->relation->createTags($request->tags);
        $this->categories = $this->relation->createCategories(array_filter($request->categories));
        $this->social_networks = $this->relation->addSocialMedia($request->social_media, $listing);
        $this->faqs = $this->relation->createFaqs($request->faq, $listing);
        $this->timings = $this->relation->createTimings($request->timings, $listing);
        $this->audio = $this->relation->createMediaAudio($request->audio);
        $this->video = $this->relation->createMediaVideo($request->video);
        $this->location = $this->relation->manageLocations($request->location, $listing);
        DB::transaction(function () use ($listing, $request) {
            // Uploading Image
            // upload listing banner
            if ($this->type == 'yelp'):
                $banner = $request->banner;
            elseif ($request->hasFile('banner')):
                $this->deleteOldFile($listing);
                $banner = $this->relation->uploadImage($request, 'banner');
            endif;

            //Listing Media Gallery Yelp
            if ($this->type === 'yelp'):
                $this->gallery = $this->relation->mediaGalleryYelp($request->listing_gallery);
            elseif ($request->hasFile('listing_gallery')):
                //Listing Media Gallery
                $this->deleteMediaGallery($listing);
                $this->gallery = $this->relation->mediaGallery($request, 'listing_gallery');
            endif;
            if ($listing->update([
                        'name' => $request->name,
                        'tagline' => $request->tagline,
                        'business_id' => (isset($request->id) && $request->id !== '') ? $request->id : null,
                        'description' => $request->description,
                        'phone' => $request->phone,
                        'website' => $request->website,
                        'email' => $request->email,
                        'type' => $this->type,
                        'banner' => (!empty($banner) ? $banner : $listing->banner),
                        'updated_by' => access()->user()->id,
                        'status' => isset($request->status[0]) ? $request->status[0] : 'review',
                    ])) {
                $this->updateRelations($request, $listing);
                event(new Updated($listing));
                return true;
            }
            throw new GeneralException(
                    trans('exceptions.backend.listings.update_error')
            );
        });
    }

    /**
     * Delete media gallery
     * @param $model
     * @return array
     */
    public function deleteMediaGallery($model) {
        $images = $model->gallery;
        $deletedImage = [];
        if ($images) :
            foreach ($images as $image):
                $deletedImage[] = $this->storage->delete($this->gallery_path . $image->filename);
            endforeach;
        endif;
        return $deletedImage;
    }

    /**
     * Add all related items to the listing
     * @param Request $request
     * @param Listing $listing
     */
    public function updateRelations($request, Listing $listing) {
        // Inserting associated category's id in mapper table
        if (!is_null($this->categories) && count($this->categories)) {
            $listing->categories()->sync($this->categories);
        }

        // Inserting associated tag's id in mapper table
        if (!is_null($this->tags) && count($this->tags)) {
            $listing->tags()->sync($this->tags);
        }
        // Inserting FAQs
        if (!is_null($this->faqs) && count($this->faqs)) {
            $listing->faqs()->sync($this->faqs);
        }

        // Inserting FAQs
        if (!is_null($this->social_networks) && count($this->social_networks)) {
            $listing->social_network()->sync($this->social_networks);
        }
        // Inserting Timings
        if (!is_null($this->timings) && count($this->timings)) {
            $listing->timings()->sync($this->timings);
        }

        // Inserting Audio
        if (!is_null($this->audio) && count($this->audio)) {
            $listing->audios()->sync($this->audio);
        }
        // Inserting Video
        if (!is_null($this->video) && count($this->video)) {
            $listing->videos()->sync($this->video);
        }
        // Insert Gallery
        if (!is_null($this->gallery) && count($this->gallery)) {
            $listing->gallery()->sync($this->gallery);
        }
        // Insert location
        if (!is_null($this->location)) {
            $listing->location()->sync($this->location);
        }
//        // Insert location
//        if (!is_null($this->location)) {
//            $listing->region()->attach($request->region);
//        }
    }

    /**
     * Delete the listing
     * @param Listing $listing
     */
    public function delete(Listing $listing) {
        DB::transaction(function () use ($listing) {
            if ($listing->delete()) {
                $this->deleteRelations($listing);
                event(new Deleted($listing));
                return true;
            }

            throw new GeneralException(trans('exceptions.backend.listings.delete_error'));
        });
    }

    /**
     * Delete Relations
     * @param Listing $listing
     */
    public function deleteRelations(Listing $listing) {
        MapCategory::where('ezzy_listing_id', $listing->id)->delete();
        MapTag::where('ezzy_listing_id', $listing->id)->delete();
        MapFaq::where('ezzy_listing_id', $listing->id)->delete();
        MapSocialMedia::where('ezzy_listing_id', $listing->id)->delete();
        MapTiming::where('ezzy_listing_id', $listing->id)->delete();
        MapAudio::where('ezzy_listing_id', $listing->id)->delete();
        MapVideo::where('ezzy_listing_id', $listing->id)->delete();
        MapGallery::where('ezzy_listing_id', $listing->id)->delete();
        MapLocation::where('ezzy_listing_id', $listing->id)->delete();
    }

    /**
     * Destroy  Image.
     *
     * @param int $model
     */
    public function deleteOldFile($model) {
        $fileName = $model->banner;
        return $this->storage->delete($this->upload_path . $fileName);
    }

}

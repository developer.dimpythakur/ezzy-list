@extends('frontend.layouts.auth')
@section('content')
<div class="row">
    <div class="col-md-6 mx-auto mt-5">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mt-4 text-center mb-1">
                    {{ trans('labels.frontend.auth.login_box_title') }}
                </h4>
            </div>
            <div class="card-body p-4">
                @if ($errors->any())
                <div class="alert alert-danger mb-2 mt-2">
                    @foreach ($errors->all() as $error)
                    <p class="my-1">{{$error}}</p>
                    @endforeach
                </div>
                @endif

                {{ Form::open(['route' => 'frontend.auth.login', 'class' => 'form-horizontal']) }}
                <div class="form-group row">
                    {{ Form::label('email', trans('validation.attributes.frontend.register-user.email'), ['class' => 'col-md-8 control-label']) }}
                    <div class="col-md-12">
                        {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.email')]) }}
                    </div><!--col-md-12-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ Form::label('password', trans('validation.attributes.frontend.register-user.password'), ['class' => 'col-md-8 control-label']) }}
                    <div class="col-md-12">
                        {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password')]) }}
                    </div><!--col-md-12-->
                </div><!--form-group-->

                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="checkbox">
                            <label>
                                {{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}
                            </label>
                        </div>
                    </div><!--col-md-12-->
                    <div class="col-md-6">
                        {{ link_to_route('frontend.auth.password.reset', trans('labels.frontend.passwords.forgot_password')) }}
                    </div>
                </div><!--form-group-->

                <div class="form-group row">
                    <div class="col-md-12 col-md-offset-4">
                        {{ Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn btn-primary', 'style' => 'margin-right:15px']) }}
                        <a class="" href="{{ route('frontend.auth.register')}}">
                            Click here to {{ trans('labels.frontend.auth.register_button')}}
                        </a>
                    </div><!--col-md-12-->
                </div><!--form-group-->

                {{ Form::close() }}
                <div class="row text-center">

                </div>
            </div><!-- panel body -->

        </div><!-- panel -->

    </div><!-- col-md-8 -->

</div><!-- row -->

@endsection
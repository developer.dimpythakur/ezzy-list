<div class="form-group">
    {{ Form::label('categories', 'Select Category', ['class' => 'col-12 control-label']) }}
    @if(!empty($selectedCategories))
    {{ Form::select('categories[]', $categories, $selectedCategories, ['class' => 'form-control box-size', 'placeholder' => 'Select Category', 'required' => 'required'])}}
    @else
    {{ Form::select('categories[]', $categories, null, ['class' => 'form-control box-size', 'placeholder' => 'Select Category', 'required' => 'required'])}}
    @endif
</div><!--form control-->

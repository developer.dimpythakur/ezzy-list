<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-images"></i> Media</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row mb-3">
            <div class="form-group col-12">
                {{ Form::label('featured_image', 'Banner', ['class' => 'control-label required']) }}
                <div class="fake-file-upload h6 rounded-pill transition-1 d-flex align-items-center justify-content-center p-3 text-center text-muted position-relative">
                    <input type="file" id="single_img_upload" name="banner" class="position-absolute pointer" >
                    <i class="la la-cloud-upload-alt h3 m-0"></i>&nbsp; Choose image
                </div>
                <div class="uploaded-images">
                    <ul class="nostyle d-flex flex-wrap pl-0" id="uploaded_feat_img">
                        @if(!empty($listing->banner))
                        <input type="hidden" name="banner" value="{{ isset($listing)?$listing->banner:''}}">
                        <li>
                            <img src="{{  $listing->banner }}" height="80" width="80">
                        </li>
                        @endif
                    </ul>
                </div>

            </div><!--form control-->
        </div>
        <!-- Gallery -->
        <?php
//        dd($listing->gallery);
        ?>
        <div class="row mb-3">
            <div class="form-group col-12">
                <label>Gallery</label>
                <div class="fake-file-upload h6 rounded-pill transition-1 d-flex align-items-center justify-content-center p-3 text-center text-muted position-relative">
                    <input type="file" id="gallery_img_upload" name="listing_gallery[]" class="position-absolute pointer"
                           multiple=""> <i  class="la la-cloud-upload-alt h3 m-0"></i>&nbsp; Choose or drag images here
                </div>
                <div class="uploaded-images gallery">
                    <ul class="nostyle d-flex flex-wrap" id="gallery_images_list">
                        @if(!empty($listing->gallery) && $listing->gallery->count())
                        @foreach($listing->gallery as $key => $gallery)
                        <li class="mt-2 col-md-3" id="gallery-img-{{$key}}">
                            <img class="img-fluid" src="{{ $gallery->url }}">
                            <i data-targetimg="0" class="remove-gallery-img p-1 shadow pointer btn btn-outline-danger border-0 las las-trash lar la-trash-alt"></i>
                        </li>
                        @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            <!-- / Gallery -->
        </div>

    </div>
</div>

@push('after_scripts')
<script>
    var selDiv = "", imageHTML;
    document.addEventListener("DOMContentLoaded", init, false);

    function init() {
        document.querySelector('#gallery_img_upload').addEventListener('change', handleFileSelect, false);
        selDiv = document.querySelector("#gallery_images_list");
    }

    function handleFileSelect(e) {
        if (!e.target.files || !window.FileReader)
            return;
        selDiv.innerHTML = "";

        var files = e.target.files;
        var filesArr = Array.prototype.slice.call(files);
        filesArr.forEach(function (f) {

            if (!f.type.match("image.*")) {
                return;
            }

            var reader = new FileReader();
            reader.onload = function (e) {
                let col = 1;
                let className = 'col-md-3';
                var tmppath = e.target.result;
                imageHTML += `<li class="mt-2 ${className}" id="gallery-img-${col}">
                              <input type="hidden" name="listing_gallery[]" value="listing_gallery[][${tmppath}]" />
                                <img class="img-fluid" src="${tmppath}">
                               <i data-targetimg="${col}" class="remove-gallery-img p-1 shadow pointer btn btn-outline-danger border-0 las las-trash lar la-trash-alt"></i>
                             </li>`;
                col++;
                selDiv.innerHTML += imageHTML;
            };
            reader.readAsDataURL(f);
        });

    }




//-------------------------------
// Gallery Images Upload
//-------------------------------
    jQuery('#gallery_img_uploads').change(function (event) {
        var imageHTML = jQuery("#gallery_images_list").html();
        let col = 1;
        jQuery(event.target.files).each(function (index) {
            let className = 'col-md-3';
            var tmppath = URL.createObjectURL(event.target.files[index]);
            imageHTML += `<li class="mt-2 ${className}" id="gallery-img-${index}">
                <input type="hidden" name="listing_gallery[]" value="${tmppath}" />
                          <img class="img-fluid" src="${tmppath}">
                           <i data-targetimg="${index}" class="remove-gallery-img p-1 shadow pointer btn btn-outline-danger border-0 las las-trash lar la-trash-alt"></i>
                           </li>`;
            col++;
        });
        jQuery('#gallery_images_list').html(imageHTML);
        event.preventDefault();
    });

//-------------------------------
// Remove Gallery Images
//-------------------------------
    $(document).on('click', '.remove-gallery-img', function (e) {
        e.preventDefault();
        $(this).closest('li').remove();
    });
</script>
@endpush
<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use Prologue\Alerts\Facades\Alert;

class RedirectResponse implements Responsable {

    protected $route;
    protected $message;

    public function __construct($route, $message = null) {
        $this->route = $route;
        $this->message = $message;
    }

    public function toResponse($request) {
        Alert::success($this->message)->flash();
        return redirect()
                        ->to($this->route)
                        ->with($this->message);
    }

}

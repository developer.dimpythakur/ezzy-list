<?php

namespace App\Seeder;

use App\Repositories\Frontend\Listing\YelpRepository;
use App\Seeder\Repository;

class Yelp extends Repository {

    private $yelp;
    private $term;

    /**
     * Insert 
     * @return type
     */
    public function __construct() {
        parent::__construct();

        $this->yelp = ( new YelpRepository);
    }

    /**
     *  Store new business
     * @param type $term
     * @return type
     */
    public function getBusinesses($term = null) {
        if (null != $term):
            $this->term = $term;
        else:
            $this->term = [
                'location' => collect(config('listing.regions'))->pluck('name')->random(),
                'term' => collect(config('listing.categories'))->pluck('name')->random(),
                'limit' => 2
            ];
        endif;
        $results = $this->yelp->searchBusiness($this->term);
        if ($results->businesses):
            foreach ((collect($results->businesses)) as $business) {
                $businesses[] = $this->store($business->id, true);
            }
            return $businesses;
        endif;
        return [];
    }

    /**
     *  Save new business
     * @param type $business_id
     * @param type $reviews
     * @return type
     */
    public function store($business_id, $reviews = false) {
        return $this->make($this->getBusiness($business_id, $reviews));
    }

    /**
     * Get Business
     * @param type $businessId
     */
    public function getBusiness($business_id, $reviews = false) {
        $business = $this->yelp->business($business_id);
        $businessReviews = (true == $reviews) ? $this->yelp->businessReviews($business_id) : null;
        return $this->relation->businessListing($business, $businessReviews);
    }

}

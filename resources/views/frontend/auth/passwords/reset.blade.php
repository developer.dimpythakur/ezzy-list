@extends('frontend.layouts.auth')
@section('content')
<div class="row">
    <div class="col-md-6 mx-auto mt-5">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mt-4 text-center mb-1">
                    {{ trans('labels.frontend.passwords.reset_password_box_title') }}
                </h4>
            </div>

            <div class="card-body">

                {{ Form::open(['route' => 'frontend.auth.password.reset', 'class' => 'form-horizontal']) }}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group">
                    {{ Form::label('email', trans('validation.attributes.frontend.register-user.email'), ['class' => 'col-md-4 control-label']) }}
                    <div class="col-md-6">
                        <p class="form-control-static">{{ $email }}</p>
                        {{ Form::input('hidden', 'email', $email, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.email')]) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ Form::label('password', trans('validation.attributes.frontend.register-user.password'), ['class' => 'col-md-4 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password')]) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->

                <div class="form-group">
                    {{ Form::label('password_confirmation', trans('validation.attributes.frontend.register-user.password_confirmation'), ['class' => 'col-md-4 control-label']) }}
                    <div class="col-md-6">
                        {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.password_confirmation')]) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {{ Form::submit(trans('labels.frontend.passwords.reset_password_button'), ['class' => 'btn btn-primary']) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->

                {{ Form::close() }}

            </div><!-- panel body -->

        </div><!-- panel -->

    </div><!-- col-md-8 -->

</div><!-- row -->
@endsection

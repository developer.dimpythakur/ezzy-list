@push("after_styles")
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
@endpush
<?php
$options = [
    'file_picker_callback' => 'elFinderBrowser',
    'selector' => 'textarea.tinymce',
    'plugins' => 'image,link,media,anchor',
];
?>
<div class="card">
    <div class="card-body row">
        <div class="form-group col-sm-12">
            {{ Form::label('title', trans('validation.attributes.backend.pages.title'), ['class' => 'control-label required']) }}
            {{ Form::text('title', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.pages.title'), 'required' => 'required']) }}
        </div><!--form control-->

        <div class="form-group col-sm-12">
            {{ Form::label('description', trans('validation.attributes.backend.pages.description'), ['class' => 'control-label required']) }}
            {{ Form::textarea('description', null,[
                    'class' => 'form-control box-size tinymce',
                     'data-options'=> trim(json_encode($options)),
                            'placeholder' => trans('validation.attributes.backend.pages.description')]) }}



        </div><!--form control-->

        <div class="form-group col-sm-12">
            {{ Form::label('cannonical_link', trans('validation.attributes.backend.pages.cannonical_link'), ['class' => 'control-label']) }}
            {{ Form::text('cannonical_link', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.pages.cannonical_link')]) }}
        </div><!--form control-->

        <div class="form-group col-sm-12">
            {{ Form::label('seo_title', trans('validation.attributes.backend.pages.seo_title'), ['class' => 'control-label']) }}
            {{ Form::text('seo_title', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.pages.seo_title')]) }}
        </div><!--form control-->

        <div class="form-group col-sm-12">
            {{ Form::label('seo_keyword', trans('validation.attributes.backend.pages.seo_keyword'), ['class' => 'control-label']) }}
            {{ Form::text('seo_keyword', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.pages.seo_keyword')]) }}
        </div><!--form control-->

        <div class="form-group col-sm-12">
            {{ Form::label('seo_description', trans('validation.attributes.backend.pages.seo_description'), ['class' => 'control-label']) }}
            {{ Form::textarea('seo_description', null,[
                    'class' => 'form-control box-size tinymce',
                     'data-options'=> trim(json_encode($options)),
                            'placeholder' => trans('validation.attributes.backend.pages.seo_description')]) }}

        </div><!--form control-->

        @include('backend.layouts.sections.upload_form')
        <div class="form-group col-sm-12" element="div">
            <div>
                <label>Status</label>
            </div>
            <div class="form-check form-check-inline">
                <input type="radio" class="form-check-input" checked="" value="1" name="status" id="status_1">
                <label class="radio-inline form-check-label font-weight-normal" for="status_1">Published</label>
            </div>

            <div class="form-check form-check-inline">
                <input type="radio" class="form-check-input" value="0" name="status" id="status_0">
                <label class="radio-inline form-check-label font-weight-normal" for="status_0">Draft</label>
            </div>
        </div>


    </div><!-- /.card-body -->
</div><!--card-->


@push("after_scripts")
<!-- include select2 js-->
<script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
@if (app()->getLocale() !== 'en')
<script src="{{ asset('packages/select2/dist/js/i18n/' . app()->getLocale() . '.js') }}"></script>
@endif
<script src="{{ asset('packages/tinymce/tinymce.min.js') }}"></script>
<script>

$(document).ready(function () {
//        select2
    $('.select2_multiple').select2();
    //tinymce
    var $element = "textarea.form-control.tinymce";
    tinymce.init(jQuery($element).data('options'));
    tinymce.activeEditor.setContent(html, {format: 'raw'});
});

</script>
@endpush
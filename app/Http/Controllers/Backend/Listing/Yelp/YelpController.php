<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Backend\Listing\Yelp;

use App\Http\Controllers\Controller;
use Stevenmaguire\Yelp\ClientFactory;
use Stevenmaguire\Yelp\Version;
// use 
use App\Models\Listing\Categories\Category;
//regions
use App\Models\Listing\Region\Region;
// request
use Illuminate\Http\Request;

/**
 * Description of YelpController
 *
 * @author codepluck
 */
class YelpController extends Controller {

    private $accessToken; // Required, unless apiKey is provided
    private $apiHost; // Optional, default 'api.yelp.com',
    private $apiKey; // Required, unless apiKey is provided
    protected $client;
    protected $options;

    public function __construct() {
        $this->options = [
            'accessToken' => config('yelp.v3.accessToken'),
            'apiHost' => config('yelp.v3.apiHost'),
            'apiKey' => config('yelp.v3.apiKey')
        ];
        $this->client = ClientFactory::makeWith($this->options, Version::THREE);
    }

    /**
     * 
     * @return typeMain index function
     */
    public function index() {
        $categories = Category::getSelectData('name');
        $locations = Region::getSelectData();
        return view('backend.listing.yelp.index')->with([
                    'business' => '',
                    'categories' => $categories,
                    'locations' => $locations,
        ]);
    }

    /**
     * Get Yelp Business 
     * @param Request $request
     * @return type
     */
    public function searchBusiness(Request $request) {
        $business = [];
        $params = [
            'location' => ($request->location) ? Region::find($request->location)->name : 'New York',
            'term' => ($request->term) ? $request->term : 'Food',
        ];
        $results = $this->client->getBusinessesSearchResults($params);
        $totalResults = $results->total;
        $region = $results->region;
        $businesses = $results->businesses;
        if ($results):
            foreach ($businesses as $result):
                $business[] = $result;
            endforeach;
        endif;
        return view('backend.listing.yelp.results')->with([
                    'business' => $business,
                    'totalResults' => $totalResults,
                    'region' => $region,
        ]);
    }

}

<?php

namespace App\Http\Requests\Frontend\Auth;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class RegisterRequest.
 */
class RegisterRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => ['required', 'email', 'max:255', Rule::unique('users')],
//            'password' => 'required|min:8|confirmed|regex:"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,}$"',
            'password' => 'required|min:4|confirmed',
//            'is_term_accept' => 'required',
//            'captcha.required_if' => 'required|captcha'
            'captcha-response' => 'required_if:captcha,true|captcha',
        ];
    }

    /**
     * @return array
     */
    public function messages() {
        return [
            'captcha' => 'Invalid Captcha',
//            'captcha-response.required_if' => trans('validation.required', ['attribute' => 'captcha']),
            'captcha-response.required_if' => trans('validation.required', ['attribute' => 'captcha']),
            'password.regex' => 'Password must contain at least 1 uppercase letter and 1 number.',
        ];
    }

    public function validated() {
        
    }

}

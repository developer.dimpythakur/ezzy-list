<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
//Model User
use App\Models\Access\User\User;
//Model Listing
use App\Models\Listing\Listing\Listing;

/**
 * Class AccountController.
 */
class AccountController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $listings = Listing::where('created_by', access()->user()->id)->get();
        return view('frontend.user.account')
                        ->with(['listings' => $listings]);
    }

}

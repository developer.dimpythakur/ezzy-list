<!-- Featured Image -->
@if(!empty($page->featured_image))
<div class="form-group col-sm-12">
    <div class="card p-4">
        <div class="col-lg-3">
            <img src="{{ Storage::disk('public')->url($page->featured_image) }}" height="80"
                 width="80">
        </div>
    </div>
</div>
@endif
<!-- /Featured Image -->
<div data-field-name="upload" class="form-group col-sm-12" element="div" data-initialized="true">  
    <label>Featured Image</label>
    <div class="backstrap-file ">
        <input type="file" name="featured_image" value="" class="file_input backstrap-file-input">
        <label class="backstrap-file-label" for="customFile"></label>
    </div>
</div>
@push("after_styles")
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
<style>
    .normal-labels label{
        font-weight: 400
    }
    /*
     * File
     */
    .file input:focus ~ .file-custom {
        box-shadow: 0 0 0 .075rem #fff, 0 0 0 .2rem #0074d9;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu .collapse:not(.show){
        display: block !important
    }
    .select2-container .select2-selection--single,
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        height: 40px;
        line-height: 40px;
    }


    /*FAKE FILE*/
    .fake-file-upload {
        border: 2px dashed #ccc;
    }
    .fake-file-upload input {
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
    }
    .fake-file-upload:hover{
        border-color: #e53935 !important;
    }
    .nostyle{list-style: none;margin: 0;padding: 0}

</style>
@endpush

<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-location-arrow"></i> Add New Location</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row my-3">
            <div class="form-group col-12">
                {{ Form::label('name', 'Location Title', ['class' => ' control-label required']) }}
                {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => 'Location Title', 'required' => 'required']) }}
            </div><!--form-group-->
            <div class="form-group col-12">
                {{ Form::label('country', 'Country', ['class' => ' control-label required']) }}
                {{ Form::text('country', 'USA', ['class' => 'form-control box-size', 'readonly']) }}
            </div><!--form-group-->

            <div class="form-group col-12">
                {{ Form::label('region', 'Region', ['class' => ' control-label required']) }}
                <select name="region" class="form-control box-size">
                    @foreach(getStates() as $state)
                    <option {{  (isset($listing_location)? getSelectedLocation($listing_location,$state['abbreviation']):'')  }} value="{{$state['abbreviation']}}">{{ $state['name'] }}</option>
                    @endforeach
                </select>
            </div><!--form-group-->

            <div class="form-group col-12">
                {{ Form::label('category_image', 'Image', ['class' => ' control-label']) }}
                <div class="fake-file-upload h6 rounded-pill transition-1 d-flex align-items-center justify-content-center p-3 text-center text-muted position-relative">
                    <input type="file" id="image" name="image" class="position-absolute pointer"
                           > <i  class="icon-cloud-computing h3 m-0"></i>&nbsp; Choose or drag image here
                </div>
            </div>

            @if(isset($listing_location) && !empty($listing_location->image))
            <div class="form-group col-12">
                <div class="card py-4">
                    <div class="col-lg-4">
                        <img src="{{ Storage::url('images/listing/locations/' . $listing_location->image) }}" height="100"
                             class="img-thumbnail img-preview" width="100%">
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>



@push("after_scripts")

<!-- include select2 js-->
<script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
@if (app()->getLocale() !== 'en')
<script src="{{ asset('packages/select2/dist/js/i18n/' . app()->getLocale() . '.js') }}"></script>
@endif
<script src="{{ asset('packages/tinymce/tinymce.min.js') }}"></script>
<script>

$(document).ready(function () {
//        select2
    $('.select2_multiple').select2();
    //tinymce
    var $element = "textarea.form-control.tinymce";
    tinymce.init(jQuery($element).data('options'));

});

</script>
@endpush
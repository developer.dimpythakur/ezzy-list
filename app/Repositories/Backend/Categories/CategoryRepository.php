<?php

namespace App\Repositories\Backend\Categories;

use DB;
use Carbon\Carbon;
use App\Models\Listing\Categories\Category;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
//Events
use App\Events\Backend\Listing\Categories\Created;
use App\Events\Backend\Listing\Categories\Update;
use App\Events\Backend\Listing\Categories\Deleted;
//Storage
use Illuminate\Support\Facades\Storage;

/**
 * Class CategoryRepository.
 */
class CategoryRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Category::class;

    public function __construct() {
        $this->upload_path = 'images' . DIRECTORY_SEPARATOR . 'category' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.ezzy_categories.table') . '.id',
                            config('module.ezzy_categories.table') . '.name',
                            config('module.ezzy_categories.table') . '.slug',
                            config('module.ezzy_categories.table') . '.image',
                            config('module.ezzy_categories.table') . '.description',
                            config('module.ezzy_categories.table') . '.parent_id',
                            config('module.ezzy_categories.table') . '.status',
                            config('module.ezzy_categories.table') . '.created_at',
                            config('module.categories.table') . '.updated_at',
                        ])->orderBy(config('module.ezzy_categories.table') . '.created_at', 'DESC');
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input) {
        if ($this->query()->where('name', $input['name'])->first()) {
            throw new GeneralException(trans('exceptions.backend.categories.already_exists'));
        }
        DB::transaction(function () use ($input) {
            $input['status'] = isset($input['status']) ? 1 : 0;
            $input['slug'] = Str::slug($input['name']);
            $input['image'] = $this->uploadImage($input);
            $input['icon'] = $input['icon'];
            $input['created_by'] = access()->user()->id;
            if ($category = Category::create($input)) {
                event(new Created($category));
                return true;
            }

            throw new GeneralException(trans('exceptions.backend.listingcategories.create_error'));
        });
    }

    /**
     * Upload Image.
     *
     * @param array $input
     *
     * @return array $input
     */
    public function uploadImage($input) {
        if (isset($input['category_image']) && !empty($input['category_image'])) {
            $avatar = $input['category_image'];
            $fileName = time() . $avatar->getClientOriginalName();
            $this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()));
            return $fileName;
        }
    }

    /**
     * For updating the respective Model in storage
     *
     * @param Category $category
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(Category $category, array $input) {
        if ($category->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.categories.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Category $category
     * @throws GeneralException
     * @return bool
     */
    public function delete(Category $category) {
        DB::transaction(function () use ($category) {
            if ($category->delete()) {
                event(new Deleted($category));
                return true;
            }
            throw new GeneralException(trans('exceptions.backend.listingcategories.delete_error'));
        });
    }

}

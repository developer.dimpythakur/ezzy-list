@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.listing.tags.management') . ' | ' . trans('labels.backend.listing.tags.edit'))
@section('page-header')
<h1>{{ trans('labels.backend.listing.tags.edit') }}</h1>
@endsection

@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        @include('backend.listing.component.grouped_errors')
        {{ Form::model($listing_tag ?? '', ['route' => ['admin.listing-tags.update', $listing_tag ?? ''], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-listing-tags']) }}

        <div class="card">
            <div class="card-body row">
                @include("backend.listing.tags.form")
            </div><!-- /.card-body -->
        </div><!--card-->

        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=update_and_back">Update</span>
                </button>
            </div>
            <a href="{{ route('admin.listing-tags.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>
@endsection
@section("after_scripts")

@endsection







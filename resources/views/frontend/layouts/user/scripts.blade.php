@if (config('backend.scripts') && count(config('backend.scripts')))
@foreach (config('backend.scripts') as $path)
<script type="text/javascript" id="{{ ($path) }}" src="{{ asset($path) }}"></script>
@endforeach
@endif
@include('frontend.layouts.sections.alerts')
<!-- page script -->
<script type="text/javascript">
// To make Pace works on Ajax calls
$(document).ajaxStart(function () {
    Pace.restart();
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var activeTab = $('[href="' + location.hash.replace("#", "#tab_") + '"]');
location.hash && activeTab && activeTab.tab('show');
$('.nav-tabs a').on('shown.bs.tab', function (e) {
    location.hash = e.target.hash.replace("#tab_", "#");
});
</script>
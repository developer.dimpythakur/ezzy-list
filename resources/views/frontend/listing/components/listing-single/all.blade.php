<div class="row">
    <!-- Left column -->
    <div class="col-lg-6">
        <!-- Description -->
        @include('frontend.listing.components.listing-single.description')
        <!-- /Description -->
        <!-- Services Offered -->
        <!--include('frontend.listing.components.listing-single.services')-->
        <!-- /Services Offered -->
        <!-- Additional Details -->
        <!--include('frontend.listing.components.listing-single.addition-details')-->
        <!-- /Additional Details -->
        <!-- Ratings -->
        @include('frontend.listing.components.listing-single.ratings')
        <!-- /Ratings -->
        <!-- Comment form -->
        @include('frontend.listing.components.listing-single.review-add')

        <!-- /Comment form -->
    </div>
    <!-- /Left column -->
    <!-- Right column -->
    <div class="col-lg-6">
        <!-- Gallery -->
        @include('frontend.listing.components.listing-single.gallery')
        <!-- /Gallery -->
        <!-- Category -->
        @include('frontend.listing.components.listing-single.category')

        <!-- /Category -->

        @if($listing && count($listing->videos))
        <!-- Video -->
        @include('frontend.listing.components.listing-single.video')
        <!-- /Video -->
        @endif
        @if($listing && count($listing->faqs))
        <!-- General Questions -->
        @include('frontend.listing.components.listing-single.faqs')
        <!-- /General Questions -->
        @endif

        <!-- Customer Reviews -->
        {{--    @include('frontend.listings.section.listing.single.reviews')--}}

        <!-- /Customer Reviews -->
    </div>
    <!-- /Right column -->

</div>
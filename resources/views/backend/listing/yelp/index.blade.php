@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.yelps.management'))
@section('page-header')
<h1>Search Yelp</h1>
@endsection
@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.yelp.management') }}</span>
    </h2>
</div>
@endsection



@section('after_styles')
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/backend/datatables.css') }}">
@endsection

@section('content')
<!-- Default box -->

<div class="row">
    <div class="col-md-8 bold-labels">
        {{ Form::open(['route' => 'admin.yelp-search', 'class' => 'form-horizontal add-listing','enctype' =>'multipart/form-data', 'role' => 'form', 'method' => 'post', 'id' => 'create-listing']) }}
        @include('backend.listing.yelp.form-search')
        {{ Form::close() }}

    </div>
</div>
@endsection


@section('after_scripts')
<!-- DATA TABLES SCRIPT -->
<script type="text/javascript" src="{{ asset('js/backend/datatables.js') }}"></script>

<script>
jQuery(document).ready(function () {

});
</script>

@endsection
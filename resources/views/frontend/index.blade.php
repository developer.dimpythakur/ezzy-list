@extends('frontend.layouts.app')
@section('content')
@include('frontend.listing.components.listing-slider.slider')
@include('frontend.listing.components.listing-locations.style-one')
@include('frontend.listing.components.listing-top-rated.style-one')
@include('frontend.listing.components.listing-categories.style-one')
@include('frontend.listing.components.pricing')
@include('frontend.listing.components.blog.style-one')
@include('frontend.listing.components.listing-newsletter.newsletter')
@endsection

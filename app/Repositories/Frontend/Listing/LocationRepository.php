<?php

namespace App\Repositories\Frontend\Listing;

use App\Exceptions\GeneralException;
use App\Models\Listing\ListingCategories\ListingCategory;
use App\Models\Listing\Locations\Location;
use App\Repositories\BaseRepository;

/**
 * Class LocationRepository.
 */
class LocationRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = ListingLocation::class;

    /*
     * Find page by page_slug
     */

    public function findBySlug($regionID) {
        if (!is_null($this->query()->whereRegionId($regionID)->get())) {
            return $this->query()->whereRegionId($regionID)->get();
        }
        throw new GeneralException(trans('exceptions.backend.access.pages.not_found'));
    }

}

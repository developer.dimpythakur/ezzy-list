@extends('frontend.layouts.auth')
@section('content')
<div class="row">

    <div class="col-md-6 mx-auto mt-5">
        @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
        @endif
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mt-4 text-center mb-1">
                    {{ trans('labels.frontend.passwords.reset_password_box_title') }}
                </h4>
            </div>
            <div class="card-body">

                {{ Form::open(['route' => 'frontend.auth.password.email', 'class' => 'form-horizontal']) }}
                <div class="form-group">
                    {{ Form::label('email', trans('validation.attributes.frontend.register-user.email'), ['class' => 'col-md-8 control-label']) }}
                    <div class="col-md-12">
                        {{ Form::input('email', 'email', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.frontend.register-user.email')]) }}
                    </div><!--col-md-12-->
                </div><!--form-group-->

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        {{ Form::submit(trans('labels.frontend.passwords.send_password_reset_link_button'), ['class' => 'btn btn-primary']) }}
                    </div><!--col-md-6-->
                </div><!--form-group-->

                {{ Form::close() }}

            </div><!-- panel body -->

        </div><!-- panel -->

    </div><!-- col-md-8 -->

</div><!-- row -->
@endsection
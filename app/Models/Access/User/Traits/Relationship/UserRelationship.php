<?php

namespace App\Models\Access\User\Traits\Relationship;

use App\Models\Access\User\SocialLogin;
use App\Models\System\Session;
use App\Models\Access\User\Profile;
use App\Models\Listing\Listing\Listing;

/**
 * Class UserRelationship.
 */
trait UserRelationship {

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles() {
        return $this->belongsToMany(config('access.role'), config('access.role_user_table'), 'user_id', 'role_id');
    }

    /**
     * Many-to-Many relations with Permission.
     * ONLY GETS PERMISSIONS ARE NOT ASSOCIATED WITH A ROLE.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions() {
        return $this->belongsToMany(config('access.permission'), config('access.permission_user_table'), 'user_id', 'permission_id');
    }

    /**
     * @return mixed
     */
    public function providers() {
        return $this->hasMany(SocialLogin::class);
    }

    /**
     * @return mixed
     */
    public function sessions() {
        return $this->hasMany(Session::class);
    }

    /**
     * User has one profile
     */
    public function profile() {
        return $this->hasOne(Profile::class);
    }

    /**
     * User Listings
     * @return type
     */
    public function listings() {
        return $this->hasMany(Listing::class);
    }

    /**
     * User Bookmarks
     * @return type
     */
    public function bookmarks() {
        return $this->hasMany('App\Models\Bookmark');
    }

    /**
     * User Bookmark Type
     * @return type
     */
    public function bookmarksByType($type) {
        return $this->bookmarks()->where('ezzy_bookmarks.model_type', $type);
    }

    /**
     * Bookmark
     * @param type $object
     * @return type
     */
    public function bookmark($object) {
        if ($this->isBookmarked($object)) {
            return $this->bookmarks()->where([
                        ['ezzy_bookmarks.model_type', get_class($object)],
                        ['ezzy_bookmarks.model_id', $object->id]
                    ])->delete();
        }

        return $this->bookmarks()->create(['model_type' => get_class($object), 'model_id' => $object->id]);
    }

    /**
     * Check if bookmarked
     * @param type $object
     * @return type
     */
    public function isBookmarked($object) {
        return $this->bookmarks()->where([
                    ['ezzy_bookmarks.model_type', get_class($object)],
                    ['ezzy_bookmarks.model_id', $object->id]
                ])->exists();
    }

}

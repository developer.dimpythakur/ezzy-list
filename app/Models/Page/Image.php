<?php

namespace App\Models\Page;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Page\Page;

class Image extends Model {

    use SoftDeletes;

    protected $fillable = ['filename'];

    public function page() {
        return $this->belongsTo(Page::class);
    }

}

@extends ('backend.layouts.dashboard')
@section ('title', trans('labels.backend.pages.management'))
@section('page-header')
<h1>{{ trans('labels.backend.pages.management') }}</h1>
@endsection


<?php
$defaultBreadcrumbs = [
    'admin' => route('admin.dashboard'),
    trans('labels.backend.pages.management') => false,
];

// if breadcrumbs aren't defined in the CrudController, use the default breadcrumbs
$breadcrumbs = $breadcrumbs ?? $defaultBreadcrumbs;
?>


@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.pages.management') }}</span>
        <small id="datatable_info_stack">
            <div class="hidden-print with-border">
                <a href="{{route('admin.pages.create')}}" class="btn btn-primary" data-style="zoom-in">
                    <span class="ladda-label"><i class="la la-plus"></i> {{trans('menus.backend.pages.create')}}</span>
                </a>
            </div>
        </small>
    </h2>
</div>
@endsection



@section('after_styles')
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">

@endsection


@section('content')
<div class="row">
    <div class="table-responsive data-table-wrapper">
        <table id="pages-table" class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2 dataTable dtr-inline">
            <thead>
                <tr>
                    <th>{{ trans('labels.backend.pages.table.title') }}</th>
                    <th>{{ trans('labels.backend.pages.table.status') }}</th>
                    <th>{{ trans('labels.backend.pages.table.createdat') }}</th>
                    <th>{{ trans('labels.backend.pages.table.createdby') }}</th>
                    <th>{{ trans('labels.general.actions') }}</th>
                </tr>
            </thead>
            <thead class="transparent-bg">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div><!--table-responsive-->
</div>
@endsection


@section('after_scripts')
<script type="text/javascript" src="{{ asset('js/backend/datatables.js') }}"></script>
<script>
$(function() {
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});
var dataTable = $('#pages-table').dataTable({
processing: true,
        serverSide: true,
        ajax: {
        url: '{{ route("admin.pages.get") }}',
                type: 'post'
        },
        columns: [
        {data: 'title', name: '{{config('module.pages.table')}}.title'},
        {data: 'status', name: '{{config('module.pages.table')}}.status'},
        {data: 'created_at', name: '{{config('module.pages.table')}}.created_at'},
        {data: 'created_by', name: '{{config('access.users_table')}}.first_name'},
        {data: 'actions', name: 'actions', searchable: false, sortable: false}
        ],
        order: [[1, "asc"]],
        searchDelay: 500,
        dom: "<'row hidden'<'col-sm-6 hidden-xs'i><'col-sm-6 hidden-print'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row mt-2 '<'col-sm-6 col-md-4'l><'col-sm-2 col-md-4 text-center'B><'col-sm-6 col-md-4 hidden-print'p>>",
        buttons: {
        buttons: [
        { extend: 'copy', className: 'copyButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
        { extend: 'csv', className: 'csvButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
        { extend: 'excel', className: 'excelButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
        { extend: 'pdf', className: 'pdfButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }},
        { extend: 'print', className: 'printButton', exportOptions: {columns: [ 0, 1, 2, 3 ]  }}
        ]
        },
        language: {
        @lang('datatable.strings')
        }
});
Backend.DataTableSearch.init(dataTable);
});
</script>

@endsection
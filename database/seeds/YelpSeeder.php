<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of YelpSeeder
 *
 * @author codepluck
 */
class YelpSeeder {
    //put your code here

    /**
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $results = $this->yelp->searchBusiness([
            'location' => ($request->location) ? Region::find($request->location)->name : 'New York',
            'term' => ($request->term) ? $request->term : 'Food',
        ]);
        $businesses = [];
        foreach ((collect($results->businesses)) as $business) {
            $businesses[] = $this->listingRepository->create(($this->getBusiness($business->id)));
        }
        dd($businesses);


    }

    /**
     * Get Business
     * @param type $businessId
     */
    public function getBusiness($businessId) {
        $listing = ($this->yelp->business($businessId));
        if ($listing):
            $business['id'] = ($listing->id);
            $business['name'] = $listing->name;
            $business['tags'] = [];
            $business['description'] = $listing->name;
            $business['tagline'] = $listing->alias;
            $business['phone'] = $listing->display_phone;
            $business['website'] = null;
            $business['email'] = null;
            $business['social_media'] = null;
            $business['banner'] = $listing->image_url;
            $business['url'] = $listing->url;
            $business['categories'] = $this->categories($listing->categories);
            $business['address'] = $this->filterLocations($listing);
            $business['listing_gallery'] = isset($listing->photos) ? $listing->photos : null;
            if (!$listing->is_closed):
                $business['timings'] = $this->hours($listing);
            endif;
        endif;
        return $business;
    }

    /**
     * Location address including city and country
     * @param type $business
     * @return type
     */
    public function filterLocations($business) {
        $address = [];
        if (isset($business->location)):
            $address['address'] = collect($business->location->display_address)->implode(' ');
            $address['region'] = Region::where('name', 'LIKE', "%{$business->location->city}%")->value('name');
            $address['latitude'] = $business->coordinates->latitude;
            $address['longitude'] = $business->coordinates->longitude;
        endif;
        return $address;
    }

    /**
     * Filter Hours
     * @param type $hours
     * @return type
     */
    public function hours($hours = null) {
        $timings = [];
        $timing = (isset($hours->hours) && isset(collect($hours->hours)->first()->open)) ? (collect($hours->hours)->first()->open) : null;
        if (null !== $timing):
            foreach ($timing as $timing) {
                $timings[strtolower(daysArray()[$timing->day])] = [
                    'opening' => substr_replace($timing->start, ':', 2, 0),
                    'closing' => substr_replace($timing->end, ':', 2, 0),
                ];
            }
        endif;
        return $timings;
    }

    /**
     * Categories
     * @param type $categories
     * @return type
     */
    public function categories($categories) {
        if (count($categories)):
            foreach ($categories as $category):
                $available = Category::where('name', 'like', '%' . $category->title . '%');
                if ($available->count()):
                    return $category->title;
                endif;
            endforeach;
        endif;
        return [0];
    }

    /**
     * Categories
     * @param type $tags
     * @return type
     */
    public function tags($tags) {
        if (count($tags)):
            foreach ($tags as $tag):
                return $tag->title;
            endforeach;
        endif;
        return [0];
    }

}

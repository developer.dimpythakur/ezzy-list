<div class="tags-row pt-2 pb-2 d-md-flex d-block text-center text-md-left">
    @if(count($post->tags))
    <div class="tags-cloud d-md-inline-block d-block"><span>Tags:</span>
        @foreach($post->tags as $tag)
        <a  href="javascript:void(0)" class="rounded-pill border border text-muted pt-1 pb-1 pl-2 pr-2 transition-1">
            {{ $tag->name }}
        </a>
        @endforeach
    </div>
    @endif
    <div class="social-share d-inline-block ml-auto position-relative">
        <i class="icon-share share-icon transition-1 bg-primary text-white rounded-pill pointer d-inline-block"
           aria-hidden="true"></i>
        <div class="social-icons transition-1 shadow p-2 rounded-pill position-absolute bg-white">
            <a target="_blank" href="{{ $post->getShareUrl('facebook') }}">
                <i class="lab la-facebook" aria-hidden="true"></i>
            </a>
            <a target="_blank" href="{{ $post->getShareUrl('google') }}">
                <i class="lab la-google" aria-hidden="true"></i>
            </a>
            <a target="_blank" href="{{ $post->getShareUrl('pinterest') }}">
                <i class="lab la-pinterest" aria-hidden="true"></i>
            </a>
            <a target="_blank" href="{{ $post->getShareUrl('instagram') }}">
                <i class="lab la-instagram" aria-hidden="true"></i>
            </a>
            <a target="_blank" href="{{ $post->getShareUrl('linkedin') }}">
                <i class="lab la-linkedin" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>

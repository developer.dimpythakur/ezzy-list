@if($categories && count($categories))
{{ Form::open(['route' => 'frontend.search.result', 'class' => 'form-listing','autocomplete'=>'off', 'method' => 'GET']) }}
<div class="input-wrap d-flex border rounded-pill p-1">
    <div class="near-field position-relative d-flex align-items-center">
        {{ Form::text('listing',null,['placeholder'=>"What You Are Looking For",'class'=>'form-control bg-transparent border-0 ml-2 pl-2 text-white']) }}
    </div>
    <div class="find-field position-relative">
        <select name="category" class="form-control form-control wide bg-transparent border-0">
            <option value="0" data-display="Any Category">Any Category</option>
            @foreach($categories as $id =>$category)
            <option value="{{ ($id)}}">{{$category}}</option>
            @endforeach
        </select>
    </div>
    @if(isset($regions) && count($regions))
    <div class="find-field position-relative">
        <select name="location" class="form-control form-control wide bg-transparent border-0">
            <option value="0" data-display="Any Location">Any Location</option>
            @foreach($regions as $region)
            <option value="{{$region->name}}">{{$region->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="submit-btn rounded-pill btn btn-primary border-0 transition-1 ml-auto position-relative pointer-event d-flex justify-content-center align-items-center">
        <i class="las la-search" aria-hidden="true"></i>
        <input type="submit" class="btn transition-1 field-btn position-absolute" value="">
    </div>
    @endif
</div>
{{ Form::close() }}
@endif

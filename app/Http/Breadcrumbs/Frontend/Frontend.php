<?php

Breadcrumbs::register('frontend.user.dashboard', function ($breadcrumbs) {
    $breadcrumbs->push(__('navs.frontend.dashboard'), route('frontend.user.dashboard'));
});
Breadcrumbs::register('frontend.user.listings.index', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('menus.backend.listings.management'), route('frontend.user.listings.index'));
});

Breadcrumbs::register('frontend.user.listings.create', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.listings.index');
    $breadcrumbs->push(trans('menus.backend.listings.create'), route('frontend.user.listings.index'));
});

Breadcrumbs::register('frontend.user.listings.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('frontend.user.listings.index');
    $breadcrumbs->push(trans('menus.backend.listings.edit'), route('frontend.user.listings.edit', $id));
});

Breadcrumbs::register('frontend.user.listings-active', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('menus.backend.listings.active'), route('frontend.user.listings-active'));
});

Breadcrumbs::register('frontend.user.listings-pending', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('menus.backend.listings.pending'), route('frontend.user.listings-pending'));
});
Breadcrumbs::register('frontend.user.listings-expired', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('menus.backend.listings.expired'), route('frontend.user.listings-expired'));
});
Breadcrumbs::register('frontend.user.reviews', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('menus.backend.listings.reviews'), route('frontend.user.reviews'));
});
Breadcrumbs::register('frontend.user.bookmark', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('menus.backend.listings.bookmark'), route('frontend.user.bookmark'));
});
Breadcrumbs::register('frontend.user.account', function ($breadcrumbs) {
    $breadcrumbs->parent('frontend.user.dashboard');
    $breadcrumbs->push(trans('menus.backend.access.users.account'), route('frontend.user.account'));
});

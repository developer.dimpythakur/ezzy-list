<div class="card">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-video-camera"></i> Video</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <!-- Video -->
        <div class="row mb-3">
            <div class="form-group col-sm-8">
                <input type="text" class="form-control" placeholder="Video URL" id="video_url">
            </div>
            <div class="form-group col-sm-4 text-left">
                <button type="button" class="add-video btn btn-secondary border-transparent border-0">
                    <span class="icon"><i class="la la-plus-square-o"></i></span>
                </button>
            </div>
        </div>
        <div class="row mb-3">
            <div class="selected-videos mt-4">
                <ul class="nostyle d-flex flex-wrap">
                    @if( !empty($listing) && !empty($listing->videos))
                    @foreach ($listing->videos as $video)
                    <li>
                        <iframe width="100%" scrolling="no" frameborder="no" allow="autoplay"
                                src="{{$video->url}}"></iframe>
                        <i class="p-1 remove shadow pointer btn btn-secondary border-0 las la-trash-alt"></i></li>
                    <input type="hidden" value="{{$video->url}}"
                           name="audio[{{$video->filename}}][{{$video->url}}]">
                    </li>
                    @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <!--/ Video -->
    </div>
</div>
@push('after_scripts')

<script>
    //-------------------------------
    // Add Video
    //-------------------------------
    jQuery(document).ready(function () {

        jQuery(document).on('click', ".add-video", function () {
            var $audioUrl = jQuery("input[id='video_url']");
            $audioUrl.removeClass('is-invalid');
            if ((typeof $audioUrl.val() === "undefined") || $audioUrl.val() === '') {
                $audioUrl.addClass('is-invalid');
                return false;

            }
            var ifrmaeSRC = '';
            var $src = $audioUrl.val();
            var ifrmaeSRC = `<iframe width="100%" 
                                 scrolling="no" frameborder="no"
                                  allow="autoplay"
                                   src="${$src}"></iframe>`;
            var $input = `<input type="hidden" value="${$audioUrl.val()}" name="video[custom_url][${$audioUrl.val()}]"/>`
            var $audioDOM = `<li>${$input} ${ifrmaeSRC}<i class="p-1 remove shadow pointer btn btn-primary border-0 las la-trash-alt"></i></li>`;

            jQuery(".selected-videos").show();
            jQuery(".selected-videos ul").append($audioDOM);
            $audioUrl.val("");
        });
        jQuery(document).on("click", ".selected-videos .remove", function (u) {
            jQuery(this).closest("li").remove();

        });
    });
</script>
@endpush
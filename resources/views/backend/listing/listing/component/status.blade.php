<div class="card">
    <div class="card-body listing-form bold-labels">
        <div class="row mb-3">
            <div class="form-group col-sm-12" element="div">
                <div><label>Status</label> </div>
                <div class="form-check form-check-inline">
                    <input type="radio" {{ (isset($listing)&& ($listing->status === 'publish') ? 'checked' : '' ) }} class="form-check-input" checked="" value="publish" name="status[]" id="status_publish">
                    <label class="radio-inline form-check-label font-weight-normal" for="status_publish">Publish</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" {{ (isset($listing)&& ($listing->status === 'review') ? 'checked' : '' ) }} class="form-check-input" value="review" name="status[]" id="status_review">
                    <label class="radio-inline form-check-label font-weight-normal" for="status_review">In Review</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" {{ (isset($listing)&& ($listing->status === 'pending') ? 'checked' : '' ) }} class="form-check-input" value="pending" name="status[]" id="status_pending">
                    <label class="radio-inline form-check-label font-weight-normal" for="status_pending">Pending</label>
                </div>
                <div class="form-check form-check-inline">
                    <input type="radio" {{ (isset($listing)&& ($listing->status === 'expired') ? 'checked' : '' ) }} class="form-check-input" value="expired" name="status[]" id="status_expired">
                    <label class="radio-inline form-check-label font-weight-normal" for="status_expired">Expired</label>
                </div>
            </div><!--form group-->
        </div>
    </div>
</div>
[
    {
        "view_permission_id": "view-listings",
        "icon": "la la-commenting",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "",
        "name": "Listings",
        "id": 135,
        "content": "Listings",
        "children": [
            {
                "view_permission_id": "view-listings",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.listings.index",
                "name": "Listings",
                "id": 136,
                "icon": "las la-braille",
                "content": "Listings"
            },
            {
                "view_permission_id": "view-listing-category",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.listing-categories.index",
                "name": "Categories",
                "id": 16,
                "icon": "la la-list",
                "content": "Categories"
            },
            {
                "view_permission_id": "view-listing-tags",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.listing-tags.index",
                "name": "Tags",
                "id": 169,
                "icon": "la la-tag",
                "content": "Tags"
            },
            {
                "view_permission_id": "view-listing-locations",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.listing-locations.index",
                "name": "Locations",
                "id": 138,
                "icon": "la lar la-map",
                "content": "Locations"
            },
            {
                "view_permission_id": "view-listing-regions",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.listing-regions.index",
                "name": "Regions",
                "id": 138,
                "icon": "la lar la-map",
                "content": "Regions"
            },
            {
                "view_permission_id": "view-listing-locations",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.yelp",
                "name": "Yelp",
                "id": 138,
                "icon": "la lar la-yelp",
                "content": "Yelp"
            }
        ]
    },
    {
        "view_permission_id": "view-blog",
        "icon": "la la-commenting",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "",
        "name": "Articles",
        "id": 15,
        "content": "Articles",
        "children": [
            {
                "view_permission_id": "view-blog",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.blogs.index",
                "name": "Articles",
                "id": 18,
                "icon": "la la-tag",
                "content": "Articles"
            },
            {
                "view_permission_id": "view-blog-category",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.blogCategories.index",
                "name": "Category",
                "id": 16,
                "icon": "la la-list",
                "content": "Category"
            },
            {
                "view_permission_id": "view-blog-tag",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.blogTags.index",
                "name": "Tag",
                "id": 17,
                "icon": "la la-tag",
                "content": "Tag "
            }

        ]
    },
    {
        "view_permission_id": "view-access-management",
        "icon": "la la-users",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "",
        "name": "Authentication",
        "id": 11,
        "content": "Authentication",
        "children": [
            {
                "view_permission_id": "view-user-management",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.access.user.index",
                "name": "Users",
                "id": 12,
                "icon": "la la-user",
                "content": "User"
            },
            {
                "view_permission_id": "view-role-management",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.access.role.index",
                "name": "Roles",
                "id": 13,
                "icon": "la la-group",
                "content": "Roles"
            },
            {
                "view_permission_id": "view-permission-management",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "admin.access.permission.index",
                "name": "Permission",
                "id": 14,
                "icon": "la la-key",
                "content": "Permission"
            }
        ]
    },

    {
        "view_permission_id": "view-page",
        "icon": "la la-file-text",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "admin.pages.index",
        "name": "Pages",
        "id": 2,
        "content": "Pages"
    },
    {
        "view_permission_id": "edit-settings",
        "icon": "la la-gear",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "admin.settings.edit?setting=1",
        "name": "Settings",
        "id": 9,
        "content": "Settings"
    },
    {
        "view_permission_id": "view-module",
        "icon": "la la-wrench",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "admin.modules.index",
        "name": "Module",
        "id": 1,
        "content": "Module"
    },
    {
        "view_permission_id": "view-menu",
        "icon": "la la-bars",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "admin.menus.index",
        "name": "Menus",
        "id": 3,
        "content": "Menus"
    },
    {
        "view_permission_id": "view-faq",
        "icon": "la la-question-circle",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "admin.faqs.index",
        "name": "Faq Management",
        "id": 19,
        "content": "Faq Management"
    }
]
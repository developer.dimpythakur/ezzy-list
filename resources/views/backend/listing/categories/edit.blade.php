@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.listing.categories.management') . ' | ' . trans('labels.backend.listing.categories.edit'))
@section('page-header')
<h1>{{ trans('labels.backend.listing.categories.edit') }}</h1>
@endsection

@section('content')
<section class="row">
    <div class="col-md-8 bold-labels">
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::model($listing_category, ['route' => ['admin.listing-categories.update',$listing_category], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-category','enctype' =>'multipart/form-data',]) }}
        <div class="card">
            <div class="card-body row">
                @include("backend.listing.categories.form")
            </div><!-- /.card-body -->
        </div><!--card-->

        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=update_and_back">Update</span>
                </button>
            </div>
            <a href="{{ route('admin.listing-categories.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>


@endsection
@section("after_scripts")

@endsection







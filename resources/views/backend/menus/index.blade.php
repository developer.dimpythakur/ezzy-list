@extends ('backend.layouts.dashboard')
@section ('title', trans('labels.backend.menus.management'))
@section('page-header')
<h1>{{ trans('labels.backend.menus.management') }}</h1>
@endsection
@section('header')
<div class="container-fluid">
    <h2>
        <span class="text-capitalize">{{ trans('labels.backend.menus.management') }}</span>
    </h2>
</div>
@endsection



@section('after_styles')
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css') }}">

@endsection


@section('content')
<!-- Default box -->
<div class="row">

    <!-- THE ACTUAL CONTENT -->
    <div class="col-12 mb-4">
        <div class="row mb-0">
            <div class="col-sm-6">
                <div class="hidden-print with-border">
                    <a href="{{route('admin.menus.create')}}" class="btn btn-primary" data-style="zoom-in">
                        <span class="ladda-label"><i class="la la-plus"></i> {{trans('menus.backend.menus.create')}}</span>
                    </a>
                </div>
            </div>
            <div class="col-sm-6">
                <div id="datatable_search_stack" class="mt-sm-0 mt-2"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="table-responsive data-table-wrapper">
        <table id="menus-table" 
               class="bg-white table table-striped table-hover nowrap rounded shadow-xs border-xs mt-2 dataTable dtr-inline">
            <thead>
                <tr>
                    <th>{{ trans('labels.backend.menus.table.name') }}</th>
                    <th>{{ trans('labels.backend.menus.table.type') }}</th>
                    <th>{{ trans('labels.backend.menus.table.createdat') }}</th>
                    <th>{{ trans('labels.general.actions') }}</th>
                </tr>
            </thead>
            <thead class="transparent-bg">
            </thead>
        </table>
    </div><!--table-responsive-->
</div>
@endsection

@section('after_scripts')
<!-- DATA TABLES SCRIPT -->
<script type="text/javascript" src="{{ asset('packages/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('packages/datatables.net-fixedheader-bs4/js/fixedHeader.bootstrap4.min.js') }}"></script>

<script>
$(function() {

$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
var dataTable = $('#menus-table').dataTable({
processing: true,
        serverSide: true,
        ajax: {
        url: '{{ route("admin.menus.get") }}',
                type: 'post'
        },
        columns: [
        {data: 'name', name: '{{config('access.menus_table')}}.name'},
        {data: 'type', name: '{{config('access.menus_table')}}.type'},
        {data: 'created_at', name: '{{config('access.menus_table')}}.created_at'},
        {data: 'actions', name: 'actions', searchable: false, sortable: false}
        ],
        order: [[3, "asc"]],
        searchDelay: 500,
        dom: "<'row hidden'<'col-sm-6 hidden-xs'i><'col-sm-6 hidden-print'f>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row mt-2 '<'col-sm-6 col-md-4'l><'col-sm-2 col-md-4 text-center'B><'col-sm-6 col-md-4 hidden-print'p>>",
        buttons: {
        buttons: [
        { extend: 'copy', className: 'copyButton', exportOptions: {columns: [ 0, 1, 2, 3, 4 ]  }},
        { extend: 'csv', className: 'csvButton', exportOptions: {columns: [ 0, 1, 2, 3, 4 ]  }},
        { extend: 'excel', className: 'excelButton', exportOptions: {columns: [ 0, 1, 2, 3, 4 ]  }},
        { extend: 'pdf', className: 'pdfButton', exportOptions: {columns: [ 0, 1, 2, 3, 4 ]  }},
        { extend: 'print', className: 'printButton', exportOptions: {columns: [ 0, 1, 2, 3, 4 ]  }}
        ]
        },
        language: {
        @lang('datatable.strings')
        }
});
Backend.DataTableSearch.init(dataTable);
});
</script>


@endsection

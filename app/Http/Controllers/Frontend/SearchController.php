<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Listing\Listing\Listing;
use App\Models\Listing\Categories\Category;
use App\Models\Listing\Region\Region;
use Illuminate\Http\Request;
use Spatie\Searchable\Search;
use App\Http\Controllers\Controller;
use Prologue\Alerts\Facades\Alert;

class SearchController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('search');
    }

    /**
     * Search results
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function search(Request $request) {
        $title = 'Search local business';
        $listing = $request->search_keywords;
        $searchTermCategory = str_replace('0', '', $request->category);
        $location = str_replace('0', '', $request->location);
        #search array
        $searchterm = [
            'search_keywords' => $listing,
            'category' => $searchTermCategory,
            'location' => $location
        ];
        if (!array_filter($searchterm)):
            $searchResults = Listing::inRandomOrder()->where('status', 'publish')->limit(5)->paginate(10);
        else:
            $searchResults = $this->filter($request);
        endif;
        //categories
        $categories = Category::withCount('listing')->orderBy('listing_count', 'desc')->limit(6)->get();
        $regions = Region::withCount('locations')->orderBy('locations_count', 'desc')->limit(6)->get();
        $searchResults->appends($request->all())->links();
        return view('frontend.listing.listing-search.index-map',
                compact(
                        'title',
                        'searchResults',
                        'listing',
                        'location',
                        'searchTermCategory',
                        'categories',
                        'regions',
                        'searchterm'
        ));
    }

    /**
     * search records in database and display  results
     * @param Request $request [description]
     * @return view      [description]
     */
    public function filter(Request $request) {

        $listings = new Listing;
        $listing = $listings->newQuery();
        // Search for a Listing based on their name.
        if ($request->has('search_keywords')) {
            $listing->where('name', 'LIKE', '%' . $request->search_keywords . '%');
        }
        if ($request->has('category') && $request->category !== '0') {
            $listing->whereHas('categories', function ($listing) use ($request) {
                $listing->where('name', 'like', '%' . $request->category . '%');
            });
        }
        if ($request->has('location') && $request->location !== '0') {
            $region = Region::where('name', $request->location)->first();
            if ($region) {
                $listing->whereHas('location', function ($listing) use ($request, $region) {
                    $listing->where('region_id', 'like', '%' . $region->id . '%');
                });
            }
        }
        return $listing->paginate(8);
    }

}

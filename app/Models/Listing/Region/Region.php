<?php

namespace App\Models\Listing\Region;

use App\Models\ModelTrait;
use App\Models\Listing\Region\Traits\RegionAttribute;
use App\Models\Listing\Region\Traits\RegionRelationship;
//Slugable
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
///base model
use App\Models\BaseModel;

class Region extends BaseModel {

    use Sluggable;
    use SluggableScopeHelpers;
    use ModelTrait,
        RegionAttribute,
        RegionRelationship {
        // RegionAttribute::getEditButtonAttribute insteadof ModelTrait;
    }

    /**
     * NOTE : If you want to implement Soft Deletes in this model,
     * then follow the steps here : https://laravel.com/docs/6.x/eloquent#soft-deleting
     */

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table;

    /**
     * Default values for model fields
     * @var array
     */
    protected $attributes = [
    ];

    /**
     * Dates
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Guarded fields of model
     * @var array
     */
    protected $guarded = [];
    protected $with = ['locations'];

//    protected $withCount = ['locations'];

    /**
     * Constructor of Model
     * @param array $attributes
     */
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.ezzy_regions.table');
    }

    /**
     * Listing Locations
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getMapTable() {
        return config('module.listing_locations.table');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Listing
     * @return mixed
     */
    public function listingCountAttribute($value) {
        return $value;
    }

}

<?php

namespace App\Http\Responses\Backend\Listing\Regions;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable {

    /**
     * @var App\Models\Listing\Regions\Region $listing_region
     */
    protected $listing_region;

    /**
     * @param App\Models\Listing\Regions\Region $listing_region
     */
    public function __construct($listing_region) {
        $this->listing_region = $listing_region;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request) {
        return view('backend.listing.regions.edit')->with([
                    'listing_region' => $this->listing_region
        ]);
    }

}

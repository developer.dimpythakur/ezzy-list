<?php

namespace App\Http\Responses\Backend\Listing;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable {

    /**
     * @var
     */
    protected $with = [];
    protected $listing;

//    public function __construct($listing, $with) {
//        $this->listing = $listing;
    public function __construct($with) {
        $this->with = $with;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request) {
        return view('backend.listing.listing.edit')->with($this->with);
    }

}

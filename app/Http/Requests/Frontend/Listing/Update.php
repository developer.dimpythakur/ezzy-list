<?php

namespace App\Http\Requests\Frontend\Listing;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return access()->allow('update-listing');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|max:191',
            'banner' => 'required',
            'categories' => 'required',
            'description' => 'required',
        ];
    }

    /**
     * Get the validation message that apply to the request.
     *
     * @return array
     */
    public function messages() {
        return [
            'name.required' => 'Please insert Title',
            'name.max' => 'Title may not be greater than 191 characters.',
        ];
    }

}

@if (! $logged_in_user)
<div class="card card-border-radius customer-reviews shadow mb-4 p-4 border-0">
    <div class="w-100 text-center">
        <a href="{{ route('frontend.auth.login') }}"
           target="_self" class="text-center btn btn-primary border-0 m-auto">
            <i class="las la-star-o"></i> <span class="">Add a review</span>  <!----></a>
    </div>
</div>
@else
<div class="card card-border-radius customer-reviews shadow mb-4 p-4 border-0">
    <h5 class="card-title mb-4">Add review</h5>
    <div class="post-comment-form">

        {{ Form::model($logged_in_user, ['route' => 'frontend.add-review',  'autocomplete'=> 'off', 'class' => 'form-horizontal', 'method' => 'POST']) }}
        {{ Form::hidden('listing_id',$listing->id) }}
        <div class="row">
            <div class="form-group col-md-12 col-sm-12">
                <input type="text" class="form-control rounded-pill" placeholder="Title" name="title">
            </div>
            <div class="form-group col-sm-12">
                <textarea rows="6" class="form-control resize-none" placeholder="Type your comment"
                          name="body"></textarea>
            </div>
        </div>
        <!-- Add rating -->
        @include('frontend.listing.components.listing-single.rating-bar')

        <!-- /Add rating -->
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-primary border-0 transition-1 text-capitalize shadow"
                   value="Submit Review" name="submit">
        </div>
        {{ Form::close() }}
    </div>
</div>
@endif
<?php

return array(
    array('id' => '1', 'name' => 'New York', 'slug' => 'new-york', 'city' => 'NY', 'state' => NULL, 'region' => 'NY', 'country' => 'USA', 'image' => '1597740823new-york.jpg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-08 01:13:01', 'updated_at' => '2020-08-18 08:53:43', 'deleted_at' => NULL),
    array('id' => '2', 'name' => 'Alabama', 'slug' => 'alabama', 'city' => 'AL', 'state' => NULL, 'region' => 'AL', 'country' => 'USA', 'image' => '1597740782alabama.jpg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-08 01:13:15', 'updated_at' => '2020-08-18 08:53:02', 'deleted_at' => NULL),
    array('id' => '3', 'name' => 'Alaska', 'slug' => 'alaska', 'city' => 'AK', 'state' => NULL, 'region' => 'AK', 'country' => 'USA', 'image' => '1597740770alaska.jpg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-08 01:13:56', 'updated_at' => '2020-08-18 08:52:50', 'deleted_at' => NULL),
    array('id' => '4', 'name' => 'Arizona', 'slug' => 'arizona', 'city' => 'AZ', 'state' => NULL, 'region' => 'AZ', 'country' => 'USA', 'image' => '1597740642arizona.jpeg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-08 01:14:26', 'updated_at' => '2020-08-18 08:50:42', 'deleted_at' => NULL),
    array('id' => '5', 'name' => 'California', 'slug' => 'california', 'city' => 'CA', 'state' => NULL, 'region' => 'CA', 'country' => 'USA', 'image' => '1597740625califronia.jpg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-09 04:32:34', 'updated_at' => '2020-08-18 08:50:25', 'deleted_at' => NULL),
    array('id' => '6', 'name' => 'Florida', 'slug' => 'florida', 'city' => 'FL', 'state' => NULL, 'region' => 'FL', 'country' => 'USA', 'image' => '1597740612florida.jpg', 'created_by' => '1', 'updated_by' => '1', 'created_at' => '2020-08-09 04:33:32', 'updated_at' => '2020-08-18 08:50:12', 'deleted_at' => NULL)
);

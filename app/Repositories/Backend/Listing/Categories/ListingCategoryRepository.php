<?php

namespace App\Repositories\Backend\ListingCategories;

use DB;
use Carbon\Carbon;
use App\Models\ListingCategories\ListingCategory;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ListingCategoryRepository.
 */
class ListingCategoryRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = ListingCategory::class;

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable()
    {
        return $this->query()
            ->select([
                config('module.listingcategories.table').'.id',
                config('module.listingcategories.table').'.created_at',
                config('module.listingcategories.table').'.updated_at',
            ]);
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @throws GeneralException
     * @return bool
     */
    public function create(array $input)
    {
        if (ListingCategory::create($input)) {
            return true;
        }
        throw new GeneralException(trans('exceptions.backend.listingcategories.create_error'));
    }

    /**
     * For updating the respective Model in storage
     *
     * @param ListingCategory $listingcategory
     * @param  $input
     * @throws GeneralException
     * return bool
     */
    public function update(ListingCategory $listingcategory, array $input)
    {
    	if ($listingcategory->update($input))
            return true;

        throw new GeneralException(trans('exceptions.backend.listingcategories.update_error'));
    }

    /**
     * For deleting the respective model from storage
     *
     * @param ListingCategory $listingcategory
     * @throws GeneralException
     * @return bool
     */
    public function delete(ListingCategory $listingcategory)
    {
        if ($listingcategory->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.listingcategories.delete_error'));
    }
}

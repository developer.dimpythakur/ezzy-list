<?php

use Carbon\Carbon;
use Database\DisableForeignKeys;
use Database\TruncateTable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\BlogCategories\BlogCategory;
use App\Models\BlogTags\BlogTag;
use App\Models\Blogs\Blog;
use App\Repositories\Backend\Blogs\BlogsRepository;
use Illuminate\Support\Str;

class BlogTableSeeder extends Seeder {

    use DisableForeignKeys,
        TruncateTable;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function runBefore() {
        if (BlogCategory::count() <= 0) {
            DB::table('blog_categories')->insert(config('listing.blog_categories'));
        }
        if (BlogTag::count() <= 0) {
            DB::table('blog_tags')->insert(config('listing.blog_tags'));
        }
    }

    public function run() {
        $this->runBefore();
        $this->updateFeeds(config('listing.blogs'));
    }

    /**
     * Update blog feeds
     * @param type $urls
     */
    public function updateFeeds($urls) {
        $blogRepo = new BlogsRepository;
        if (count($urls)):
            foreach ($urls as $key => $feedUrl):
                $rss = Feed::loadRss($feedUrl);
                foreach ($rss->item as $ky => $item) {
                    if ($item->children('media', True)->content):
                        $image = $item->children('media', True)->content->attributes();
                    else:
                        $image = ($item->enclosure['url']) ? (string) $item->enclosure['url'] : 'default-image.jpg';
                    endif;
                    $title = ($item->title) ? (string) ($item->title) : '';
                    $link = ($item->link) ? (string) ($item->link) : '';
                    $blogRepo->make([
                        'name' => $title,
                        'blog' => (string) $rss->title,
                        'categories' => categories(),
                        'type' => 'feed',
                        'url' => $link,
                        'publish_datetime' => (string) $item->pubDate,
                        'content' => (string) $item->description,
                        'tags' => tags(),
                        'meta_title' => $title,
                        'slug' => Str::slug($title),
                        'cannonical_link' => Str::slug($title),
                        'meta_keywords' => $title,
                        'meta_description' => $title,
                        'featured_image' => (string) $image,
                        'status' => 'Published'
                    ]);
                }
            endforeach;
        endif;
    }

}

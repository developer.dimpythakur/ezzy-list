<?php

namespace App\Repositories\Frontend\Listing;

use App\Exceptions\GeneralException;
use App\Models\Listing\Listing\Listing;
use App\Repositories\BaseRepository;

/**
 * Class ListingsRepository.
 */
class ListingRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Listing::class;

    /*
     * Find page by page_slug
     */

    public function findBySlug($listing_slug) {
        if (!is_null($this->query()->whereSlug($listing_slug)->firstOrFail())) {
            return $this->query()->whereSlug($listing_slug)->firstOrFail();
        }
        throw new GeneralException(trans('exceptions.backend.access.pages.not_found'));
    }

    /**
     * Related Posts
     * @param $listing
     * @return mixed
     */
    public function related($listing) {
        return MODEL::with('categories')->where('id', '!=', $listing->id)->get();
    }

    /**
     * Get by Slug
     * @param $listing_slug
     */
    public function getIDBySlug($listing_slug) {
        $listing = MODEL::where('slug', $listing_slug)->get();
    }

    public function getCategories() {
        return Category::getSelectData();
    }

}

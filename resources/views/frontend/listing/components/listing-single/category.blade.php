<div class="card card-border-radius sidebar-widget category shadow mb-5 p-4 border-0">
    <h5 class="card-title mb-4">Category</h5>
    <div class="card d-block text-center card-border-radius p-4 single-listing-category-wrapper">
        @if($listing)
            @foreach($listing->categories as $category)
                @if($category->icon && $category->icon !='null' && categoryCount($category->id))
                    <a class="single-listing-category" href="{{ route('frontend.listing-category',$category->slug) }}">
                    <span class="rounded-pill category align-items-center justify-content-center bg-primary text-white text-center cat-circle"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="{{ $category->name }}"
                          data-original-title="{{ $category->name }}">
                        <i class="{{ $category->icon }}" aria-hidden="true"></i>
				</span>
                    </a>

                @else
                    <span class="rounded-pill category align-items-center justify-content-center bg-primary text-white text-center cat-circle"
                          data-toggle="tooltip"
                          data-placement="top"
                          title="Category"
                          data-original-title="{{ $category->name }}">
                    <i class="las la-shopping-cart"></i>
                    </span>
                @endif
            @endforeach
        @else
            <span class="rounded-pill align-items-center justify-content-center bg-primary text-white text-center cat-circle"
                  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Automotive">
										<i class="fa fa-car" aria-hidden="true"></i>
										</span>
            <span class="rounded-pill align-items-center justify-content-center bg-primary text-white text-center cat-circle"
                  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Restaurants">
										<i class="icon-coffee-cup"></i>
										</span>
        @endif
    </div>
</div>
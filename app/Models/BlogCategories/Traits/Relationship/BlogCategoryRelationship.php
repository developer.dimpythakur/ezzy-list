<?php

namespace App\Models\BlogCategories\Traits\Relationship;

use App\Models\Access\User\User;
use App\Models\Blogs\Blog;

/**
 * Class BlogCategoryRelationship.
 */
trait BlogCategoryRelationship {

    /**
     * BlogCategories belongs to relationship with state.
     */
    public function creator() {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * BlogCategories belongs to relationship with state.
     */
    public function blog() {
        return $this->belongsTo(Blog::class);
    }

}

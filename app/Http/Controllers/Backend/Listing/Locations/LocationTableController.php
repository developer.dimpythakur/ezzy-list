<?php

namespace App\Http\Controllers\Backend\Listing\Locations;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Listing\Locations\LocationRepository;
use App\Http\Requests\Backend\Listing\Locations\Manage;

/**
 * Class LocationsTableController.
 */
class LocationTableController extends Controller {

    /**
     * variable to store the repository object
     * @var LocationRepository
     */
    protected $location;

    /**
     * contructor to initialize repository object
     * @param LocationRepository $location;
     */
    public function __construct(LocationRepository $location) {
        $this->location = $location;
    }

    /**
     * This method return the data of the model
     * @param ManageLocationRequest $request
     *
     * @return mixed
     */
    public function __invoke(Manage $request) {
//        return Datatables::of($this->location->getForDataTable())
//                        ->escapeColumns(['id'])
//                        ->addColumn('created_at', function ($location) {
//                            return Carbon::parse($location->created_at)->toDateString();
//                        })
//                        ->addColumn('actions', function ($location) {
//                            return $location->action_buttons;
//                        })
//                        ->make(true);

        $query = $this->location->getForDataTable();
        return Datatables::of($query)
                        ->escapeColumns(['id'])
                        ->addColumn('name', function ($location) {
                            return $location->name;
                        })
                        ->addColumn('created_at', function ($location) {
                            return Carbon::parse($location->created_at)->toDateString();
                        })
//            ->addColumn('region_id', function ($location) {
//                $region = Region::where('id', $location->region_id)->first();
//                if ($region):
//                    return $region->name;
//                else:
//                    return 'Ezzy';
//                endif;
//            })
                        ->addColumn('listings', function ($location) {
                            if ($location->locations->count()):
                                return $location->locations->count();
                            else:
                                return 0;
                            endif;
                        })
                        ->addColumn('actions', function ($location) {
                            return $location->action_buttons;
                        })
                        ->make(true);
    }

}

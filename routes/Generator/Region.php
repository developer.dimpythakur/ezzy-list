<?php

/**
 * Region
 *
 */
Route::group(['namespace' => 'Backend\Listing', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {

    Route::group(['namespace' => 'Regions'], function () {
        Route::resource('listing-regions', 'RegionController');
        //For Datatable
        Route::post('listing-regions/get', 'RegionTableController')->name('regions.get');
    });
});

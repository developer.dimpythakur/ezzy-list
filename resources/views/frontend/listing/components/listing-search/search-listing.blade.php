<?php
$cat = [];
if ($categories && count($categories)):
    foreach ($categories as $category):
        $cat[$category['id']] = $category['name'];
    endforeach;
endif;
$searchText = isset($listing) ? $listing : '';
?>
{{ Form::open(['route' => 'frontend.search.result', 'class' => 'home-search form-horizontal','autocomplete'=>'off', 'method' => 'GET']) }}
<div class="col-lg-12 mb-3">
    <button type="button" id="show-map-button"
            class="btn btn-outline-primary w-100 mx-auto transition-1 my-2 position-relative pointer-event d-flex justify-content-center align-items-center" data-enabled="Show Map " data-disabled="Hide Map ">
        <i aria-hidden="true" class="las la-map-marked la-2x"></i> 
        <span>Show Map</span>
    </button>
</div>


<!--<div class="input-wrap d-flex border rounded-pill p-1">-->
<div class="input-wrap d-flex border rounded p-2 search-box-shadow">
    <div class="near-field position-relative d-flex align-items-center">
        {{ Form::text('search_keywords',$searchText,['placeholder'=>"What You Are Looking For",'class'=>'form-control bg-transparent border-0 ml-2 pl-2 text-white']) }}
    </div>
    <div class="find-field position-relative">
        @if($categories && count($categories))
        @if($categories)
        <select name="category" id="search_categories" class="form-control form-control wide bg-transparent border-0">
            <option value="0" data-display="Any Category">Any Category</option>
            @foreach($categories as $category)
            <option {{ (isset($searchTermCategory) && ($searchTermCategory) ===strtolower($category->name))?'selected':'' }} 
                value="{{ strtolower($category->name)}}">{{$category->name}}
            </option>
            @endforeach
        </select>
        @endif
        @endif
    </div>
    <div class="find-field position-relative">
        <select name="location" class="form-control form-control wide bg-transparent border-0">
            @if($regions)
            <option value="0" data-display="Any Location">Any Location</option>
            @foreach($regions as $region)
            <option {{ (isset($location) && ($location) === strtolower($region->name))?'selected':'' }} 
                value="{{strtolower($region->name)}}">
                {{$region->name}} </option>
            @endforeach
            @endif
        </select>
    </div>
    <div class="submit-btn rounded-pill btn btn-primary border-0 transition-1 ml-auto position-relative pointer-event d-flex justify-content-center align-items-center">
        <i class="las la-search" aria-hidden="true"></i>
        <input type="submit" class="w-100 d-block btn transition-1 field-btn position-absolute" value="">
    </div>
</div>
{{ Form::close() }}

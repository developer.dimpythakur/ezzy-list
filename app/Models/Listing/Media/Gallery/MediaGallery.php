<?php

namespace App\Models\Listing\Media\Gallery;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use App\Models\Listing\Media\Gallery\Traits\Attribute\Attribute;
use App\Models\Listing\Media\Gallery\Traits\Relationship;
use Illuminate\Database\Eloquent\SoftDeletes;

class MediaGallery extends BaseModel {

    use ModelTrait,
        SoftDeletes,
        Relationship,
        Attribute;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'created_by' => 1,
    ];
    protected $fillable = [
        'filename',
        'type',
        'media_type',
        'media_count',
        'path',
        'url',
        'status',
        'created_by'
    ];

    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.ezzy_media_gallery.table');
    }

    /**
     * Return ListingMedia map Table
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function getMapTable() {
        return config('module.listing_media_gallery.table');
    }

}

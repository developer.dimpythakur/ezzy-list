/**
 * Star Output
 * @param firstStar
 * @param secondStar
 * @param thirdStar
 * @param fourthStar
 * @param fifthStar
 * @returns {string}
 */
function outPutStar(firstStar, secondStar, thirdStar, fourthStar, fifthStar) {
    return ('' +
            '<span class="las ' + firstStar + '"></span>' +
            '<span class="las ' + secondStar + '"></span>' +
            '<span class="las ' + thirdStar + '"></span>' +
            '<span class="las ' + fourthStar + '"></span>' +
            '<span class="las ' + fifthStar + '"></span>');
}

function starRating(ratingElem) {
    $(ratingElem).each(function () {
        var dataRating = $(this).attr('data-rating');


        if (jQuery(this).attr('data-class')) {
            var classReview = jQuery(this).attr('data-class');
        }
        var fiveStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star', 'la-star');
        var fourHalfStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star', 'la-star half');
        var fourStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star', 'la-star empty');
        var threeHalfStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star half', 'la-star empty');
        var threeStars = outPutStar('la-star', 'la-star', 'la-star', 'la-star empty', 'la-star empty');
        var twoHalfStars = outPutStar('la-star', 'la-star', 'la-star half', 'la-star empty', 'la-star empty');
        var twoStars = outPutStar('la-star', 'la-star', 'la-star empty', 'la-star empty', 'la-star empty');
        var oneHalfStar = outPutStar('la-star', 'la-star half', 'la-star empty', 'la-star empty', 'la-star empty');
        var oneStar = outPutStar('la-star', 'la-star empty', 'la-star empty', 'la-star empty', 'la-star empty');
        var empty = outPutStar('la-star empty', 'la-star empty', 'la-star empty', 'la-star empty', 'la-star empty');

        if (!dataRating) {
            $(this).append(empty);
            return;
        }

        if (dataRating >= 4.75) {
            $(this).append(fiveStars);
        } else if (dataRating >= 4.25) {
            $(this).append(fourHalfStars);
        } else if (dataRating >= 3.75) {
            $(this).append(fourStars);
        } else if (dataRating >= 3.25) {
            $(this).append(threeHalfStars);
        } else if (dataRating >= 2.75) {
            $(this).append(threeStars);
        } else if (dataRating >= 2.25) {
            $(this).append(twoHalfStars);
        } else if (dataRating >= 1.75) {
            $(this).append(twoStars);
        } else if (dataRating >= 1.25) {
            $(this).append(oneHalfStar);
        } else if (dataRating < 1.25) {
            $(this).append(oneStar);
        } else {
            $(this).append(empty);
        }

    });
}

jQuery(document).ready(function () {
    /*----------------------------------------------------*/
    /*  Ratings Script
     /*----------------------------------------------------*/
    starRating('.star-rating');
});



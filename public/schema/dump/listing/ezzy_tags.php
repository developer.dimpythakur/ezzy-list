<?php

return array(
    array('id' => '1', 'name' => 'Restaurants', 'slug' => 'restaurants', 'description' => NULL, 'status' => '1', 'created_by' => NULL, 'updated_by' => NULL, 'created_at' => '2020-08-09 04:47:07', 'updated_at' => '2020-08-09 04:47:07', 'deleted_at' => NULL),
    array('id' => '2', 'name' => 'Good Food', 'slug' => 'good-food', 'description' => NULL, 'status' => '1', 'created_by' => NULL, 'updated_by' => NULL, 'created_at' => '2020-08-09 04:47:56', 'updated_at' => '2020-08-09 04:47:56', 'deleted_at' => NULL),
    array('id' => '3', 'name' => 'Best Hotels', 'slug' => 'best-hotels', 'description' => NULL, 'status' => '1', 'created_by' => NULL, 'updated_by' => NULL, 'created_at' => '2020-08-09 04:48:14', 'updated_at' => '2020-08-09 04:48:14', 'deleted_at' => NULL)
);

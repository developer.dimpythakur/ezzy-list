<div class="card card-border-radius services-offered shadow mb-5 p-4 border-0">
    <h5 class="card-title mb-4">Additional Details</h5>
    <p class="mb-0">
        <i class="las la-wheelchair"></i>&nbsp; Wheet-chair Access
        <span class="las la-check-circle float-right text-danger"></span>
    </p>
    <hr>
    <p class="mb-0">
        <i class="las la-smoking"></i> Smoking Allowed
        <span class="las la-times-circle float-right text-success"></span>
    </p>
    <hr>
</div>
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEzzyUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ezzy_users';

    /**
     * Run the migrations.
     * @table ezzy_users
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 191);
            $table->string('last_name', 191);
            $table->string('email', 191);
            $table->string('password', 191)->nullable()->default(null);
            $table->tinyInteger('status')->default('1');
            $table->string('confirmation_code', 191)->nullable()->default(null);
            $table->tinyInteger('confirmed')->default('0');
            $table->tinyInteger('is_term_accept')->default('0')->comment(' 0 = not accepted,1 = accepted');
            $table->rememberToken();
            $table->integer('created_by')->nullable()->default(null);
            $table->unsignedInteger('updated_by')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}

<div class="et-nav-menu">
    <div class="container">
        <div class="d-flex pt-md-0 pt-3 pb-md-0 pb-3 position-relative flex-nowrap align-items-center">
            <!-- Mobile Menu -->
            <button id="nav-toggle" class="hc-nav-trigger hc-nav-1">
                <i class="las la-bars"></i>
            </button>
            <nav id="mobile-nav" class="hc-nav hc-nav-1">
                <ul class="text-capitalize">
                    @php renderMenuItems(getMenuItems('frontend'), 'frontend.includes.snippet.site.nav-menus') @endphp
                    @if (! $logged_in_user)
                    <li class="et-login-btn pb-3">
                        <a class="btn bg-primary transition-1 border-0 text-capitalize text-white"
                           href="{{  route('frontend.user.listings.create')}}">
                            <i class="la la-plus"></i>
                            Add Listing</a>
                    </li>
                    <li class="et-add-list-btn pb-3">
                        <a href="javascript:void(0)" class="et-add-listing-btn bg-primary btn transition-1 border-0 text-capitalize text-white" data-toggle="modal" data-target="#loginSignup"> Login/Signup</a>
                    </li>
                    @else
                    <li class="nav-item p-0">
                        <div class="dropdown-menu position-absolute nav-dark" aria-labelledby="userProfileDropdown">
                            <div class="">
                                <div class="dropdown-item">
                                    <a class="" href="{{route('frontend.user.account')}}">
                                        <i class="las la-user-circle"></i>
                                        My Profile
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <a class="" href="{{route('frontend.user.listings.index')}}">
                                        <i class="las la-list-alt"></i>
                                        My Listings
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <a class="" href="{{route('frontend.user.listings.create')}}">
                                        <i class="las la-plus-circle"></i>
                                        Add Listings
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <a class="" href="{{route('frontend.auth.logout')}}">
                                        <i class="las la-sign-out-alt" aria-hidden="true"></i>
                                        Sign Out
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endif
                </ul>
            </nav>
            <!-- /Mobile Menu -->
            <!-- Navigation menu -->
            <div class="nav-section align-items-center d-none d-md-block d-lg-flex">
                <nav class="menu-items header-menu-items">
                    <ul class="nostyle">
                        @php renderMenuItems(getMenuItems('frontend'), 'frontend.includes.snippet.site.nav-menus') @endphp
                    </ul>
                </nav>
            </div>
            <!-- /Navigation menu -->
            <!-- Logo -->
            <div class="header-logo m-auto">
                <!--   Show this only lg screens and up   -->
                @if(isset($setting->logo))
                <a href="{{ route('frontend.index') }}" class="logo d-lg-block">
                    <img class="navbar-brand"
                         src="{{ Storage::url('images/logo/').settings()->logo}}">
                </a>
                @else
                <a href="{{ route('frontend.index') }}" class="logo d-lg-block">
                    <img  class="navbar-brand"
                          src="{{ Storage::url('/images/frontend/logo.png')}}">
                </a>
                @endif

            </div>
            <!-- /Logo -->
            <!-- Action buttons -->
            <div class="action-buttons d-md-block d-none text-right ml-auto">
                @if (! $logged_in_user)
                <a href="#" class="text-white ml-3" data-toggle="modal" data-target="#loginSignup"><i class="icon-block"></i> Login/Signup</a>
                <a class="btn btn-primary ml-3 border-0 transition-1 text-capitalize text-white" 
                   href="{{  route('frontend.user.listings.create')}}">
                    <i class="la la-plus"></i> Add Listing
                </a>
                @else
                <ul class="navbar-nav">
                    <li class="nav-item dropdown user-profile-dropdown">
                        <a href="javascript:void(0);" class="nav-link dropdown-toggle user mt-2"
                           id="userProfileDropdown"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <img src="{{ (access()->user()->picture) }}" alt="avatar">
                        </a>
                        <div class="dropdown-menu position-absolute nav-dark" aria-labelledby="userProfileDropdown">
                            <div class="">
                                <div class="dropdown-item">
                                    <a class="" href="{{route('frontend.user.dashboard')}}">
                                        <i class="las la-home"></i>
                                        Dashboard
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <a class="" href="{{route('frontend.user.account')}}">
                                        <i class="las la-user-circle"></i>
                                        My Profile
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <a class="" href="{{route('frontend.user.listings.index')}}">
                                        <i class="las la-list-alt"></i>
                                        My Listings
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <a class="" href="{{route('frontend.auth.logout')}}">
                                        <i class="las la-sign-out-alt" aria-hidden="true"></i>
                                        Sign Out
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                @endif
            </div>
            <!-- /Action buttons -->
        </div>
    </div>
</div>
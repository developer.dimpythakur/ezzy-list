@if(!empty($item->children)) 
<li class="nav-title">{{ $item->name }}</li>
<li data-name="{{ $item->name }}" class="nav-item @if(!empty($item->children)) {{ 'nav-dropdown' }} @endif ">                    <!-- <li class="divider"></li> -->
@else
<li data-name="{{ $item->name }}" class="nav-item @if(!empty($item->children)) {{ 'nav-dropdown' }} @endif ">
@endif
<a  @if (!empty($item->children)) 
    class="nav-link nav-dropdown-toggle" 
    @else
    class="nav-link" 
    @endif
    href="{{ getRouteUrl($item->url, $item->url_type) }}" 
    @if(!empty($item->open_in_new_tab) && ($item->open_in_new_tab == 1)) 
    {{ 'target="_blank"' }} @endif>
    <i class="{{ @$item->icon }}"></i> {{ $item->name }}
</a>
@if (!empty($item->children))
<ul class="nav-dropdown-items {{ active_class(isActiveMenuItem($item), '') }}">
    {{ renderMenuItems($item->children) }}
</ul>
@endif
</li>
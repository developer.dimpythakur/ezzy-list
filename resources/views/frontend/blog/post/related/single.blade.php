<!-- Single Post -->
<a href="{{ URL::to('post').'/'.$post->slug }}"
   class="single-post d-block overflow-hidden shadow card-border-radius img-overlay position-relative transition-1">
    @if(($post->featured_image) && ($post->featured_image) != 'default-image.jpg' && $post->type =='feed')
    <img src="{{ $post->featured_image }}"
         alt="{{ $post->name }}"
         class="img-fluid rounded-top">
    @else
    <img src="{{ Storage::url('images/blog/' . $post->featured_image) }}" alt="{{ $post->name }}"
         class="img-fluid rounded-top">
    @endif
    <div class="post-content position-absolute rounded-pill fixed-top h-100">

        @if(!empty($post->categories))
        <span class="badge badge-primary mt-2 ml-5 position-absolute sticky-top p-1">
                     {{ $post->blog }}
        </span>
        @endif
        <div class="fixed-bottom p-3 position-absolute text-white bottom-content transition-1">
            <p class="mb-2 text-white">
                {{\Carbon\Carbon::parse($post->publish_datetime)->format('M, d, Y ')}}
                By {{ $post->owner->first_name}}
            </p>
            @if(!empty($post->categories))
            @php $limit=1 @endphp
            <h6 class="text-white">
                {{ $post->name}}
            </h6>
            @endif
            <p class="description transition-1 mb-0 text-white">{{ strip_tags(substr($post->content,0,100)) }}.</p>
        </div>
    </div>
</a>
<!-- Single Post -->
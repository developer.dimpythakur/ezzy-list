<?php

/**
 * Listings
 *
 */
Route::group(['namespace' => 'Backend\Listing', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {

    Route::group(['namespace' => 'Yelp'], function () {
        Route::get('yelp', 'YelpController@index')->name('yelp');
        Route::post('yelp/search', 'YelpController@searchBusiness')->name('yelp-search');
        Route::post('yelp/get', 'YelpController@business')->name('yelp.get');
    });
});

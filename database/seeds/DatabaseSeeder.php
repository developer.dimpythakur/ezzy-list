<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * Class DatabaseSeeder.
 */
class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();
//        $this->call(AccessTableSeeder::class);
//        $this->call(HistoryTypeTableSeeder::class);
//        $this->call(SettingsTableSeeder::class);
//        $this->call(PagesTableSeeder::class);
//        $this->call(MenuTableSeeder::class);
//        $this->call(ModulesTableSeeder::class);
//        $this->call(BlogTableSeeder::class);
        $this->call(ListingTableSeeder::class);
        Model::reguard();
    }

}

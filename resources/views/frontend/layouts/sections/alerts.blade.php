@php
    $responseMessage = '';
    $responseType = '';
    $dontHide = false;

    if(session()->has('dontHide')) {
        $dontHide = session()->get('dontHide');
    }
@endphp
@if ($errors->any())
    @php
        $responseType = 'danger';
    @endphp
    @foreach ($errors->all() as $error)
        @php
            $responseMessage .= $error . '<br/>';
        @endphp
    @endforeach
@elseif (session()->get('flash_success'))
    @php
        $responseType = 'success';
    @endphp
    @if(is_array(json_decode(session()->get('flash_success'), true)))
        @php
            $responseMessage = implode('', session()->get('flash_success')->all(':message<br/>'));
        @endphp
    @else
        @php
            $responseMessage = session()->get('flash_success');
        @endphp
    @endif
@elseif (session()->get('flash_warning'))
    @php
        $responseType = 'warning';
    @endphp
    @if(is_array(json_decode(session()->get('flash_warning'), true)))
        @php
            $responseMessage = implode('', session()->get('flash_warning')->all(':message<br/>'));
        @endphp
    @else
        @php
            $responseMessage = session()->get('flash_warning');
        @endphp
    @endif
@elseif (session()->get('flash_info'))
    @php
        $responseType = 'info';
    @endphp
    @if(is_array(json_decode(session()->get('flash_info'), true)))
        @php
            $responseMessage = implode('', session()->get('flash_info')->all(':message<br/>'));
        @endphp
    @else
        @php
            $responseMessage = session()->get('flash_info');
        @endphp
    @endif
@elseif (session()->get('flash_danger'))
    @php
        $responseType = 'danger';
    @endphp
    @if(is_array(json_decode(session()->get('flash_danger'), true)))
        @php
            $responseMessage = implode('', session()->get('flash_danger')->all(':message<br/>'));
        @endphp
    @else
        @php
            $responseMessage = session()->get('flash_danger');
        @endphp
    @endif
@elseif (session()->get('flash_message'))
    @php
        $responseType = 'info';
    @endphp
    @if(is_array(json_decode(session()->get('flash_message'), true)))
        @php
            $responseMessage = implode('', session()->get('flash_message')->all(':message<br/>'));
        @endphp
    @else
        @php
            $responseMessage = session()->get('flash_message');
        @endphp
    @endif
@endif
    @if(isset($responseMessage) && $responseMessage!=='')
            <script type="text/javascript">
                Noty.overrideDefaults({
                layout   : 'topRight',
                        theme    : 'ezzy',
                        timeout  : 25000,
                        closeWith: ['click', 'button'],
                });
                        new Noty({
                        type: "{{ $responseType }}",
                                text: "{!! str_replace('"', "'", $responseMessage) !!}"
                        }).show();
            </script>
    @else

            <script type="text/javascript">
                Noty.overrideDefaults({
                layout   : 'topRight',
                        theme    : 'ezzy',
                        timeout  : 25000,
                        closeWith: ['click', 'button'],
                });
                @foreach (\Alert::getMessages() as $type => $messages)
                        @foreach ($messages as $message)

                        new Noty({
                        type: "{{ $type }}",
                                text: "{!! str_replace('"', "'", $message) !!}"
                        }).show();
                @endforeach
                        @endforeach
            </script>
    @endif


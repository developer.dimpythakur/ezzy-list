@if(count($post->categories))
@foreach($post->categories as $category)
<a href="#" class="badge badge-primary mt-3 ml-3 position-absolute sticky-top p-2">
    {{ $category->name }}
</a>
@endforeach
@endif
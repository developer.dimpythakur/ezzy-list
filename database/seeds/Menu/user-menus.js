[
    {
        "view_permission_id": "",
        "icon": "la la-commenting",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "",
        "name": "Listings",
        "id": 456,
        "content": "Listings",
        "children": [
            {
                "view_permission_id": "",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "frontend.user.listings-active",
                "name": "Listings",
                "id": 555,
                "icon": "las la-check-circle",
                "content": "Listings"
            },
            {
                "view_permission_id": "",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "frontend.user.listings-pending",
                "name": "Pending",
                "id": 344,
                "icon": "las la-check-circle",
                "content": "Pending"
            },
            {
                "view_permission_id": "",
                "open_in_new_tab": 0,
                "url_type": "route",
                "url": "frontend.user.listings-expired",
                "name": "Expired",
                "id": 21,
                "icon": "las la-check-circle",
                "content": "Expired"
            }
        ]
    },
    {
        "view_permission_id": "",
        "icon": "la la-plus-circle",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "frontend.user.listings.create",
        "name": "Add New",
        "id": 89,
        "content": "Add New"
    },
    {
        "view_permission_id": "",
        "icon": "la la-yelp",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "frontend.user.listings-yelp",
        "name": "Add Yelp",
        "id": 890,
        "content": "Add Yelp"
    },

    {
        "view_permission_id": "",
        "icon": "la la-bookmark-o",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "frontend.user.listings.create",
        "name": "Saved",
        "id": 222,
        "content": "Saved"
    },
    {
        "view_permission_id": "",
        "icon": "la la-star-o",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "frontend.user.reviews",
        "name": "Reviews",
        "id": 312,
        "content": "Reviews"
    },
    {
        "view_permission_id": "",
        "icon": "la la-user-alt",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "frontend.user.account",
        "name": "Account",
        "id": 392,
        "content": "Account"
    },
    {
        "view_permission_id": "",
        "icon": "la la-power-off",
        "open_in_new_tab": 0,
        "url_type": "route",
        "url": "frontend.auth.logout",
        "name": "Logout",
        "id": 30,
        "content": "Logout"
    }
]

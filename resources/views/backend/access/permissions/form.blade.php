<div class="card mt-2">
    <div class="card-header">
        <h6 class="my-2"><i class="la la-file-text"></i> Contact Information</h6>
    </div>
    <div class="card-body listing-form bold-labels">
        <div class="row my-3">
            <div class="form-group col-12">
                {{ Form::label('name', trans('validation.attributes.backend.access.permissions.name'), ['class' => 'control-label required']) }}
                {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.permissions.name'), 'required' => 'required']) }}
            </div><!--form control-->

            <div class="form-group col-12">
                {{ Form::label('display_name', trans('validation.attributes.backend.access.permissions.display_name'), ['class' => 'control-label required']) }}

                {{ Form::text('display_name', null,['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.permissions.display_name'), 'required' => 'required']) }}
            </div><!--form control-->

            <div class="form-group col-12">
                {{ Form::label('sort', trans('validation.attributes.backend.access.permissions.sort'), ['class' => 'control-label']) }}
                {{ Form::text('sort', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.access.permissions.sort')]) }}
            </div><!--form control-->
        </div>
    </div>
</div>
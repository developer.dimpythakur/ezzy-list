<?php

return [
    "pages" => [
        "table" => "pages",
    ],
    "email_templates" => [
        "table" => "email_templates",
        "placeholders_table" => "email_template_placeholders",
        "types_table" => "email_template_types",
    ],
    "blog_tags" => [
        "table" => "blog_tags",
    ],
    "blog_categories" => [
        "table" => "blog_categories",
    ],
    "ezzy_social_networks" => [
        "table" => "ezzy_social_networks",
    ],
    "ezzy_categories" => [
        "table" => "ezzy_categories",
    ],
    "ezzy_timings" => [
        "table" => "ezzy_timings",
    ],
    "ezzy_tags" => [
        "table" => "ezzy_tags",
    ],
    "ezzy_faqs" => [
        "table" => "ezzy_faqs",
    ],
    "ezzy_media" => [
        "table" => "ezzy_media",
    ],
    "ezzy_media_audio" => [
        "table" => "ezzy_media_audio",
    ],
    "ezzy_media_video" => [
        "table" => "ezzy_media_video",
    ],
    "ezzy_reviews" => [
        "table" => "ezzy_reviews",
    ],
    "ezzy_media_gallery" => [
        "table" => "ezzy_media_gallery",
    ],
    "ezzy_locations" => [
        "table" => "ezzy_locations",
    ],
    "ezzy_regions" => [
        "table" => "ezzy_regions",
    ],
    "listings" => [
        "table" => "ezzy_listing",
    ],
    "listing_categories" => [
        "table" => "ezzy_listing_categories",
    ],
    "listing_tags" => [
        "table" => "ezzy_listing_tags",
    ],
    "listing_social_networks" => [
        "table" => "ezzy_listing_social_networks",
    ],
    "listing_faqs" => [
        "table" => "ezzy_listing_faqs",
    ],
    "listing_timings" => [
        "table" => "ezzy_listing_timings",
    ],
    "listing_media" => [
        "table" => "ezzy_listing_media",
    ],
    "listing_media_audio" => [
        "table" => "ezzy_listing_media_audio",
    ],
    "listing_media_video" => [
        "table" => "ezzy_listing_media_video",
    ],
    "listing_media_gallery" => [
        "table" => "ezzy_listing_media_gallery",
    ],
    "listing_locations" => [
        "table" => "ezzy_listing_locations",
    ],
    "listing_reviews" => [
        "table" => "ezzy_listing_reviews",
    ],
    "blogs" => [
        "table" => "blogs",
    ],
    "faqs" => [
        "table" => "faqs",
    ],
    "yelp" => [
        "table" => "yelps",
    ],
    "locations" => [
        "table" => "location",
    ],
    "yelps" => [
        "table" => "yelps",
    ],
    "listingcategories" => [
        "table" => "listing_categories",
    ],
    "locationcategories" => [
        "table" => "locationcategories",
    ],
    "regions" => [
        "table" => "regionregions",
    ],
    "tags" => [
        "table" => "tags",
    ],
];

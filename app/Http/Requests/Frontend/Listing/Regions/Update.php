<?php

namespace App\Http\Requests\Backend\Listing\Regions;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return access()->allow('update-region');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required'
        ];
    }

    public function messages() {
        return [
            'name.required' => 'required'

                //The Custom messages would go in here
                //For Example : 'title.required' => 'You need to fill in the title field.'
                //Further, see the documentation : https://laravel.com/docs/6.x/validation#customizing-the-error-messages
        ];
    }

}

<div class="owl-carousel post-slider mt-4 mb-4 owl-loaded owl-drag">


    <div class="owl-stage-outer">
        <div class="owl-stage"
             style="transform: translate3d(-1110px, 0px, 0px); transition: all 0.25s ease 0s; width: 2960px;">
            <div class="owl-item cloned" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-1.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item cloned" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-2.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item cloned" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-5.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item cloned" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-6.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-1.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-2.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item active" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-5.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item active" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-6.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item active" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-1.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item active" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-2.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-5.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-6.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item cloned" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-1.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item cloned" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-2.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item cloned" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-5.jpg" alt="listing">
                </div>
            </div>
            <div class="owl-item cloned" style="width: 175px; margin-right: 10px;">
                <div class="slide img-overlay transition-1">
                    <img src="img/cities/city-6.jpg" alt="listing">
                </div>
            </div>
        </div>
    </div>
    <div class="owl-dots disabled"></div>
</div>

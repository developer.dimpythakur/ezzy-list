<?php

namespace App\Http\Controllers\Backend\Listing\Tags;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Listing\Tags\TagRepository;
use App\Http\Requests\Backend\Listing\Tags\Manage;

/**
 * Class TagsTableController.
 */
class TagTableController extends Controller {

    /**
     * variable to store the repository object
     * @var TagRepository
     */
    protected $tag;

    /**
     * contructor to initialize repository object
     * @param TagRepository $tag;
     */
    public function __construct(TagRepository $tag) {
        $this->tag = $tag;
    }

    /**
     * This method return the data of the model
     * @param ManageTagRequest $request
     *
     * @return mixed
     */
    public function __invoke(Manage $request) {
        return Datatables::of($this->tag->getForDataTable())
                        ->escapeColumns(['id'])
                        ->addColumn('created_at', function ($tag) {
                            return Carbon::parse($tag->created_at)->toDateString();
                        })
                        ->addColumn('actions', function ($tag) {
                            return $tag->action_buttons;
                        })
                        ->filterColumn('name', function($query, $keyword) {
                            $sql = "name like ?";
                            $query->whereRaw($sql, ["%{$keyword}%"]);
                        })
                        ->orderColumn('id', function($query, $keyword) {
                            $query->orderBy('id', "$keyword");
                        })
                        ->make(true);
    }

}

<header class="{{ config('backend.header_class') }}">
    <!-- Logo -->
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto ml-3" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="{{route('frontend.index')}}">
        @if(isset($setting) && $setting->logo)
        <img height="48" width="226" class="navbar-brand" src="{{route('frontend.index')}}/img/site_logo/{{$setting->logo}}">
        @else
        {!! config('backend.project_logo') !!}
        @endif
    </a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav navbar-nav ml-auto ">
        <li class="nav-item dropdown pr-4">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img class="img-avatar" src="{{ access()->user()->picture}}" alt="">
            </a>
        </li>
    </ul>
</header>

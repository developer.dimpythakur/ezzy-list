<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEzzyListingMediaAudioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ezzy_listing_media_audio', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ezzy_listing_id');
            $table->unsignedBigInteger('ezzy_media_audio_id');

            $table->foreign('ezzy_listing_id')->references('id')->on('ezzy_listing');
            $table->foreign('ezzy_media_audio_id')->references('id')->on('ezzy_media_audio');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ezzy_listing_media_audio');
    }
}

<?php

/**
 * Listings Controller
 * All route names are prefixed with 'frontend.'.
 */
/*
 * Show locations
 */
Route::get('location', 'Listing\LocationController@index')->name('locations.all');
Route::get('location/{slug}', 'Listing\LocationController@show')->name('location.show');
Route::get('locations', 'Listing\LocationController@index')->name('locations');


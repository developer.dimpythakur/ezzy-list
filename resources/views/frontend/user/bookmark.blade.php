@extends ('frontend.layouts.user')
@section ('title', trans('labels.backend.listings.management'))
@section('page-header')
<h2>{{ trans('labels.backend.listings.bookmark') }}</h2>
@endsection
@section('after_styles')
<!-- DATA TABLES -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/backend/datatables.css') }}">

@endsection

@section('content')
<div class="col-md-7">
    @if(count($listings))
    @foreach($listings as $listing)
    <div class="row listing-single">
        <div class="col-md-3 mb-1">
            <div class="row mb-0">
                <figure class="listing-image">
                    @if ($listing->banner && !is_null($listing->banner))
                    <img src="{{ $listing->banner }}"
                         class="img-fluid" alt="{{ $listing->name }}">
                    @else
                    <img src="{{route('frontend.index')}}/img/frontend/listings/coffee-1.jpg"
                         class="img-fluid" alt="listing">
                    @endif
                </figure>
            </div>
        </div>
        <div class="col-md-9 mb-1">
            <div class="col-box">
                <a class="btn btn-flat  btn-link remove-link text-danger"
                   data-method="delete"
                   href="{{ route('frontend.user.bookmark-remove')}}"
                   data-trans-button-cancel="Cancel" 
                   data-listing-id="{{ $listing->model_id }}"
                   data-user-id="{{ $listing->user_id }}"
                   data-trans-button-confirm="Delete"
                   data-trans-title="Are you sure you want to do this?"
                   style="cursor:pointer;" onclick="$(this).find( & quot; form & quot; ).submit();">
                    <i data-toggle="tooltip" data-placement="top" title="Delete" class="la la-trash"></i> Remove
                </a>
                <?php
                $rating = $listing->averageRating(2);
                $filterRatings = collect($rating)->filter();
                ?>
                <span class="badge star-rating bookmark-rating ml-2"
                      data-rating="{{ (count($filterRatings))?$listing->averageRating(2):'No ratings' }}">
                </span>

                <h3 class="listing-title">{{ $listing->name }}</h3>
                <div class="text listing-description">{{ $listing->tagline }}</div>
                <ul class="info">
                    <li><span class="la la-list"></span>&nbsp;{{ $listing->categories->first()->name }} </li>
                    <li><span class="la la-location-arrow"></span>&nbsp;{{ substr($listing->location->first()->address,0,40) }} </li>
                </ul>
                <ul class="info mt-2">
                    <li><span class="la la-phone"></span>&nbsp;{{ $listing->phone }} </li>
                    <li><span class="la la-html5"></span>&nbsp;{{ $listing->website }} </li>
                </ul>
            </div>
        </div>
    </div>
    @endforeach
    <div class="row">
        {{ $listings->links('vendor.pagination.user-listing') }}
    </div>
    @else
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h6 class="my-2"><i class="la la-bookmark-o"></i> Saved listings</h6>
                </div>
                <div class="card-body bold-labels">
                    <div class="w-100 p-4">
                        <div class="p-3 alert alert-danger">
                            <p class="m-0"><i class="la la-list"></i> You haven't bookmarked any listing.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
@section('after_scripts')
@endsection
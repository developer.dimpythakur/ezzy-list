@extends ('backend.layouts.app')
@section ('title', trans('labels.backend.blogs.management') . ' | ' . trans('labels.backend.blogs.edit'))
@section('page-header')
<h1>{{ trans('labels.backend.blogs.edit') }}</h1>
@endsection
@section("after_styles")
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}">
<style>
    /*
     * File
     */

    .file {
        position: relative;
        display: inline-block;
        cursor: pointer;
        height: 2.5rem;
    }
    .file input {
        min-width: 14rem;
        margin: 0;
        filter: alpha(opacity=0);
        opacity: 0;
    }
    .file-custom {
        position: absolute;
        top: 0;
        right: 0;
        left: 0;
        z-index: 5;
        height: 2.5rem;
        padding: .5rem 1rem;
        line-height: 1.5;
        color: #555;
        background-color: #fff;
        border: .075rem solid #ddd;
        border-radius: .25rem;
        box-shadow: inset 0 .2rem .4rem rgba(0,0,0,.05);
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }
    .file-custom:after {
        content: "Choose file...";
    }
    .file-custom:before {
        position: absolute;
        top: -.075rem;
        right: -.075rem;
        bottom: -.075rem;
        z-index: 6;
        display: block;
        content: "Browse";
        height: 2.5rem;
        padding: .5rem 1rem;
        line-height: 1.5;
        color: #555;
        background-color: #eee;
        border: .075rem solid #ddd;
        border-radius: 0 .25rem .25rem 0;
    }

    /* Focus */
    .file input:focus ~ .file-custom {
        box-shadow: 0 0 0 .075rem #fff, 0 0 0 .2rem #0074d9;
    }
    .bootstrap-datetimepicker-widget.dropdown-menu .collapse:not(.show){
        display: block !important
    }
    .select2-container .select2-selection--single,
    .select2-container--default .select2-selection--single .select2-selection__rendered{
        height: 40px;
        line-height: 40px;
    }
</style>
@endsection
@section('content')

<section class="row">
    <div class="col-md-8 bold-labels">
        <!-- Default box -->
        @include(('backend.layouts.sections.grouped_errors'))
        {{ Form::model($blog, ['route' => ['admin.blogs.update', $blog], 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH', 'id' => 'edit-role', 'files' => true]) }}

        <div class="card">
            <div class="card-body row">
                @include("backend.blogs.form")
            </div><!-- /.card-body -->
        </div><!--card-->

        <div id="updateForm" class="form-group">
            <div class="btn-group" role="group">
                <button type="submit" class="btn btn-success">
                    <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                    <span data-value=update_and_back">Update</span>
                </button>
            </div>
            <a href="{{ route('admin.blogs.index') }}" class="btn btn-default"><span class="la la-ban"></span> &nbsp;Cancel</a>
        </div>
        {{ Form::close() }}
    </div>
</section>


@endsection

@section("after_scripts")

<!-- include select2 js-->
<script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
@if (app()->getLocale() !== 'en')
<script src="{{ asset('packages/select2/dist/js/i18n/' . app()->getLocale() . '.js') }}"></script>
@endif
<script src="{{ asset('packages/tinymce/tinymce.min.js') }}"></script>
<script>

$(document).ready(function () {
//        select2
    $('.select2_multiple').select2();
    //tinymce
    var $element = "textarea.form-control.tinymce";
    tinymce.init(jQuery($element).data('options'));

});

</script>
@endsection





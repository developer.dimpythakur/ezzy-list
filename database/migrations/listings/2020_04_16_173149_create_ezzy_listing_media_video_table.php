<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEzzyListingMediaVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ezzy_listing_media_video', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('ezzy_listing_id');
            $table->unsignedBigInteger('ezzy_media_video_id');

            $table->foreign('ezzy_listing_id')->references('id')->on('ezzy_listing');
            $table->foreign('ezzy_media_video_id')->references('id')->on('ezzy_media_video');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ezzy_listing_media_video');
    }
}

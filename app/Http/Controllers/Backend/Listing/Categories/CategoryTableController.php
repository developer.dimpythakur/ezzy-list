<?php

namespace App\Http\Controllers\Backend\Listing\Categories;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Repositories\Backend\Categories\CategoryRepository;
use App\Http\Requests\Backend\Listing\Categories\Manage;
use Illuminate\Support\Facades\Storage;

/**
 * Class CategoryTableController.
 */
class CategoryTableController extends Controller {

    /**
     * variable to store the repository object
     * @var CategoryRepository
     */
    protected $category;

    /**
     * contructor to initialize repository object
     * @param CategoryRepository $category;
     */
    public function __construct(CategoryRepository $category) {
        $this->category = $category;
    }

    /**
     * This method return the data of the model
     * @param ManageCategoryRequest $request
     *
     * @return mixed
     */
    public function __invoke(Manage $request) {
        return Datatables::of($this->category->getForDataTable())
                        ->escapeColumns(['name'])
                        ->orderColumn('name', 'name $1')
                        ->addColumn('name', function ($category) {
                            return $category->name;
                            if ($category->image):
                                $listingImage = '<img style="height:40px;width:40px; border-radius:5px" class="img img-responsive" src="' . Storage::url('images/category/' . $category->image) . '"/>';
                            else:
                                $listingImage = '<img style="height:40px;width:40px; border-radius:5px" class="img img-responsive" src="' . Storage::url('images/category/default-image.png') . '"/>';
                            endif;
//                return '<a target="_blank" href="' . URL::to("listing/" . $listingcategory->slug) . '">' . $listingImage . ' ' . $listingcategory->name . '</a>';
//                return '<a target="_blank" href="' . URL::to('listing-category/' . $listingcategory->slug) . '">' . $listingcategory->name . '</a>';
                        })
                        ->addColumn('slug', function ($category) {
                            return $category->slug;
                        })
                        ->addColumn('image', function ($category) {
                            if ($category->image):
                                return '<img style="height:40px;width:40px; border-radius:5px" class="img img-responsive" src="' . Storage::url('images/category/' . $category->image) . '"/>';
                            else:
                                return '<img style="height:40px;width:40px; border-radius:5px" class="img img-responsive" src="' . Storage::url('images/category/default-image.png') . '"/>';
                            endif;
                        })
                        ->addColumn('description', function ($category) {
                            return $category->description;
                        })
                        ->addColumn('[parent_id]', function ($category) {
                            return $category->status_label;
                        })
                        ->addColumn('status', function ($category) {
                            return $category->status_label;
                        })
                        ->addColumn('created_at', function ($category) {
                            return Carbon::parse($category->created_at)->toDateString();
                        })
                        ->addColumn('actions', function ($category) {
                            return $category->action_buttons;
                        })
                        ->make(true);
    }

}

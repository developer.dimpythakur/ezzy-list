<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', 'FrontendController@index')->name('index');
Route::post('/get/states', 'FrontendController@getStates')->name('get.states');
Route::post('/get/cities', 'FrontendController@getCities')->name('get.cities');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'prefix' => 'user', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');
        /*
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');
        /*
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
        /*
         * User Profile Picture
         */
        Route::patch('profile-picture/update', 'ProfileController@updateProfilePicture')->name('profile-picture.update');


        Route::resource('listings', 'ListingController');
        Route::get('listings-active', 'ListingController@active')->name('listings-active');
        Route::get('listings-pending', 'ListingController@pending')->name('listings-pending');
        Route::get('listings-expired', 'ListingController@expired')->name('listings-expired');
        Route::get('listings-yelp', 'ListingController@yelp')->name('yelp');
        
        Route::post('listings-yelp', 'DashboardController@yelpSearch')->name('listings-yelp');

        Route::get('account', 'AccountController@index')->name('account');
        Route::get('reviews', 'DashboardController@reviews')->name('reviews');
        Route::get('bookmark', 'DashboardController@bookmark')->name('bookmark');
        Route::delete('bookmark-remove', 'DashboardController@remove')->name('bookmark-remove');
    });
});
/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
//Route::group(['middleware' => 'auth'], function () {
////Route::group(['namespace' => 'User', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
//    Route::group(['namespace' => 'User', 'prefix' => 'user', 'as' => 'user.'], function () {
//   
//    });
//});
/*
 * Show pages
 */
Route::get('pages/{slug}', 'FrontendController@showPage')->name('pages.show');
Route::get('pricing', 'FrontendController@pricing')->name('pricing');

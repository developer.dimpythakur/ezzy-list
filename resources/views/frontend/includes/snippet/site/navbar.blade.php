<nav class="navbar navbar-expand-lg navbar-dark bg-dark et-nav-menu">
    <div class="container">
        <!--  Show this only on mobile to medium screens  -->

        @if(isset($setting->logo))
        <a href="{{ route('frontend.index') }}" class="logo navbar-brand d-lg-none">
            <img height="48" width="226" class="navbar-brand"
                 src="{{ Storage::url('images/logo/').$setting->logo}}">
        </a>
        @else
        <a href="{{ route('frontend.index') }}" class="logo navbar-brand d-lg-none">
            <img height="48" width="133" class="navbar-brand"
                 src="{{ Storage::url('/images/frontend/logo.png')}}">
        </a>
        @endif


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle"
                aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!--  Use flexbox utility classes to change how the child elements are justified  -->
        <div class="collapse navbar-collapse justify-content-between" id="navbarToggle">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" href="{{ route('frontend.index') }}">Home</a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('frontend.listing.all') }}">Listings</a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('frontend.blog') }}">Blog</a>
                </li>
            </ul>
            <!--   Show this only lg screens and up   -->

            @if(isset($setting->logo))
            <a href="{{ route('frontend.index') }}" class="logo navbar-brand d-none d-lg-block">
                <img height="48" width="170" class="navbar-brand"
                     src="{{ Storage::url('images/logo/').settings()->logo}}">
            </a>
            @else
            <a href="{{ route('frontend.index') }}" class="logo navbar-brand d-none d-lg-block">
                <img height="48" width="133" class="navbar-brand"
                     src="{{ Storage::url('/images/frontend/logo.png')}}">
            </a>
            @endif
            <ul class="navbar-nav">
                @if ($logged_in_user)
                <li class="nav-item">
                    <a style="display:none;"
                       class="btn btn-primary ml-3 border-0 transition-1 text-capitalize text-white"
                       href="add-listing.html">
                        <i class="las la-plus"></i> Add Listing
                    </a>
                </li>
                @endif

                @if (! $logged_in_user)
                <li class="nav-item">
                    {{--                        <a id="sign-up" href="{{ route('frontend.auth.login') }}">--}}
                    {{--                            <i class="icon-block"></i> Login/Signup--}}
                    {{--                        </a>--}}
                    <a href="#" class="text-white ml-3" data-toggle="modal" data-target="#loginSignup">
                        <i class="icon-block"></i> Login/Signup
                    </a>
                </li>
                @else


                <li class="nav-item dropdown user-profile-dropdown">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle user mt-2"
                       id="userProfileDropdown"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <img src="{{ (access()->user()->picture) }}" alt="avatar">
                    </a>
                    <div class="dropdown-menu position-absolute nav-dark" aria-labelledby="userProfileDropdown">
                        <div class="">

                            @permission('view-backend')
                            <div class="dropdown-item">
                                <a class="" href="{{route('admin.dashboard')}}">
                                    <i class="las la-user-ninja"></i>
                                    {{ trans('navs.frontend.user.administration') }}
                                </a>
                            </div>
                            @endauth
                            <div class="dropdown-item">
                                <a class="" href="{{route('frontend.user.account')}}">
                                    <i class="las la-user-circle"></i>
                                    My Profile
                                </a>
                            </div>
                            <div class="dropdown-item">
                                <a class="" href="{{route('frontend.user.listings.index')}}">
                                    <i class="las la-list-alt"></i>
                                    My Listings
                                </a>
                            </div>
                            <div class="dropdown-item">
                                <a class="" href="{{route('frontend.user.listings.create')}}">
                                    <i class="las la-plus-circle"></i>
                                    Add Listings
                                </a>
                            </div>
                            <div class="dropdown-item">
                                <a class="" href="{{route('frontend.auth.logout')}}">
                                    <i class="las la-sign-out-alt" aria-hidden="true"></i>
                                    Sign Out
                                </a>
                            </div>
                        </div>
                    </div>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<?php

namespace App\Models\LocationCategory\Traits;

/**
 * Class LocationCategoryAttribute.
 */
trait LocationCategoryAttribute
{
    // Make your attributes functions here
    // Further, see the documentation : https://laravel.com/docs/6.x/eloquent-mutators#defining-an-accessor


    /**
     * Action Button Attribute to show in grid
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return '<div class="btn-group action-btn"> {$this->getEditButtonAttribute("edit-locationcategory", "admin.locationcategories.edit")}
                {$this->getDeleteButtonAttribute("delete-locationcategory", "admin.locationcategories.destroy")}
                </div>';
    }
}

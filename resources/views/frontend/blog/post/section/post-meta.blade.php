<div class="post-meta pb-3 pt-3">
    <a class="text-muted pr-2" href="javascript:void(0)"> <i class="la la-clock"></i> {{\Carbon\Carbon::parse($post->publish_datetime)->format('M, d, Y ')}}</a>
    <a class="text-muted pr-2" href="javascript:void(0)"> <i class="la la-user-circle"></i>   {{ $post->owner->first_name}}</a>
</div>
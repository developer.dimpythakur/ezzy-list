<?php

namespace App\Repositories\Backend\Listing\Locations;

use App\Events\Backend\Listing\Locations\Created;
use App\Events\Backend\Listing\Locations\Updated;
use App\Http\Requests\Request;
use DB;
use Carbon\Carbon;
use App\Models\Listing\Region\Region as Location;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
//use App\Models\Listing\Region\Region as Location;
use App\Models\Listing\Region\Region;
//SlugService
use Cviebrock\EloquentSluggable\Services\SlugService;

//use App\Models\Listing\Locations\Location;
/**
 * Class LocationRepository.
 */
class LocationRepository extends BaseRepository {

    /**
     * Associated Repository Model.
     */
    const MODEL = Region::class;

    protected $upload_path;

    /**
     * Storage Class Object.
     *
     * @var \Illuminate\Support\Facades\Storage
     */
    protected $storage;

    public function __construct() {
        $this->upload_path = 'images' . DIRECTORY_SEPARATOR . 'listing/locations' . DIRECTORY_SEPARATOR;
        $this->storage = Storage::disk('public');
    }

    /**
     * This method is used by Table Controller
     * For getting the table data to show in
     * the grid
     * @return mixed
     */
    public function getForDataTable() {
        return $this->query()
                        ->select([
                            config('module.ezzy_regions.table') . '.id',
                            config('module.ezzy_regions.table') . '.name',
//                config('module.ezzy_regions.table') . '.region_id',
                            config('module.ezzy_regions.table') . '.created_at',
                            config('module.ezzy_regions.table') . '.updated_at',
                        ])->orderBy('created_at', 'DESC');
    }

    /**
     * For Creating the respective model in storage
     *
     * @param array $input
     * @return bool
     * @throws GeneralException
     */
    public function create(array $request) {
        if ($this->query()->where('name', $request['name'])->first()) {
            throw new GeneralException('Name already exists');
        }
        DB::transaction(function () use ($request) {
            $image = $this->uploadImage($request);
            $request['image'] = $image;
            $request['city'] = isset($request['region']) ? $request['region'] : "NY";
            $request['created_by'] = access()->user()->id;
            if ($location = Region::create($request)) {
                event(new Created($location));
                return true;
            }
            throw new GeneralException(trans('exceptions.backend.locations.create_error'));
        });
    }

    /**
     * Upload Image.
     * @param $input
     * @return string|null
     */
    public function uploadImage($input) {
        if (isset($input['image']) && !empty($input['image'])):
            $avatar = $input['image'];
            $fileName = time() . $avatar->getClientOriginalName();
            if ($this->storage->put($this->upload_path . $fileName, file_get_contents($avatar->getRealPath()))):
                return $fileName;
            endif;
        endif;
        return 'location-default.jpeg';
    }

    /**
     *  For updating the respective Model in storage
     * @param Location $listing_location
     * @param array $input
     * @throws GeneralException
     */
    public function update(Location $listing_location, array $input) {
        $query = $this->query()->where('name', $input['name'])->where('id', '!=', $listing_location->id)->first();
        dd($query);
        if ($query) {
            throw new GeneralException(trans('exceptions.backend.locations.update_error'));
        }
        DB::transaction(function () use ($listing_location, $input) {
            // Uploading Image
            if (isset($input['image']) && $input->hasFile('image')) {
                $input['image'] = $this->uploadImage($input);
            }
            $input['updated_by'] = access()->user()->id;
            if ($listing_location->update($input)) {
                event(new Updated($listing_location));
                return true;
            }
            throw new GeneralException(trans('exceptions.backend.locations.update_error'));
        });
    }

    /**
     * For deleting the respective model from storage
     *
     * @param Location $location
     * @return bool
     * @throws GeneralException
     */
    public function delete(Location $listing_location) {
        dd($listing_location);
        if ($location->delete()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.locations.delete_error'));
    }

}

<?php

namespace App\Http\Responses\Backend\Listings\Locations;

use Illuminate\Contracts\Support\Responsable;

class EditResponse implements Responsable {

    /**
     * @var App\Models\Locations\Location
     */
    protected $listing_location;

    /**
     * @param App\Models\Locations\Location $locations
     */
    public function __construct($listing_location) {
        $this->listing_location = $listing_location;
    }

    /**
     * To Response
     *
     * @param \App\Http\Requests\Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function toResponse($request) {
        return view('backend.listing.locations.edit')->with([
                    'listing_location' => $this->listing_location
        ]);
    }

}

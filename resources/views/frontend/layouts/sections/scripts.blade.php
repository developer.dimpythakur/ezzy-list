@if (config('backend.ezzy_app.scripts') && count(config('backend.ezzy_app.scripts')))
@foreach (config('backend.ezzy_app.scripts') as $path)
<script type="text/javascript" id="{{ ($path) }}" src="{{ asset($path) }}"></script>
@endforeach
@endif
@include('frontend.layouts.sections.alerts')
<script type="text/javascript">
// To make Pace works on Ajax calls
$(document).ajaxStart(function () {
    Pace.restart();
});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<?php

namespace App\Models\Listing\Locations\Traits;

use App\Models\Listing\Listing\Listing;
use App\Models\Listing\Region\Region;

/**
 * Class LocationRelationship
 */
trait Relationship {
    /*
     * put you model relationships here
     * Take below example for reference
     */
    /*
      public function users() {
      //Note that the below will only work if user is represented as user_id in your table
      //otherwise you have to provide the column name as a parameter
      //see the documentation here : https://laravel.com/docs/6.x/eloquent-relationships
      $this->belongsTo(User::class);
      }
     */

    public function region() {
        return $this->belongsTo(Region::class, 'region_id');
    }

    /**
     * Listing
     * @return mixed
     */
    public function listing() {
        return $this->belongsTo(Listing::class, 'id');
    }

}

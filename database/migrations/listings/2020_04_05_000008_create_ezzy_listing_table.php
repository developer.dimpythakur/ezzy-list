<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateEzzyListingTable extends Migration {

    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ezzy_listing';

    /**
     * Run the migrations.
     * @table ezzy_listing
     *
     * @return void
     */
    public function up() {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('business_id', 255)->nullable()->default(null);
            $table->string('slug', 100)->nullable()->default(null);
            $table->string('tagline', 200)->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->string('banner', 200)->nullable()->default(null);
            $table->string('type', 200)->nullable()->default('listing');
            $table->string('website', 100)->nullable()->default(null);
            $table->string('phone', 100)->nullable()->default(null);
            $table->string('email', 100)->nullable()->default(null);
            $table->string('status', 20)->nullable()->default('review');

            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table($this->tableName, function ($table) {
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists($this->tableName);
    }

}

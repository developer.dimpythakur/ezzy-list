@push('after_styles')
<style>
    #imgPreview img,
    .imgPreview img{
        height: 150px;
        width: 100%;
        border: 1px solid #ddd;
        border-radius: 5px;
        margin-top: 1rem;
    }
    /*FAKE FILE*/
    .fake-file-upload {
        border: 2px dashed #ccc;
    }
    .fake-file-upload input {
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        opacity: 0;
    }
    .fake-file-upload:hover{
        border-color: #e53935 !important;
    }
    .nostyle{list-style: none;margin: 0;padding: 0}
</style>
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2/dist/css/select2.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('packages/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />

@endpush

<div class="form-group col-12">
    {{ Form::label('name', trans('validation.attributes.backend.categories.title'), ['class' => 'control-label required']) }}
    {{ Form::text('name', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.categories.title'), 'required' => 'required']) }}
</div><!--form group-->

<div class="form-group col-12">
    {{ Form::label('description', trans('validation.attributes.backend.categories.description'), ['class' => 'control-label required']) }}
    {{ Form::textarea('description', null, ['class' => 'form-control box-size', 'placeholder' => trans('validation.attributes.backend.categories.description')]) }}
</div><!--form group-->

<?php
$icon = isset($listing_category) ? $listing_category->icon : null;
?>

<div class="form-group col-12">
    {{ Form::label('icon', 'Select Icon', ['class' => 'control-label']) }}
    {{ Form::select('icon', categoryIcons(), $icon, ['class' => 'form-control select2', 'placeholder' => 'Select Icon', 'required' => 'required'])}}
</div><!--form control-->
@if(isset($listing_category) && !empty($listing_category->image))
<div class="form-group col-12">
    <div class="col-lg-4 px-0">
        <img src="{{ Storage::url('images/category/' . $listing_category->image) }}" height="150"
             class="img-thumbnail img-preview" width="150">
    </div>
</div>
@endif
<div class="form-group col-12">
    {{ Form::label('category_image', 'Category Image', ['class' => 'control-label']) }}
    <div class="fake-file-upload h6 rounded-pill transition-1 d-flex align-items-center justify-content-center p-3 text-center text-muted position-relative">
        <input type="file" id="category_image" name="category_image" class="position-absolute pointer"
               multiple=""> <i  class="icon-cloud-computing h3 m-0"></i>&nbsp; Upload category image
    </div>
    <div class="col-lg-3 pl-0">
        <div class="user-image mb-3 text-center">
            <div class="imgPreview"></div>
        </div> 
    </div>
</div>


<!--<div class="form-group col-sm-12" element="div">
    <div>
        <label>Status</label>
    </div>

    <div class="form-check form-check-inline">
        <input type="radio" {{ (isset($category)&& ($category->status == 1) ? 'checked' : '' ) }} class="form-check-input" checked="" value="1" name="status" id="status_1">
        <label class="radio-inline form-check-label font-weight-normal" for="status_1">Published</label>
    </div>
    <div class="form-check form-check-inline">
        <input type="radio" {{ (isset($category)&& ($category->status == 0) ? 'checked' : '' ) }} class="form-check-input" value="0" name="status" id="status_0">
        <label class="radio-inline form-check-label font-weight-normal" for="status_0">Draft</label>
    </div>
</div>form group-->


<div class="form-group col-sm-12" element="div">
    <div><label>Status</label> </div>
    <div class="form-check form-check-inline">
        <input type="radio" {{ (isset($category)&& ($category->status === 1) ? 'checked' : '' ) }} class="form-check-input" checked="" value="1" name="status" id="status_1">
        <label class="radio-inline form-check-label font-weight-normal" for="status_1">Active</label>
    </div>
    <div class="form-check form-check-inline">
        <input type="radio" {{ (isset($category)&& ($category->status === 0) ? 'checked' : '' ) }} class="form-check-input" value="0" name="status" id="status_0">
        <label class="radio-inline form-check-label font-weight-normal" for="status_0">In Active</label>
    </div>
</div>




@push("after_scripts")
<!-- jQuery -->
<!-- include select2 js-->
<script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
<script>
$(function () {
// Multiple images preview with JavaScript
    var multiImgPreview = function (input, imgPreviewPlaceholder) {
        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function (event) {
                    $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(imgPreviewPlaceholder);
                }

                reader.readAsDataURL(input.files[i]);
            }
        } else {
            console.log(input);
        }
    };

    $('#category_image').on('change', function () {
        let $el = 'div.imgPreview';
        jQuery($el).html('');
        multiImgPreview(this, 'div.imgPreview');
    });

    $('.select2').select2();
});
</script>
@endpush
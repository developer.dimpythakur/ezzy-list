<div class="{{ config('backend.sidebar_class') }}">
    <nav class="sidebar-nav overflow-hidden">
        <div class="sidebar sidebar-pills bg-light">
            <nav class="sidebar-nav overflow-hidden ps">
                <ul class="nav">
                    <!-- ================================================ -->
                    {{ renderMenuItems(getMenuItems()) }}
                    <!-- ======================================= -->
                </ul>
            </nav>
        </div>
    </nav>
    <!-- /.sidebar -->
</div>
@push('after_scripts')
<script>
    // Set active state on menu element
    var full_url = "{{ Request::fullUrl() }}";
    var $navLinks = $(".sidebar-nav li a");

    // First look for an exact match including the search string
    var $curentPageLink = $navLinks.filter(
            function () {
                return $(this).attr('href') === full_url;
            }
    );

    // If not found, look for the link that starts with the url
    if (!$curentPageLink.length > 0) {
        $curentPageLink = $navLinks.filter(function () {
            if ($(this).attr('href').startsWith(full_url)) {
                return true;
            }

            if (full_url.startsWith($(this).attr('href'))) {
                return true;
            }

            return false;
        });
    }

    $curentPageLink.parents('li').addClass('open');
    // - the actual element is active
    $curentPageLink.each(function () {
        $(this).addClass('active');
    });
</script>


<script>
//    check if url contains the string
    function checkUri(urlPrefix) {
        return (window.location.href.indexOf(urlPrefix) > -1) ? true : false;
    }
    jQuery(document).ready(function (e) {
        // move the bottom buttons before pagination
        $("#bottom_buttons").insertBefore($('#crudTable_wrapper .row:last-child'));

    });
</script>
@endpush

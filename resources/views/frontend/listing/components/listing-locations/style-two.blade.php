<!-- Explore by city -->
<section class="et-explore-by-city-wrap home-2 position-relative">
    <div class="container">
        <div class="section-heading pb-4 mb-3 text-center d-block full-width">
            <h2 class="text-capitalize">Explore Your Places</h2>
            <p>Explore the greates places in the city</p>
        </div>
        @php
            $count=1 @endphp
        @if(isset($regions) && !is_null($regions))
            <div class="owl-carousel explore-by-city">
            @foreach($regions as $location)
                <!-- Slide -->
                    <div class="et-explore-by-image grid img-overlay img-box transition-1 card-border-radius position-relative shadow overflow-hidden">
                        <a href="#" class="d-block location-style2">
                            <div class="city-thumbnail img-scale h-300">
                                @if ($location->image && !is_null($location->image))
                                    @php echo $location->image @endphp
                                @else
                                    <img src="{{route('frontend.index')}}/img/frontend/listings/coffee-1.jpg"
                                         class="img-fluid" alt="listing">
                                @endif
                            </div>
                            <div class="city-data text-white position-absolute fixed-bottom text-right p-3">
                                <h4 class="title text-white">
                                    {{ ($location->locations->count()) }} Entries</h4>
                                <h5 class="city-name mb-0 text-white">{{ $location->name }}</h5>
                            </div>
                        </a>
                    </div>
                    <!-- Slide -->
                @endforeach
            </div>
        @endif
    </div>
</section>
<!-- /Explore By City -->
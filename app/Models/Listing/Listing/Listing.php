<?php

namespace App\Models\Listing\Listing;

use App\Models\BaseModel;
use App\Models\ModelTrait;
use App\Models\Listing\Listing\Traits\Attribute\Attribute;
use App\Models\Listing\Listing\Traits\Relationship;
use App\Models\Listing\Listing\Traits\Shareable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Listing\Categories\Category;
//Search
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
//Viewable
use CyrildeWit\EloquentViewable\InteractsWithViews;
use CyrildeWit\EloquentViewable\Contracts\Viewable;
//Reviews
use App\Models\Listing\Reviews\Contracts\ReviewRateable;
use App\Models\Listing\Reviews\Traits\ReviewRateable as ReviewRateableTrait;
//Slugable
use Cviebrock\EloquentSluggable\Sluggable;
//Bookmark
use App\Models\Bookmark;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

//class Listing extends BaseModel

class Listing extends BaseModel implements Viewable, ReviewRateable {

    use InteractsWithViews;

    use ReviewRateableTrait;
    use Sluggable;
    use ModelTrait,
        SoftDeletes,
        Relationship,
        Attribute {
        // PageAttribute::getEditButtonAttribute insteadof ModelTrait;
    }
    use Shareable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The guarded field which are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Fillable Array
     * @var type 
     */
    protected $fillable = [
        'name',
        'slug',
        'tagline',
        'description',
        'banner',
        'business_id',
        'type',
        'website',
        'email',
        'phone',
        'status',
        'created_by'
    ];

    /**
     *
     * @var type 
     */
    public $banner_dir = 'images/listing/';


//    public $banner;

    /**
     * Banner location
     * @var type 
     */
    public $upload_path = '';

    /**
     * The default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'created_by' => 1,
    ];

    /**
     * Define relation
     * @var string[]
     */
    protected $with = ['owner'];

    /**
     * Share listing
     * @var array
     */
    protected $shareOptions = [
        'columns' => [
            'title' => 'name'
        ],
        'url' => 'slug'
    ];

    /**
     * Main Constructor function
     * @param array $attributes
     */
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('module.listings.table');
        $this->upload_path = Storage::url($this->banner_dir);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function getPublishedAttribute() {
        return $this->published();
    }

    /**
     * Return bookmarks
     * @return boolean
     */
    public function getBookmarkAttribute() {
        
    }

    /**
     * Return saved listings
     * @return boolean
     */
    public function saved_listings($num = 8) {

        return Listing::join('ezzy_bookmarks', $this->table . '.id', '=', 'ezzy_bookmarks.model_id')
                        ->join('users', $this->table . '.created_by', '=', 'users.id')
                        ->paginate($num);
    }

    /**
     * Get active categories
     */
    public function activeCategories() {
        Listing::where('status', 'publish')
                ->whereHas('categories', function ($q) use ($category) {
                    $q->where('ezzy_listing_categories.ezzy_categories_id', '=', $category->id);
                })
                ->limit(6)
                ->get();
    }

}

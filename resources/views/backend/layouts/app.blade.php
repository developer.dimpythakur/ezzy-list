<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ config('backend.html_direction') }}">
    <head>
        @include(('backend.layouts.sections.head'))
        <!-- Scripts -->
        <script>
            window.Laravel = {!! json_encode([ 'csrfToken' => csrf_token() ]) !!}
            ;
        </script>
        <style>
            .dataTables_wrapper{
                overflow-x:  hidden
            }
            #updateForm{
                background: #fff;
                position: fixed;
                bottom: 0;
                padding: 2rem;
                margin: 0;
            }
        </style>
    </head>

    <body class="{{ config('backend.body_class') }}">
        @include(('backend.layouts.sections.main_header'))
        <div class="app-body" id="app">
            @include('backend.layouts.sections.sidebar')
            <main class="main pt-2">
                <section class="container-fluid animated fadeIn my-4">
                    <div class="row">
                        <div class="col-md-6">
                            @yield('page-header')
                        </div>
                        <div class="col-md-6">
                            @if(Breadcrumbs::exists())
                            {!! Breadcrumbs::render() !!}
                            @endif
                        </div>
                    </div>
                </section>
                <div class="container-fluid animated fadeIn">
                    @yield('content')
                </div>
            </main>
        </div><!-- ./app-body -->

        <footer class="{{ config('backend.footer_class') }}">
            @include(('backend.layouts.sections.footer'))
        </footer>
        @yield('before_scripts')
        @stack('before_scripts')

        @include(('backend.layouts.sections.scripts'))

        @yield('after_scripts')
        @stack('after_scripts')

    </body>
</html>

<!--   @yield('page-header')
                     Breadcrumbs would render from routes/breadcrumb.php 
                    @if(Breadcrumbs::exists())
                        {!! Breadcrumbs::render() !!}
                    @endif-->
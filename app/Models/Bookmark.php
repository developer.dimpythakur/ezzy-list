<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model {

    /**

     * Associated  Model.
     */
    const MODEL = Bookmark::class;

    protected $table = 'ezzy_bookmarks';
    public $timestamps = false;
    public $guarded = [];

    /**
     * Return Listings 
     * @return type
     */
    public function listings() {
        $listing = MODEL::where('user_id', access()->user()->id)->get('model_id')->toArray();
        if (count($listing)):
            return collect($listing)->map(function($listing) {
                        return $listing['model_id'];
                    })->toArray();
        endif;
        return [];
    }

}
